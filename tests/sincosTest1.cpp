#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../shMath/functions.h"

unsigned int shMath :: ITERATION_NUM = 10u;

int main()
{
 float x = 0.0f;
 const float jump = 0.1f;
 for(;;x += jump)
 {
  const float resSin = shMath :: sin<float>(x);
  const float resCos = shMath :: cos<float>(x);
  printf("Should be:\t1.0\nis:\t%f\n",(resSin * resSin) + (resCos * resCos));
  sleep(1);
 }
 return EXIT_SUCCESS;
}
