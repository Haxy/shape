#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../shMath/functions.h"

unsigned int shMath :: ITERATION_NUM = 20u;

int main()
{
 float x = 0.0f;
 const float jump = 0.1f;
 for(;;x += jump)
 {
  const float resLn = shMath :: ln<float>(x * x);
  const float resExp = shMath :: exp<float>(0.5f * resLn);
  printf("Should be:\t%f\nis:\t%f\n",x,resExp);
  sleep(1);
 }
 return EXIT_SUCCESS;
}
