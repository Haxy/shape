#include <iostream>
#include <stdlib.h>
#include "../shMath/dictionary.h"

shMath :: dictionary<unsigned int> tost;

int main()
{
 tost.add("Ala Makoto", 42);
 tost.add("Kot ma Makoto", 72);
 tost["Kot"] = 14;
 std :: cout << "tost[\"Ala Makoto\"] = " << (unsigned int)(tost["Ala Makoto"]) << " should be 42" << std :: endl;
 std :: cout << "tost[\"Kot ma Makoto\"] = " << (unsigned int)(tost["Kot ma Makoto"]) << " should be 72" << std :: endl;
 std :: cout << "tost[\"Kot\"] = " << (unsigned int)(tost["Kot"]) << " should be 14" << std :: endl;
 const shMath :: dictionary<unsigned int> copy = tost;
 tost["tost"] = 27;
 std :: cout << "copy[\"Kot\"] = " << (unsigned int)(copy["Kot"]) << " should be 14" << std :: endl;
 std :: cout << "tost[\"Kot\"] = " << (unsigned int)(tost["Kot"]) << " should be 14" << std :: endl;
 std :: cout << "copy[\"tost\"] = " << (unsigned int)(copy["tost"]) << " should be 0" << std :: endl;
 std :: cout << "tost[\"tost\"] = " << (unsigned int)(tost["tost"]) << " should be 27" << std :: endl; 
 std :: cout << "tost.remove(\"Kot ma Makoto\") " << tost.remove("Kot ma Makoto") << " should not be 0" << std :: endl;
 std :: cout << "copy[\"Kot ma Makoto\"] = " << (unsigned int)(copy["Kot ma Makoto"]) << " should be 72" << std :: endl;
 std :: cout << "tost[\"Kot ma Makoto\"] = " << (unsigned int)(tost["Kot ma Makoto"]) << " should be 0" << std :: endl;
 return EXIT_SUCCESS;
}
