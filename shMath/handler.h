#ifndef SH_MATH_HANDLER
#define SH_MATH_HANDLER

#include <new>
#include <stdlib.h>

namespace shMath
{
 template <class T> class handler
 {
  public:
  handler(void);
  handler(const handler<T>& a);
  template <class X> handler(/*const */handler<X>& a);
  template <class X> handler(const X& a);
  ~handler(void);
  handler& operator = (const handler& a);
  template <class X> handler& operator = (const handler<X>& a);
  bool operator == (const handler& a) const;
  bool operator != (const handler& a) const;
  const T* operator -> () const;
  T* operator -> ();
  operator const T* () const;
  operator T* ();
  operator const void* () const;
  operator void* ();
  const T& operator * () const;
  T& operator * ();
  private:
  void kill();
  T* data;
  unsigned int* counter;
 };
}

template <class T> inline shMath :: handler <T> :: handler() : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0;
 try
 {
  data = new T();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> inline shMath :: handler <T> :: handler(const shMath :: handler<T>& a) : data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T> template <class X> inline shMath :: handler <T> :: handler(/*const */shMath :: handler<X>& a) : data((T*)a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T> template <class X> inline shMath :: handler <T> :: handler(const X& a) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  data = (T*)new X(a);
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> inline shMath :: handler <T> :: ~handler()
{
 kill();
}

template <class T> inline shMath :: handler<T>& shMath :: handler <T> :: operator = (const shMath :: handler<T>& a)
{
 if (&a != this)
 {
  kill();
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> template <class X> inline shMath :: handler<T>& shMath :: handler <T> :: operator = (const shMath :: handler<X>& a)
{
 if (&a != this)
 {
  kill();
  data = (const T*)(a.data);
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline bool shMath :: handler <T> :: operator == (const shMath :: handler<T>& a) const
{
 return (data == a.data);
}

template <class T> inline bool shMath :: handler <T> :: operator != (const shMath :: handler<T>& a) const
{
 return (data != a.data);
}

template <class T> inline const T* shMath :: handler <T> :: operator -> () const
{
 return data;
}

template <class T> inline T* shMath :: handler <T> :: operator -> ()
{
 return data;
}

template <class T> inline shMath :: handler <T> :: operator const T* () const
{
 return data;
}

template <class T> inline shMath :: handler <T> :: operator T* ()
{
 return data;
}

template <class T> inline shMath :: handler <T> :: operator const void* () const
{
 return (const void*)(data);
}

template <class T> inline shMath :: handler <T> :: operator void* ()
{
 return (void*)(data);
}

template <class T> inline const T& shMath :: handler <T> :: operator * () const
{
 return *data;
}

template <class T> inline T& shMath :: handler <T> :: operator * ()
{
 return *data;
}

template <class T> inline void shMath :: handler <T> :: kill()
{
 if ((*counter) == 0u)
 {
  free(counter);
  delete data;
 }
 else
 {
  --(*counter);
 }
}
#endif
