#ifndef SH_MATH_RCVECTORNFX
#define SH_MATH_RCVECTORNFX

#include <new>
#include <stdlib.h>

#include "ops.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include "rcvec2.h"
#include "rcvec3.h"
#include "rcvec4.h"
#include "vectorNfx.h"
#include "functions.h"

namespace shMath
{
 template <unsigned int n,class T> class rcvectorNfx
 {
  public:
  rcvectorNfx();
  rcvectorNfx(const rcvectorNfx&);
  rcvectorNfx(const vectorNfx<n,T>&);
  ~rcvectorNfx();
  rcvectorNfx& operator = (const rcvectorNfx& a);
  const T& operator [] (unsigned int a) const;
  T& operator [] (unsigned int a);
  rcvectorNfx operator - () const;
  rcvectorNfx operator + (const rcvectorNfx& a) const;
  rcvectorNfx operator - (const rcvectorNfx& a) const;
  rcvectorNfx operator * (const T& a) const;
  rcvectorNfx operator / (const T& a) const;
  rcvectorNfx& operator += (const rcvectorNfx& a);
  rcvectorNfx& operator -= (const rcvectorNfx& a);
  rcvectorNfx& operator *= (const T& a);
  rcvectorNfx& operator /= (const T& a);
  operator const vectorNfx<n,T> () const;
  operator vectorNfx<n,T> ();
  operator const T* () const;
  operator T* ();
  void normalise();
  T squaredLength() const;
  T length() const;
  private:
  void kill();
  void copy();
  T* data;
  unsigned int* counter;
 };

 template <class T> class rcvectorNfx <2u,T>
 {
  public:
  rcvectorNfx();
  rcvectorNfx(const vec2<T>& a);
  rcvectorNfx(const rcvec2<T>& a);
  rcvectorNfx(const rcvectorNfx&);
  rcvectorNfx(const vectorNfx<2u,T>&);
  ~rcvectorNfx();
  rcvectorNfx& operator = (const rcvectorNfx& a);
  const T& operator [] (unsigned int a) const;
  T& operator [] (unsigned int a);
  rcvectorNfx operator - () const;
  rcvectorNfx operator + (const rcvectorNfx& a) const;
  rcvectorNfx operator - (const rcvectorNfx& a) const;
  rcvectorNfx operator * (const T& a) const;
  rcvectorNfx operator / (const T& a) const;
  rcvectorNfx& operator += (const rcvectorNfx& a);
  rcvectorNfx& operator -= (const rcvectorNfx& a);
  rcvectorNfx& operator *= (const T& a);
  rcvectorNfx& operator /= (const T& a);
  operator const rcvec2<T> () const;
  operator rcvec2<T> ();
  operator const vec2<T>() const;
  operator const vectorNfx<2u,T> () const;
  operator vectorNfx<2u,T> ();
  operator const T* () const;
  operator T* ();
  void normalise();
  T squaredLength() const;
  T length() const;
  private:
  void kill();
  void copy();
  T* data;
  unsigned int* counter;
 };

 template <class T> class rcvectorNfx <3u,T>
 {
  public:
  rcvectorNfx();
  rcvectorNfx(const vec3<T>& a);
  rcvectorNfx(const rcvec3<T>& a);
  rcvectorNfx(const rcvectorNfx&);
  rcvectorNfx(const vectorNfx<3u,T>&);
  ~rcvectorNfx();
  rcvectorNfx& operator = (const rcvectorNfx& a);
  const T& operator [] (unsigned int a) const;
  T& operator [] (unsigned int a);
  rcvectorNfx operator - () const;
  rcvectorNfx operator + (const rcvectorNfx& a) const;
  rcvectorNfx operator - (const rcvectorNfx& a) const;
  rcvectorNfx operator * (const T& a) const;
  rcvectorNfx operator / (const T& a) const;
  rcvectorNfx& operator += (const rcvectorNfx& a);
  rcvectorNfx& operator -= (const rcvectorNfx& a);
  rcvectorNfx& operator *= (const T& a);
  rcvectorNfx& operator /= (const T& a);
  operator const rcvec3<T> () const;
  operator rcvec3<T> ();
  operator const vec3<T>() const;
  operator const vectorNfx<3u,T> () const;
  operator vectorNfx<3u,T> ();
  operator const T* () const;
  operator T* ();
  void normalise();
  T squaredLength() const;
  T length() const;
  private:
  void kill();
  void copy();
  T* data;
  unsigned int* counter;
 };

 template <class T> class rcvectorNfx <4u,T>
 {
  public:
  rcvectorNfx();
  rcvectorNfx(const vec4<T>& a);
  rcvectorNfx(const rcvec4<T>& a);
  rcvectorNfx(const rcvectorNfx&);
  rcvectorNfx(const vectorNfx<4u,T>&);
  ~rcvectorNfx();
  rcvectorNfx& operator = (const rcvectorNfx& a);
  const T& operator [] (unsigned int a) const;
  T& operator [] (unsigned int a);
  rcvectorNfx operator - () const;
  rcvectorNfx operator + (const rcvectorNfx& a) const;
  rcvectorNfx operator - (const rcvectorNfx& a) const;
  rcvectorNfx operator * (const T& a) const;
  rcvectorNfx operator / (const T& a) const;
  rcvectorNfx& operator += (const rcvectorNfx& a);
  rcvectorNfx& operator -= (const rcvectorNfx& a);
  rcvectorNfx& operator *= (const T& a);
  rcvectorNfx& operator /= (const T& a);
  operator const rcvec4<T> () const;
  operator rcvec4<T> ();
  operator const vec4<T>() const;
  operator const vectorNfx<4u,T> () const;
  operator vectorNfx<4u,T> ();
  operator const T* () const;
  operator T* ();
  void normalise();
  T squaredLength() const;
  T length() const;
  private:
  void kill();
  void copy();
  T* data;
  unsigned int* counter;
 };

 template <unsigned int n,class T> T length(const rcvectorNfx<n,T>& a);
 template <unsigned int n,class T> T squaredLength(const rcvectorNfx<n,T>& a);
 template <unsigned int n,class T> rcvectorNfx<n,T> zero(const rcvectorNfx<n,T>& a);
 template <unsigned int n,class T> rcvectorNfx<n,T> identity(const rcvectorNfx<n,T>& a);
 template <unsigned int n,class T> rcvectorNfx<n,T> normalise(const rcvectorNfx<n,T>& a);
 template <unsigned int n,class T> T dot(const rcvectorNfx<n,T>& a,const rcvectorNfx<n,T>& b);
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx <n,T> :: rcvectorNfx() : data(new T [n] ()), counter(NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> inline shMath :: rcvectorNfx <2u,T> :: rcvectorNfx() : data(new T [2u] ()), counter(NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> inline shMath :: rcvectorNfx <3u,T> :: rcvectorNfx() : data(new T [3u] ()), counter(NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> inline shMath :: rcvectorNfx <4u,T> :: rcvectorNfx() : data(new T [4u] ()), counter(NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> shMath :: rcvectorNfx<2u,T> :: rcvectorNfx(const vec2<T>& a) : data(new T [2u] ()), counter(NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  (*((shMath :: vec2<T>*)(data))) = a;
 }
 catch(...)
 {
  delete [] data;
  free(counter );
  throw;
 }
}

template <class T> shMath :: rcvectorNfx<3u,T> :: rcvectorNfx(const vec3<T>& a) : data(new T [3u] ()), counter(NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  (*((shMath :: vec3<T>*)(data))) = a;
 }
 catch(...)
 {
  delete [] data;
  free(counter );
  throw;
 }
}

template <class T> shMath :: rcvectorNfx<4u,T> :: rcvectorNfx(const vec4<T>& a) : data(new T [4u] ()), counter(NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  (*((shMath :: vec4<T>*)(data))) = a;
 }
 catch(...)
 {
  delete [] data;
  free(counter );
  throw;
 }
}

template <class T> inline shMath :: rcvectorNfx <2u,T> :: rcvectorNfx(const shMath :: rcvec2<T>& a) : data(((const shMath :: rcvectorNfx<2u,T>*)(&a))->data), counter(((const shMath :: rcvectorNfx<2,T>*)(&a))->counter)
{
 ++(*counter);
}

template <class T> inline shMath :: rcvectorNfx <3u,T> :: rcvectorNfx(const shMath :: rcvec3<T>& a) : data(((const shMath :: rcvectorNfx<3u,T>*)(&a))->data), counter(((const shMath :: rcvectorNfx<3,T>*)(&a))->counter)
{
 ++(*counter);
}

template <class T> inline shMath :: rcvectorNfx <4u,T> :: rcvectorNfx(const shMath :: rcvec4<T>& a) : data(((const shMath :: rcvectorNfx<4u,T>*)(&a))->data), counter(((const shMath :: rcvectorNfx<4,T>*)(&a))->counter)
{
 ++(*counter);
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx <n,T> :: rcvectorNfx(const shMath :: rcvectorNfx<n,T>& a) : data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T> inline shMath :: rcvectorNfx <2u,T> :: rcvectorNfx(const shMath :: rcvectorNfx<2u,T>& a) : data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T> inline shMath :: rcvectorNfx <3u,T> :: rcvectorNfx(const shMath :: rcvectorNfx<3u,T>& a) : data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T> inline shMath :: rcvectorNfx <4u,T> :: rcvectorNfx(const shMath :: rcvectorNfx<4u,T>& a) : data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <unsigned int n, class T> inline shMath :: rcvectorNfx <n,T> :: rcvectorNfx(const shMath :: vectorNfx<n,T>& a) : data((T*)malloc(n * sizeof(T))), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if ((counter == NULL) || (data == NULL))
 {
  if (counter != NULL)
  {
   free(counter);
  }
  if (data != NULL)
  {
   free(data);
  }
 }
 (*counter) = 0u;
 new (data) vectorNfx<n,T>(a); /*is it ok??*/
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx <n,T> :: ~rcvectorNfx()
{
 kill();
}

template <class T> inline shMath :: rcvectorNfx <2u,T> :: ~rcvectorNfx()
{
 kill();
}

template <class T> inline shMath :: rcvectorNfx <3u,T> :: ~rcvectorNfx()
{
 kill();
}

template <class T> inline shMath :: rcvectorNfx <4u,T> :: ~rcvectorNfx()
{
 kill();
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T>& shMath :: rcvectorNfx <n,T> :: operator = (const shMath :: rcvectorNfx<n,T>& a)
{
 if (&a != this)
 {
  kill();
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<2u,T>& shMath :: rcvectorNfx <2u,T> :: operator = (const shMath :: rcvectorNfx<2u,T>& a)
{
 if (&a != this)
 {
  kill();
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<3u,T>& shMath :: rcvectorNfx <3u,T> :: operator = (const shMath :: rcvectorNfx<3u,T>& a)
{
 if (&a != this)
 {
  kill();
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<4u,T>& shMath :: rcvectorNfx <4u,T> :: operator = (const shMath :: rcvectorNfx<4u,T>& a)
{
 if (&a != this)
 {
  kill();
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <unsigned int n,class T> inline const T& shMath :: rcvectorNfx <n,T> :: operator [] (unsigned int a) const
{
 return data[a];
}

template <class T> inline const T& shMath :: rcvectorNfx <2u,T> :: operator [] (unsigned int a) const
{
 return data[a];
}

template <class T> inline const T& shMath :: rcvectorNfx <3u,T> :: operator [] (unsigned int a) const
{
 return data[a];
}

template <class T> inline const T& shMath :: rcvectorNfx <4u,T> :: operator [] (unsigned int a) const
{
 return data[a];
}

template <unsigned int n,class T> inline T& shMath :: rcvectorNfx <n,T> :: operator [] (unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data[a];
}

template <class T> inline T& shMath :: rcvectorNfx <2u,T> :: operator [] (unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data[a];
}

template <class T> inline T& shMath :: rcvectorNfx <3u,T> :: operator [] (unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data[a];
}

template <class T> inline T& shMath :: rcvectorNfx <4u,T> :: operator [] (unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data[a];
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T> shMath :: rcvectorNfx <n,T> :: operator - () const
{
 shMath :: rcvectorNfx<n,T> ans;
 const T* temp1 = data;
 T* temp2 = ans.data;
 unsigned int i = 0u;
 for(;i < n;++i,*temp2++ = -(*temp1++));
 return ans;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T> shMath :: rcvectorNfx <n,T> :: operator + (const shMath :: rcvectorNfx<n,T>& a) const
{
 shMath :: rcvectorNfx<n,T> ans;
 const T* temp1 = a.data;
 const T* temp2 = data;
 T* temp = ans.data;
 unsigned int i = 0u;
 for(;i < n;++i,*temp++ = (*temp2++) + (*temp1++));
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<2u,T> shMath :: rcvectorNfx <2u,T> :: operator + (const shMath :: rcvectorNfx<2u,T>& a) const
{
 shMath :: rcvectorNfx<2u,T> ans;
 const T* temp1 = a.data;
 const T* temp2 = data;
 T* temp = ans.data;
 *temp++ = (*temp2++) + (*temp1++);
 *temp++ = (*temp2++) + (*temp1++);
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<3u,T> shMath :: rcvectorNfx <3u,T> :: operator + (const shMath :: rcvectorNfx<3u,T>& a) const
{
 shMath :: rcvectorNfx<3u,T> ans;
 const T* temp1 = a.data;
 const T* temp2 = data;
 T* temp = ans.data;
 *temp++ = (*temp2++) + (*temp1++);
 *temp++ = (*temp2++) + (*temp1++);
 *temp++ = (*temp2++) + (*temp1++);
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<4u,T> shMath :: rcvectorNfx <4u,T> :: operator + (const shMath :: rcvectorNfx<4u,T>& a) const
{
 shMath :: rcvectorNfx<4u,T> ans;
 const T* temp1 = a.data;
 const T* temp2 = data;
 T* temp = ans.data;
 *temp++ = (*temp2++) + (*temp1++);
 *temp++ = (*temp2++) + (*temp1++);
 *temp++ = (*temp2++) + (*temp1++);
 *temp++ = (*temp2++) + (*temp1++);
 return ans;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T> shMath :: rcvectorNfx <n,T> :: operator - (const shMath :: rcvectorNfx<n,T>& a) const
{
 shMath :: rcvectorNfx<n,T> ans;
 const T* temp1 = a.data;
 const T* temp2 = data;
 T* temp = ans.data;
 unsigned int i = 0u;
 for(;i < n;++i,*temp++ = (*temp2++) - (*temp1++));
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<2u,T> shMath :: rcvectorNfx <2u,T> :: operator - (const shMath :: rcvectorNfx<2u,T>& a) const
{
 shMath :: rcvectorNfx<2u,T> ans;
 const T* temp1 = a.data;
 const T* temp2 = data;
 T* temp = ans.data;
 *temp++ = (*temp2++) - (*temp1++);
 *temp++ = (*temp2++) - (*temp1++);
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<3u,T> shMath :: rcvectorNfx <3u,T> :: operator - (const shMath :: rcvectorNfx<3u,T>& a) const
{
 shMath :: rcvectorNfx<3u,T> ans;
 const T* temp1 = a.data;
 const T* temp2 = data;
 T* temp = ans.data;
 *temp++ = (*temp2++) - (*temp1++);
 *temp++ = (*temp2++) - (*temp1++);
 *temp++ = (*temp2++) - (*temp1++);
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<4u,T> shMath :: rcvectorNfx <4u,T> :: operator - (const shMath :: rcvectorNfx<4u,T>& a) const
{
 shMath :: rcvectorNfx<4u,T> ans;
 const T* temp1 = a.data;
 const T* temp2 = data;
 T* temp = ans.data;
 *temp++ = (*temp2++) - (*temp1++);
 *temp++ = (*temp2++) - (*temp1++);
 *temp++ = (*temp2++) - (*temp1++);
 *temp++ = (*temp2++) - (*temp1++);
 return ans;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T> shMath :: rcvectorNfx <n,T> :: operator * (const T& a) const
{
 shMath :: rcvectorNfx<n,T> ans;
 const T* tempd = data;
 T* temp = ans.data;
 unsigned int i = 0u;
 for(;i < n;++i,*temp++ = (*tempd++) * a);
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<2u,T> shMath :: rcvectorNfx <2u,T> :: operator * (const T& a) const
{
 shMath :: rcvectorNfx<2u,T> ans;
 const T* tempd = data;
 T* temp = ans.data;
 *temp++ = (*tempd++) * a;
 *temp++ = (*tempd++) * a;
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<3u,T> shMath :: rcvectorNfx <3u,T> :: operator * (const T& a) const
{
 shMath :: rcvectorNfx<3u,T> ans;
 const T* tempd = data;
 T* temp = ans.data;
 *temp++ = (*tempd++) * a;
 *temp++ = (*tempd++) * a;
 *temp++ = (*tempd++) * a;
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<4u,T> shMath :: rcvectorNfx <4u,T> :: operator * (const T& a) const
{
 shMath :: rcvectorNfx<4u,T> ans;
 const T* tempd = data;
 T* temp = ans.data;
 *temp++ = (*tempd++) * a;
 *temp++ = (*tempd++) * a;
 *temp++ = (*tempd++) * a;
 *temp++ = (*tempd++) * a;
 return ans;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T> shMath :: rcvectorNfx <n,T> :: operator / (const T& a) const
{
 shMath :: rcvectorNfx<n,T> ans;
 const T* tempd = data;
 T* temp = ans.data;
 unsigned int i = 0u;
 for(;i < n;++i,*temp++ = (*tempd++) / a);
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<2u,T> shMath :: rcvectorNfx <2u,T> :: operator / (const T& a) const
{
 shMath :: rcvectorNfx<2u,T> ans;
 const T* tempd = data;
 T* temp = ans.data;
 *temp++ = (*tempd++) / a;
 *temp++ = (*tempd++) / a;
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<3u,T> shMath :: rcvectorNfx <3u,T> :: operator / (const T& a) const
{
 shMath :: rcvectorNfx<3u,T> ans;
 const T* tempd = data;
 T* temp = ans.data;
 *temp++ = (*tempd++) / a;
 *temp++ = (*tempd++) / a;
 *temp++ = (*tempd++) / a;
 return ans;
}

template <class T> inline shMath :: rcvectorNfx<4u,T> shMath :: rcvectorNfx <4u,T> :: operator / (const T& a) const
{
 shMath :: rcvectorNfx<4u,T> ans;
 const T* tempd = data;
 T* temp = ans.data;
 *temp++ = (*tempd++) / a;
 *temp++ = (*tempd++) / a;
 *temp++ = (*tempd++) / a;
 *temp++ = (*tempd++) / a;
 return ans;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T>& shMath :: rcvectorNfx <n,T> :: operator += (const shMath :: rcvectorNfx<n,T>& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 const T* tempa = a.data;
 T* temp = data;
 unsigned int i = 0u;
 for(;i < n;++i,(*temp++) += (*tempa++));
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<2u,T>& shMath :: rcvectorNfx <2u,T> :: operator += (const shMath :: rcvectorNfx<2u,T>& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 const T* tempa = a.data;
 T* temp = data;
 (*temp++) += (*tempa++);
 (*temp++) += (*tempa++);
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<3u,T>& shMath :: rcvectorNfx <3u,T> :: operator += (const shMath :: rcvectorNfx<3u,T>& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 const T* tempa = a.data;
 T* temp = data;
 (*temp++) += (*tempa++);
 (*temp++) += (*tempa++);
 (*temp++) += (*tempa++);
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<4u,T>& shMath :: rcvectorNfx <4u,T> :: operator += (const shMath :: rcvectorNfx<4u,T>& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 const T* tempa = a.data;
 T* temp = data;
 (*temp++) += (*tempa++);
 (*temp++) += (*tempa++);
 (*temp++) += (*tempa++);
 (*temp++) += (*tempa++);
 return *this;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T>& shMath :: rcvectorNfx <n,T> :: operator -= (const shMath :: rcvectorNfx<n,T>& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 const T* tempa = a.data;
 T* temp = data;
 unsigned int i = 0u;
 for(;i < n;++i,(*temp++) -= (*tempa++));
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<2u,T>& shMath :: rcvectorNfx <2u,T> :: operator -= (const shMath :: rcvectorNfx<2u,T>& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 const T* tempa = a.data;
 T* temp = data;
 (*temp++) -= (*tempa++);
 (*temp++) -= (*tempa++);
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<3u,T>& shMath :: rcvectorNfx <3u,T> :: operator -= (const shMath :: rcvectorNfx<3u,T>& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 const T* tempa = a.data;
 T* temp = data;
 (*temp++) -= (*tempa++);
 (*temp++) -= (*tempa++);
 (*temp++) -= (*tempa++);
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<4u,T>& shMath :: rcvectorNfx <4u,T> :: operator -= (const shMath :: rcvectorNfx<4u,T>& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 const T* tempa = a.data;
 T* temp = data;
 (*temp++) -= (*tempa++);
 (*temp++) -= (*tempa++);
 (*temp++) -= (*tempa++);
 (*temp++) -= (*tempa++);
 return *this;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T>& shMath :: rcvectorNfx <n,T> :: operator *= (const T& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 T* temp = data;
 unsigned int i = 0u;
 for(;i < n;++i,(*temp++) *= a);
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<2u,T>& shMath :: rcvectorNfx <2u,T> :: operator *= (const T& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 T* temp = data;
 (*temp++) *= a;
 (*temp++) *= a;
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<3u,T>& shMath :: rcvectorNfx <3u,T> :: operator *= (const T& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 T* temp = data;
 (*temp++) *= a;
 (*temp++) *= a;
 (*temp++) *= a;
 return *this;
}

template <class T> inline shMath :: rcvectorNfx<4u,T>& shMath :: rcvectorNfx <4u,T> :: operator *= (const T& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 T* temp = data;
 (*temp++) *= a;
 (*temp++) *= a;
 (*temp++) *= a;
 (*temp++) *= a;
 return *this;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T>& shMath :: rcvectorNfx <n,T> :: operator /= (const T& a)
{
 return ((*this) *= shMath :: inverse(a));
}

template <class T> inline shMath :: rcvectorNfx<2u,T>& shMath :: rcvectorNfx <2u,T> :: operator /= (const T& a)
{
 return ((*this) *= shMath :: inverse(a));
}

template <class T> inline shMath :: rcvectorNfx<3u,T>& shMath :: rcvectorNfx <3u,T> :: operator /= (const T& a)
{
 return ((*this) *= shMath :: inverse(a));
}

template <class T> inline shMath :: rcvectorNfx<4u,T>& shMath :: rcvectorNfx <4u,T> :: operator /= (const T& a)
{
 return ((*this) *= shMath :: inverse(a));
}

template <class T> inline shMath :: rcvectorNfx <2u,T> :: operator const shMath :: rcvec2<T> () const
{
 return *((const shMath :: rcvec2<T>*)(this));
}

template <class T> inline shMath :: rcvectorNfx <3u,T> :: operator const shMath :: rcvec3<T> () const
{
 return *((const shMath :: rcvec3<T>*)(this));
}

template <class T> inline shMath :: rcvectorNfx <4u,T> :: operator const shMath :: rcvec4<T> () const
{
 return *((const shMath :: rcvec4<T>*)(this));
}

template <class T> inline shMath :: rcvectorNfx <2u,T> :: operator shMath :: rcvec2<T> ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return *((shMath :: rcvec2<T>*)(this));
}

template <class T> inline shMath :: rcvectorNfx <3u,T> :: operator shMath :: rcvec3<T> ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return *((shMath :: rcvec3<T>*)(this));
}

template <class T> inline shMath :: rcvectorNfx <4u,T> :: operator shMath :: rcvec4<T> ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return *((shMath :: rcvec4<T>*)(this));
}

template <class T> inline shMath :: rcvectorNfx <2u,T> :: operator const shMath :: vec2<T> () const
{
 return *((const shMath :: vec2<T>*)(data));
}

template <class T> inline shMath :: rcvectorNfx <3u,T> :: operator const shMath :: vec3<T> () const
{
 return *((const shMath :: vec3<T>*)(data));
}

template <class T> inline shMath :: rcvectorNfx <4u,T> :: operator const shMath :: vec4<T> () const
{
 return *((const shMath :: vec4<T>*)(data));
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx <n,T> :: operator const T* () const
{
 return data;
}

template <class T> inline shMath :: rcvectorNfx <2u,T> :: operator const T* () const
{
 return data;
}

template <class T> inline shMath :: rcvectorNfx <3u,T> :: operator const T* () const
{
 return data;
}

template <class T> inline shMath :: rcvectorNfx <4u,T> :: operator const T* () const
{
 return data;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx <n,T> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T> inline shMath :: rcvectorNfx <2u,T> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T> inline shMath :: rcvectorNfx <3u,T> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T> inline shMath :: rcvectorNfx <4u,T> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <unsigned int n,class T> inline void shMath :: rcvectorNfx <n,T> :: normalise()
{
 const T ilen = shMath :: inverse(length());
 (*this) *= ilen;
}

template <class T> inline void shMath :: rcvectorNfx <2u,T> :: normalise()
{
 const T ilen = shMath :: inverse(length());
 (*this) *= ilen;
}

template <class T> inline void shMath :: rcvectorNfx <3u,T> :: normalise()
{
 const T ilen = shMath :: inverse(length());
 (*this) *= ilen;
}

template <class T> inline void shMath :: rcvectorNfx <4u,T> :: normalise()
{
 const T ilen = shMath :: inverse(length());
 (*this) *= ilen;
}

template <unsigned int n,class T> inline T shMath :: rcvectorNfx <n,T> :: squaredLength() const
{
 const T* temp = data;
 unsigned int i = 0u;
 T ans = (*temp) * (*temp);
 for(++temp;i < n;++i,++temp)
 {
  ans += (*temp) * (*temp);
 }
 return ans;
}

template <class T> inline T shMath :: rcvectorNfx <2u,T> :: squaredLength() const
{
 const T* temp = data;
 T ans = (*temp) * (*temp);
 ++temp;
 ans += (*temp) * (*temp);
 return ans;
}

template <class T> inline T shMath :: rcvectorNfx <3u,T> :: squaredLength() const
{
 const T* temp = data;
 T ans = (*temp) * (*temp);
 ++temp;
 ans += (*temp) * (*temp);
 ++temp;
 ans += (*temp) * (*temp);
 return ans;
}

template <class T> inline T shMath :: rcvectorNfx <4u,T> :: squaredLength() const
{
 const T* temp = data;
 T ans = (*temp) * (*temp);
 ++temp;
 ans += (*temp) * (*temp);
 ++temp;
 ans += (*temp) * (*temp);
 ++temp;
 ans += (*temp) * (*temp);
 return ans;
}

template <unsigned int n,class T> inline T shMath :: rcvectorNfx <n,T> :: length() const
{
 return shMath :: sqrt(squaredLength());
}

template <class T> inline T shMath :: rcvectorNfx <2u,T> :: length() const
{
 return shMath :: sqrt(squaredLength());
}

template <class T> inline T shMath :: rcvectorNfx <3u,T> :: length() const
{
 return shMath :: sqrt(squaredLength());
}

template <class T> inline T shMath :: rcvectorNfx <4u,T> :: length() const
{
 return shMath :: sqrt(squaredLength());
}

template <unsigned int n,class T> inline void shMath :: rcvectorNfx <n,T> :: kill()
{
 if ((*counter) == 0u)
 {
  delete [] data;
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> inline void shMath :: rcvectorNfx <2u,T> :: kill()
{
 if ((*counter) == 0u)
 {
  delete [] data;
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> inline void shMath :: rcvectorNfx <3u,T> :: kill()
{
 if ((*counter) == 0u)
 {
  delete [] data;
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> inline void shMath :: rcvectorNfx <4u,T> :: kill()
{
 if ((*counter) == 0u)
 {
  delete [] data;
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <unsigned int n,class T> void shMath :: rcvectorNfx <n,T> :: copy()
{
 T* temp = new T [n] ();
 unsigned int* tempcount = (unsigned int*)malloc(sizeof(unsigned int));
 if (tempcount == NULL)
 {
  delete [] temp;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  unsigned int i = 0u;
  const T* tempdata = data;
  T* tempr = temp;
  for(;i < n;++i,(*tempr++) = (*tempdata++));
 }
 catch(...)
 {
  delete [] temp;
  free(tempcount);
  throw;
 }
 --(*counter);
 counter = tempcount;
 data = temp;
}

template <class T> void shMath :: rcvectorNfx <2u,T> :: copy()
{
 T* temp = new T [2u] ();
 unsigned int* tempcount = (unsigned int*)malloc(sizeof(unsigned int));
 if (tempcount == NULL)
 {
  delete [] temp;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  T* tempr = temp;
  const T* tempdata = data;
  (*tempr++) = (*tempdata++);
  (*tempr++) = (*tempdata++);
 }
 catch(...)
 {
  delete [] temp;
  free(tempcount);
  throw;
 }
 --(*counter);
 counter = tempcount;
 data = temp;
}

template <class T> void shMath :: rcvectorNfx <3u,T> :: copy()
{
 T* temp = new T [3u] ();
 unsigned int* tempcount = (unsigned int*)malloc(sizeof(unsigned int));
 if (tempcount == NULL)
 {
  delete [] temp;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  T* tempr = temp;
  const T* tempdata = data;
  (*tempr++) = (*tempdata++);
  (*tempr++) = (*tempdata++);
  (*tempr++) = (*tempdata++);
 }
 catch(...)
 {
  delete [] temp;
  free(tempcount);
  throw;
 }
 --(*counter);
 counter = tempcount;
 data = temp;
}

template <class T> void shMath :: rcvectorNfx <4u,T> :: copy()
{
 T* temp = new T [4u] ();
 unsigned int* tempcount = (unsigned int*)malloc(sizeof(unsigned int));
 if (tempcount == NULL)
 {
  delete [] temp;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  T* tempr = temp;
  const T* tempdata = data;
  (*tempr++) = (*tempdata++);
  (*tempr++) = (*tempdata++);
  (*tempr++) = (*tempdata++);
  (*tempr++) = (*tempdata++);
 }
 catch(...)
 {
  delete [] temp;
  free(tempcount);
  throw;
 }
 --(*counter);
 counter = tempcount;
 data = temp;
}

template <unsigned int n,class T> inline T shMath :: length(const shMath :: rcvectorNfx<n,T>& a)
{
 return shMath :: sqrt(shMath :: squaredLength(a));
}

template <unsigned int n,class T> inline T shMath :: squaredLength(const shMath :: rcvectorNfx<n,T>& a)
{
 return shMath :: dot(a,a);
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T> shMath :: zero(const shMath :: rcvectorNfx<n,T>& a)
{
 shMath :: rcvectorNfx<n,T> ans;
 T* temp = (T*)(ans);
 unsigned int i = 0u;
 for(;i < n;++temp)
 {
  (*temp) = shMath :: zero(*temp);
 }
 return ans;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T> shMath :: identity(const shMath :: rcvectorNfx<n,T>& a)
{
 shMath :: rcvectorNfx<n,T> ans;
 T* temp = (T*)(ans);
 unsigned int i = 1u;
 (*temp) = shMath :: identity(*temp);
 for(++temp;i < n;++temp)
 {
  (*temp) = shMath :: zero(*temp);
 }
 return ans;
}

template <unsigned int n,class T> inline shMath :: rcvectorNfx<n,T> shMath :: normalise(const shMath :: rcvectorNfx<n,T>& a)
{
 const T ilen = shMath :: inverse(shMath :: length(a));
 return a * ilen;
}

template <unsigned int n,class T> inline T shMath :: dot(const shMath :: rcvectorNfx<n,T>& a, const shMath :: rcvectorNfx<n,T>& b)
{
 const T* tempa = (const T*)(a);
 const T* tempb = (const T*)(b);
 T ans = (*tempa++) * (*tempb++);
 unsigned int i = 0u;
 for(;i < n;++i,ans += (*tempa++) * (*tempb++));
 return ans;
}
#endif
