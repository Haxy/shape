#ifndef SH_MATH_VEC4
#define SH_MATH_VEC4

#include "ops.h"
#include "vec3.h"
#include "vec2.h"
#include "functions.h"

namespace shMath
{
 template <class T> class vec4
 {
  public:
  vec4();
  vec4(const T& xx,const T& yy = shMath :: zero(T()), const T& zz = shMath :: zero(T()), const T& ww = shMath :: zero(T()));
  vec4(const vec3<T>& xxyyzz, const T& ww = shMath :: zero(T()));
  vec4(const T& xx, const vec3<T>& yyzzww);
  vec4(const vec2<T>& xxxyy, const vec2<T>& zzww);
  vec4(const vec2<T>& xxyy, const T& zz = shMath :: zero(T()), const T& ww = shMath :: zero(T()));
  vec4(const T& xx, const vec2<T>& yyzz, const T& ww = shMath :: zero(T()));
  vec4(const T& xx, const T& yy, const vec2<T>& zzww);
  vec4 operator - () const;
  operator const T* () const;
  operator T* ();
  operator const vec3<T> () const;
  operator vec3<T> ();
  operator const vec2<T> () const;
  operator vec2<T> ();
  void normalise();
  const vec3<T>& xyz() const;
  const vec3<T>& yzw() const;
  const vec2<T>& xy() const;
  const vec2<T>& yz() const;
  const vec2<T>& zw() const;
  const T& x() const;
  const T& y() const;
  const T& z() const;
  const T& w() const;
  vec3<T>& xyz();
  vec3<T>& yzw();
  vec2<T>& xy();
  vec2<T>& yz();
  vec2<T>& zw();
  T& x();
  T& y();
  T& z();
  T& w();
  private:
  T X;
  T Y;
  T Z;
  T W;
 };

 template <class T> vec4<T> zero(const vec4<T>& a);

 template <class T> T slength(const vec4<T>& a);
 template <class T> T length(const vec4<T>& a);
 template <class T> vec4<T> normalise(const vec4<T>& a);

 template <class T> T dot(const vec4<T>& a, const vec4<T>& b);
 template <class T> vec4<T> cross(const vec4<T>& a, const vec4<T>& b, const vec4<T>& c);

 template <class T> vec4<T> operator + (const vec4<T>& a, const vec4<T>& b);
 template <class T> vec4<T> operator - (const vec4<T>& a, const vec4<T>& b);
 template <class T> vec4<T> operator * (const vec4<T>& a, const T& b);
 template <class T> vec4<T> operator / (const vec4<T>& a, const T& b);
 template <class T> vec4<T> operator * (float a, const vec4<T>& b);
 template <class T> vec4<T> operator * (double a, const vec4<T>& b);
 template <class T> vec4<T> operator * (long double a, const vec4<T>& b);
 template <class T> vec4<T>& operator += (vec4<T>& a, const vec4<T>& b);
 template <class T> vec4<T>& operator -= (vec4<T>& a, const vec4<T>& b);
 template <class T> vec4<T>& operator *= (vec4<T>& a, const T& b);
 template <class T> vec4<T>& operator /= (vec4<T>& a, const T& b);
}

template <class T> inline shMath :: vec4 <T> :: vec4() : X(T()), Y(T()), Z(T()), W(T())
{
}

template <class T> inline shMath :: vec4 <T> :: vec4(const T& xx, const T& yy, const T& zz, const T& ww) : X(xx), Y(yy), Z(zz), W(ww)
{
}

template <class T> inline shMath :: vec4 <T> :: vec4(const shMath :: vec3<T>& xxyyzz, const T& ww) : X(xxyyzz.x), Y(xxyyzz.y), Z(xxyyzz.z), W(ww)
{
}

template <class T> inline shMath :: vec4 <T> :: vec4(const T& xx, const shMath :: vec3<T>& yyzzww) : X(xx), Y(yyzzww.x), Z(yyzzww.y), W(yyzzww.z)
{
}

template <class T> inline shMath :: vec4 <T> :: vec4(const shMath :: vec2<T>& xxyy, const shMath :: vec2<T>& zzww) : X(xxyy.x), Y(xxyy.y), Z(zzww.x), W(zzww.y)
{
}

template <class T> inline shMath :: vec4 <T> :: vec4(const shMath :: vec2<T>& xxyy, const T& zz, const T& ww) : X(xxyy.x), Y(xxyy.y), Z(zz), W(ww)
{
}

template <class T> inline shMath :: vec4 <T> :: vec4(const T& xx, const shMath :: vec2<T>& yyzz, const T& ww) : X(xx), Y(yyzz.x), Z(yyzz.y), W(ww)
{
}

template <class T> inline shMath :: vec4 <T> :: vec4(const T& xx, const T& yy, const shMath :: vec2<T>& zzww) : X(xx), Y(yy), Z(zzww.x), W(zzww.y)
{
}

template <class T> inline shMath :: vec4<T> shMath :: vec4 <T> :: operator - () const
{
 return shMath :: vec4<T>(-X,-Y,-Z,-W);
}

template <class T> inline shMath :: vec4 <T> :: operator const T* () const
{
 return (const T*)(this);
}

template <class T> inline shMath :: vec4 <T> :: operator T* ()
{
 return (T*)(this);
}

template <class T> inline shMath :: vec4 <T> :: operator const shMath :: vec3<T> () const
{
 return *((const shMath :: vec3<T>*)(this));
}

template <class T> inline shMath :: vec4 <T> :: operator shMath :: vec3<T> ()
{
 return *((shMath :: vec3<T>*)(this));
}

template <class T> inline shMath :: vec4 <T> :: operator const shMath :: vec2<T> () const
{
 return *((const shMath :: vec2<T>*)(this));
}

template <class T> inline shMath :: vec4 <T> :: operator shMath :: vec2<T> ()
{
 return *((shMath :: vec2<T>*)(this));
}

template <class T> inline void shMath :: vec4 <T> :: normalise()
{
 const T len = shMath :: inverse(shMath :: length(*this));
 X *= len;
 Y *= len;
 Z *= len;
 W *= len;
}

template <class T> inline const shMath :: vec3<T>& shMath :: vec4 <T> :: xyz() const
{
 return *((const shMath :: vec3<T>*)(this));
}

template <class T> inline const shMath :: vec3<T>& shMath :: vec4 <T> :: yzw() const
{
 return *((const shMath :: vec3<T>*)(++(const T*)(this)));
}

template <class T> inline const shMath :: vec2<T>& shMath :: vec4 <T> :: xy() const
{
 return *((const shMath :: vec2<T>*)(this));
}

template <class T> inline const shMath :: vec2<T>& shMath :: vec4 <T> :: yz() const
{
 return *((const shMath :: vec2<T>*)(++(const T*)(this)));
}

template <class T> inline const shMath :: vec2<T>& shMath :: vec4 <T> :: zw() const
{
 return *((const shMath :: vec2<T>*)(((const T*)(this)) + 2));
}

template <class T> inline const T& shMath :: vec4 <T> :: x() const
{
 return X;
}

template <class T> inline const T& shMath :: vec4 <T> :: y() const
{
 return Y;
}

template <class T> inline const T& shMath :: vec4 <T> :: z() const
{
 return Z;
}

template <class T> inline const T& shMath :: vec4 <T> :: w() const
{
 return W;
}

template <class T> inline shMath :: vec3<T>& shMath :: vec4 <T> :: xyz()
{
 return *((shMath :: vec3<T>*)(this));
}

template <class T> inline shMath :: vec3<T>& shMath :: vec4 <T> :: yzw()
{
 return *((shMath :: vec3<T>*)(++(T*)(this)));
}

template <class T> inline shMath :: vec2<T>& shMath :: vec4 <T> :: xy()
{
 return *((shMath :: vec2<T>*)(this));
}

template <class T> inline shMath :: vec2<T>& shMath :: vec4 <T> :: yz()
{
 return *((shMath :: vec2<T>*)(++(T*)(this)));
}

template <class T> inline shMath :: vec2<T>& shMath :: vec4 <T> :: zw()
{
 return *((shMath :: vec2<T>*)(((T*)(this)) + 2));
}

template <class T> inline T& shMath :: vec4 <T> :: x()
{
 return X;
}

template <class T> inline T& shMath :: vec4 <T> :: y()
{
 return Y;
}

template <class T> inline T& shMath :: vec4 <T> :: z()
{
 return Z;
}

template <class T> inline T& shMath :: vec4 <T> :: w()
{
 return W;
}

template <class T> inline shMath :: vec4<T> shMath :: zero(const shMath :: vec4<T>& a)
{
 return shMath :: vec4<T>(shMath :: zero(a.x()),shMath :: zero(a.y()),shMath :: zero(a.z()),shMath :: zero(a.w()));
}

template <class T> inline T shMath :: slength(const shMath :: vec4<T>& a)
{
 return ((a.x() * a.x()) + (a.y() * a.y()) + (a.z() * a.z()) + (a.w() * a.w()));
}

template <class T> inline T shMath :: length(const shMath :: vec4<T>& a)
{
 return shMath :: sqrt((a.x() * a.x()) + (a.y() * a.y()) + (a.z() * a.z()) + (a.w() * a.w()));
}

template <class T> inline shMath :: vec4<T> shMath :: normalise(const shMath :: vec4<T>& a)
{
 return a / shMath :: length(a);
}

template <class T> inline T shMath :: dot(const shMath :: vec4<T>& a, const shMath :: vec4<T>& b)
{
 return ((a.x() * b.x()) + (a.y() * b.y()) + (a.z() * b.z()) + (a.w() * b.w()));
}

template <class T> inline shMath :: vec4<T> shMath :: cross(const shMath :: vec4<T>& a, const shMath :: vec4<T>& b, const shMath :: vec4<T>& c) //poprawi� - co drugi minusy - niby s�, ale da rad� lepiej
{
 return shMath :: vec4<T>((a.y() * b.z() * c.w()) + (a.z() * b.w() * c.y()) + (a.w() * b.y() * c.z()) - (a.w() * b.z() * c.y()) - (a.z() * b.y() * c.w()) - (a.w() * b.z() * c.y()),-((a.x() * b.z() * c.w()) + (a.z() * b.w() * c.x()) + (a.w() * b.x() * c.z()) - (a.w() * b.z() * c.x()) - (a.z() * b.x() * c.w()) - (a.x() * b.w() * c.z())),(a.x() * b.y() * c.w()) + (a.y() * b.w() * c.x()) + (a.w() * b.x() * c.y()) - (a.w() * b.y() * c.x()) - (a.y() * b.x() * c.w()) - (a.x() * b.w() * c.x()),-((a.x() * b.y() * c.z()) + (a.y() * b.z() * c.x()) + (a.z() * b.x() * c.y()) - (a.z() * b.y() * c.x()) - (a.y() * b.x() * c.z()) - (a.x() * b.z() * c.y())));
}

template <class T> inline shMath :: vec4<T> shMath :: operator + (const shMath :: vec4<T>& a, const shMath :: vec4<T>& b)
{
 return shMath :: vec4<T>(a.x() + b.x(),a.y() + b.y(),a.z() + b.z(),a.w() + b.w());
}

template <class T> inline shMath :: vec4<T> shMath :: operator - (const shMath :: vec4<T>& a, const shMath :: vec4<T>& b)
{
 return shMath :: vec4<T>(a.x() - b.x(),a.y() - b.y(),a.z() - b.z(),a.w() - b.w());
}

template <class T> inline shMath :: vec4<T> shMath :: operator * (const shMath :: vec4<T>& a, const T& b)
{
 return shMath :: vec4<T>(a.x() * b, a.y() * b, a.z() * b, a.w() * b);
}

template <class T> inline shMath :: vec4<T> shMath :: operator / (const shMath :: vec4<T>& a, const T& b)
{
 const T bb = shMath :: inverse(b);
 return shMath :: vec4<T>(a.x() * bb, a.y() * bb, a.z() * bb, a.w() * bb);
}

template <class T> inline shMath :: vec4<T> shMath :: operator * (float a, const shMath :: vec4<T>& b)
{
 return shMath :: vec4<T>(a * b.x(), a * b.y(), a * b.z(), a * b.w());
}

template <class T> inline shMath :: vec4<T> shMath :: operator * (double a, const shMath :: vec4<T>& b)
{
 return shMath :: vec4<T>(a * b.x(), a * b.y(), a * b.z(), a * b.w());
}

template <class T> inline shMath :: vec4<T> shMath :: operator * (long double a, const shMath :: vec4<T>& b)
{
 return shMath :: vec4<T>(a * b.x(), a * b.y(), a * b.z(), a * b.w());
}

template <class T> inline shMath :: vec4<T>& shMath :: operator += (shMath :: vec4<T>& a, const shMath :: vec4<T>& b)
{
 a.x() += b.x();
 a.y() += b.y();
 a.z() += b.z();
 a.w() += b.w();
 return a;
}

template <class T> inline shMath :: vec4<T>& shMath :: operator -= (shMath :: vec4<T>& a, const shMath :: vec4<T>& b)
{
 a.x() -= b.x();
 a.y() -= b.y();
 a.z() -= b.z();
 a.w() -= b.w();
 return a;
}

template <class T> inline shMath :: vec4<T>& shMath :: operator *= (shMath :: vec4<T>& a, const T& b)
{
 a.x() *= b;
 a.y() *= b;
 a.z() *= b;
 a.w() *= b;
 return a;
}

template <class T> inline shMath :: vec4<T>& shMath :: operator /= (shMath :: vec4<T>& a , const T& b)
{
 const T bb = shMath :: inverse(b);
 a.x() *= bb;
 a.y() *= bb;
 a.z() *= bb;
 a.w() *= bb;
 return a;
}
#endif
