#ifndef SH_MATH_STACK
#define SH_MATH_STACK

namespace shMath
{
 template <class T> class stack
 {
  public:
  stack(void);
  explicit stack(unsigned int size);
  stack(const stack& a);
  ~stack(void);
  stack& operator = (const stack& a);
  void push(const T& a);
  T pop();
  T pick() const;
  unsigned int getSize() const;
  unsigned int getIndex() const;
  void resize(unsigned int a);
  void clear();
  private:
  unsigned int num;
  unsigned int index;
  T* tab;
 };
}

template <class T> inline shMath :: stack <T> :: stack() : num(0u), index(0u), tab(NULL)
{
}

template <class T> inline shMath :: stack <T> :: stack(unsigned int size) : num(size), index(0u), tab((size != 0u) ? new T [size] : NULL)
{
}

template <class T> shMath :: stack <T> :: stack(const shMath :: stack<T>& a) : num(a.getSize()), index(a.getIndex()), tab((a.getSize() != 0u) ? new T [a.getSize()] : NULL)
{
 try
 {
  unsigned int i = 0u;
  const T* temp1 = a.tab;
  T* temp2 = tab;
  for(;i < num;++i)
  {
   (*temp2++) = (*temp1++);
  }
 }
 catch(...)
 {
  delete [] tab;
  throw;
 }
}

template <class T> inline shMath :: stack <T> :: ~stack()
{
 if (tab != NULL)
 {
  delete [] tab;
 }
}

template <class T> shMath :: stack<T>& shMath :: stack <T> :: operator = (const shMath :: stack<T>& a)
{
 const T* temp2 = a.tab;
 T* temp1 = NULL;
 T* temp = NULL;
 unsigned int i = 0u;
 try
 {
  temp = ((a.getSize() != 0u) ? new T [a.getSize()] () : NULL);
  for(temp1 = temp;i < a.getSize();++i)
  {
   (*temp1++) = (*temp2++);
  }
 }
 catch(...)
 {
  if (temp != NULL)
  {
   delete [] temp;
  }
  throw;
 }
 if (tab != NULL)
 {
  delete [] tab;
 }
 tab = temp;
 num = a.num;
 index = a.index;
 return *this;
}

template <class T> inline void shMath :: stack <T> :: push(const T& a)
{
 if ((++index) >= num)
 {
  resize(num + 1u);
 }
 tab[index - 1u] = a;
}

template <class T> inline T shMath :: stack <T> :: pop()
{
 return ((index != 0u) ? tab[--index] : T());
}

template <class T> inline T shMath :: stack <T> :: pick() const
{
 return ((index != 0u) ? tab[index - 1u] : T());
}

template <class T> inline unsigned int shMath :: stack <T> :: getSize() const
{
 return num;
}

template <class T> inline unsigned int shMath :: stack <T> :: getIndex() const
{
 return index;
}

template <class T> void shMath :: stack <T> :: resize(unsigned int a)
{
 unsigned int i = 0u;
 T* temp = ((a != 0u) ? new T [a] () : NULL);
 T* temp1 = temp;
 const T* temp2 = tab;
 try
 {
  for(;(i < a) && (i < num);++i,++temp1,++temp2)
  {
   (*temp1) = (*temp2);
  }
 }
 catch(...)
 {
  delete [] temp;
  throw;
 }
 if (a < index)
 {
  index = a;
 }
 if (tab != NULL)
 {
  delete [] tab;
 }
 tab = temp;
 num = a;
}

template <class T> inline void shMath :: stack <T> :: clear()
{
 index = 0u;
}
#endif
