#ifndef SH_MATH_MAT2
#define SH_MATH_MAT2

#include "ops.h"
#include "vec2.h"


/*|ac|
  |bd|*/

namespace shMath
{
 template <class T> class mat2
 {
  public:
  mat2();
  mat2(const T& aa, const T& bb = shMath :: zero(T()), const T& cc = shMath :: zero(T()), const T& dd = shMath :: zero(T());
  mat2 operator - () const;
  mat2 operator + (const mat2<T>& m) const;
  mat2 operator - (const mat2<T>& m) const;
  mat2 operator * (const mat2<T>& m) const;
  vec2<T> operator * (const vec2<T>& m) const;
  mat2 operator * (const T& m) const;
  mat2 operator / (const mat2<T>& m) const;
  mat2 operator / (const T& m) const;
  mat2<T>& operator += (const mat2<T>& m);
  mat2<T>& operator -= (const mat2<T>& m);
  mat2<T>& operator *= (const mat2<T>& m);
  mat2<T>& operator *= (const T& m);
  mat2<T>& operator /= (const mat2<T>& m);
  mat2<T>& operator /= (const T& m);
  operator const T* () const;
  operator T* ();
  T a;
  T b;
  T c;
  T d;
 };

 template <class T> mat2<T> zero(const mat2<T>& a);
 template <class T> mat2<T> identity(const mat2<T>& a);
 template <class T> mat2<T> inverse(const mat2<T>& a);

 template <class T> T determinant(const mat2<T>& a);
 template <class T> mat2<T> adj(const mat2<T>& a);
 template <class T> mat2<T> transpose(const mat2<T>& a);

 template <class T> vec2<T> operator * (const vec2<T>& a, const mat2<T>& b);
 template <class T> mat2<T> operator * (float a, const mat2<T>& b);
 template <class T> mat2<T> operator * (double a, const mat2<T>& b);
 template <class T> mat2<T> operator * (long double a, const mat2<T>& b);
 template <class T> vec2<T> operator / (const vec2<T>& a, const mat2<T>& b);
 template <class T> mat2<T> operator / (float a, const mat2<T>& b);
 template <class T> mat2<T> operator / (double a, const mat2<T>& b);
 template <class T> mat2<T> operator / (long double a, const mat2<T>& b);
 template <class T> vec2<T>& operator *= (vec2<T>& a, const mat2<T>& b);
 template <class T> vec2<T>& operator /= (vec2<T>& a, const mat2<T>& b);
}

template <class T> inline shMath :: mat2 <T> :: mat2() : a(T()), b(T()), c(T()), d(T())
{
}

template <class T> inline shMath ::mat2 <T> :: mat2(const T& aa, const T& bb, const T& cc, const T& dd) : a(aa), b(bb), c(cc), d(dd)
{
}

template <class T> inline shMath :: mat2<T> shMath :: mat2 <T> :: operator - () const
{
 return shMath :: mat2<T>(-a, -b, -c, -d);
}

template <class T> inline shMath :: mat2<T> shMath :: mat2 <T> :: operator + (const shMath :: mat2<T>& m) const
{
 return shMath :: mat2<T>(a + m.a, b + m.b, c + m.c, d + m.d);
}

template <class T> inline shMath :: mat2<T> shMath :: mat2 <T> :: operator - (const shMath :: mat2<T>& m) const
{
 return shMath :: mat2<T>(a - m.a, b - m.b, c - m.c, d - m.d);
}

template <class T> inline shMath :: mat2<T> shMath :: mat2 <T> :: operator * (const shMath :: mat2<T>& m) const
{
 return shMath :: mat2<T>((a * m.a) + (c * m.b), (a * m.c) + (c * m.d), (b * m.a) + (d * m.b), (b * m.c) + (d * m.d));
}

template <class T> inline shMath :: vec2<T> shMath :: mat2 <T> :: operator * (const shMath :: vec2<T>& m) const
{
 return shMath :: vec2<T>((a * m.x()) + (c * m.y()), (b * m.x()) + (d * m.y()));
}

template <class T> inline shMath :: mat2<T>  shMath :: mat2 <T> :: operator * (const T& m) const
{
 return shMath :: mat2<T>(a * m, b * m, c * m, d * m);
}

template <class T> inline shMath :: mat2<T> shMath :: mat2 <T> :: operator / (const shMath :: mat2<T>& m) const
{
 return (*this) * shMath :: inverse(m);
}

template <class T> inline shMath :: mat2<T> shMath :: mat2 <T> :: operator / (const T& m) const
{
 const T mm = shMath :: inverse(m);
 return shMath :: mat2<T>(a * mm, b * mm, c * mm, d * mm);
}

template <class T> inline shMath :: mat2<T>& shMath :: mat2 <T> :: operator += (const shMath :: mat2<T>& m)
{
 a += m.a;
 b += m.b;
 c += m.c;
 d += m.d;
 return *this;
}

template <class T> inline shMath :: mat2<T>& shMath :: mat2 <T> :: operator -= (const shMath :: mat2<T>& m)
{
 a -= m.a;
 b -= m.b;
 c -= m.c;
 d -= m.d;
 return *this;
}

template <class T> inline shMath :: mat2<T>& shMath :: mat2 <T> :: operator *= (const shMath :: mat2<T>& m)
{
 return ((*this) = (*this) * m);
}

template <class T> inline shMath :: mat2<T>& shMath :: mat2 <T> :: operator *= (const T& m)
{
 a *= m;
 b *= m;
 c *= m;
 d *= m;
 return *this;
}

template <class T> inline shMath :: mat2<T>& shMath :: mat2 <T> :: operator /= (const shMath :: mat2<T>& m)
{
 return ((*this) = (*this) / m);
}

template <class T> inline shMath :: mat2<T>& shMath :: mat2 <T> :: operator /= (const T& m)
{
 const T mm = shMath :: inverse(m);
 a *= mm;
 b *= mm;
 c *= mm;
 d *= mm;
 return *this;
}

template <class T> inline shMath :: mat2 <T> :: operator const T* () const
{
 return (const T*)(this);
}

template <class T> inline shMath :: mat2 <T> :: operator T* ()
{
 return (T*)(this);
}

template <class T> inline shMath :: mat2<T> shMath :: zero(const shMath :: mat2<T>& a)
{
 return shMath :: mat2<T>(shMath :: zero(a.a), shMath :: zero(a.b), shMath :: zero(a.c), shMath :: zero(a.d));
}

template <class T> inline shMath :: mat2<T> shMath :: identity(const shMath :: mat2<T>& a)
{
 return shMath :: mat2<T>(shMath :: identity(a.a), shMath :: zero(a.b), shMath :: zero(a.c), shMath :: identity(a.d));
}

template <class T> inline shMath :: mat2<T> shMath :: inverse(const shMath :: mat2<T>& a)
{
 return shMath :: adj(a) / shMath :: determinant(a);
}

template <class T> inline T shMath :: determinant(const shMath :: mat2<T>& a)
{
 return ((a.a * a.d) - (a.c * a.b));
}

template <class T> inline shMath :: mat2<T> shMath :: adj(const shMath :: mat2<T>& a)
{
 return shMath :: mat2<T>(a.d, -a.b, -a.c, a.a);
}

template <class T> inline shMath :: mat2<T> shMath :: transpose(const shMath :: mat2<T>& a)
{
 return shMath :: mat2<T>(a.a, a.c, a.b, a.d);
}

template <class T> inline shMath :: vec2<T> shMath :: operator * (const shMath :: vec2<T>& a, const shMath :: mat2<T>& b)
{
 return shMath :: vec2<T>((a.x() * b.a) + (a.y() * b.b),(a.x() * b.c) + (a.y() * b.d));
}

template <class T> inline shMath :: mat2<T> shMath :: operator * (float a, const shMath :: mat2<T>& b)
{
 return shMath :: mat2<T>(a * b.a, a * b.b, a * b.c, a * b.d);
}

template <class T> inline shMath :: mat2<T> shMath :: operator * (double a, const shMath :: mat2<T>& b)
{
 return shMath :: mat2<T>(a * b.a, a * b.b, a * b.c, a * b.d);
}

template <class T> inline shMath :: mat2<T> shMath :: operator * (long double a, const shMath :: mat2<T>& b)
{
 return shMath :: mat2<T>(a * b.a, a * b.b, a * b.c, a * b.d);
}

template <class T> inline shMath :: vec2<T> shMath :: operator / (const shMath :: vec2<T>& a, const shMath :: mat2<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: mat2<T> shMath :: operator / (float a, const shMath :: mat2<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: mat2<T> shMath :: operator / (double a, const shMath :: mat2<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: mat2<T> shMath :: operator / (long double a, const shMath :: mat2<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: vec2<T>& shMath :: operator *= (shMath :: vec2<T>& a, const shMath :: mat2<T>& b)
{
 return (a = (a * b));
}

template <class T> inline shMath :: vec2<T>& shMath :: operator /= (shMath :: vec2<T>& a, const shMath :: mat2<T>& b)
{
 return (a = (a / b));
}
#endif
