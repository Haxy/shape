#ifndef SH_MATH_RCVEC4
#define SH_MATH_RCVEC4

#include <new>
#include <stdlib.h>

#include "ops.h"
#include "rcvec2.h"
#include "rcvec3.h"
#include "functions.h"

namespace shMath
{
 template <class T> class rcvec4
 {
  public:
  rcvec4(void);
  rcvec4(const rcvec4& a);
  rcvec4(const T& x, const rcvec3<T>& yzw);
  rcvec4(const rcvec3<T>& xyz, const T& w = T());
  rcvec4(const T& x, const T& y, const rcvec2<T>& zw);
  rcvec4(const T& x, const rcvec2<T>& yz, const T& w = T());
  rcvec4(const rcvec2<T>& xy, const T& z = T(), const T& w = T());
  rcvec4(const T& x, const T& y = T(), const T& z = T(), const T& w = T());
  ~rcvec4(void);
  rcvec4& operator = (const rcvec4& a);
  rcvec4 operator - () const;
  operator const T* () const;
  operator T* ();
  operator const rcvec3<T> () const;
  operator const rcvec2<T> () const;
  operator rcvec3<T> ();
  operator rcvec2<T> ();
  void normalise();
  const T& x() const;
  const T& y() const;
  const T& z() const;
  const T& w() const;
  T& x();
  T& y();
  T& z();
  T& w();
  private:
  void kill();
  void copy();
  T* data;
  unsigned int* counter;
 };

 template <class T> rcvec4<T> zero(const rcvec4<T>& a);

 template <class T> T slength(const rcvec4<T>& a);
 template <class T> T length(const rcvec4<T>& a);
 template <class T> rcvec4<T> normalise(const rcvec4<T>& a);

 template <class T> T dot(const rcvec4<T>& a, const rcvec4<T>& b);
 template <class T> rcvec4<T> cross(const rcvec4<T>& a, const rcvec4<T>& b, const rcvec4<T>& c);

 template <class T> rcvec4<T> operator + (const rcvec4<T>& a, const rcvec4<T>& b);
 template <class T> rcvec4<T> operator - (const rcvec4<T>& a, const rcvec4<T>& b);
 template <class T> rcvec4<T> operator * (const rcvec4<T>& a, const T& b);
 template <class T> rcvec4<T> operator / (const rcvec4<T>& a, const T& b);
 template <class T> rcvec4<T> operator * (float a, const rcvec4<T>& b);
 template <class T> rcvec4<T> operator * (double a, const rcvec4<T>& b);
 template <class T> rcvec4<T> operator * (long double a, const rcvec4<T>& b);
 template <class T> rcvec4<T>& operator += (rcvec4<T>& a, const rcvec4<T>& b);
 template <class T> rcvec4<T>& operator -= (rcvec4<T>& a, const rcvec4<T>& b);
 template <class T> rcvec4<T>& operator *= (rcvec4<T>& a, const T& b);
 template <class T> rcvec4<T>& operator /= (rcvec4<T>& a, const T& b);
}

template <class T> inline shMath :: rcvec4 <T> :: rcvec4() : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  data = new T [4u] ();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> inline shMath :: rcvec4 <T> :: rcvec4(const shMath :: rcvec4<T>& a) : data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T> shMath :: rcvec4 <T> :: rcvec4(const T& x, const shMath :: rcvec3<T>& yzw) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  const T* temp2 = yzw;
  T* temp = data = new T [4u] ();
  (*temp) = x;
  (*(++temp)) = (*temp2);
  (*(++temp)) = (*(++temp2));
  (*(++temp)) = (*(++temp2));
 }
 catch(...)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
  throw;
 }
}

template <class T> shMath :: rcvec4 <T> :: rcvec4(const shMath :: rcvec3<T>& xyz, const T& w) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  const T* temp2 = xyz;
  T* temp = data = new T [4u] ();
  (*temp) = (*temp2);
  (*(++temp)) = (*(++temp2));
  (*(++temp)) = (*(++temp2));
  (*(++temp)) = w;
 }
 catch(...)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
  throw;
 }
}

template <class T> shMath :: rcvec4 <T> :: rcvec4(const T& x, const T& y, const shMath :: rcvec2<T>& zw) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  const T* temp2 = zw;
  T* temp = data = new T [4u] ();
  (*temp) = x;
  (*(++temp)) = y;
  (*(++temp)) = (*temp2);
  (*(++temp)) = (*(++temp2));
 }
 catch(...)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
  throw;
 }
}

template <class T> shMath :: rcvec4 <T> :: rcvec4(const T& x, const shMath :: rcvec2<T>& yz, const T& w) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  const T* temp2 = yz;
  T* temp = data = new T [4u] ();
  (*temp) = x;
  (*(++temp)) = (*temp2);
  (*(++temp)) = (*(++temp2));
  (*(++temp)) = w;
 }
 catch(...)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
  throw;
 }
}

template <class T> shMath :: rcvec4 <T> :: rcvec4(const shMath :: rcvec2<T>& xy, const T& z, const T& w) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  const T* temp2 = xy;
  T* temp = data = new T [4u] ();
  (*temp) = (*temp2);
  (*(++temp)) = (*(++temp2));
  (*(++temp)) = z;
  (*(++temp)) = w;
 }
 catch(...)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
  throw;
 }
}

template <class T> shMath :: rcvec4 <T> :: rcvec4(const T& x, const T& y, const T& z, const T& w) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  T* temp = data = new T [4u] ();
  (*temp) = x;
  (*(++temp)) = y;
  (*(++temp)) = z;
  (*(++temp)) = w;
 }
 catch(...)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
  throw;
 }
}

template <class T> inline shMath :: rcvec4 <T> :: ~rcvec4()
{
 kill();
}

template <class T> inline shMath :: rcvec4<T>& shMath :: rcvec4 <T> :: operator = (const shMath :: rcvec4<T>& a)
{
 if (&a != this)
 {
  kill();
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline shMath :: rcvec4<T> shMath :: rcvec4 <T> :: operator - () const
{
 const T* temp = *this;
 const T x = -(*temp);
 const T y = -(*(++temp));
 const T z = -(*(++temp));
 const T w = -(*(++temp));
 return shMath :: rcvec4<T>(x,y,z,w);
}

template <class T> inline shMath :: rcvec4 <T> :: operator const T* () const
{
 return data;
}

template <class T> inline shMath :: rcvec4 <T> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T> inline shMath :: rcvec4 <T> :: operator const shMath :: rcvec3<T> () const
{
 return (*(const shMath :: rcvec3<T>*)(this));
}

template <class T> inline shMath :: rcvec4 <T> :: operator const shMath :: rcvec2<T> () const
{
 return (*(const shMath :: rcvec2<T>*)(this));
}

template <class T> inline shMath :: rcvec4 <T> :: operator shMath :: rcvec3<T> ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*(shMath :: rcvec3<T>*)(this));
}

template <class T> inline shMath :: rcvec4 <T> :: operator shMath :: rcvec2<T> ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*(shMath :: rcvec2<T>*)(this));
}

template <class T> inline void shMath :: rcvec4 <T> :: normalise()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 T* temp = data;
 const T ilen = shMath :: inverse(shMath :: length(*this));
 (*temp) *= ilen;
 (*(++temp)) *= ilen;
 (*(++temp)) *= ilen;
 (*(++temp)) *= ilen;
}

template <class T> inline const T& shMath :: rcvec4 <T> :: x() const
{
 return (*data);
}

template <class T> inline const T& shMath :: rcvec4 <T> :: y() const
{
 return (*(data + 1u));
}

template <class T> inline const T& shMath :: rcvec4 <T> :: z() const
{
 return (*(data + 2u));
}

template <class T> inline const T& shMath :: rcvec4 <T> :: w() const
{
 return (*(data + 3u));
}

template <class T> inline T& shMath :: rcvec4 <T> :: x()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*data);
}

template <class T> inline T& shMath :: rcvec4 <T> :: y()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*(data + 1u));
}

template <class T> inline T& shMath :: rcvec4 <T> :: z()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*(data + 2u));
}

template <class T> inline T& shMath :: rcvec4 <T> :: w()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*(data + 3u));
}

template <class T> inline void shMath :: rcvec4 <T> :: kill()
{
 if ((*counter) == 0u)
 {
  delete [] data;
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> void shMath :: rcvec4 <T> :: copy()
{
 T* temp = NULL;
 unsigned int* tempcount;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  const T* temp2 = data;
  T* temp3 = temp = new T [4u] ();
  (*temp3) = (*temp2);
  (*(++temp3)) = (*(++temp2));
  (*(++temp3)) = (*(++temp2));
  (*(++temp3)) = (*(++temp2));
 }
 catch(...)
 {
  if (temp != NULL)
  {
   delete [] temp;
  }
  free(tempcount);
  throw;
 }
 --(*counter);
 data = temp;
 counter = tempcount;
}

template <class T> inline shMath :: rcvec4<T> shMath :: zero(const shMath :: rcvec4<T>& a)
{
 const T* temp = a;
 const T x = shMath :: zero(*temp);
 const T y = shMath :: zero(*(++temp));
 const T z = shMath :: zero(*(++temp));
 const T w = shMath :: zero(*(++temp));
 return shMath :: rcvec4<T>(x,y,z,w);
}

template <class T> inline T shMath :: slength(const shMath :: rcvec4<T>& a)
{
 const T* temp = a;
 const T x = (*temp);
 const T y = (*(++temp));
 const T z = (*(++temp));
 const T w = (*(++temp));
 return ((x * x) + (y * y) + (z * z) + (w * w));
}

template <class T> inline T shMath :: length(const shMath :: rcvec4<T>& a)
{
 return shMath :: sqrt(shMath :: slength(a));
}

template <class T> inline shMath :: rcvec4<T> shMath :: normalise(const shMath :: rcvec4<T>& a)
{
 const T* temp = a;
 const T x = (*temp);
 const T y = (*(++temp));
 const T z = (*(++temp));
 const T w = (*(++temp));
 const T ilen = shMath :: inverse(shMath :: length(a));
 return shMath :: rcvec4<T>(x * ilen, y * ilen, z * ilen, w * ilen);
}

template <class T> inline T shMath :: dot(const shMath :: rcvec4<T>& a, const shMath :: rcvec4<T>& b)
{
 const T* temp = a;
 const T* temp2 = b;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T ya = (*(++temp));
 const T za = (*(++temp));
 const T wa = (*(++temp));
 const T yb = (*(++temp2));
 const T zb = (*(++temp2));
 const T wb = (*(++temp2));
 return ((xa * xb) + (ya * yb) + (za * zb) + (wa * wb));
}

template <class T> shMath :: rcvec4<T> shMath :: cross(const shMath :: rcvec4<T>& a, const shMath :: rcvec4<T>& b, const shMath :: rcvec4<T>& c)
{
 const T* temp = a;
 const T* temp2 = b;
 const T* temp3 = c;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T xc = (*temp3);
 const T ya = (*(++temp));
 const T za = (*(++temp));
 const T wa = (*(++temp));
 const T yb = (*(++temp2));
 const T zb = (*(++temp2));
 const T wb = (*(++temp2));
 const T yc = (*(++temp3));
 const T zc = (*(++temp3));
 const T wc = (*(++temp3));
 return shMath :: rcvec4<T>((ya * zb * wc) + (za * wb * yc) + (wa * yb * zc) - (wa * zb * yc) - (za * yb * wc) - (wa * zb * yc),-((xa * zb * wc) + (za * wb * xc) + (wa * xb * zc) - (wa * zb * xc) - (za * xb * wc) - (xa * wb * zc)),(xa * yb * wc) + (ya * wb * xc) + (wa * xb * yc) - (wa * yb * xc) - (ya * xb * wc) - (xa * wb * xc),-((xa * yb * zc) + (ya * zb * xc) + (za * xb * yc) - (za * yb * xc) - (ya * xb * zc) - (xa * zb * yc)));
}

template <class T> inline shMath :: rcvec4<T> shMath :: operator + (const shMath :: rcvec4<T>& a, const shMath :: rcvec4<T>& b)
{
 const T* temp = a;
 const T* temp2 = b;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T ya = (*(++temp));
 const T za = (*(++temp));
 const T wa = (*(++temp));
 const T yb = (*(++temp2));
 const T zb = (*(++temp2));
 const T wb = (*(++temp2));
 return shMath :: rcvec4<T>(xa + xb, ya + yb, za + zb, wa + wb);
}

template <class T> inline shMath :: rcvec4<T> shMath :: operator - (const shMath :: rcvec4<T>& a, const shMath :: rcvec4<T>& b)
{
 const T* temp = a;
 const T* temp2 = b;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T ya = (*(++temp));
 const T za = (*(++temp));
 const T wa = (*(++temp));
 const T yb = (*(++temp2));
 const T zb = (*(++temp2));
 const T wb = (*(++temp2));
 return shMath :: rcvec4<T>(xa - xb, ya - yb, za - zb, wa - wb);
}

template <class T> inline shMath :: rcvec4<T> shMath :: operator * (const shMath :: rcvec4<T>& a, const T& b)
{
 const T* temp = a;
 const T x = (*temp);
 const T y = (*(++temp));
 const T z = (*(++temp));
 const T w = (*(++temp));
 return shMath :: rcvec4<T>(x * b, y * b, z * b, w * b);
}

template <class T> inline shMath :: rcvec4<T> shMath :: operator / (const shMath :: rcvec4<T>& a, const T& b)
{
 const T* temp = a;
 const T x = (*temp);
 const T y = (*(++temp));
 const T z = (*(++temp));
 const T w = (*(++temp));
 const T i = shMath :: inverse(b);
 return shMath :: rcvec4<T>(x * i, y * i, z * i, w * i);
}

template <class T> inline shMath :: rcvec4<T> shMath :: operator * (float a, const shMath :: rcvec4<T>& b)
{
 const T* temp = b;
 const T x = (*temp);
 const T y = (*(++temp));
 const T z = (*(++temp));
 const T w = (*(++temp));
 return shMath :: rcvec4<T>(a * x, a * y, a * z, a * w);
}

template <class T> inline shMath :: rcvec4<T> shMath :: operator * (double a, const shMath :: rcvec4<T>& b)
{
 const T* temp = b;
 const T x = (*temp);
 const T y = (*(++temp));
 const T z = (*(++temp));
 const T w = (*(++temp));
 return shMath :: rcvec4<T>(a * x, a * y, a * z, a * w);
}

template <class T> inline shMath :: rcvec4<T> shMath :: operator * (long double a, const shMath :: rcvec4<T>& b)
{
 const T* temp = b;
 const T x = (*temp);
 const T y = (*(++temp));
 const T z = (*(++temp));
 const T w = (*(++temp));
 return shMath :: rcvec4<T>(a * x, a * y, a * z, a * w);
}

template <class T> inline shMath :: rcvec4<T>& shMath :: operator += (shMath :: rcvec4<T>& a, const shMath :: rcvec4<T>& b)
{
 T* temp = a;
 const T* temp2 = b;
 (*temp) += (*temp2);
 (*(++temp)) += (*(++temp2));
 (*(++temp)) += (*(++temp2));
 (*(++temp)) += (*(++temp2));
 return a;
}

template <class T> inline shMath :: rcvec4<T>& shMath :: operator -= (shMath :: rcvec4<T>& a, const shMath :: rcvec4<T>& b)
{
 T* temp = a;
 const T* temp2 = b;
 (*temp) -= (*temp2);
 (*(++temp)) -= (*(++temp2));
 (*(++temp)) -= (*(++temp2));
 (*(++temp)) -= (*(++temp2));
 return a;
}

template <class T> inline shMath :: rcvec4<T>& shMath :: operator *= (shMath :: rcvec4<T>& a, const T& b)
{
 T* temp = a;
 (*temp) *= b;
 (*(++temp)) *= b;
 (*(++temp)) *= b;
 (*(++temp)) *= b;
 return a;
}

template <class T> inline shMath :: rcvec4<T>& shMath :: operator /= (shMath :: rcvec4<T>& a, const T& b)
{
 T* temp = a;
 const T i = shMath :: inverse(b);
 (*temp) *= i;
 (*(++temp)) *= i;
 (*(++temp)) *= i;
 (*(++temp)) *= i;
 return a;
}
#endif
