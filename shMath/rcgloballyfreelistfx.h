#ifndef SH_MATH_RCGLOBALLYFREELISTFX
#define SH_MATH_RCGLOBALLYFREELISTFX

#include <new>
#include <stdlib.h>

#include "pair.h"

namespace shMath
{
 template <class T> class rcgloballyfreelistfx
 {
  public:
  rcgloballyfreelistfx();
  explicit rcgloballyfreelistfx(unsigned int size);
  rcgloballyfreelistfx(const rcgloballyfreelistfx& a);
  ~rcgloballyfreelistfx();
  rcgloballyfreelistfx& operator = (const rcgloballyfreelistfx& a);
  const T& getCurrent() const;
  T& getCurrent();
  T& getNext();
  T& getPrev();
  T& getFirst();
  T& getLast();
  T& pickNext();
  T& pickPrev();
  T& pickFirst();
  T& pickLast();
  const T& pickNext() const;
  const T& pickPrev() const;
  const T& pickFirst() const;
  const T& pickLast() const;
  void gotoNext();
  void gotoPrev();
  void gotoFirst();
  void gotoLast();
  T& getNext(unsigned int a);
  T& getPrev(unsigned int a);
  T& getFirst(unsigned int a);
  T& getLast(unsigned int a);
  T& pickNext(unsigned int a);
  T& pickPrev(unsigned int a);
  T& pickFirst(unsigned int a);
  T& pickLast(unsigned int a);
  const T& pickNext(unsigned int a) const;
  const T& pickPrev(unsigned int a) const;
  const T& pickFirst(unsigned int a) const;
  const T& pickLast(unsigned int a) const;
  void gotoNext(unsigned int a);
  void gotoPrev(unsigned int a);
  void gotoFirst(unsigned int a);
  void gotoLast(unsigned int a);
  void add(const T& a);
  void addEmpty();
  void remove();
  void clear();
  unsigned int getSize() const;
  unsigned int getIndex() const;
  class iterator;
  const iterator getCurrentIterator() const;
  const iterator getFirstIterator() const;
  const iterator getLastIterator() const;
  iterator getCurrentIterator();
  iterator getFirstIterator();
  iterator getLastIterator();
  void jumpto(const iterator& a);
  private:
  void kill();
  void copy();
  class nod;
  class freelist;
  nod* first;
  nod* last;
  nod* current;
  unsigned int num;
  unsigned int index;
  unsigned int* counter;
  static freelist freenodes;
 };

 template <class T> class rcgloballyfreelistfx <T> :: iterator
 {
  friend class rcgloballyfreelistfx;
  public:
  iterator();
  const iterator& operator = (const iterator& a) const;
  const iterator& operator += (unsigned int a) const;
  const iterator& operator -= (unsigned int a) const;
  const iterator operator + (unsigned int a) const;
  const iterator operator - (unsigned int a) const;
  bool operator == (const iterator& a) const;
  bool operator != (const iterator& a) const;
  bool operator >= (const iterator& a) const;
  bool operator <= (const iterator& a) const;
  bool operator > (const iterator& a) const;
  bool operator < (const iterator& a) const;
  const iterator operator ++ (int) const;
  const iterator operator -- (int) const;
  const iterator& operator ++ () const;
  const iterator& operator -- () const;
  operator const unsigned int () const;
  const T* operator -> () const;
  const T& operator * () const;
  iterator& operator += (unsigned int a);
  iterator& operator -= (unsigned int a);
  iterator operator + (unsigned int a);
  iterator operator - (unsigned int a);
  iterator operator ++ (int);
  iterator operator -- (int);
  iterator& operator ++ ();
  iterator& operator -- ();
  T* operator -> ();
  T& operator * ();
  unsigned int getIndex() const;
  bool isInList() const;
  private:
  iterator(typename rcgloballyfreelistfx <T> :: nod* a, unsigned int i);
  mutable typename rcgloballyfreelistfx <T> :: nod* accessed;
  mutable unsigned int index;
 };

 template <class T> class rcgloballyfreelistfx <T> :: nod
 {
  public:
  nod();
  nod(const T& a);
  ~nod();
  nod* next;
  nod* prev;
  T cont;
 };

 template <class T> class rcgloballyfreelistfx <T> :: freelist
 {
  public:
  freelist();
  freelist(const freelist& a);
  ~freelist();
  freelist& operator = (const freelist& a);
  void add(typename rcgloballyfreelistfx <T> :: nod* a);
  void add(typename rcgloballyfreelistfx <T> :: nod* from, typename rcgloballyfreelistfx <T> :: nod* to);
  typename rcgloballyfreelistfx <T> :: nod* get();
  pair<typename rcgloballyfreelistfx <T> :: nod*,typename rcgloballyfreelistfx <T> :: nod*> get(unsigned int a);
  private:
  typename rcgloballyfreelistfx <T> :: nod* first;
  typename rcgloballyfreelistfx <T> :: nod* last;
 };

 template <class T> typename rcgloballyfreelistfx <T> :: freelist rcgloballyfreelistfx <T> :: freenodes = rcgloballyfreelistfx <T> :: freelist();
}

template <class T> shMath :: rcgloballyfreelistfx <T> :: rcgloballyfreelistfx() : first(NULL), last(NULL), current(NULL), num(0), index(0), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> shMath :: rcgloballyfreelistfx <T> :: rcgloballyfreelistfx(unsigned int size) : first(NULL), last(NULL), current(NULL), num(size), index(0), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  const typename shMath :: pair<typename shMath :: rcgloballyfreelistfx <T> :: nod*,typename shMath :: rcgloballyfreelistfx <T> :: nod*> ans = freenodes.get(size);
  first = current = ans.getA();
  last = ans.getB(); 
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> inline shMath :: rcgloballyfreelistfx <T> :: rcgloballyfreelistfx(const shMath :: rcgloballyfreelistfx<T>& a) : first(a.first), last(a.last), current(a.current), num(a.num), index(a.index), counter(a.counter)
{
 ++(*counter);
}

template <class T> inline shMath :: rcgloballyfreelistfx <T> :: ~rcgloballyfreelistfx()
{
 kill();
}

template <class T> shMath :: rcgloballyfreelistfx<T>& shMath :: rcgloballyfreelistfx <T> :: operator = (const shMath :: rcgloballyfreelistfx<T>& a)
{
 if (&a != this)
 {
  kill();
  first = a.first;
  last = a.last;
  current = a.current;
  num = a.num;
  index = a.index;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline const T& shMath :: rcgloballyfreelistfx <T> :: getCurrent() const
{
 return current->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: getCurrent()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return current->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: getNext()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 gotoNext();
 return current->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: getPrev()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 gotoPrev();
 return current->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: getFirst()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 gotoFirst();
 return current->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: getLast()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 gotoLast();
 return current->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: pickNext()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return current->next->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: pickPrev()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return current->prev->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: pickFirst()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return first->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: pickLast()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return last->cont;
}

template <class T> inline const T& shMath :: rcgloballyfreelistfx <T> :: pickNext() const
{
 return current->next->cont;
}

template <class T> inline const T& shMath :: rcgloballyfreelistfx <T> :: pickPrev() const
{
 return current->prev->cont;
}

template <class T> inline const T& shMath :: rcgloballyfreelistfx <T> :: pickFirst() const
{
 return first->cont;
}

template <class T> inline const T& shMath :: rcgloballyfreelistfx <T> :: pickLast() const
{
 return last->cont;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: gotoNext()
{
 current = current->next;
 ++index;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: gotoPrev()
{
 current = current->prev;
 --index;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: gotoFirst()
{
 current = first;
 index = 0u;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: gotoLast()
{
 current = last;
 index = num - 1u;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: getNext(unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 gotoNext(a);
 return current->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: getPrev(unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 gotoPrev(a);
 return current->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: getFirst(unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 gotoFirst(a);
 return current->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: getLast(unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 gotoLast(a);
 return current->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: pickNext(unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 unsigned int i = 0u;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* temp = current;
 for(;i < a;++i,temp = temp->next);
 return temp->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: pickPrev(unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 unsigned int i = 0u;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* temp = current;
 for(;i < a;++i,temp = temp->prev);
 return temp->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: pickFirst(unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 unsigned int i = 0u;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* temp = first;
 for(;i < a;++i,temp = temp->next);
 return temp->cont;
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: pickLast(unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 unsigned int i = 0u;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* temp = last;
 for(;i < a;++i,temp = temp->prev);
 return temp->cont;
}

template <class T> inline const T& shMath :: rcgloballyfreelistfx <T> :: pickNext(unsigned int a) const
{
 unsigned int i = 0u;
 const typename shMath :: rcgloballyfreelistfx <T> :: nod* temp = current;
 for(;i < a;++i,temp = temp->next);
 return temp->cont;
}

template <class T> inline const T& shMath :: rcgloballyfreelistfx <T> :: pickPrev(unsigned int a) const
{
 unsigned int i = 0u;
 const typename shMath :: rcgloballyfreelistfx <T> :: nod* temp = current;
 for(;i < a;++i,temp = temp->prev);
 return temp->cont;
}

template <class T> inline const T& shMath :: rcgloballyfreelistfx <T> :: pickFirst(unsigned int a) const
{
 unsigned int i = 0u;
 const typename shMath :: rcgloballyfreelistfx <T> :: nod* temp = first;
 for(;i < a;++i,temp = temp->next);
 return current->cont;
}

template <class T> inline const T& shMath :: rcgloballyfreelistfx <T> :: pickLast(unsigned int a) const
{
 unsigned int i = 0u;
 const typename shMath :: rcgloballyfreelistfx <T> :: nod* temp = last;
 for(;i < a;++i,temp = temp->prev);
 return temp->cont;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: gotoNext(unsigned int a)
{
 unsigned int i = 0u;
 for(;i < a;++i,current = current->next);
 index += a;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: gotoPrev(unsigned int a)
{
 unsigned int i = 0u;
 for(;i < a;++i,current = current->prev);
 index -= a;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: gotoFirst(unsigned int a)
{
 unsigned int i = 0u;
 for(current = first;i < a;++i,current = current->next);
 index = a;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: gotoLast(unsigned int a)
{
 unsigned int i = 0u;
 for(current = last;i < a;++i,current = current->prev);
 index = num - a - 1u;
}

template <class T> void shMath :: rcgloballyfreelistfx <T> :: add(const T& a)
{
 typename shMath :: rcgloballyfreelistfx <T> :: nod* newnod = freenodes.get();
 try
 {
  newnod->cont = a;
 }
 catch(...)
 {
  newnod->prev = NULL;
  newnod->next = NULL;
  freenodes.add(newnod);
  throw;
 }
 newnod->prev = current;
 if (current != NULL)
 {
  if ((newnod->next = current->next) == NULL)
  {
   last = newnod;
  }
  else
  {
   current->next->prev = newnod;
  }
  current->next = newnod;
 }
 if (first == NULL)
 {
  first = newnod;
  last = newnod;
  current = newnod;
 }
 ++num;
}

template <class T> void shMath :: rcgloballyfreelistfx <T> :: addEmpty()
{
 typename shMath :: rcgloballyfreelistfx <T> :: nod* newnod = freenodes.get();
 newnod->prev = current;
 if (current != NULL)
 {
  if ((newnod->next = current->next) == NULL)
  {
   last = newnod;
  }
  else
  {
   current->next->prev = newnod;
  }
  current->next = newnod;
 }
 if (first == NULL)
 {
  first = newnod;
  last = newnod;
  current = newnod;
 }
 ++num;
}

template <class T> void shMath :: rcgloballyfreelistfx <T> :: remove()
{
 if (current->next != NULL)
 {
  current->next->prev = current->prev;
 }
 if (current->prev != NULL)
 {
  current->prev->next = current->next;
 }
 if (first == current)
 {
  first = current->next;
 }
 if (last == current)
 {
  last = current->prev;
 }
 freenodes.add(current);
 current = first;
 --num;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: clear()
{
 freenodes.add(first,last);
 current = first = last = NULL;
 num = index = 0u;
}

template <class T> inline unsigned int shMath :: rcgloballyfreelistfx <T> :: getSize() const
{
 return num;
}

template <class T> inline unsigned int shMath :: rcgloballyfreelistfx <T> :: getIndex() const
{
 return index;
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: getCurrentIterator() const
{
 return shMath :: rcgloballyfreelistfx <T> :: iterator(current,index);
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: getFirstIterator() const
{
 return shMath :: rcgloballyfreelistfx <T> :: iterator(first,0);
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: getLastIterator() const
{
 return shMath :: rcgloballyfreelistfx <T> :: iterator(last,num - 1);
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: getCurrentIterator()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return shMath :: rcgloballyfreelistfx <T> :: iterator(current,index);
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: getFirstIterator()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return shMath :: rcgloballyfreelistfx <T> :: iterator(first,0u);
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: getLastIterator()
{
 if ((*counter) != 0)
 {
  copy();
 }
 return shMath :: rcgloballyfreelistfx <T> :: iterator(last,num - 1u);
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: jumpto(const typename shMath :: rcgloballyfreelistfx <T> :: iterator& a)
{
 current = a.accessed;
 index = a.index;
}

template <class T> void shMath :: rcgloballyfreelistfx <T> :: kill()
{
 if ((*counter) == 0u)
 {
  free(counter);
  if (first != NULL)
  {
   freenodes.add(first,last);
  }
 }
 else
 {
  --(*counter);
 }
}

template <class T> void shMath :: rcgloballyfreelistfx <T> :: copy()
{
 unsigned int* tempcount;
 const typename shMath :: rcgloballyfreelistfx <T> :: nod* tempfir = first;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* tempcur = current;
 const shMath :: pair<typename shMath :: rcgloballyfreelistfx <T> :: nod*,typename shMath :: rcgloballyfreelistfx <T> :: nod*> ans = freenodes.get(num);
 typename shMath :: rcgloballyfreelistfx <T> :: nod* tempnewfir = ans.getA();
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  freenodes.add(ans.getA(),ans.getB());
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  unsigned int i = 0;
  for(;i < num;++i)
  {
   if (tempfir == current)
   {
    tempcur = tempnewfir;
   }
   tempnewfir->cont = tempfir->cont;
   tempnewfir = tempnewfir->next;
   tempfir = tempfir->next;
  }
 }
 catch(...)
 {
  free(tempcount);
  freenodes.add(ans.getA(),ans.getB());
  throw;
 }
 --(*counter);
 counter = tempcount;
 first = ans.getA();
 last = ans.getB();
 current = tempcur;
}

template <class T> inline shMath :: rcgloballyfreelistfx <T> :: iterator :: iterator() : accessed(NULL), index(0)
{
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator = (const typename shMath :: rcgloballyfreelistfx <T> :: iterator& a) const
{
 accessed = a.accessed;
 index = a.index;
 return *this;
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator += (unsigned int a) const
{
 unsigned int i = 0u;
 for(index += a;i < a;++i,accessed = accessed->next);
 return *this;
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator -= (unsigned int a) const
{
 unsigned int i = 0u;
 for(index -= a;i < a;++i,accessed = accessed->prev);
 return *this;
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: iterator :: operator + (unsigned int a) const
{
 unsigned int i = 0u;
 const typename shMath :: rcgloballyfreelistfx <T> :: iterator ans = shMath :: rcgloballyfreelistfx <T> :: iterator(accessed,index + a);
 for(;i < a;++i,ans.accessed = ans.accessed->next);
 return ans;
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: iterator :: operator - (unsigned int a) const
{
 unsigned int i = 0u;
 const typename shMath :: rcgloballyfreelistfx <T> :: iterator ans = shMath :: rcgloballyfreelistfx <T> :: iterator(accessed,index - a);
 for(;i < a;++i,ans.accessed = ans.accessed->prev);
 return ans;
}

template <class T> inline bool shMath :: rcgloballyfreelistfx <T> :: iterator :: operator == (const typename shMath :: rcgloballyfreelistfx <T> :: iterator& a) const
{
 return (accessed == a.accessed);
}

template <class T> inline bool shMath :: rcgloballyfreelistfx <T> :: iterator :: operator != (const typename shMath :: rcgloballyfreelistfx <T> :: iterator& a) const
{
 return (accessed != a.accessed);
}

template <class T> inline bool shMath :: rcgloballyfreelistfx <T> :: iterator :: operator >= (const typename shMath :: rcgloballyfreelistfx <T> :: iterator& a) const
{
 return (index >= a.index);
}

template <class T> inline bool shMath :: rcgloballyfreelistfx <T> :: iterator :: operator <= (const typename shMath :: rcgloballyfreelistfx <T> :: iterator& a) const
{
 return (index <= a.index);
}

template <class T> inline bool shMath :: rcgloballyfreelistfx <T> :: iterator :: operator > (const typename shMath :: rcgloballyfreelistfx <T> :: iterator& a) const
{
 return (index > a.index);
}

template <class T> inline bool shMath :: rcgloballyfreelistfx <T> :: iterator :: operator < (const typename shMath :: rcgloballyfreelistfx <T> :: iterator& a) const
{
 return (index < a.index);
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: iterator :: operator ++ (int) const
{
 const typename shMath :: rcgloballyfreelistfx <T> :: iterator ans = shMath :: rcgloballyfreelistfx <T> :: iterator(accessed,index);
 accessed = accessed->next;
 ++index;
 return ans;
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: iterator :: operator -- (int) const
{
 const typename shMath :: rcgloballyfreelistfx <T> :: iterator ans = shMath :: rcgloballyfreelistfx <T> :: iterator(accessed,index);
 accessed = accessed->prev;
 --index;
 return ans;
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator ++ () const
{
 accessed = accessed->next;
 ++index;
 return *this;
}

template <class T> inline const typename shMath :: rcgloballyfreelistfx <T> :: iterator& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator -- () const
{
 accessed = accessed->prev;
 --index;
 return *this;
}

template <class T> inline const T* shMath :: rcgloballyfreelistfx <T> :: iterator :: operator -> () const
{
 return &(accessed->cont);
}

template <class T> inline const T& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator * () const
{
 return accessed->cont;
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator += (unsigned int a)
{
 unsigned int i = 0u;
 for(index += a;i < a;++i,accessed = accessed->next);
 return *this;
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator -= (unsigned int a)
{
 unsigned int i = 0u;
 for(index -= a;i < a;++i,accessed = accessed->prev);
 return *this;
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: iterator :: operator + (unsigned int a)
{
 unsigned int i = 0u;
 typename shMath :: rcgloballyfreelistfx <T> :: iterator ans = shMath :: rcgloballyfreelistfx <T> :: iterator(accessed,index + a);
 for(;i < a;++i,ans.accessed = ans.accessed->next);
 return ans;
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: iterator :: operator - (unsigned int a)
{
 unsigned int i = 0u;
 typename shMath :: rcgloballyfreelistfx <T> :: iterator ans = shMath :: rcgloballyfreelistfx <T> :: iterator(accessed,index - a);
 for(;i < a;++i,ans.accessed = ans.accessed->prev);
 return ans;
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: iterator :: operator ++ (int)
{
 typename shMath :: rcgloballyfreelistfx <T> :: iterator ans = shMath :: rcgloballyfreelistfx <T> :: iterator(accessed,index);
 accessed = accessed->next;
 ++index;
 return ans;
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator shMath :: rcgloballyfreelistfx <T> :: iterator :: operator -- (int)
{
 typename shMath :: rcgloballyfreelistfx <T> :: iterator ans = shMath :: rcgloballyfreelistfx <T> :: iterator(accessed,index);
 accessed = accessed->prev;
 --index;
 return ans;
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator ++ ()
{
 accessed = accessed->next;
 ++index;
 return *this;
}

template <class T> inline typename shMath :: rcgloballyfreelistfx <T> :: iterator& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator -- ()
{
 accessed = accessed->prev;
 --index;
 return *this;
}

template <class T> inline T* shMath :: rcgloballyfreelistfx <T> :: iterator :: operator -> ()
{
 return &(accessed->cont);
}

template <class T> inline T& shMath :: rcgloballyfreelistfx <T> :: iterator :: operator * ()
{
 return accessed->cont;
}

template <class T> inline unsigned int shMath :: rcgloballyfreelistfx <T> :: iterator :: getIndex() const
{
 return index;
}

template <class T> inline bool shMath :: rcgloballyfreelistfx <T> :: iterator :: isInList() const
{
 return accessed != NULL;
}

template <class T> inline shMath :: rcgloballyfreelistfx <T> :: iterator :: iterator(typename shMath :: rcgloballyfreelistfx <T> :: nod* a,unsigned int i) : accessed(a), index(i)
{
}

template <class T> inline shMath :: rcgloballyfreelistfx <T> :: nod :: nod() : next(NULL), prev(NULL), cont(T())
{
}

template <class T> inline shMath :: rcgloballyfreelistfx <T> :: nod :: nod(const T& a) : next(NULL), prev(NULL), cont(a)
{
}

template <class T> inline shMath :: rcgloballyfreelistfx <T> :: nod :: ~nod()
{
 if (next != NULL)
 {
  delete next;
 }
}

template <class T> inline shMath :: rcgloballyfreelistfx <T> :: freelist :: freelist() : first(NULL), last(NULL)
{
}

template <class T> shMath :: rcgloballyfreelistfx <T> :: freelist :: freelist(const typename shMath :: rcgloballyfreelistfx <T> :: freelist& a) : first(NULL), last(NULL)
{
 const typename shMath :: rcgloballyfreelistfx <T> :: nod* afir = a.first;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* temp;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* tempprev = temp = first = ((afir != NULL) ? new shMath :: rcgloballyfreelistfx <T> :: nod(afir->cont) : NULL);
 try
 {
  for(afir = afir->next;afir != NULL;afir = afir->next)
  {
   temp = new shMath :: rcgloballyfreelistfx <T> :: nod(afir->cont);
   temp->prev = tempprev;
   tempprev->next = temp;
   tempprev = temp;
  }
 }
 catch(...)
 {
  delete first;
  throw;
 }
 last = temp;
}

template <class T> inline shMath :: rcgloballyfreelistfx <T> :: freelist :: ~freelist()
{
 if (first != NULL)
 {
  delete first;
 }
}

template <class T> typename shMath :: rcgloballyfreelistfx <T> :: freelist& shMath :: rcgloballyfreelistfx <T> :: freelist :: operator = (const typename shMath :: rcgloballyfreelistfx <T> :: freelist& a)
{
 const typename shMath :: rcgloballyfreelistfx <T> :: nod* afir = a.first;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* temp;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* tempfir;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* tempprev = temp = tempfir = ((afir != NULL) ? new shMath :: rcgloballyfreelistfx <T> :: nod(afir->cont) : NULL);
 try
 {
  for(afir = afir->next;afir != NULL;afir = afir->next)
  {
   temp = new shMath :: rcgloballyfreelistfx <T> :: nod(afir->cont);
   temp->prev = tempprev;
   tempprev->next = temp;
   tempprev = temp;
  }
 }
 catch(...)
 {
  delete tempfir;
  throw;
 }
 if (first != NULL)
 {
  delete first;
 }
 first = tempfir;
 last = temp;
 return *this;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: freelist :: add(typename shMath :: rcgloballyfreelistfx <T> :: nod* a)
{
 a->next = first;
 a->prev = NULL;
 if (first != NULL)
 {
  first->prev = a;
 }
 else
 {
  last = a;
 }
 first = a;
}

template <class T> inline void shMath :: rcgloballyfreelistfx <T> :: freelist :: add(typename shMath :: rcgloballyfreelistfx <T> :: nod* from,typename shMath :: rcgloballyfreelistfx <T> :: nod* to)
{
 to->next = first;
 if (first != NULL)
 {
  first->prev = to;
 }
 else
 {
  last = to;
 }
 first = from;
 from->prev = NULL;
}

template <class T> typename shMath :: rcgloballyfreelistfx <T> :: nod* shMath :: rcgloballyfreelistfx <T> :: freelist :: get()
{
 if (last != NULL)
 {
  typename shMath :: rcgloballyfreelistfx <T> :: nod* ans = last;
  last = last->prev;
  if (last == NULL)
  {
   first = NULL;
  }
  else
  {
   last->next = NULL;
  }
  return ans;
 }
 else
 {
  return new shMath :: rcgloballyfreelistfx <T> :: nod();
 }
}

template <class T> shMath :: pair<typename shMath :: rcgloballyfreelistfx <T> :: nod*,typename shMath :: rcgloballyfreelistfx <T> :: nod*> shMath :: rcgloballyfreelistfx <T> :: freelist :: get(unsigned int a)
{
 unsigned int i = 1u;
 if (a == 0u)
 {
  return shMath :: pair<typename shMath :: rcgloballyfreelistfx <T> :: nod*,typename shMath :: rcgloballyfreelistfx <T> :: nod*>(NULL,NULL);
 }
 typename shMath :: rcgloballyfreelistfx <T> :: nod* ansfirst;
 typename shMath :: rcgloballyfreelistfx <T> :: nod* anslast = ansfirst = ((last != NULL) ? last : new shMath :: rcgloballyfreelistfx <T> :: nod());
 for(;(ansfirst->prev != NULL) && (i < a);++i,ansfirst = ansfirst->prev);
 if ((i != a) || (ansfirst == first))
 {
  typename shMath :: rcgloballyfreelistfx <T> :: nod* temp = ansfirst;
  try
  {
   for(;i < a;++i)
   {
    ansfirst = new shMath :: rcgloballyfreelistfx <T> :: nod();
    ansfirst->next = temp;
    temp->prev = ansfirst;
    temp = ansfirst;
   }
  }
  catch(...)
  {
   first = ansfirst;
   last = anslast;
   throw;
  }
  first = last = NULL;
 }
 else
 {
  last = ansfirst->prev;
  last->next = NULL;
 }
 return shMath :: pair<typename shMath :: rcgloballyfreelistfx <T> :: nod*,typename shMath :: rcgloballyfreelistfx <T> :: nod*>(ansfirst,anslast);
}
#endif
