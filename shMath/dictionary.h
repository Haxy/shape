#ifndef SH_MATH_DICTIONARY
#define SH_MATH_DICTIONARY

#include <new>
#include <limits.h>
#include <stdlib.h>

namespace shMath
{
 template <class T> class dictionary
 {
  public:
  dictionary();
  dictionary(const dictionary& a);
  ~dictionary();
  dictionary& operator = (const dictionary& a);
  class node;
  bool isEmpty() const;
  bool has(const char* string) const;
  const node& operator [] (const char* string) const;
  node& operator [] (const char* string);
  bool add(const char* stirng, const T& a);
  bool remove(const char* string);
  private:
  node* root;
  void copy();
  void kill();
  unsigned int* counter;
 };

 template <class T> class dictionary <T> :: node
 {
  public:
  node();
  node(const dictionary& Owner);
  const T& getData() const;
  const node getChild(const char a) const;
  const node* getChildAddress(const char a) const;
  bool hasChild(const char a) const;
  bool hasChildren() const;
  T& getData();
  node& getChild(const char a);
  node* getChildAddress(const char a);
  operator const T () const;
  T& operator = (const T& a);
  void releaseChild(const char a);
  void releaseChildren();
  void removeEmpty();
  node* copy(const dictionary& a);
  private:
  T data;
  node* parent;
  node* children[1 << CHAR_BIT];
 };
}

template <class T> shMath :: dictionary <T> :: node :: node() : data(T()), parent(NULL)
{
 unsigned int i = 0u;
 for(;i < (sizeof(children) / sizeof(shMath :: dictionary <T> :: node*));children[i++] = NULL);
}

template <class T> shMath :: dictionary <T> :: node :: node(const shMath :: dictionary<T>& Owner) : data(T()), parent(NULL)
{
 unsigned int i = 0u;
 for(;i < (sizeof(children) / sizeof(shMath :: dictionary <T> :: node*));children[i++] = NULL);
}

template <class T> inline const T& shMath :: dictionary <T> :: node :: getData() const
{
 return data;
}

template <class T> inline const typename shMath :: dictionary <T> :: node shMath :: dictionary <T> :: node :: getChild(const char a) const
{
 return ((hasChild(a)) ? (*(children[a])) : node());
}

template <class T> inline const typename shMath :: dictionary <T> :: node* shMath :: dictionary <T> :: node :: getChildAddress(const char a) const
{
 return children[a];
}

template <class T> inline bool shMath :: dictionary <T> :: node :: hasChild(const char a) const
{
 return (children[a] != NULL);
}

template <class T> bool shMath :: dictionary <T> :: node :: hasChildren() const
{
 unsigned int i = 0u;
 for(;i < (sizeof(children) / sizeof(shMath :: dictionary <T> :: node));++i)
 {
  if (children[i] != NULL)
  {
   return true;
  }
 }
 return false;
}

template <class T> inline T& shMath :: dictionary <T> :: node :: getData()
{
 return data;
}

template <class T> inline typename shMath :: dictionary <T> :: node& shMath :: dictionary <T> :: node :: getChild(const char a)
{
 if (children[a] == NULL)
 {
  children[a] = new shMath :: dictionary <T> :: node();
  (children[a])->parent = this;
 }
 return *(children[a]);
}

template <class T> inline typename shMath :: dictionary <T> :: node* shMath :: dictionary <T> :: node :: getChildAddress(const char a)
{
 return children[a];
}

template <class T> inline shMath :: dictionary <T> :: node :: operator const T () const
{
 return data;
}

template <class T> inline T& shMath :: dictionary <T> :: node :: operator = (const T& a)
{
 return (data = a);
}

template <class T> void shMath :: dictionary <T> :: node :: releaseChild(const char a)
{
 shMath :: dictionary <T> :: node* child = children[a];
 if (child != NULL)
 {
  child->releaseChildren();
  delete child;
  children[a] = NULL; 
 }
}

template <class T> void shMath :: dictionary <T> :: node :: releaseChildren()
{
 unsigned int i = 0u;
 for(;i < (sizeof(children) / sizeof(shMath :: dictionary <T> :: node*));++i)
 {
  if (children[i] != NULL)
  {
   (children[i])->releaseChildren();
   delete children[i];
   children[i] = NULL;
  }
 }
}

template <class T> void shMath :: dictionary <T> :: node :: removeEmpty()
{
 if ((parent != NULL) && (!hasChildren()))
 {
  shMath :: dictionary <T> :: node** siblings = (shMath :: dictionary <T> :: node**)(&(parent->children));
  for(;(*siblings) != this;++siblings);
  (*siblings) = NULL;
  parent->removeEmpty();
  delete this;
 }
} 

template <class T> typename shMath :: dictionary <T> :: node* shMath :: dictionary <T> :: node :: copy(const shMath :: dictionary<T>& a)
{
 node* ans = new shMath :: dictionary <T> :: node(a);
 try
 {
  unsigned int i = 0u;
  ans->data = data;
  for(;i < (sizeof(children) / sizeof(shMath :: dictionary <T> :: node*));++i)
  {
   if (children[i] != NULL)
   {
    (ans->children)[i] = (children[i])->copy(a);
    (ans->children)[i]->parent = ans;
   }
  }
 }
 catch(...)
 {
  ans->releaseChildren();
  delete ans;
  throw;
 }
 return ans;
}

template <class T> inline shMath :: dictionary <T> :: dictionary() : root(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  root = new shMath :: dictionary <T> :: node(*this);
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> inline shMath :: dictionary <T> :: dictionary(const shMath :: dictionary<T>& a) : root(a.root), counter(a.counter)
{
 ++(*counter);
}

template <class T> inline shMath :: dictionary <T> :: ~dictionary()
{
 kill();
}

template <class T> inline shMath :: dictionary<T>& shMath :: dictionary <T> :: operator = (const shMath :: dictionary<T>& a)
{
 if (&a != this)
 {
  kill();
  root = a.root;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline bool shMath :: dictionary <T> :: isEmpty() const
{
 return root->hasChildren();
}

template <class T> bool shMath :: dictionary <T> :: has(const char* string) const
{
 const shMath :: dictionary <T> :: node* current = root;
 do
 {
  if (((current = current->getChildAddress(*string))) == NULL)
  {
   return false;
  }
 }
 while ((*string++) != '\0');
 return true;
}

template <class T> inline const typename shMath :: dictionary <T> :: node& shMath :: dictionary <T> :: operator [] (const char* string) const
{
 const shMath :: dictionary <T> :: node* current = root;
 const static shMath :: dictionary <T> :: node empty;
 do
 {
  if ((current = current->getChildAddress(*string)) == NULL)
  {
   return empty;
  }
 }
 while ((*string++) != '\0');
 return *current;
}

template <class T> inline typename shMath :: dictionary <T> :: node& shMath :: dictionary <T> :: operator [] (const char* string)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 shMath :: dictionary <T> :: node* current = root;
 for(;(*string) != '\0';current = &(current->getChild(*string++)));
 return current->getChild('\0');
}

template <class T> bool shMath :: dictionary <T> :: add(const char* string, const T& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 shMath :: dictionary <T> :: node* current = root;
 for(;(*string) != '\0';current = &(current->getChild(*string++)));
 if (current->hasChild('\0'))
 {
  return false;
 } 
 else
 {
  current->getChild('\0').getData() = a;
 }
 return true;
}

template <class T> bool shMath :: dictionary <T> :: remove(const char* string)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 shMath :: dictionary <T> :: node* current = root;
 do
 {
  if ((current = current->getChildAddress(*string)) == NULL)
  {
   return false;
  }
 }
 while((*string++) != '\0');
 current->removeEmpty();
 return true;
}

template <class T> void shMath :: dictionary <T> :: copy()
{
 shMath :: dictionary <T> ans;
 ans.root = root->copy(ans);
 --(*counter);
 root = ans.root;
 counter = ans.counter;
 ++(*counter);
}

template <class T> void shMath :: dictionary <T> :: kill()
{
 if ((*counter) == 0u)
 {
  root->releaseChildren();
  delete root;
  free(counter);
 }
 else
 {
  --(*counter);
 }
}
#endif
