#ifndef SH_MATH_VEC3
#define SH_MATH_VEC3

#include "ops.h"
#include "vec2.h"
#include "functions.h"

namespace shMath
{
 template <class T> class vec3
 {
  public:
  vec3();
  vec3(const T& xx, const T& yy = T(), const T& zz = T());
  vec3(const vec2<T>& xxyy, const T& zz = T());
  vec3(const T& xx, const vec2<T>& yyzz);
  vec3 operator - () const;
  operator const T* () const;
  operator T* ();
  operator const vec2<T> () const;
  operator vec2<T> ();
  void normalise();
  const vec2<T>& xy() const;
  const vec2<T>& yz() const;
  const T& x() const;
  const T& y() const;
  const T& z() const;
  vec2<T>& xy();
  vec2<T>& yz();
  T& x();
  T& y();
  T& z();
  private:
  T X;
  T Y;
  T Z;
 };
 
 template <class T> vec3<T> zero(const vec3<T>& a);

 template <class T> T slength(const vec3<T>& a);
 template <class T> T length(const vec3<T>& a);
 template <class T> vec3<T> normalise(const vec3<T>& a);

 template <class T> T dot(const vec3<T>& a, const vec3<T>& b);
 template <class T> vec3<T> cross(const vec3<T>& a, const vec3<T>& b);
 
 template <class T> vec3<T> operator + (const vec3<T>& a, const vec3<T>& b);
 template <class T> vec3<T> operator - (const vec3<T>& a, const vec3<T>& b);
 template <class T> vec3<T> operator * (const vec3<T>& a, const T& b);
 template <class T> vec3<T> operator / (const vec3<T>& a, const T& b);
 template <class T> vec3<T> operator * (float a, const vec3<T>& b);
 template <class T> vec3<T> operator * (double a, const vec3<T>& b);
 template <class T> vec3<T> operator * (long double a, const vec3<T>& b);
 template <class T> vec3<T>& operator += (vec3<T>& a, const vec3<T>& b);
 template <class T> vec3<T>& operator -= (vec3<T>& a, const vec3<T>& b);
 template <class T> vec3<T>& operator *= (vec3<T>& a, const T& b);
 template <class T> vec3<T>& operator /= (vec3<T>& a, const T& b);
}

template <class T> inline shMath :: vec3 <T> :: vec3() : X(T()), Y(T()), Z(T())
{
}

template <class T> inline shMath :: vec3 <T> :: vec3(const T& xx, const T& yy, const T& zz) : X(xx), Y(yy), Z(zz)
{
}

template <class T> inline shMath :: vec3 <T> :: vec3(const shMath :: vec2<T>& xxyy, const T& zz) : X(xxyy.x()), Y(xxyy.y()), Z(zz)
{
}

template <class T> inline shMath :: vec3 <T> :: vec3(const T& xx, const shMath :: vec2<T>& yyzz) : X(xx), Y(yyzz.x()), Z(yyzz.y())
{
}

template <class T> inline shMath :: vec3<T> shMath :: vec3 <T> :: operator - () const
{
 return shMath :: vec3<T>(-X,-Y,-Z);
}

template <class T> inline shMath :: vec3 <T> :: operator const T* () const
{
 return (const T*) this;
}

template <class T> inline shMath :: vec3 <T> :: operator T* ()
{
 return (T*) this;
}

template <class T> inline shMath :: vec3 <T> :: operator const shMath :: vec2<T> () const
{
 return *((const shMath :: vec2<T>*)(this));
}

template <class T> inline shMath :: vec3 <T> :: operator shMath :: vec2<T> ()
{
 return *((shMath :: vec2<T>*)(this));
}

template <class T> inline void shMath :: vec3 <T> :: normalise()
{
 const T len = shMath :: inverse(shMath :: length(*this));
 X *= len;
 Y *= len;
 Z *= len;
}

template <class T> inline const shMath :: vec2<T>& shMath :: vec3 <T> :: xy() const
{
 return *((const shMath :: vec2<T>*)(this));
}

template <class T> inline const shMath :: vec2<T>& shMath :: vec3 <T> :: yz() const
{
 return *((const shMath :: vec2<T>*)(++(const T*)(this)));
}

template <class T> inline const T& shMath :: vec3 <T> :: x() const
{
 return X;
}

template <class T> inline const T& shMath :: vec3 <T> :: y() const
{
 return Y;
}

template <class T> inline const T& shMath :: vec3 <T> :: z() const
{
 return Z;
}

template <class T> inline shMath :: vec2<T>& shMath :: vec3 <T> :: xy()
{
 return *((shMath :: vec2<T>*)(this));
}

template <class T> inline shMath :: vec2<T>& shMath :: vec3 <T> :: yz()
{
 return *((shMath :: vec2<T>*)(++(T*)(this)));
}

template <class T> inline T& shMath :: vec3 <T> :: x()
{
 return X;
}

template <class T> inline T& shMath :: vec3 <T> :: y()
{
 return Y;
}

template <class T> inline T& shMath :: vec3 <T> :: z()
{
 return Z;
}

template <class T> inline shMath :: vec3<T> shMath :: zero(const shMath :: vec3<T>& a)
{
 return shMath :: vec3<T>(shMath :: zero(a.x()),shMath :: zero(a.y()),shMath :: zero(a.z()));
}

template <class T> inline T shMath :: slength(const shMath :: vec3<T>& a)
{
 return ((a.x() * a.x()) + (a.y() * a.y()) + (a.z() * a.z()));
}

template <class T> inline T shMath :: length(const shMath :: vec3<T>& a)
{
 return shMath :: sqrt((a.x() * a.x()) + (a.y() * a.y()) + (a.z() * a.z()));
}

template <class T> inline shMath :: vec3<T> shMath :: normalise(const shMath :: vec3<T>& a)
{
 return a / shMath :: length(a);
}

template <class T> inline T shMath :: dot(const shMath :: vec3<T>& a, const shMath :: vec3<T>& b)
{
 return ((a.x() * b.x()) + (a.y() * b.y()) + (a.z() * b.z()));
}

template <class T> inline shMath :: vec3<T> shMath :: cross(const shMath :: vec3<T>& a, const shMath :: vec3<T>& b)
{
 return shMath :: vec3<T>((a.y() * b.z()) - (a.z() * b.y()),(a.z() * b.x()) - (a.x() * b.z()),(a.x() * b.y()) - (a.y() * b.x()));
}

template <class T> inline shMath :: vec3<T> shMath :: operator + (const shMath :: vec3<T>& a, const shMath :: vec3<T>& b)
{
 return shMath :: vec3<T>(a.x() + b.x(), a.y() + b.y(), a.z() + b.z());
}

template <class T> inline shMath :: vec3<T> shMath :: operator - (const shMath :: vec3<T>& a, const shMath :: vec3<T>& b)
{
 return shMath :: vec3<T>(a.x() - b.x(), a.y() - b.y(), a.z() - b.z());
}

template <class T> inline shMath :: vec3<T> shMath :: operator * (const shMath :: vec3<T>& a, const T& b)
{
 return shMath :: vec3<T>(a.x() * b,a.y() * b,a.z() * b);
}

template <class T> inline shMath :: vec3<T> shMath :: operator / (const shMath :: vec3<T>& a, const T& b)
{
 const T bb = shMath :: inverse(b);
 return shMath :: vec3<T>(a.x() * bb,a.y() * bb,a.z() * bb);
}

template <class T> inline shMath :: vec3<T> shMath :: operator * (float a, const shMath :: vec3<T>& b)
{
 return shMath :: vec3<T>(a * b.x(),a * b.y(), a * b.z());
}

template <class T> inline shMath :: vec3<T> shMath :: operator * (double a, const shMath :: vec3<T>& b)
{
 return shMath :: vec3<T>(a * b.x(),a * b.y(), a * b.z());
}

template <class T> inline shMath :: vec3<T> shMath :: operator * (long double a, const shMath :: vec3<T>& b)
{
 return shMath :: vec3<T>(a * b.x(),a * b.y(), a * b.z());
}

template <class T> inline shMath :: vec3<T>& shMath :: operator += (shMath :: vec3<T>& a, const shMath :: vec3<T>& b)
{
 a.x() += b.x();
 a.y() += b.y();
 a.z() += b.z();
 return a;
}

template <class T> inline shMath :: vec3<T>& shMath :: operator -= (shMath :: vec3<T>& a, const shMath :: vec3<T>& b)
{
 a.x() -= b.x();
 a.y() -= b.y();
 a.z() -= b.z();
 return a;
}

template <class T> inline shMath :: vec3<T>& shMath :: operator *= (shMath :: vec3<T>& a, const T& b)
{
 a.x() *= b;
 a.y() *= b;
 a.z() *= b;
 return a;
}

template <class T> inline shMath :: vec3<T>& shMath :: operator /= (shMath :: vec3<T>& a, const T& b)
{
 const T bb = shMath :: inverse(b);
 a.x() *= bb;
 a.y() *= bb;
 a.z() *= bb;
 return a;
}
#endif
