#ifndef SH_MATH_VECTORNFX
#define SH_MATH_VECTORNFX

#include "ops.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include "rcvec2.h"
#include "rcvec3.h"
#include "rcvec4.h"
#include "functions.h"

namespace shMath
{
 template <unsigned int dim, class T> class vectorNfx
 {
  public:
  vectorNfx();
  explicit vectorNfx(const T (& in)[dim]);
  const T& operator [] (unsigned int a) const;
  T& operator [] (unsigned int a);
  vectorNfx operator - () const;
  vectorNfx operator + (const vectorNfx& a) const;
  vectorNfx operator - (const vectorNfx& a) const;
  template <class X> vectorNfx operator * (const X& a) const;
  template <class X> vectorNfx operator / (const X& a) const;
  vectorNfx& operator += (const vectorNfx& a);
  vectorNfx& operator -= (const vectorNfx& a);
  template <class X> vectorNfx& operator *= (const X& a);
  template <class X> vectorNfx& operator /= (const X& a);
  template <class X> operator const vectorNfx<dim,X>() const;
  operator const T* () const;
  operator T* ();
  void normalise();
  T squaredLength() const;
  T length() const;
  private:
  T data[dim];
 };

 template <class T> class vectorNfx <2u,T>
 {
  public:
  vectorNfx();
  explicit vectorNfx(const T (& in)[2u]);
  vectorNfx(const vec2<T>& a);
  vectorNfx(const rcvec2<T>& a);
  const T& operator [] (unsigned int a) const;
  T& operator [] (unsigned int a);
  vectorNfx operator - () const;
  vectorNfx operator + (const vectorNfx& a) const;
  vectorNfx operator - (const vectorNfx& a) const;
  template <class X> vectorNfx operator * (const X& a) const;
  template <class X> vectorNfx operator / (const X& a) const;
  vectorNfx& operator += (const vectorNfx& a);
  vectorNfx& operator -= (const vectorNfx& a);
  template <class X> vectorNfx& operator *= (const X& a);
  template <class X> vectorNfx& operator /= (const X& a);
  template <class X> operator const vectorNfx<2u,X>() const;
  operator const rcvec2<T> () const;
  operator const vec2<T> () const;
  operator vec2<T> ();
  operator const T* () const;
  operator T* ();
  void normalise();
  T squaredLength() const;
  T length() const;
  private:
  T data[2u];
 };

 template <class T> class vectorNfx <3u,T>
 {
  public:
  vectorNfx();
  explicit vectorNfx(const T (& in)[3u]);
  vectorNfx(const vec3<T>& a);
  vectorNfx(const rcvec3<T>& a);
  const T& operator [] (unsigned int a) const;
  T& operator [] (unsigned int a);
  vectorNfx operator - () const;
  vectorNfx operator + (const vectorNfx& a) const;
  vectorNfx operator - (const vectorNfx& a) const;
  template <class X> vectorNfx operator * (const X& a) const;
  template <class X> vectorNfx operator / (const X& a) const;
  vectorNfx& operator += (const vectorNfx& a);
  vectorNfx& operator -= (const vectorNfx& a);
  template <class X> vectorNfx& operator *= (const X& a);
  template <class X> vectorNfx& operator /= (const X& a);
  template <class X> operator const vectorNfx<3u,X>() const;
  operator const rcvec3<T> () const;
  operator const vec3<T> () const;
  operator vec3<T> ();
  operator const T* () const;
  operator T* ();
  void normalise();
  T squaredLength() const;
  T length() const;
  private:
  T data[3u];
 };

 template <class T> class vectorNfx <4u,T>
 {
  public:
  vectorNfx();
  explicit vectorNfx(const T (& in)[4u]);
  vectorNfx(const vec4<T>& a);
  vectorNfx(const rcvec4<T>& a);
  const T& operator [] (unsigned int a) const;
  T& operator [] (unsigned int a);
  vectorNfx operator - () const;
  vectorNfx operator + (const vectorNfx& a) const;
  vectorNfx operator - (const vectorNfx& a) const;
  template <class X> vectorNfx operator * (const X& a) const;
  template <class X> vectorNfx operator / (const X& a) const;
  vectorNfx& operator += (const vectorNfx& a);
  vectorNfx& operator -= (const vectorNfx& a);
  template <class X> vectorNfx& operator *= (const X& a);
  template <class X> vectorNfx& operator /= (const X& a);
  template <class X> operator const vectorNfx<4u,X>() const;
  operator const rcvec4<T> () const;
  operator const vec4<T> () const;
  operator vec4<T> ();
  operator const T* () const;
  operator T* ();
  void normalise();
  T squaredLength() const;
  T length() const;
  private:
  T data[4u];
 };

 template <unsigned int dim, class T> T length(const vectorNfx<dim,T>& a);
 template <unsigned int dim, class T> T squaredLength(const vectorNfx<dim,T>& a);
 template <unsigned int dim, class T> vectorNfx<dim,T> zero(const vectorNfx<dim,T>& a);
 template <unsigned int dim, class T> vectorNfx<dim,T> identity(const vectorNfx<dim,T>& a);
 template <unsigned int dim, class T> vectorNfx<dim,T> normalise(const vectorNfx<dim,T>& a);
 template <unsigned int dim, class T> T dot(const vectorNfx<dim,T>& a, const vectorNfx<dim,T>& b);
}

template <unsigned int dim, class T> inline shMath :: vectorNfx <dim,T> :: vectorNfx()
{
}

template <class T> inline shMath :: vectorNfx <2u,T> :: vectorNfx()
{
}

template <class T> inline shMath :: vectorNfx <3u,T> :: vectorNfx()
{
}

template <class T> inline shMath :: vectorNfx <4u,T> :: vectorNfx()
{
}

template <unsigned int dim, class T> inline shMath :: vectorNfx <dim,T> :: vectorNfx(const T (& in) [dim])
{
 T* thisData = (T*)(this);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(thisData++)) = in[i]);
}

template <class T> inline shMath :: vectorNfx <2u,T> :: vectorNfx(const T (& in) [2])
{
 T* thisData = (T*)(this);
 const T* inData = in;
 (*thisData) = (*inData);
 (*(++thisData)) = (*(++inData));
}

template <class T> inline shMath :: vectorNfx <3u,T> :: vectorNfx(const T (& in) [3])
{
 T* thisData = (T*)(this);
 const T* inData = in;
 (*thisData) = (*inData);
 (*(++thisData)) = (*(++inData));
 (*(++thisData)) = (*(++inData));
}

template <class T> inline shMath :: vectorNfx <4u,T> :: vectorNfx(const T (& in) [4])
{
 T* thisData = (T*)(this);
 const T* inData = in;
 (*thisData) = (*inData);
 (*(++thisData)) = (*(++inData));
 (*(++thisData)) = (*(++inData));
 (*(++thisData)) = (*(++inData));
}

template <class T> inline shMath :: vectorNfx <2u,T> :: vectorNfx(const shMath :: vec2<T>& a)
{
 (*((shMath :: vec2<T>*)(this))) = a;
}

template <class T> inline shMath :: vectorNfx <3u,T> :: vectorNfx(const shMath :: vec3<T>& a)
{
 (*((shMath :: vec3<T>*)(this))) = a;
}

template <class T> inline shMath :: vectorNfx <4u,T> :: vectorNfx(const shMath :: vec4<T>& a)
{
 (*((shMath :: vec4<T>*)(this))) = a;
}

template <class T> inline shMath :: vectorNfx <2u,T> :: vectorNfx(const shMath :: rcvec2<T>& a)
{
 (*((shMath :: vec2<T>*)(this))) = a;
}

template <class T> inline shMath :: vectorNfx <3u,T> :: vectorNfx(const shMath :: rcvec3<T>& a)
{
 (*((shMath :: vec3<T>*)(this))) = a;
}

template <class T> inline shMath :: vectorNfx <4u,T> :: vectorNfx(const shMath :: rcvec4<T>& a)
{
 (*((shMath :: vec4<T>*)(this))) = a;
}

template <unsigned int dim, class T> inline const T& shMath :: vectorNfx <dim,T> :: operator [] (unsigned int a) const
{
 return data[a];
}

template <class T> inline const T& shMath :: vectorNfx <2u,T> :: operator [] (unsigned int a) const
{
 return data[a];
}

template <class T> inline const T& shMath :: vectorNfx <3u,T> :: operator [] (unsigned int a) const
{
 return data[a];
}

template <class T> inline const T& shMath :: vectorNfx <4u,T> :: operator [] (unsigned int a) const
{
 return data[a];
}

template <unsigned int dim, class T> inline T& shMath :: vectorNfx <dim,T> :: operator [] (unsigned int a)
{
 return data[a];
}

template <class T> inline T& shMath :: vectorNfx <2u,T> :: operator [] (unsigned int a)
{
 return data[a];
}

template <class T> inline T& shMath :: vectorNfx <3u,T> :: operator [] (unsigned int a)
{
 return data[a];
}

template <class T> inline T& shMath :: vectorNfx <4u,T> :: operator [] (unsigned int a)
{
 return data[a];
}

template <unsigned int dim, class T> inline shMath :: vectorNfx<dim,T> shMath :: vectorNfx <dim,T> :: operator - () const
{
 shMath :: vectorNfx<dim,T> ans = shMath :: vectorNfx<dim,T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 unsigned int i = 0;
 for(;i < dim;++i,(*(ansData++)) = -(*(thisData++)));
 return ans;
}

template <class T> inline shMath :: vectorNfx<2u,T> shMath :: vectorNfx <2u,T> :: operator - () const
{
 shMath :: vectorNfx<2u,T> ans = shMath :: vectorNfx<2u,T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = -(*thisData);
 (*(++ansData)) = -(*(++thisData));
 return ans;
}

template <class T> inline shMath :: vectorNfx<3u,T> shMath :: vectorNfx <3u,T> :: operator - () const
{
 shMath :: vectorNfx<3u,T> ans = shMath :: vectorNfx<3u,T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = -(*thisData);
 (*(++ansData)) = -(*(++thisData));
 (*(++ansData)) = -(*(++thisData));
 return ans;
}

template <class T> inline shMath :: vectorNfx<4u,T> shMath :: vectorNfx <4u,T> :: operator - () const
{
 shMath :: vectorNfx<4u,T> ans = shMath :: vectorNfx<4u,T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = -(*thisData);
 (*(++ansData)) = -(*(++thisData));
 (*(++ansData)) = -(*(++thisData));
 (*(++ansData)) = -(*(++thisData));
 return ans;
}

template <unsigned int dim, class T> inline shMath :: vectorNfx<dim,T> shMath :: vectorNfx <dim,T> :: operator + (const shMath :: vectorNfx<dim,T>& a) const
{
 shMath :: vectorNfx<dim,T> ans = shMath :: vectorNfx<dim,T>();
 const T* thisData = (const T*)(this);
 const T* aData = (const T*)(a);
 T* ansData = (T*)(ans);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(ansData++)) = (*(thisData++)) + (*(aData++)));
 return ans;
}

template <class T> inline shMath :: vectorNfx<2u,T> shMath :: vectorNfx <2u,T> :: operator + (const shMath :: vectorNfx<2u,T>& a) const
{
 shMath :: vectorNfx<2u,T> ans = shMath :: vectorNfx<2u,T>();
 const T* thisData = (const T*)(this);
 const T* aData = (const T*)(a);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) + (*aData);
 (*(++ansData)) = (*(++thisData)) + (*(++aData));
 return ans;
}

template <class T> inline shMath :: vectorNfx<3u,T> shMath :: vectorNfx <3u,T> :: operator + (const shMath :: vectorNfx<3u,T>& a) const
{
 shMath :: vectorNfx<3u,T> ans = shMath :: vectorNfx<3u,T>();
 const T* thisData = (const T*)(this);
 const T* aData = (const T*)(a);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) + (*aData);
 (*(++ansData)) = (*(++thisData)) + (*(++aData));
 (*(++ansData)) = (*(++thisData)) + (*(++aData));
 return ans;
}

template <class T> inline shMath :: vectorNfx<4u,T> shMath :: vectorNfx <4u,T> :: operator + (const shMath :: vectorNfx<4u,T>& a) const
{
 shMath :: vectorNfx<4u,T> ans = shMath :: vectorNfx<4u,T>();
 const T* thisData = (const T*)(this);
 const T* aData = (const T*)(a);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) + (*aData);
 (*(++ansData)) = (*(++thisData)) + (*(++aData));
 (*(++ansData)) = (*(++thisData)) + (*(++aData));
 (*(++ansData)) = (*(++thisData)) + (*(++aData));
 return ans;
}

template <unsigned int dim, class T> inline shMath :: vectorNfx<dim,T> shMath :: vectorNfx <dim,T> :: operator - (const shMath :: vectorNfx<dim,T>& a) const
{
 shMath :: vectorNfx<dim,T> ans = shMath :: vectorNfx<dim,T>();
 const T* thisData = (const T*)(this);
 const T* aData = (const T*)(a);
 T* ansData = (T*)(ans);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(ansData++)) = (*(thisData++)) - (*(aData++)));
 return ans;
}

template <class T> inline shMath :: vectorNfx<2u,T> shMath :: vectorNfx <2u,T> :: operator - (const shMath :: vectorNfx<2u,T>& a) const
{
 shMath :: vectorNfx<2u,T> ans = shMath :: vectorNfx<2u,T>();
 const T* thisData = (const T*)(this);
 const T* aData = (const T*)(a);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) - (*aData);
 (*(++ansData)) = (*(++thisData)) - (*(++aData));
 return ans;
}

template <class T> inline shMath :: vectorNfx<3u,T> shMath :: vectorNfx <3u,T> :: operator - (const shMath :: vectorNfx<3u,T>& a) const
{
 shMath :: vectorNfx<3u,T> ans = shMath :: vectorNfx<3u,T>();
 const T* thisData = (const T*)(this);
 const T* aData = (const T*)(a);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) - (*aData);
 (*(++ansData)) = (*(++thisData)) - (*(++aData));
 (*(++ansData)) = (*(++thisData)) - (*(++aData));
 return ans;
}

template <class T> inline shMath :: vectorNfx<4u,T> shMath :: vectorNfx <4u,T> :: operator - (const shMath :: vectorNfx<4u,T>& a) const
{
 shMath :: vectorNfx<4u,T> ans = shMath :: vectorNfx<4u,T>();
 const T* thisData = (const T*)(this);
 const T* aData = (const T*)(a);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) - (*aData);
 (*(++ansData)) = (*(++thisData)) - (*(++aData));
 (*(++ansData)) = (*(++thisData)) - (*(++aData));
 (*(++ansData)) = (*(++thisData)) - (*(++aData));
 return ans;
}

template <unsigned int dim, class T> template <class X> inline shMath :: vectorNfx<dim,T> shMath :: vectorNfx <dim,T> :: operator * (const X& a) const
{
 shMath :: vectorNfx<dim,T> ans = shMath :: vectorNfx<dim,T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(ansData++)) = (*(thisData++)) * a);
 return ans;
}

template <class T> template <class X> inline shMath :: vectorNfx<2u,T> shMath :: vectorNfx <2u,T> :: operator * (const X& a) const
{
 shMath :: vectorNfx<2u,T> ans = shMath :: vectorNfx<2u,T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) * a;
 (*(++ansData)) = (*(++thisData)) * a;
 return ans;
}

template <class T> template <class X> inline shMath :: vectorNfx<3u,T> shMath :: vectorNfx <3u,T> :: operator * (const X& a) const
{
 shMath :: vectorNfx<3u,T> ans = shMath :: vectorNfx<3u,T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) * a;
 (*(++ansData)) = (*(++thisData)) * a;
 (*(++ansData)) = (*(++thisData)) * a;
 return ans;
}

template <class T> template <class X> inline shMath :: vectorNfx<4u,T> shMath :: vectorNfx <4u,T> :: operator * (const X& a) const
{
 shMath :: vectorNfx<4u,T> ans = shMath :: vectorNfx<4u,T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) * a;
 (*(++ansData)) = (*(++thisData)) * a;
 (*(++ansData)) = (*(++thisData)) * a;
 (*(++ansData)) = (*(++thisData)) * a;
 return ans;
}

template <unsigned int dim, class T> template <class X> inline shMath :: vectorNfx<dim,T> shMath :: vectorNfx <dim,T> :: operator / (const X& a) const
{
 shMath :: vectorNfx<dim,T> ans = shMath :: vectorNfx<dim,T>();
 const T* thisData = (const T*)(this);
 const X invA = shMath :: inverse(a);
 T* ansData = (T*)(ans);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(ansData++)) = (*(thisData++)) * invA);
 return ans;
}

template <class T> template <class X> inline shMath :: vectorNfx<2u,T> shMath :: vectorNfx <2u,T> :: operator / (const X& a) const
{
 shMath :: vectorNfx<2u,T> ans = shMath :: vectorNfx<2u,T>();
 const T* thisData = (const T*)(this);
 const X invA = shMath :: inverse(a);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) * invA;
 (*(++ansData)) = (*(++thisData)) * invA;
 return ans;
}

template <class T> template <class X> inline shMath :: vectorNfx<3u,T> shMath :: vectorNfx <3u,T> :: operator / (const X& a) const
{
 shMath :: vectorNfx<3u,T> ans = shMath :: vectorNfx<3u,T>();
 const T* thisData = (const T*)(this);
 const X invA = shMath :: inverse(a);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) * invA;
 (*(++ansData)) = (*(++thisData)) * invA;
 (*(++ansData)) = (*(++thisData)) * invA;
 return ans;
}

template <class T> template <class X> inline shMath :: vectorNfx<4u,T> shMath :: vectorNfx <4u,T> :: operator / (const X& a) const
{
 shMath :: vectorNfx<4u,T> ans = shMath :: vectorNfx<4u,T>();
 const T* thisData = (const T*)(this);
 const X invA = shMath :: inverse(a);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData) * invA;
 (*(++ansData)) = (*(++thisData)) * invA;
 (*(++ansData)) = (*(++thisData)) * invA;
 (*(++ansData)) = (*(++thisData)) * invA;
 return ans;
}

template <unsigned int dim, class T> inline shMath :: vectorNfx<dim,T>& shMath :: vectorNfx <dim,T> :: operator += (const shMath :: vectorNfx<dim,T>& a)
{
 const T* aData = (const T*)(a);
 T* thisData = (T*)(this);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(thisData++)) += (*(aData++)));
 return *this;
}

template <class T> inline shMath :: vectorNfx<2u,T>& shMath :: vectorNfx <2u,T> :: operator += (const shMath :: vectorNfx<2u,T>& a)
{
 const T* aData = (const T*)(a);
 T* thisData = (T*)(this);
 (*thisData) += (*aData);
 (*(++thisData)) += (*(++aData));
 return *this;
}

template <class T> inline shMath :: vectorNfx<3u,T>& shMath :: vectorNfx <3u,T> :: operator += (const shMath :: vectorNfx<3u,T>& a)
{
 const T* aData = (const T*)(a);
 T* thisData = (T*)(this);
 (*thisData) += (*aData);
 (*(++thisData)) += (*(++aData));
 (*(++thisData)) += (*(++aData));
 return *this;
}

template <class T> inline shMath :: vectorNfx<4u,T>& shMath :: vectorNfx <4u,T> :: operator += (const shMath :: vectorNfx<4u,T>& a)
{
 const T* aData = (const T*)(a);
 T* thisData = (T*)(this);
 (*thisData) += (*aData);
 (*(++thisData)) += (*(++aData));
 (*(++thisData)) += (*(++aData));
 (*(++thisData)) += (*(++aData));
 return *this;
}

template <unsigned int dim, class T> inline shMath :: vectorNfx<dim,T>& shMath :: vectorNfx <dim,T> :: operator -= (const shMath :: vectorNfx<dim,T>& a)
{
 const T* aData = (const T*)(a);
 T* thisData = (T*)(this);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(thisData++)) -= (*(aData++)));
 return *this;
}

template <class T> inline shMath :: vectorNfx<2u,T>& shMath :: vectorNfx <2u,T> :: operator -= (const shMath :: vectorNfx<2u,T>& a)
{
 const T* aData = (const T*)(a);
 T* thisData = (T*)(this);
 (*thisData) -= (*aData);
 (*(++thisData)) -= (*(++aData));
 return *this;
}

template <class T> inline shMath :: vectorNfx<3u,T>& shMath :: vectorNfx <3u,T> :: operator -= (const shMath :: vectorNfx<3u,T>& a)
{
 const T* aData = (const T*)(a);
 T* thisData = (T*)(this);
 (*thisData) -= (*aData);
 (*(++thisData)) -= (*(++aData));
 (*(++thisData)) -= (*(++aData));
 return *this;
}

template <class T> inline shMath :: vectorNfx<4u,T>& shMath :: vectorNfx <4u,T> :: operator -= (const shMath :: vectorNfx<4u,T>& a)
{
 const T* aData = (const T*)(a);
 T* thisData = (T*)(this);
 (*thisData) -= (*aData);
 (*(++thisData)) -= (*(++aData));
 (*(++thisData)) -= (*(++aData));
 (*(++thisData)) -= (*(++aData));
 return *this;
}

template <unsigned int dim, class T> template <class X> inline shMath :: vectorNfx<dim,T>& shMath :: vectorNfx <dim,T> :: operator *= (const X& a)
{
 T* thisData = (T*)(this);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(thisData++)) *= a);
 return *this;
}

template <class T> template <class X> inline shMath :: vectorNfx<2u,T>& shMath :: vectorNfx <2u,T> :: operator *= (const X& a)
{
 T* thisData = (T*)(this);
 (*thisData) *= a;
 (*(++thisData)) *= a;
 return *this;
}

template <class T> template <class X> inline shMath :: vectorNfx<3u,T>& shMath :: vectorNfx <3u,T> :: operator *= (const X& a)
{
 T* thisData = (T*)(this);
 (*thisData) *= a;
 (*(++thisData)) *= a;
 (*(++thisData)) *= a;
 return *this;
}

template <class T> template <class X> inline shMath :: vectorNfx<4u,T>& shMath :: vectorNfx <4u,T> :: operator *= (const X& a)
{
 T* thisData = (T*)(this);
 (*thisData) *= a;
 (*(++thisData)) *= a;
 (*(++thisData)) *= a;
 (*(++thisData)) *= a;
 return *this;
}

template <unsigned int dim, class T> template <class X> inline shMath :: vectorNfx<dim,T>& shMath :: vectorNfx <dim,T> :: operator /= (const X& a)
{
 const X invA = shMath :: inverse(a);
 T* thisData = (T*)(this);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(thisData++)) *= invA);
 return *this;
}

template <class T> template <class X> inline shMath :: vectorNfx<2u,T>& shMath :: vectorNfx <2u,T> :: operator /= (const X& a)
{
 const X invA = shMath :: inverse(a);
 T* thisData = (T*)(this);
 (*thisData) *= invA;
 (*(++thisData)) *= invA;
 return *this;
}

template <class T> template <class X> inline shMath :: vectorNfx<3u,T>& shMath :: vectorNfx <3u,T> :: operator /= (const X& a)
{
 const X invA = shMath :: inverse(a);
 T* thisData = (T*)(this);
 (*thisData) *= invA;
 (*(++thisData)) *= invA;
 (*(++thisData)) *= invA;
 return *this;
}

template <class T> template <class X> inline shMath :: vectorNfx<4u,T>& shMath :: vectorNfx <4u,T> :: operator /= (const X& a)
{
 const X invA = shMath :: inverse(a);
 T* thisData = (T*)(this);
 (*thisData) *= invA;
 (*(++thisData)) *= invA;
 (*(++thisData)) *= invA;
 (*(++thisData)) *= invA;
 return *this;
}

template <unsigned int dim, class T> template <class X> inline shMath :: vectorNfx <dim,T> :: operator const shMath :: vectorNfx<dim,X> () const
{
 shMath :: vectorNfx<dim,X> ans = shMath :: vectorNfx<dim,X>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(ansData++)) = (const X)(*(thisData++)));
 return ans;
}

template <class T> template <class X> inline shMath :: vectorNfx <2u,T> :: operator const shMath :: vectorNfx<2u,X> () const
{
 shMath :: vectorNfx<2u,X> ans = shMath :: vectorNfx<2u,X>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = (const X)(*thisData);
 (*(++ansData)) = (const X)(*(++thisData));
 return ans;
}

template <class T> template <class X> inline shMath :: vectorNfx <3u,T> :: operator const shMath :: vectorNfx<3u,X> () const
{
 shMath :: vectorNfx<3u,X> ans = shMath :: vectorNfx<3u,X>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = (const X)(*thisData);
 (*(++ansData)) = (const X)(*(++thisData));
 (*(++ansData)) = (const X)(*(++thisData));
 return ans;
}

template <class T> template <class X> inline shMath :: vectorNfx <4u,T> :: operator const shMath :: vectorNfx<4u,X> () const
{
 shMath :: vectorNfx<4u,X> ans = shMath :: vectorNfx<4u,X>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = (const X)(*thisData);
 (*(++ansData)) = (const X)(*(++thisData));
 (*(++ansData)) = (const X)(*(++thisData));
 (*(++ansData)) = (const X)(*(++thisData));
 return ans;
}

template <class T> inline shMath :: vectorNfx <2u,T> :: operator const shMath :: rcvec2<T> () const
{
 shMath :: rcvec2<T> ans = shMath :: rcvec2<T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData);
 (*(++ansData)) = (*(++thisData));
 return ans;
}

template <class T> inline shMath :: vectorNfx <3u,T> :: operator const shMath :: rcvec3<T> () const
{
 shMath :: rcvec3<T> ans = shMath :: rcvec3<T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData);
 (*(++ansData)) = (*(++thisData));
 (*(++ansData)) = (*(++thisData));
 return ans;
}

template <class T> inline shMath :: vectorNfx <4u,T> :: operator const shMath :: rcvec4<T> () const
{
 shMath :: rcvec4<T> ans = shMath :: rcvec4<T>();
 const T* thisData = (const T*)(this);
 T* ansData = (T*)(ans);
 (*ansData) = (*thisData);
 (*(++ansData)) = (*(++thisData));
 (*(++ansData)) = (*(++thisData));
 (*(++ansData)) = (*(++thisData));
 return ans;
}

template <class T> inline shMath :: vectorNfx <2u,T> :: operator const shMath :: vec2<T> () const
{
 return (*((const shMath :: vec2<T>*)(this)));
}

template <class T> inline shMath :: vectorNfx <3u,T> :: operator const shMath :: vec3<T> () const
{
 return (*((const shMath :: vec3<T>*)(this)));
}

template <class T> inline shMath :: vectorNfx <4u,T> :: operator const shMath :: vec4<T> () const
{
 return (*((const shMath :: vec4<T>*)(this)));
}

template <class T> inline shMath :: vectorNfx <2u,T> :: operator shMath :: vec2<T> ()
{
 return (*((shMath :: vec2<T>*)(this)));
}

template <class T> inline shMath :: vectorNfx <3u,T> :: operator shMath :: vec3<T> ()
{
 return (*((shMath :: vec3<T>*)(this)));
}

template <class T> inline shMath :: vectorNfx <4u,T> :: operator shMath :: vec4<T> ()
{
 return (*((shMath :: vec4<T>*)(this)));
}

template <unsigned int dim, class T> inline shMath :: vectorNfx <dim,T> :: operator const T* () const
{
 return (const T*)(this);
}

template <class T> inline shMath :: vectorNfx <2u,T> :: operator const T* () const
{
 return (const T*)(this);
}

template <class T> inline shMath :: vectorNfx <3u,T> :: operator const T* () const
{
 return (const T*)(this);
}

template <class T> inline shMath :: vectorNfx <4u,T> :: operator const T* () const
{
 return (const T*)(this);
}

template <unsigned int dim, class T> inline shMath :: vectorNfx <dim,T> :: operator T* ()
{
 return (T*)(this);
}

template <class T> inline shMath :: vectorNfx <2u,T> :: operator T* ()
{
 return (T*)(this);
}

template <class T> inline shMath :: vectorNfx <3u,T> :: operator T* ()
{
 return (T*)(this);
}

template <class T> inline shMath :: vectorNfx <4u,T> :: operator T* ()
{
 return (T*)(this);
}

template <unsigned int dim, class T> inline void shMath :: vectorNfx <dim,T> :: normalise()
{
 ((*this) /= length());
}

template <class T> inline void shMath :: vectorNfx <2u,T> :: normalise()
{
 ((*this) /= length());
}

template <class T> inline void shMath :: vectorNfx <3u,T> :: normalise()
{
 ((*this) /= length());
}

template <class T> inline void shMath :: vectorNfx <4u,T> :: normalise()
{
 ((*this) /= length());
}

template <unsigned int dim, class T> inline T shMath :: vectorNfx <dim,T> :: squaredLength() const
{
 return shMath :: dot(*this,*this);
}

template <class T> inline T shMath :: vectorNfx <2u,T> :: squaredLength() const
{
 return shMath :: dot(*this,*this);
}

template <class T> inline T shMath :: vectorNfx <3u,T> :: squaredLength() const
{
 return shMath :: dot(*this,*this);
}

template <class T> inline T shMath :: vectorNfx <4u,T> :: squaredLength() const
{
 return shMath :: dot(*this,*this);
}

template <unsigned int dim, class T> inline T shMath :: vectorNfx <dim,T> :: length() const
{
 return shMath :: sqrt(squaredLength());
}

template <class T> inline T shMath :: vectorNfx <2u,T> :: length() const
{
 return shMath :: sqrt(squaredLength());
}

template <class T> inline T shMath :: vectorNfx <3u,T> :: length() const
{
 return shMath :: sqrt(squaredLength());
}

template <class T> inline T shMath :: vectorNfx <4u,T> :: length() const
{
 return shMath :: sqrt(squaredLength());
}

template <unsigned int dim, class T> inline T shMath :: length(const shMath :: vectorNfx<dim,T>& a)
{
 return shMath :: sqrt(shMath :: squaredLength(a));
}

template <unsigned int dim, class T> inline T shMath :: squaredLength(const shMath :: vectorNfx<dim,T>& a)
{
 return shMath :: dot(a,a);
}

template <unsigned int dim, class T> inline shMath :: vectorNfx<dim,T> shMath :: zero(const shMath :: vectorNfx<dim,T>& a)
{
 shMath :: vectorNfx<dim,T> ans = shMath :: vectorNfx<dim,T>();
 const T* aData = (const T*)(a);
 T* ansData = (T*)(ans);
 unsigned int i = 0u;
 for(;i < dim;++i,(*(ansData++)) = shMath :: zero(*(aData++)));
 return ans;
}

template <unsigned int dim, class T> inline shMath :: vectorNfx<dim,T> shMath :: identity(const shMath :: vectorNfx<dim,T>& a)
{
 shMath :: vectorNfx<dim,T> ans = shMath :: vectorNfx<dim,T>();
 const T* aData = (const T*)(a);
 T* ansData = (T*)(ans);
 unsigned int i = 1u;
 for((*(ansData++)) = shMath :: identity(*(aData++));i < dim;++i,(*(ansData++)) = shMath :: zero(*(aData++)));
 return ans;
}

template <unsigned int dim, class T> inline shMath :: vectorNfx<dim,T> shMath :: normalise(const shMath :: vectorNfx<dim,T>& a)
{
 return (a * shMath :: inverse(shMath :: length(a)));
}

template <unsigned int dim, class T> inline T shMath :: dot(const shMath :: vectorNfx<dim,T>& a, const shMath :: vectorNfx<dim,T>& b)
{
 const T* aData = (const T*)(a);
 const T* bData = (const T*)(b);
 T ans = (*(aData++)) * (*(bData++));
 unsigned int i = 1u;
 for(;i < dim;++i,ans += (*(aData++)) * (*(bData++)));
 return ans;
}
#endif
