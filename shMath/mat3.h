#ifndef SH_MATH_MAT3
#define SH_MATH_MAT3

#include "ops.h"
#include "mat2.h"
#include "vec3.h"

/*|adg|
  |beh|
  |cfi|*/

namespace shMath
{
 template <class T> class mat3
 {
  public:
  mat3();
  mat3(const T& aa, const T& bb = shMath :: zero(T()), const T& cc = shMath :: zero(T()), const T& dd = shMath :: zero(T()), const T& ee = shMath :: zero(T()), const T& ff = shMath :: zero(T()), const T& gg = shMath :: zero(T()), const T& hh = shMath :: zero(T()), const T& ii = shMath :: zero(T()));
  mat3 operator - () const;
  mat3 operator + (const mat3<T>& m) const;
  mat3 operator - (const mat3<T>& m) const;
  mat3 operator * (const mat3<T>& m) const;
  vec3<T> operator * (const vec3<T>& m) const;
  mat3 operator * (const T& m) const;
  mat3 operator / (const mat3<T>& m) const;
  mat3 operator / (const T& m) const;
  mat3& operator += (const mat3<T>& m);
  mat3& operator -= (const mat3<T>& m);
  mat3& operator *= (const mat3<T>& m);
  mat3& operator *= (const T& m);
  mat3& operator /= (const mat3<T>& m);
  mat3& operator /= (const T& m);
  operator const T* () const;
  operator T* ();
  mat2<T> getMinor(unsigned char col, unsigned char row) const;
  T a;
  T b;
  T c;
  T d;
  T e;
  T f;
  T g;
  T h;
  T i;
 };

 template <class T> mat3<T> zero(const mat3<T>& a);
 template <class T> mat3<T> identity(const mat3<T>& a);
 template <class T> mat3<T> inverse(const mat3<T>& a);

 template <class T> T determinant(const mat3<T>& a);
 template <class T> mat3<T> adj(const mat3<T>& a);
 template <class T> mat3<T> transpose(const mat3<T>& a);

 template <class T> vec3<T> operator * (const vec3<T>& a, const mat3<T>& b);
 template <class T> mat3<T> operator * (float a, const mat3<T>& b);
 template <class T> mat3<T> operator * (double a, const mat3<T>& b);
 template <class T> mat3<T> operator * (long double a, const mat3<T>& b);
 template <class T> vec3<T> operator / (const vec3<T>& a, const mat3<T>& b);
 template <class T> mat3<T> operator / (float a, const mat3<T>& b);
 template <class T> mat3<T> operator / (double a, const mat3<T>& b);
 template <class T> mat3<T> operator / (long double a, const mat3<T>& b);
 template <class T> vec3<T>& operator *= (vec3<T>& a, const mat3<T>& b);
 template <class T> vec3<T>& operator /= (vec3<T>& a, const mat3<T>& b);
}

template <class T> inline shMath :: mat3 <T> :: mat3() : a(T()), b(T()), c(T()), d(T()), e(T()), f(T()), g(T()), h(T()), i(T())
{
}

template <class T> inline shMath :: mat3 <T> :: mat3(const T& aa, const T& bb, const T& cc, const T& dd, const T& ee, const T& ff, const T& gg, const T& hh, const T& ii) : a(aa), b(bb), c(cc), d(dd), e(ee), f(ff), g(gg), h(hh), i(ii)
{
}

template <class T> inline shMath :: mat3<T> shMath :: mat3 <T> :: operator - () const
{
 return shMath :: mat3<T>(-a, -b, -c, -d, -e, -f, -g, -h, -i);
}

template <class T> inline shMath :: mat3<T> shMath :: mat3 <T> :: operator + (const shMath :: mat3<T>& m) const
{
 return shMath :: mat3<T>(a + m.a, b + m.b, c + m.c, d + m.d, e + m.e, f + m.f, g + m.g, h + m.h, i + m.i);
}

template <class T> inline shMath :: mat3<T> shMath :: mat3 <T> :: operator - (const shMath :: mat3<T>& m) const
{
 return shMath :: mat3<T>(a - m.a, b - m.b, c - m.c, d - m.d, e - m.e, f - m.f, g - m.g, h - m.h, i - m.i);
}

template <class T> inline shMath :: mat3<T> shMath :: mat3 <T> :: operator * (const shMath :: mat3<T>& m) const
{
 return shMath :: mat3<T>((a * m.a) + (d * m.b) + (g * m.c), (b * m.a) + (e * m.b) + (h * m.c), (c * m.a) + (f * m.b) + (i * m.c), (a * m.d) + (d * m.e) + (g * m.f), (b * m.d) + (e * m.e) + (h * m.f), (c * m.d) + (f * m.e) + (i * m.f), (a * m.g) + (d * m.h) + (g * m.i), (b * m.g) + (e * m.h) + (h * m.i), (c * m.g) + (f * m.h) + (i * m.i));
}

template <class T> inline shMath :: vec3<T> shMath :: mat3 <T> :: operator * (const shMath :: vec3<T>& m) const
{
 return shMath :: vec3<T>((a * m.x()) + (d * m.y()) + (g * m.z()), (b * m.x()) + (e * m.y()) + (h * m.z()), (c * m.x()) + (f * m.y()) + (i * m.z()));
}

template <class T> inline shMath :: mat3<T> shMath :: mat3 <T> :: operator * (const T& m) const
{
 return shMath :: mat3<T>(a * m, b * m, c * m, d * m, e * m, f * m, g * m, h * m, i * m);
}

template <class T> inline shMath :: mat3<T> shMath :: mat3 <T> :: operator / (const shMath :: mat3<T>& m) const
{
 return ((*this) * shMath :: inverse(m));
}

template <class T> inline shMath :: mat3<T> shMath :: mat3 <T> :: operator / (const T& m) const
{
 const T mm = shMath :: inverse(m);
 return shMath :: mat3<T>(a * mm, b * mm, c * mm, d * mm, e * mm, f * mm, g * mm, h * mm, i * mm);
}

template <class T> inline shMath :: mat3<T>& shMath :: mat3 <T> :: operator += (const shMath :: mat3<T>& m)
{
 a += m.a;
 b += m.b;
 c += m.c;
 d += m.d;
 e += m.e;
 f += m.f;
 g += m.g;
 h += m.h;
 i += m.i;
 return *this;
}

template <class T> inline shMath :: mat3<T>& shMath :: mat3 <T> :: operator -= (const shMath :: mat3<T>& m)
{
 a -= m.a;
 b -= m.b;
 c -= m.c;
 d -= m.d;
 e -= m.e;
 f -= m.f;
 g -= m.g;
 h -= m.h;
 i -= m.i;
 return *this;
}

template <class T> inline shMath :: mat3<T>& shMath :: mat3 <T> :: operator *= (const shMath :: mat3<T>& m)
{
 return ((*this) = ((*this) * m));
}

template <class T> inline shMath :: mat3<T>& shMath :: mat3 <T> :: operator *= (const T& m)
{
 a *= m;
 b *= m;
 c *= m;
 d *= m;
 e *= m;
 f *= m;
 g *= m;
 h *= m;
 i *= m;
 return *this;
}

template <class T> inline shMath :: mat3<T>& shMath :: mat3 <T> :: operator /= (const shMath :: mat3<T>& m)
{
 return ((*this) = ((*this) / m));
}

template <class T> inline shMath :: mat3<T>& shMath :: mat3 <T> :: operator /= (const T& m)
{
 const T mm = shMath :: inverse(m);
 a *= mm;
 b *= mm;
 c *= mm;
 d *= mm;
 e *= mm;
 f *= mm;
 g *= mm;
 h *= mm;
 i *= mm;
 return *this;
}

template <class T> inline shMath :: mat3 <T> :: operator const T* () const
{
 return (const T*)(this);
}

template <class T> inline shMath :: mat3 <T> :: operator T* ()
{
 return (T*)(this);
}

template <class T> inline shMath :: mat2<T> shMath :: mat3 <T> :: getMinor(unsigned char col, unsigned char row) const
{
 switch(col)
 {
  case 0:
  switch(row)
  {
   case 0:
   return shMath :: mat2<T>(e,f,h,i);
   case 1:
   return shMath :: mat2<T>(d,f,g,i);
   case 2:
   return shMath :: mat2<T>(d,e,g,h);
   default:
   return shMath :: mat2<T>();
  }
  break;
  case 1:
  switch(row)
  {
   case 0:
   return shMath :: mat2<T>(b,c,h,i);
   case 1:
   return shMath :: mat2<T>(a,c,g,i);
   case 2:
   return shMath :: mat2<T>(a,b,g,h);
   default:
   return shMath :: mat2<T>();
  }
  break;
  case 2:
  switch(row)
  {
   case 0:
   return shMath :: mat2<T>(b,c,e,f);
   case 1:
   return shMath :: mat2<T>(a,c,d,f);
   case 2:
   return shMath :: mat2<T>(a,b,d,e);
   default:
   return shMath :: mat2<T>();
  }
  break;
  default:
  return shMath :: mat2<T>();
 }
}

template <class T> inline shMath :: mat3<T> shMath :: zero(const shMath :: mat3<T>& a)
{
 return shMath :: mat3<T>(shMath :: zero(a.a), shMath :: zero(a.b), shMath :: zero(a.c), shMath :: zero(a.d), shMath :: zero(a.e), shMath :: zero(a.f), shMath :: zero(a.g), shMath :: zero(a.h), shMath :: zero(a.i));
}

template <class T> inline shMath :: mat3<T> shMath :: identity(const shMath :: mat3<T>& a)
{
 return shMath :: mat3<T>(shMath :: identity(a.a), shMath :: zero(a.b), shMath :: zero(a.c), shMath :: zero(a.d), shMath :: identity(a.a), shMath :: zero(a.f), shMath :: zero(a.g), shMath :: zero(a.h), shMath :: identity(a.a));
}

template <class T> inline shMath :: mat3<T> shMath :: inverse(const shMath :: mat3<T>& a)
{
 return (shMath :: adj(a) / shMath :: determinant(a));
}

template <class T> inline T shMath :: determinant(const shMath :: mat3<T>& a)
{
 return ((a.a * a.e * a.i) + (a.d * a.h * a.c) + (a.g * a.b * a.f) - (a.g * a.e * a.c) - (a.d * a.b * a.i) - (a.a * a.h * a.f));
}

template <class T> inline shMath :: mat3<T> shMath :: adj(const shMath :: mat3<T>& a)
{
 return shMath :: mat3<T>((a.e * a.i) - (a.h * a.f), (a.h * a.c) - (a.b * a.i), (a.b * a.f) - (a.e * a.c), (a.g * a.f) - (a.d * a.i), (a.a * a.i) - (a.g * a.c), (a.d * a.c) - (a.a * a.f), (a.d * a.h) - (a.g * a.e), (a.g * a.b) - (a.a * a.h), (a.a * a.e) - (a.d * a.b));
}

template <class T> inline shMath :: mat3<T> shMath :: transpose(const shMath :: mat3<T>& a)
{
 return shMath :: mat3<T>(a.a, a.d, a.g, a.b, a.e, a.h, a.c, a.f, a.i);
}

template <class T> inline shMath :: vec3<T> shMath :: operator * (const shMath :: vec3<T>& a, const shMath :: mat3<T>& b)
{
 return shMath :: vec3<T>((a.x() * b.a) + (a.y() * b.b) + (a.z() * b.c), (a.x() * b.d) + (a.y() * b.e) + (a.z() * b.f), (a.x() * b.g) + (a.y() * b.h) + (a.z() * b.i));
}

template <class T> inline shMath :: mat3<T> shMath :: operator * (float a, const shMath :: mat3<T>& b)
{
 return shMath :: mat3<T>(a * b.a, a * b.b, a * b.c, a * b.d, a * b.e, a * b.f, a * b.g, a * b.h, a * b.i);
}

template <class T> inline shMath :: mat3<T> shMath :: operator * (double a, const shMath :: mat3<T>& b)
{
 return shMath :: mat3<T>(a * b.a, a * b.b, a * b.c, a * b.d, a * b.e, a * b.f, a * b.g, a * b.h, a * b.i);
}

template <class T> inline shMath :: mat3<T> shMath :: operator * (long double a, const shMath :: mat3<T>& b)
{
 return shMath :: mat3<T>(a * b.a, a * b.b, a * b.c, a * b.d, a * b.e, a * b.f, a * b.g, a * b.h, a * b.i);
}

template <class T> inline shMath :: vec3<T> shMath :: operator / (const shMath :: vec3<T>& a, const shMath :: mat3<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: mat3<T> shMath :: operator / (float a, const shMath :: mat3<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: mat3<T> shMath :: operator / (double a, const shMath :: mat3<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: mat3<T> shMath :: operator / (long double a, const shMath :: mat3<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: vec3<T>& shMath :: operator *= (shMath :: vec3<T>& a, const shMath :: mat3<T>& b)
{
 return (a = (a * b));
}

template <class T> inline shMath :: vec3<T>& shMath :: operator /= (shMath :: vec3<T>& a, const shMath :: mat3<T>& b)
{
 return (a = (a / b));
}
#endif
