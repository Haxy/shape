#ifndef SH_MATH_OPS
#define SH_MATH_OPS

#include <complex>
#include <limits.h>

namespace shMath
{
 template <class T> T zero(const T& a);
 template <class T> std :: complex<T> zero(const std :: complex<T>& a);
 template <class T> T identity(const T& a);
 template <class T> std :: complex<T> identity(const std :: complex<T>& a);
 template <class T> T inverse(const T& a);
 template <class T> std :: complex<T> inverse(const std :: complex<T>& a);
}

namespace shMath
{
 template <> inline float zero <float> (const float& a)
 {
  return 0.0f;
 }
}

namespace shMath
{
 template <> inline double zero <double> (const double& a)
 {
  return 0.0;
 }
}

namespace shMath
{
 template <> inline long double zero <long double> (const long double& a)
 {
  return (long double)(0.0);
 }
}

namespace shMath
{
 template <> inline signed char zero <signed char> (const signed char& a)
 {
  return (signed char)(0);
 }
}

namespace shMath
{
 template <> inline unsigned char zero <unsigned char> (const unsigned char& a)
 {
  return (unsigned char)(0);
 }
}

namespace shMath
{
 template <> inline signed short int zero <signed short int> (const signed short int& a)
 {
  return (signed short int)(0);
 }
}

namespace shMath
{
 template <> inline unsigned short int zero <unsigned short int> (const unsigned short int& a)
 {
  return (unsigned short int)(0);
 }
}

namespace shMath
{
 template <> inline signed int zero <signed int> (const signed int& a)
 {
  return 0;
 }
}

namespace shMath
{
 template <> inline unsigned int zero <unsigned int> (const unsigned int& a)
 {
  return (unsigned int)(0);
 }
}

namespace shMath
{
 template <> inline signed long int zero <signed long int> (const signed long int& a)
 {
  return (signed long int)(0);
 }
}

namespace shMath
{
 template <> inline unsigned long int zero <unsigned long int> (const unsigned long int& a)
 {
  return (unsigned long int)(0);
 }
}

namespace shMath
{
 template <> inline signed long long int zero <signed long long int> (const signed long long int& a)
 {
  return (signed long long int)(0);
 }
}

namespace shMath
{
 template <> inline unsigned long long int zero <unsigned long long int> (const unsigned long long int& a)
 {
  return (unsigned long long int)(0);
 }
}

template <class T> inline std :: complex<T> shMath :: zero(const std :: complex<T>& a)
{
 return std :: complex <T>(zero(a.real()),zero(a.imag()));
}

namespace shMath
{
 template <> inline float identity <float> (const float& a)
 {
  return 1.0f;
 }
}

namespace shMath
{
 template <> inline double identity <double> (const double& a)
 {
  return 1.0;
 }
}

namespace shMath
{
 template <> inline long double identity <long double> (const long double& a)
 {
  return (long double)(1.0);
 }
}

namespace shMath
{
 template <> inline signed char identity <signed char> (const signed char& a)
 {
  return (signed char)(1);
 }
}

namespace shMath
{
 template <> inline unsigned char identity <unsigned char> (const unsigned char& a)
 {
  return (unsigned char)(1);
 }
}

namespace shMath
{
 template <> inline signed short int identity <signed short int> (const signed short int& a)
 {
  return (signed short int)(1);
 }
}

namespace shMath
{
 template <> inline unsigned short int identity <unsigned short int> (const unsigned short int& a)
 {
  return (unsigned short int)(1);
 }
}

namespace shMath
{
 template <> inline signed int identity <signed int> (const signed int& a)
 {
  return 1;
 }
}

namespace shMath
{
 template <> inline unsigned int identity <unsigned int> (const unsigned int& a)
 {
  return (unsigned int)(1);
 }
}

namespace shMath
{
 template <> inline signed long int identity <signed long int> (const signed long int& a)
 {
  return (signed long int)(1);
 }
}

namespace shMath
{
 template <> inline unsigned long int identity <unsigned long int> (const unsigned long int& a)
 {
  return (unsigned long int)(1);
 }
}

namespace shMath
{
 template <> inline signed long long int identity <signed long long int> (const signed long long int& a)
 {
  return (signed long long int)(1);
 }
}

namespace shMath
{
 template <> inline unsigned long long int identity <unsigned long long int> (const unsigned long long int& a)
 {
  return (unsigned long long int)(1);
 }
}

template <class T> inline std :: complex<T> shMath :: identity(const std :: complex<T>& a)
{
 return std :: complex <T> (shMath :: identity(a.real()),shMath :: zero(a.imag()));
}

namespace shMath
{
 template <> inline float inverse <float> (const float& a)
 {
  return (1.0f / a);
 }
}

namespace shMath
{
 template <> inline double inverse <double> (const double& a)
 {
  return (1.0 / a);
 }
}

namespace shMath
{
 template <> inline long double inverse <long double> (const long double& a)
 {
  return ((long double)(1.0) / a);
 }
}

namespace shMath
{
 template <> inline signed char inverse <signed char> (const signed char& a)
 {
  return ((a != (signed char)(0)) ? (signed char)(0) : SCHAR_MAX);
 }
}

namespace shMath
{
 template <> inline unsigned char inverse <unsigned char> (const unsigned char& a)
 {
  return ((a != (unsigned char)(0)) ? (unsigned char)(0) : UCHAR_MAX);
 }
}

namespace shMath
{
 template <> inline signed short int inverse <signed short int> (const signed short int& a)
 {
  return ((a != (signed short int)(0)) ? (signed short int)(0) : SHRT_MAX);
 }
}

namespace shMath
{
 template <> inline unsigned short int inverse <unsigned short int> (const unsigned short int& a)
 {
  return ((a != (unsigned short int)(0)) ? (unsigned short int)(0) : USHRT_MAX);
 }
}

namespace shMath
{
 template <> inline signed int inverse <signed int> (const signed int& a)
 {
  return ((a != 0) ? 0 : INT_MAX);
 }
}

namespace shMath
{
 template <> inline unsigned int inverse <unsigned int> (const unsigned int& a)
 {
  return ((a != (unsigned int)(0)) ? (unsigned int)(0) : UINT_MAX);
 }
}

namespace shMath
{
 template <> inline signed long int inverse <signed long int> (const signed long int& a)
 {
  return ((a != (signed long int)(0)) ? (signed long int)(0) : LONG_MAX);
 }
}

namespace shMath
{
 template <> inline unsigned long int inverse <unsigned long int> (const unsigned long int& a)
 {
  return ((a != (unsigned long int)(0)) ? (unsigned long int)(0) : ULONG_MAX);
 }
}
 
template <class T> inline std :: complex<T> shMath :: inverse(const std :: complex<T>& a)
{
 return (shMath :: identity(a) / a);
}
#endif
