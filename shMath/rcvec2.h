#ifndef SH_MATH_RCVEC2
#define SH_MATH_RCVEC2

#include <new>
#include <stdlib.h>

#include "ops.h"
#include "functions.h"

namespace shMath
{
 template <class T> class rcvec2
 {
  public:
  rcvec2(void);
  rcvec2(const rcvec2& a);
  rcvec2(const T& x, const T& y = T());
  ~rcvec2(void);
  rcvec2& operator = (const rcvec2& a);
  rcvec2 operator - () const;
  operator const T* () const;
  operator T* ();
  void normalise();
  const T& x() const;
  const T& y() const;
  T& x();
  T& y();
  private:
  void kill();
  void copy();
  T* data;
  unsigned int* counter;
 };

 template <class T> T slength(const rcvec2<T>& a);
 template <class T> T length(const rcvec2<T>& a);
 template <class T> rcvec2<T> normalise(const rcvec2<T>& a);

 template <class T> T dot (const rcvec2<T>& a, const rcvec2<T>& b);
 template <class T> rcvec2<T> cross(const rcvec2<T>& a);

 template <class T> rcvec2<T> operator + (const rcvec2<T>& a, const rcvec2<T>& b);
 template <class T> rcvec2<T> operator - (const rcvec2<T>& a, const rcvec2<T>& b);
 template <class T> rcvec2<T> operator * (const rcvec2<T>& a, const T& b);
 template <class T> rcvec2<T> operator / (const rcvec2<T>& a, const T& b);
 template <class T> rcvec2<T> operator * (float a, const rcvec2<T>& b);
 template <class T> rcvec2<T> operator * (double a, const rcvec2<T>& b);
 template <class T> rcvec2<T> operator * (long double a, const rcvec2<T>& b);
 template <class T> rcvec2<T>& operator += (rcvec2<T>& a, const rcvec2<T>& b);
 template <class T> rcvec2<T>& operator -= (rcvec2<T>& a, const rcvec2<T>& b);
 template <class T> rcvec2<T>& operator *= (rcvec2<T>& a, const T& b);
 template <class T> rcvec2<T>& operator /= (rcvec2<T>& a, const T& b);
}

template <class T> inline shMath :: rcvec2 <T> :: rcvec2() : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  data = new T [2u] ();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> inline shMath :: rcvec2 <T> :: rcvec2(const shMath :: rcvec2<T>& a) : data(a.data), counter(a.counter)
{
 ++(counter);
}

template <class T> shMath :: rcvec2 <T> :: rcvec2(const T& x, const T& y) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  T* temp;
  temp = data = new T [2u] ();
  (*temp) = x;
  (*(++temp)) = y;
 }
 catch(...)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
  throw;
 }
}

template <class T> inline shMath :: rcvec2 <T> :: ~rcvec2()
{
 kill();
}

template <class T> inline shMath :: rcvec2<T>& shMath :: rcvec2 <T> :: operator = (const shMath :: rcvec2<T>& a)
{
 if (&a != this)
 {
  kill();
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline shMath :: rcvec2<T> shMath :: rcvec2 <T> :: operator - () const
{
 const T* y = data;
 const T* x = y++;
 return shMath :: rcvec2<T>(-(*x),-(*y));
}

template <class T> inline shMath :: rcvec2 <T> :: operator const T* () const
{
 return data;
}

template <class T> inline shMath :: rcvec2 <T> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T> inline void shMath :: rcvec2 <T> :: normalise()
{
 const T len = shMath :: inverse(shMath :: length(*this));
 if ((*counter) != 0u)
 {
  copy();
 }
 T* temp = data;
 (*temp) *= len;
 (*(++temp)) *= len;
}

template <class T> inline const T& shMath :: rcvec2 <T> :: x() const
{
 return (*data);
}

template <class T> inline const T& shMath :: rcvec2 <T> :: y() const
{
 const T* temp = data;
 return (*(++temp));
}

template <class T> inline T& shMath :: rcvec2 <T> :: x()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*data);
}

template <class T> inline T& shMath :: rcvec2 <T> :: y()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 T* temp = data;
 return (*(++temp));
}

template <class T> inline void shMath :: rcvec2 <T> :: kill()
{
 if ((*counter) == 0u)
 {
  delete [] data;
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> void shMath :: rcvec2 <T> :: copy()
{
 T* temp = NULL;
 T* temp2;
 T* temp3 = data;
 unsigned int* tempcount;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  temp2 = temp = new T [2u] ();
  (*temp2) = (*temp3);
  (*(++temp2)) = (*(++temp3));
 }
 catch(...)
 {
  if (temp != NULL)
  {
   delete [] temp;
  }
  free(tempcount);
  throw;
 }
 --(*counter);
 counter = tempcount;
 data = temp;
}

template <class T> inline T shMath :: slength(const shMath :: rcvec2<T>& a)
{
 const T* temp = a;
 const T x = (*temp);
 const T y = (*(+temp));
 return (x * x) + (y * y);
}

template <class T> inline T shMath :: length(const shMath :: rcvec2<T>& a)
{
 return shMath :: sqrt(shMath :: slength(a));
}

template <class T> inline shMath :: rcvec2<T> shMath :: normalise(const rcvec2<T>& a)
{
 const T* temp = a;
 const T x = (*temp);
 const T y = (*(++temp));
 const T ilen = shMath :: inverse(shMath :: sqrt((x * x) + (y * y)));
 return shMath :: rcvec2<T>(x * ilen,y * ilen);
}

template <class T> inline T shMath :: dot(const shMath :: rcvec2<T>& a, const shMath :: rcvec2<T>& b)
{
 const T* temp = a;
 const T* temp2 = b;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T ya = (*(++temp));
 const T yb = (*(++temp2));
 return (xa * xb) + (ya * yb);
}

template <class T> inline shMath :: rcvec2<T> shMath :: cross(const shMath :: rcvec2<T>& a)
{
 const T* temp = a;
 const T x = (*temp);
 const T y = (*(++temp));
 return shMath :: rcvec2<T>(y,-x);
}

template <class T> inline shMath :: rcvec2<T> shMath :: operator + (const shMath :: rcvec2<T>& a, const shMath :: rcvec2<T>& b)
{
 const T* temp = a;
 const T* temp2 = b;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T ya = (*(++temp));
 const T yb = (*(++temp2));
 return shMath :: rcvec2<T>(xa + xb,ya + yb);
}

template <class T> inline shMath :: rcvec2<T> shMath :: operator - (const shMath :: rcvec2<T>& a, const shMath :: rcvec2<T>& b)
{
 const T* temp = a;
 const T* temp2 = b;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T ya = (*(++temp));
 const T yb = (*(++temp2));
 return shMath :: rcvec2<T>(xa - xb,ya - yb);
}

template <class T> inline shMath :: rcvec2<T> shMath :: operator * (const shMath :: rcvec2<T>& a, const T& b)
{
 const T* temp = a;
 const T x = (*temp);
 const T y = (*(++temp));
 return shMath :: rcvec2<T>(x * b,y * b);
}

template <class T> inline shMath :: rcvec2<T> shMath :: operator / (const shMath :: rcvec2<T>& a, const T& b)
{
 return a * shMath :: inverse(b);
}

template <class T> inline shMath :: rcvec2<T> shMath :: operator * (float a, const shMath :: rcvec2<T>& b)
{
 const T* temp = b;
 const T x = (*temp);
 const T y = (*(++temp));
 return shMath :: rcvec2<T>(a * x,a * y);
}

template <class T> inline shMath :: rcvec2<T> shMath :: operator * (double a, const shMath :: rcvec2<T>& b)
{
 const T* temp = b;
 const T x = (*temp);
 const T y = (*(++temp));
 return shMath :: rcvec2<T>(a * x,a * y);
}

template <class T> inline shMath :: rcvec2<T> shMath :: operator * (long double a, const shMath :: rcvec2<T>& b)
{
 const T* temp = b;
 const T x = (*temp);
 const T y = (*(++temp));
 return shMath :: rcvec2<T>(a * x,a * y);
}

template <class T> inline shMath :: rcvec2<T>& shMath :: operator += (shMath :: rcvec2<T>& a, const shMath :: rcvec2<T>& b)
{
 T* temp = a;
 const T* temp2 = b;
 (*temp) += (*temp2);
 (*(++temp)) += (*(++temp2));
 return a;
}

template <class T> inline shMath :: rcvec2<T>& shMath :: operator -= (shMath :: rcvec2<T>& a, const shMath :: rcvec2<T>& b)
{
 T* temp = a;
 const T* temp2 = b;
 (*temp) -= (*temp2);
 (*(++temp)) -= (*(++temp2));
 return a;
}

template <class T> inline shMath :: rcvec2<T>& shMath :: operator *= (shMath :: rcvec2<T>& a, const T& b)
{
 T* temp = a;
 (*temp) *= b;
 (*(++temp)) *= b;
 return a;
}

template <class T> inline shMath :: rcvec2<T>& shMath :: operator /= (shMath :: rcvec2<T>& a, const T& b)
{
 return a *= shMath :: inverse(b);
}
#endif
