#ifndef SH_MATH_MAT4
#define SH_MATH_MAT4

#include "ops.h"
#include "vec4.h"
#include "mat3.h"

/*|aeim|
  |bfjn|
  |cgko|
  |dhlp|*/

namespace shMath
{
 template <class T> class mat4
 {
  public:
  mat4();
  mat4(const T& aa, const T& bb = shMath :: zero(T()), const T& cc = shMath :: zero(T()), const T& dd = shMath :: zero(T()), const T& ee = shMath :: zero(T()), const T& ff = shMath :: zero(T()), const T& gg = shMath :: zero(T()), const T& hh = shMath :: zero(T()), const T& ii = shMath :: zero(T()), const T& jj = shMath :: zero(T()), const T& kk = shMath :: zero(T()), const T& ll = shMath :: zero(T()), const T& mm = shMath :: zero(T()), const T& nn = shMath :: zero(T()), const T& oo = shMath :: zero(T()), const T& pp = shMath :: zero(T()));
  mat4 operator - () const;
  mat4 operator + (const mat4<T>& z) const;
  mat4 operator - (const mat4<T>& z) const;
  mat4 operator * (const mat4<T>& z) const;
  vec4<T> operator * (const vec4<T>& z) const;
  mat4 operator * (const T& z) const;
  mat4 operator / (const mat4<T>& z) const;
  mat4 operator / (const T& z) const;
  mat4<T>& operator += (const mat4<T>& z);
  mat4<T>& operator -= (const mat4<T>& z);
  mat4<T>& operator *= (const mat4<T>& z);
  mat4<T>& operator *= (const T& z);
  mat4<T>& operator /= (const mat4<T>& z);
  mat4<T>& operator /= (const T& z);
  operator const T* () const;
  operator T* ();
  mat3<T> getMinor(unsigned char col, unsigned char row) const;
  T a;
  T b;
  T c;
  T d;
  T e;
  T f;
  T g;
  T h;
  T i;
  T j;
  T k;
  T l;
  T m;
  T n;
  T o;
  T p;
 };
 
 template <class T> mat4<T> zero(const mat4<T>& a);
 template <class T> mat4<T> identity(const mat4<T>& a);
 template <class T> mat4<T> inverse(const mat4<T>& a);

 template <class T> T determinant(const mat4<T>& a);
 template <class T> mat4<T> adj(const mat4<T>& a);
 template <class T> mat4<T> transpose(const mat4<T>& a);

 template <class T> vec4<T> operator * (const vec4<T>& a, const mat4<T>& b);
 template <class T> mat4<T> operator * (float a, const mat4<T>& b);
 template <class T> mat4<T> operator * (double a, const mat4<T>& b);
 template <class T> mat4<T> operator * (long double a, const mat4<T>& b);
 template <class T> vec4<T> operator / (const vec4<T>& a, const mat4<T>& b);
 template <class T> mat4<T> operator / (float a, const mat4<T>& b);
 template <class T> mat4<T> operator / (double a, const mat4<T>& b);
 template <class T> mat4<T> operator / (long double a, const mat4<T>& b);
 template <class T> vec4<T>& operator *= (vec4<T>& a, const mat4<T>& b);
 template <class T> vec4<T>& operator /= (vec4<T>& a, const mat4<T>& b);
}

template <class T> inline shMath :: mat4 <T> :: mat4() : a(T()), b(T()), c(T()), d(T()), e(T()), f(T()), g(T()), h(T()), i(T()), j(T()), k(T()), l(T()), m(T()), n(T()), o(T()), p(T())
{
}

template <class T> inline shMath :: mat4 <T> :: mat4(const T& aa, const T& bb, const T& cc, const T& dd, const T& ee, const T& ff, const T& gg, const T& hh, const T& ii, const T& jj, const T& kk, const T& ll, const T& mm, const T& nn, const T& oo, const T& pp) : a(aa), b(bb), c(cc), d(dd), e(ee), f(ff), g(gg), h(hh), i(ii), j(jj), k(kk), l(ll), m(mm), n(nn), o(oo), p(pp)
{
}

template <class T> inline shMath :: mat4<T> shMath :: mat4 <T> :: operator - () const
{
 return shMath :: mat4<T>(-a, -b, -c, -d, -e, -f, -g, -h, -i, -j, -k, -l, -m, -n, -o, -p);
}

template <class T> inline shMath :: mat4<T> shMath :: mat4 <T> :: operator + (const shMath :: mat4<T>& z) const
{
 return shMath :: mat4<T>(a + z.a, b + z.b, c + z.c, d + z.d, e + z.e, f + z.f, g + z.g, h + z.h, i + z.i, j + z.j, k + z.k, l + z.l, m + z.m, n + z.n, o + z.o, p + z.p);
}

template <class T> inline shMath :: mat4<T> shMath :: mat4 <T> :: operator - (const shMath :: mat4<T>& z) const
{
 return shMath :: mat4<T>(a - z.a, b - z.b, c - z.c, d - z.d, e - z.e, f - z.f, g - z.g, h - z.h, i - z.i, j - z.j, k - z.k, l - z.l, m - z.m, n - z.n, o - z.o, p - z.p);
}

template <class T> inline shMath :: mat4<T> shMath :: mat4 <T> :: operator * (const shMath :: mat4<T>& z) const
{
 return shMath :: mat4<T>((a * z.a) + (e * z.b) + (i * z.c) + (m * z.d), (b * z.a) + (f * z.b) + (j * z.c) + (n * z.d), (c * z.a) + (g * z.b) + (k * z.c) + (o * z.d), (d * z.a) + (h * z.b) + (l * z.c) + (p * z.d), (a * z.e) + (e * z.f) + (i * z.g) + (m * z.h), (b * z.e) + (f * z.f) + (j * z.g) + (n * z.h), (c * z.e) + (g * z.f) + (k * z.g) + (o * z.h), (d * z.e) + (h * z.f) + (l * z.g) + (p * z.h), (a * z.i) + (e * z.j) + (i * z.k) + (m * z.l), (b * z.i) + (f * z.j) + (j * z.k) + (n * z.l), (c * z.i) + (g * z.j) + (k * z.k) + (o * z.l), (d * z.i) + (h * z.j) + (l * z.k) + (p * z.l), (a * z.m) + (e * z.n) + (i * z.o) + (m * z.p), (b * z.m) + (f * z.n) + (j * z.o) + (n * z.p), (c * z.m) + (g * z.n) + (k * z.o) + (o * z.p), (d * z.m) + (h * z.n) + (l * z.o) + (p * z.p));
}

template <class T> inline shMath :: vec4<T> shMath :: mat4 <T> :: operator * (const shMath :: vec4<T>& z) const
{
 return shMath :: vec4<T>((a * z.x()) + (e * z.y()) + (i * z.z()) + (m * z.w()), (b * z.x()) + (f * z.y()) + (j * z.z()) + (n * z.w()), (c * z.x()) + (g * z.y()) + (k * z.z()) + (o * z.w()), (d * z.x()) + (h * z.y()) + (l * z.z()) + (p * z.w()));
}

template <class T> inline shMath :: mat4<T> shMath :: mat4 <T> :: operator * (const T& z) const
{
 return shMath :: mat4<T>(a * z, b * z, c * z, d * z, e * z, f * z, g * z, h * z, i * z, j * z, k * z, l * z, m * z, n * z, o * z, p * z);
}

template <class T> inline shMath :: mat4<T> shMath :: mat4 <T> :: operator / (const shMath :: mat4<T>& z) const
{
 return ((*this) * shMath :: inverse(z));
}

template <class T> inline shMath :: mat4<T> shMath :: mat4 <T> :: operator / (const T& z) const
{
 const T zz = shMath :: inverse(z);
 return shMath :: mat4<T>(a * zz, b * zz, c * zz, d * zz, e * zz, f * zz, g * zz, h * zz, i * zz, j * zz, k * zz, l * zz, m * zz, n * zz, o * zz, p * zz);
}

template <class T> inline shMath :: mat4<T>& shMath :: mat4 <T> :: operator += (const shMath :: mat4<T>& z)
{
 a += z.a;
 b += z.b;
 c += z.c;
 d += z.d;
 e += z.e;
 f += z.f;
 g += z.g;
 h += z.h;
 i += z.i;
 j += z.j;
 k += z.k;
 l += z.l;
 m += z.m;
 n += z.n;
 o += z.o;
 p += z.p;
 return *this;
}

template <class T> inline shMath :: mat4<T>& shMath :: mat4 <T> :: operator -= (const shMath :: mat4<T>& z)
{
 a -= z.a;
 b -= z.b;
 c -= z.c;
 d -= z.d;
 e -= z.e;
 f -= z.f;
 g -= z.g;
 h -= z.h;
 i -= z.i;
 j -= z.j;
 k -= z.k;
 l -= z.l;
 m -= z.m;
 n -= z.n;
 o -= z.o;
 p -= z.p;
 return *this;
}

template <class T> inline shMath :: mat4<T>& shMath :: mat4 <T> :: operator *= (const shMath :: mat4<T>& z)
{
 return ((*this) = ((*this) * z));
}

template <class T> inline shMath :: mat4<T>& shMath :: mat4 <T> :: operator *= (const T& z)
{
 a *= z;
 b *= z;
 c *= z;
 d *= z;
 e *= z;
 f *= z;
 g *= z;
 h *= z;
 i *= z;
 j *= z;
 k *= z;
 l *= z;
 m *= z;
 n *= z;
 o *= z;
 p *= z;
 return *this;
}

template <class T> inline shMath :: mat4<T>& shMath :: mat4 <T> :: operator /= (const shMath :: mat4<T>& z)
{
 return ((*this) = ((*this) / z));
}

template <class T> inline shMath :: mat4<T>& shMath :: mat4 <T> :: operator /= (const T& z)
{
 const T zz = shMath :: inverse(z);
 a *= zz;
 b *= zz;
 c *= zz;
 d *= zz;
 e *= zz;
 f *= zz;
 g *= zz;
 h *= zz;
 i *= zz;
 j *= zz;
 k *= zz;
 l *= zz;
 m *= zz;
 n *= zz;
 o *= zz;
 p *= zz;
 return *this;
}

template <class T> inline shMath :: mat4 <T> :: operator const T* () const
{
 return (const T*)(this);
}

template <class T> inline shMath :: mat4 <T> :: operator T* ()
{
 return (T*)(this);
}

template <class T> inline shMath :: mat3<T> shMath :: mat4 <T> :: getMinor(unsigned char row, unsigned char col) const
{
 switch(row)
 {
  case 0:
  switch(col)
  {
   case 0:
   return shMath :: mat3<T>(f,g,h,j,k,l,n,o,p);
   break;
   case 1:
   return shMath :: mat3<T>(b,c,d,j,k,l,n,o,p);
   break;
   case 2:
   return shMath :: mat3<T>(b,c,d,f,g,h,n,o,p);
   break;
   case 3:
   return shMath :: mat3<T>(b,c,d,f,g,h,j,k,l);
   break;
  }
  break;
  case 1:
  switch(col)
  {
   case 0:
   return shMath :: mat3<T>(e,g,h,i,k,l,m,o,p);
   break;
   case 1:
   return shMath :: mat3<T>(a,c,d,i,k,l,m,o,p);
   break;
   case 2:
   return shMath :: mat3<T>(a,c,d,e,g,h,m,o,p);
   break;
   case 3:
   return shMath :: mat3<T>(a,c,d,e,g,h,i,k,l);
   break;
  }
  break;
  case 2:
  switch(col)
  {
   case 0:
   return shMath :: mat3<T>(e,f,h,i,j,l,m,n,p);
   break;
   case 1:
   return shMath :: mat3<T>(a,b,d,i,j,l,m,n,p);
   break;
   case 2:
   return shMath :: mat3<T>(a,b,d,e,f,h,m,n,p);
   break;
   case 3:
   return shMath :: mat3<T>(a,b,d,e,f,h,i,j,l);
   break;
  }
  break;
  case 3:
  switch(col)
  {
   case 0:
   return shMath :: mat3<T>(e,f,g,i,j,k,m,n,o);
   break;
   case 1:
   return shMath :: mat3<T>(a,b,c,i,j,k,m,n,o);
   break;
   case 2:
   return shMath :: mat3<T>(a,b,c,e,f,g,m,n,o);
   break;
   case 3:
   return shMath :: mat3<T>(a,b,c,e,f,g,i,j,k);
   break;
  }
  break;
 }
 return shMath :: mat3<T>();
}

template <class T> inline shMath :: mat4<T> shMath :: zero(const shMath :: mat4<T>& a)
{
 return shMath :: mat4<T>(shMath :: zero(a.a),shMath :: zero(a.b),shMath :: zero(a.c),shMath :: zero(a.d),shMath :: zero(a.e),shMath :: zero(a.f),shMath :: zero(a.g),shMath :: zero(a.h),shMath :: zero(a.i),shMath :: zero(a.j),shMath :: zero(a.k),shMath :: zero(a.l),shMath :: zero(a.m),shMath :: zero(a.n),shMath :: zero(a.o),shMath :: zero(a.p));
}

template <class T> inline shMath :: mat4<T> shMath :: identity(const shMath :: mat4<T>& a)
{
 return shMath :: mat4<T>(shMath :: identity(a.a),shMath :: zero(a.b),shMath :: zero(a.c),shMath :: zero(a.d),shMath :: zero(a.e),shMath :: identity(a.f),shMath :: zero(a.g),shMath :: zero(a.h),shMath :: zero(a.i),shMath :: zero(a.j),shMath :: identity(a.k),shMath :: zero(a.l),shMath :: zero(a.m),shMath :: zero(a.n),shMath :: zero(a.o),shMath :: identity(a.p));
}

template <class T> inline shMath :: mat4<T> shMath :: inverse(const shMath :: mat4<T>& a)
{
 return (shMath :: adj(a) / shMath :: determinant(a));
}

template <class T> inline T shMath :: determinant(const shMath :: mat4<T>& a)
{
 return ((a.a * a.f * a.k * a.p) + (a.a * a.j * a.o * a.h) + (a.a * a.n * a.g * a.l) - (a.a * a.n * a.k * a.h) - (a.a * a.j * a.g * a.p) - (a.a * a.f * a.o * a.l) - (a.e * a.b * a.k * a.p) - (a.e * a.j * a.o * a.d) - (a.e * a.n * a.c * a.l) + (a.e * a.n * a.k * a.d) + (a.e * a.j * a.c * a.p) + (a.e * a.b * a.o * a.l) + (a.i * a.b * a.g * a.p) + (a.i * a.f * a.o * a.d) + (a.i * a.n * a.c * a.h) - (a.i * a.n * a.g * a.d) - (a.i * a.f * a.c * a.p) - (a.i * a.b * a.o * a.h) - (a.m * a.b * a.g * a.l) - (a.m * a.f * a.k * a.d) - (a.m * a.j * a.c * a.h) + (a.m * a.j * a.g * a.d) + (a.m * a.f * a.c * a.l) + (a.m * a.b * a.k * a.h));
}

template <class T> inline shMath :: mat4<T> shMath :: adj(const shMath :: mat4<T>& a)
{
 return shMath :: mat4<T>((a.f * a.k * a.p) + (a.j * a.o * a.h) + (a.n * a.g * a.l) - (a.n * a.k * a.h) - (a.j * a.g * a.p) - (a.f * a.o * a.l), (a.n * a.k * a.d) + (a.j * a.c * a.p) + (a.b * a.o * a.l) - (a.b * a.k * a.p) - (a.j * a.o * a.d) - (a.n * a.c * a.l), (/*a.b * a.f * a.g*/a.b * a.g * a.p) + (a.f * a.o * a.d) + (a.n * a.c * a.h) - (a.n * a.g * a.d) - (a.f * a.c * a.p) - (a.b * a.o * a.h), (a.j * a.g * a.d) + (a.f * a.c * a.l) + (a.b * a.k * a.h) - (a.b * a.g * a.l) - (a.f * a.k * a.d) - (a.j * a.c * a.h), (a.m * a.k * a.h) + (a.i * a.g * a.p) + (a.e * a.o * a.l) - (a.e * a.k * a.p) - (a.i * a.o * a.h) - (a.m * a.g * a.l), (a.a * a.k * a.p) + (a.i * a.o * a.d) + (a.m * a.c * a.l) - (a.m * a.k * a.d) - (a.i * a.c * a.p) - (a.a * a.o * a.l), (a.m * a.g * a.d) + (a.e * a.c * a.p) + (a.a * a.o * a.h) - (a.a * a.g * a.p) - (a.e * a.o * a.d) - (a.m * a.c * a.h), (a.a * a.g * a.l) + (a.e * a.k * a.d) + (a.i * a.c * a.h) - (a.i * a.g * a.d) - (a.e * a.c * a.l) - (a.a * a.k * a.h), (a.e * a.j * a.p) + (a.i * a.n * a.h) + (a.m * a.f * a.l) - (a.m * a.j * a.h) - (a.i * a.f * a.p) - (a.e * a.n * a.l), (a.m * a.j * a.d) + (a.i * a.b * a.p) + (a.a * a.n * a.l) - (a.a * a.j * a.p) - (a.i * a.n * a.d) - (a.m * a.b * a.l), (a.a * a.f * a.p) + (a.e * a.n * a.d) + (a.m * a.b * a.h) - (a.m * a.f * a.d) - (a.e * a.b * a.p) - (a.a * a.n * a.h), (a.i * a.f * a.d) + (a.e * a.b * a.l) + (a.a * a.j * a.h) - (a.a * a.f * a.l) - (a.e * a.j * a.d) - (a.i * a.b * a.h), (a.m * a.j * a.g) + (a.i * a.f * a.o) + (a.e * a.n * a.k) - (a.e * a.j * a.o) - (a.i * a.n * a.g) - (a.m * a.f * a.k), (a.a * a.j * a.o) + (a.i * a.n * a.c) + (a.m * a.b * a.k) - (a.m * a.j * a.c) - (a.i * a.b * a.o) - (a.a * a.n * a.k), (a.m * a.f * a.c) + (a.e * a.b * a.o) + (a.a * a.n * a.g) - (a.a * a.f * a.o) - (a.e * a.n * a.c) - (a.m * a.b * a.g), (a.a * a.f * a.k) + (a.e * a.j * a.c) + (a.i * a.b * a.g) - (a.i * a.f * a.c) - (a.e * a.b * a.k) - (a.a * a.j * a.g));
}

template <class T> inline shMath :: mat4<T> shMath :: transpose(const shMath :: mat4<T>& a)
{
 return shMath :: mat4<T>(a.a, a.e, a.i, a.m, a.b, a.f, a.j, a.n, a.c, a.g, a.k, a.o, a.d, a.h, a.l, a.p);
}

template <class T> inline shMath :: vec4<T> shMath :: operator * (const shMath :: vec4<T>& a, const shMath :: mat4<T>& b)
{
 return shMath :: vec4<T>((a.x() * b.a) + (a.y() * b.b) + (a.z() * b.c) + (a.w() * b.d), (a.x() * b.e) + (a.y() * b.f) + (a.z() * b.g) + (a.w() * b.h), (a.x() * b.i) + (a.y() * b.j) + (a.z() * b.k) + (a.w() * b.l), (a.x() * b.m) + (a.y() * b.n) + (a.z() * b.o) + (a.w() * b.p));
}

template <class T> inline shMath :: mat4<T> shMath :: operator * (float a, const shMath :: mat4<T>& b)
{
 return shMath :: mat4<T>(a * b.a, a * b.b, a * b.c, a * b.d, a * b.e, a * b.f, a * b.g, a * b.h, a * b.i, a * b.j, a * b.k, a * b.l, a * b.m, a * b.n, a * b.o, a * b.p);
}

template <class T> inline shMath :: mat4<T> shMath :: operator * (double a, const shMath :: mat4<T>& b)
{
 return shMath :: mat4<T>(a * b.a, a * b.b, a * b.c, a * b.d, a * b.e, a * b.f, a * b.g, a * b.h, a * b.i, a * b.j, a * b.k, a * b.l, a * b.m, a * b.n, a * b.o, a * b.p);
}

template <class T> inline shMath :: mat4<T> shMath :: operator * (long double a, const shMath :: mat4<T>& b)
{
 return shMath :: mat4<T>(a * b.a, a * b.b, a * b.c, a * b.d, a * b.e, a * b.f, a * b.g, a * b.h, a * b.i, a * b.j, a * b.k, a * b.l, a * b.m, a * b.n, a * b.o, a * b.p);
}

template <class T> inline shMath :: vec4<T> shMath :: operator / (const shMath :: vec4<T>& a, const shMath :: mat4<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: mat4<T> shMath :: operator / (float a, const shMath :: mat4<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: mat4<T> shMath :: operator / (double a, const shMath :: mat4<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: mat4<T> shMath :: operator / (long double a, const shMath :: mat4<T>& b)
{
 return (a * shMath :: inverse(b));
}

template <class T> inline shMath :: vec4<T>& shMath :: operator *= (shMath :: vec4<T>& a, const shMath :: mat4<T>& b)
{
 return (a = (a * b));
}

template <class T> inline shMath :: vec4<T>& shMath :: operator /= (shMath :: vec4<T>& a, const shMath :: mat4<T>& b)
{
 return (a = (a * shMath :: inverse(b)));
}
#endif
