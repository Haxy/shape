#ifndef SH_MATH_PAIR
#define SH_MATH_PAIR

namespace shMath
{
 template <class X, class Y> class pair
 {
  public:
  pair(void);
  pair(const X& aa, const Y& bb);
  pair(const pair& aa);
  ~pair(void);
  pair& operator = (const pair& aa);
  const X& getA() const;
  const Y& getB() const;
  X& getA();
  Y& getB();
  private:
  X a;
  Y b;
 };
}

template <class X, class Y> inline shMath :: pair <X,Y> :: pair() : a(X()), b(Y())
{
}

template <class X, class Y> inline shMath :: pair <X,Y> :: pair(const X& aa, const Y& bb) : a(aa), b(bb)
{
}

template <class X, class Y> inline shMath :: pair <X,Y> :: pair(const shMath :: pair<X,Y>& aa) : a(aa.a), b(aa.b)
{
}

template <class X, class Y> inline shMath :: pair <X,Y> :: ~pair()
{
}

template <class X, class Y> inline shMath :: pair <X,Y>& shMath :: pair <X,Y> :: operator = (const shMath :: pair<X,Y>& aa)
{
 a = aa.a;
 b = aa.b;
 return *this;
}

template <class X, class Y> inline const X& shMath :: pair <X,Y> :: getA() const
{
 return a;
}

template <class X, class Y> inline const Y& shMath :: pair <X,Y> :: getB() const
{
 return b;
}

template <class X, class Y> inline X& shMath :: pair <X,Y> :: getA()
{
 return a;
}

template <class X, class Y> inline Y& shMath :: pair <X,Y> :: getB()
{
 return b;
}
#endif

