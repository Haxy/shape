#ifndef SH_MATH_FUNCTIONS
#define SH_MATH_FUNCTIONS

#include "ops.h"
#include "pair.h"
#include <math.h>

namespace shMath
{
 extern unsigned int ITERATION_NUM;
 template <class T> T ln(const T& a);
 template <class T> T exp(const T& a);
 template <class T> T sin(const T& a);
 template <class T> T cos(const T& a);
 template <class T> T tan(const T& a);
 template <class T> T cot(const T& a);
 template <class T> T sec(const T& a);
 template <class T> T csc(const T& a);
 template <class T> T sqrt(const T& a);
 template <class T> T asin(const T& a);
 template <class T> T acos(const T& a);
 template <class T> T atan(const T& a);
 template <class T> T acot(const T& a);
 template <class T> T asec(const T& a);
 template <class T> T acsc(const T& a);
 template <class T> T sinh(const T& a);
 template <class T> T cosh(const T& a);
 template <class T> T tanh(const T& a);
 template <class T> T coth(const T& a);
 template <class T> T sech(const T& a);
 template <class T> T csch(const T& a);
 template <class T> T arsinh(const T& a);
 template <class T> T arcosh(const T& a);
 template <class T> T artanh(const T& a);
 template <class T> T arcoth(const T& a);
 template <class T> T arsech(const T& a);
 template <class T> T arcsch(const T& a);
 template <class T> T pow(const T& a,const T& b);
 template <class T> pair<T,T> sincos(const T& a);
}

template <class T> inline T shMath :: ln(const T& a)
{
 const T z = (a - shMath :: identity(a));
 const T I = shMath :: identity(z);
 T P0 = shMath :: zero(z);
 T Q0 = I;
 T P2 = z;
 T Q2 = I + (0.5l * z);
 T Ak;
 T Bk;
 T tp;
 T tq;
 long double t;
 unsigned int i = 4u;
 for(;i < (shMath :: ITERATION_NUM << 1u);i += 2u)
 {
  Ak = I + (0.5l * z);
  t = (long double)(i - 2u) * (long double)(i - 2u);
  Bk = (z * z) / 16.0l * t / (1.0l - t);
  tp = (Ak * P2) + (Bk * P0);
  tq = (Ak * Q2) + (Bk * Q0);
  P0 = P2;
  Q0 = Q2;
  P2 = tp;
  Q2 = tq;
 }
 return (P2 / Q2);
}

template <class T> inline T shMath :: exp(const T& a)
{
 unsigned int i = 1u;
 long double temp = 1.0l;
 T ans = shMath :: identity(a);
 T tempa = ans;
 for(;i < shMath :: ITERATION_NUM;++i)
 {
  temp /= (double)(i);
  tempa *= a;
  ans += (T)(temp * tempa);
 }
 return ans;
}

template <class T> inline T shMath :: sin(const T& a)
{ 
 unsigned int i = 1u;
 unsigned int j = 1u;
 long double temp = 1.0l;
 T ans = a;
 const T aa = (a * a);
 T tempa = (aa * a);
 for(;i <= shMath :: ITERATION_NUM;++i)
 {
  temp /= ((long double)(++j));
  temp /= ((long double)(++j));
  temp = -(temp);
  ans += ((T)(temp * tempa));
  tempa *= aa;
 }
 return ans;
}

template <class T> inline T shMath :: cos(const T& a)
{
 unsigned int i = 1u;
 unsigned int j = 0u;
 long double temp = 1.0l;
 T ans = shMath :: identity(a);
 T tempa = ans;
 const T aa = (a * a);
 for(;i <= shMath :: ITERATION_NUM;++i)
 {
  temp /= ((long double)(++j));
  temp /= ((long double)(++j));
  temp = -(temp);
  tempa *= aa;
  ans += ((T)(temp * tempa));
 }
 return ans;
}

template <class T> inline T shMath :: tan(const T& a)
{
 return ((shMath :: sin(a)) / (shMath :: cos(a)));
}

template <class T> inline T shMath :: cot(const T& a)
{
 return ((shMath :: cos(a)) / (shMath :: sin(a)));
}

template <class T> inline T shMath :: sec(const T& a)
{
 return shMath :: inverse(shMath :: cos(a));
}

template <class T> inline T shMath :: csc(const T& a)
{
 return shMath :: inverse(shMath :: sin(a));
}

namespace shMath
{
 template <> inline float sqrt(const float& a)
 {
  unsigned int i = 0u;
  float temp = a;
  for(;i < shMath :: ITERATION_NUM;++i)
  {
   temp = temp - (((temp * temp) - a) / (2.0f * temp));
  }
  return temp;
 }
}

template <class T> inline T shMath :: sqrt(const T& a)
{
 unsigned int i = 0u;
 T temp = a;
 T temp2 = temp;
 for(;i < shMath :: ITERATION_NUM;++i)
 {
  temp = temp2 = temp - (((temp * temp) - a) / (2.0l * temp));
 }
 return temp2;
}

template <class T> inline T shMath :: asin(const T& a)
{
 unsigned long int i = 1u;
 unsigned long int j;
 T ans = a;
 T sum = a;
 const T temp = (a * a);
 long double temp2 = 1.0l;
 long double temp3 = 1.0l;
 long double temp4 = 2.0l;
 for(;i <= shMath :: ITERATION_NUM;++i)
 {
  sum *= temp;
  for(temp2 = 1.0l,j = i;j < (2u * i);temp2 *= (long double)(++j));
  temp3 *= (long double)(i);
  temp4 = :: pow(2.0l,(long double)(2u * i));
  ans += (T)(((temp2 / (temp4 * temp3)) * (1.0l / ((2.0l * (long double)(i)) + 1.0l))) * sum);
 }
 return ans;
}

template <class T> inline T shMath :: acos(const T& a)
{
 return (a != shMath :: zero(a)) ? (shMath :: asin(a / (shMath :: tan(shMath :: asin(a))))) : shMath :: asin(shMath :: identity(a));
}

template <class T> inline T shMath :: atan(const T& a)
{
 unsigned int i = 1u;
 T sum = a;
 T ans = a;
 const T temp = (a * a);
 for(;i <= shMath :: ITERATION_NUM;++i)
 {
  sum *= temp;
  ans += ((i % 2u) == 1u) ? ((T)(-1.0l / ((2.0l * (long double)(i)) + 1.0l)) * sum) : ((T)(1.0l / ((2.0l * (long double)(i)) + 1.0l)) * sum);
 }
 return ans;
}

template <class T> inline T shMath :: acot(const T& a)
{
 return shMath :: atan(shMath :: inverse(a));
}

template <class T> inline T shMath :: asec(const T& a)
{
 return shMath :: acos(shMath :: inverse(a));
}

template <class T> inline T shMath :: acsc(const T& a)
{
 return shMath :: acsc(shMath :: inverse(a));
}

template <class T> inline T shMath :: sinh(const T& a)
{
 return (T)(0.5l * (shMath :: exp(a) - shMath :: exp(-a)));
}

template <class T> inline T shMath :: cosh(const T& a)
{
 return (T)(0.5l * (shMath :: exp(a) + shMath :: exp(-a)));
}

template <class T> inline T shMath :: tanh(const T& a)
{
 const T ex1 = shMath :: exp(a);
 const T ex2 = shMath :: exp(-a);
 return ((ex1 - ex2) / (ex1 + ex2));
}

template <class T> inline T shMath :: coth(const T& a)
{
 const T ex1 = shMath :: exp(a);
 const T ex2 = shMath :: exp(-a);
 return ((ex1 + ex2) / (ex1 - ex2));
}

template <class T> inline T shMath :: sech(const T& a)
{
 return (T)(2.0l * shMath :: inverse(shMath :: exp(a) + shMath :: exp(-a)));
}

template <class T> inline T shMath :: csch(const T& a)
{
 return (T)(2.0l * shMath :: inverse(shMath :: exp(a) - shMath :: exp(-a)));
}

template <class T> inline T shMath :: arsinh(const T& a)
{
 unsigned long int i = 1u;
 unsigned long int j;
 T ans = a;
 T sum = a;
 const T temp = (a * a);
 long double temp2 = 1.0l;
 long double temp3 = 1.0l;
 long double temp4 = 2.0l;
 for(;i <= shMath :: ITERATION_NUM;++i)
 {
  sum *= temp;
  for(temp2 = 1.0l,j = i;j < (2u * i);temp2 *= (long double)(++j));
  temp3 *= (long double)(i);
  temp4 = :: pow(2.0l,(long double)(2u * i));
  ans += ((i % 2u) == 1u) ? ((T)(((temp2 / (temp4 * temp3)) * (-1.0l / ((2.0l * (long double)(i)) + 1.0l))) * sum)) : ((T)(((temp2 / (temp4 * temp3)) * (1.0l / ((2.0l * (long double)(i)) + 1.0l))) * sum));
 }
 return ans;
}

template <class T> inline T shMath :: arcosh(const T& a)
{
 unsigned long int i = 1u;
 unsigned long int j;
 T ans = shMath :: ln(2.0l * a);
 T sum = shMath :: identity(a);
 const T temp = shMath :: inverse(a * a);
 long double temp2 = 1.0l;
 long double temp3 = 1.0l;
 long double temp4 = 2.0l;
 for(;i <= shMath :: ITERATION_NUM;++i)
 {
  sum *= temp;
  for(temp2 = 1.0l,j = i;j < (2u * i);temp2 *= (long double)(++j));
  temp3 *= (long double)(i);
  temp4 = :: pow(2.0l,(long double)(2u * i));
  ans += ((i % 2u) == 1u) ? (T)(((temp2 / (temp4 * temp3)) * (-1.0l / (2.0l * (long double)(i)))) * sum) : (T)(((temp2 / (temp4 * temp3)) * (1.0l / (2.0l * (long double)(i)))) * sum);
 }
 return ans;
}

template <class T> inline T shMath :: artanh(const T& a)
{
 unsigned int i = 1u;
 T sum = a;
 T ans = a;
 const T temp = (a * a);
 for(;i <= shMath :: ITERATION_NUM;++i)
 {
  sum *= temp;
  ans += (1.0l / ((2.0l * (long double)(i)) + 1.0l)) * sum;
 }
 return ans;
}

template <class T> inline T shMath :: arcoth(const T& a)
{
 return shMath :: artanh(shMath :: inverse(a));
}

template <class T> inline T shMath :: arsech(const T& a)
{
 return shMath :: arcosh(shMath :: inverse(a));
}

template <class T> inline T shMath :: arcsch(const T& a)
{
 return shMath :: arsinh(shMath :: inverse(a));
}

namespace shMath
{
 template <> inline shMath :: pair<float,float> sincos(const float& a)
 {
  shMath :: pair<float,float> ans(a,shMath :: identity(a));
  unsigned int i = 0u;
  unsigned int j = 1u;
  float temp = 1.0f;
  float tempa = a;
  const float ma = -a;
  for(;i < shMath :: ITERATION_NUM;++i)
  {
   temp /= (float)(++j);
   tempa *= ma;
   ans.getB() += (tempa * temp);
   temp /= (float)(++j);
   tempa *= a;
   ans.getA() += (tempa * temp);
  }
  return ans;
 }
}

template <class T> inline shMath :: pair<T,T> shMath :: sincos(const T& a)
{
 shMath :: pair<T,T> ans(a,shMath :: identity(a));
 unsigned int i = 0u;
 unsigned int j = 1u;
 long double temp = 1.0l;
 T tempa = a;
 const T ma = -a;
 for(;i < shMath :: ITERATION_NUM;++i)
 {
  temp /= (long double)(++j);
  tempa *= ma;
  ans.getB() += (tempa * temp);
  temp /= (long double)(++j);
  tempa *= a;
  ans.getA() += (tempa * temp);
 }
 return ans;
}
#endif
