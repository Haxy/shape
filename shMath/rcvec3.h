#ifndef SH_MATH_RCVEC3
#define SH_MATH_RCVEC3

#include <new>
#include <stdlib.h>

#include "ops.h"
#include "rcvec2.h"
#include "functions.h"

namespace shMath
{
 template <class T> class rcvec3
 {
  public:
  rcvec3(void);
  rcvec3(const rcvec3& a);
  rcvec3(const T& x, const rcvec2<T>& yz);
  rcvec3(const rcvec2<T>& xy, const T& z = T());
  rcvec3(const T& x, const T& y = T(), const T& z = T());
  ~rcvec3(void);
  rcvec3& operator = (const rcvec3& a);
  rcvec3 operator - () const;
  operator const T* () const;
  operator T* ();
  operator const rcvec2<T> () const;
  operator rcvec2<T> ();
  void normalise();
  const T& x() const;
  const T& y() const;
  const T& z() const;
  T& x();
  T& y();
  T& z();
  private:
  void kill();
  void copy();
  T* data;
  unsigned int* counter;
 };

 template <class T> rcvec3<T> zero(const rcvec3<T>& a);

 template <class T> T slength(const rcvec3<T>& a);
 template <class T> T length(const rcvec3<T>& a);
 template <class T> rcvec3<T> normalise(const rcvec3<T>& a);

 template <class T> T dot(const rcvec3<T>& a, const rcvec3<T>& b);
 template <class T> rcvec3<T> cross(const rcvec3<T>& a, const rcvec3<T>& b);
 
 template <class T> rcvec3<T> operator + (const rcvec3<T>& a, const rcvec3<T>& b);
 template <class T> rcvec3<T> operator - (const rcvec3<T>& a, const rcvec3<T>& b);
 template <class T> rcvec3<T> operator * (const rcvec3<T>& a, const T& b);
 template <class T> rcvec3<T> operator / (const rcvec3<T>& a, const T& b);
 template <class T> rcvec3<T> operator * (float a, const rcvec3<T>& b);
 template <class T> rcvec3<T> operator * (double a, const rcvec3<T>& b);
 template <class T> rcvec3<T> operator * (long double a, const rcvec3<T>& b);
 template <class T> rcvec3<T>& operator += (rcvec3<T>& a, const rcvec3<T>& b);
 template <class T> rcvec3<T>& operator -= (rcvec3<T>& a, const rcvec3<T>& b);
 template <class T> rcvec3<T>& operator *= (rcvec3<T>& a, const T& b);
 template <class T> rcvec3<T>& operator /= (rcvec3<T>& a, const T& b);
}

template <class T> inline shMath :: rcvec3 <T> :: rcvec3() : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  data = new T [3u] ();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> shMath :: rcvec3 <T> :: rcvec3(const T& x, const shMath :: rcvec2<T>& yz) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  const T* temp2 = yz;
  T* temp = data = new T [3u] ();
  (*temp) = x;
  (*(++temp)) = (*temp2);
  (*(++temp)) = (*(++temp2));
 }
 catch(...)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
  throw;
 }
}

template <class T> shMath :: rcvec3 <T> :: rcvec3(const shMath :: rcvec2<T>& xy, const T& z) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  const T* temp2 = xy;
  T* temp = data = new T [3u] ();
  (*temp) = (*temp2);
  (*(++temp)) = (*(++temp2));
  (*(++temp)) = z;
 }
 catch(...)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
  throw;
 }
}

template <class T> shMath :: rcvec3 <T> :: rcvec3(const T& x, const T& y, const T& z) : data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  T* temp = data = new T [3u] ();
  (*temp) = x;
  (*(++temp)) = y;
  (*(++temp)) = z;
 }
 catch(...)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
  throw;
 }
}

template <class T> inline shMath :: rcvec3 <T> :: ~rcvec3()
{
 kill();
}

template <class T> inline shMath :: rcvec3<T>& shMath :: rcvec3 <T> :: operator = (const shMath :: rcvec3<T>& a)
{
 if (&a == this)
 {
  kill();
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline shMath :: rcvec3<T> shMath :: rcvec3 <T> :: operator - () const
{
 const T* temp = data;
 const T x = -(*temp);
 const T y = -(*(++temp));
 const T z = -(*(++temp));
 return shMath :: rcvec3<T>(x,y,z);
}

template <class T> inline shMath :: rcvec3 <T> :: operator const T* () const
{
 return data;
}

template <class T> inline shMath :: rcvec3 <T> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T> inline shMath :: rcvec3 <T> :: operator const shMath :: rcvec2<T> () const
{
 return (*(const shMath :: rcvec2<T>*)(this));
}

template <class T> inline shMath :: rcvec3 <T> :: operator shMath :: rcvec2<T> ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*(shMath :: rcvec2<T>*)(this));
}

template <class T> inline void shMath :: rcvec3 <T> :: normalise()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 T* temp = data;
 const T len = shMath :: inverse(shMath :: length(*this));
 (*temp) *= len;
 (*(++temp)) *= len;
 (*(++temp)) *= len;
}

template <class T> inline const T& shMath :: rcvec3 <T> :: x() const
{
 return (*data);
}

template <class T> inline const T& shMath :: rcvec3 <T> :: y() const
{
 return (*(data + 1u));
}

template <class T> inline const T& shMath :: rcvec3 <T> :: z() const
{
 return (*(data + 2u));
}

template <class T> inline T& shMath :: rcvec3 <T> :: x()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*data);
}

template <class T> inline T& shMath :: rcvec3 <T> :: y()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*(data + 1u));
}

template <class T> inline T& shMath :: rcvec3 <T> :: z()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (*(data + 2u));
}

template <class T> inline void shMath :: rcvec3 <T> :: kill()
{
 if ((*counter) == 0u)
 {
  delete [] data;
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> void shMath :: rcvec3 <T> :: copy()
{
 T* temp = NULL;
 unsigned int* tempcount;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  T* temp3 = temp = new T [3u] ();
  const T* temp2 = data;
  (*temp3) = (*temp2);
  (*(++temp3)) = (*(++temp2));
  (*(++temp3)) = (*(++temp2));
 }
 catch(...)
 {
  if (temp != NULL)
  {
   delete [] temp;
  }
  free(tempcount);
  throw;
 }
 data = temp;
 --(*counter);
 counter = tempcount;
}

template <class T> inline shMath :: rcvec3<T> shMath :: zero(const shMath :: rcvec3<T>& a)
{
 const T* temp = a;
 const T x = shMath :: zero(*temp);
 const T y = shMath :: zero(*(++temp));
 const T z = shMath :: zero(*(++temp));
 return shMath :: rcvec3<T>(x,y,z);
}

template <class T> inline T shMath :: slength(const shMath :: rcvec3<T>& a)
{
 const T* temp = a;
 const T x = (*temp);
 const T y = (*(++temp));
 const T z = (*(++temp));
 return ((x * x) + (y * y) + (z * z));
}

template <class T> inline T shMath :: length(const shMath :: rcvec3<T>& a)
{
 return shMath :: sqrt(shMath :: slength(a));
}

template <class T> inline shMath :: rcvec3<T> shMath :: normalise(const shMath :: rcvec3<T>& a)
{
 const T* temp = a;
 const T x = (*temp);
 const T y = (*(++temp));
 const T z = (*(++temp));
 const T ilen = shMath :: inverse(shMath :: length(a));
 return shMath :: rcvec3<T>(x * ilen, y * ilen, z * ilen);
}

template <class T> inline T shMath :: dot(const shMath :: rcvec3<T>& a, const shMath :: rcvec3<T>& b)
{
 const T* temp = a;
 const T* temp2 = b;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T ya = (*(++temp));
 const T za = (*(++temp));
 const T yb = (*(++temp2));
 const T zb = (*(++temp2));
 return ((xa * xb) + (ya * yb) + (za * zb));
}

template <class T> inline shMath :: rcvec3<T> shMath :: cross(const shMath :: rcvec3<T>& a, const shMath :: rcvec3<T>& b)
{
 const T* temp = a;
 const T* temp2 = b;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T ya = (*(++temp));
 const T za = (*(++temp));
 const T yb = (*(++temp2));
 const T zb = (*(++temp2));
 return shMath :: rcvec3<T>((ya * zb) - (za * yb),(za * xb) - (xa * zb),(xa * yb) - (ya * xb));
}

template <class T> inline shMath :: rcvec3<T> shMath :: operator + (const shMath :: rcvec3<T>& a, const shMath :: rcvec3<T>& b)
{
 const T* temp = a;
 const T* temp2 = b;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T ya = (*(++temp));
 const T za = (*(++temp));
 const T yb = (*(++temp2));
 const T zb = (*(++temp2));
 return shMath :: rcvec3<T>((xa + xb),(ya + yb),(za + zb));
}

template <class T> inline shMath :: rcvec3<T> shMath :: operator - (const shMath :: rcvec3<T>& a, const shMath :: rcvec3<T>& b)
{
 const T* temp = a;
 const T* temp2 = b;
 const T xa = (*temp);
 const T xb = (*temp2);
 const T ya = (*(++temp));
 const T za = (*(++temp));
 const T yb = (*(++temp2));
 const T zb = (*(++temp2));
 return shMath :: rcvec3<T>((xa - xb),(ya - yb),(za - zb));
}

template <class T> inline shMath :: rcvec3<T> shMath :: operator * (const shMath :: rcvec3<T>& a, const T& b)
{
 const T* temp = a;
 const T x = (*temp) * b;
 const T y = (*(++temp)) * b;
 const T z = (*(++temp)) * b;
 return shMath :: rcvec3<T>(x,y,z);
}

template <class T> inline shMath :: rcvec3<T> shMath :: operator / (const shMath :: rcvec3<T>& a, const T& b)
{
 return a * shMath :: inverse(b);
}

template <class T> inline shMath :: rcvec3<T> shMath :: operator * (float a, const shMath :: rcvec3<T>& b)
{
 const T* temp = b;
 const T x = a * (*temp);
 const T y = a * (*(++temp));
 const T z = a * (*(++temp));
 return shMath :: rcvec3<T>(x,y,z);
}

template <class T> inline shMath :: rcvec3<T> shMath :: operator * (double a, const shMath :: rcvec3<T>& b)
{
 const T* temp = b;
 const T x = a * (*temp);
 const T y = a * (*(++temp));
 const T z = a * (*(++temp));
 return shMath :: rcvec3<T>(x,y,z);
}

template <class T> inline shMath :: rcvec3<T> shMath :: operator * (long double a, const shMath :: rcvec3<T>& b)
{
 const T* temp = b;
 const T x = a * (*temp);
 const T y = a * (*(++temp));
 const T z = a * (*(++temp));
 return shMath :: rcvec3<T>(x,y,z);
}

template <class T> inline shMath :: rcvec3<T>& shMath :: operator += (shMath :: rcvec3<T>& a, const shMath :: rcvec3<T>& b)
{
 T* temp = a;
 const T* temp2 = b;
 (*temp) += (*temp2);
 (*(++temp)) += (*(++temp2));
 (*(++temp)) += (*(++temp2));
 return a;
}

template <class T> inline shMath :: rcvec3<T>& shMath :: operator -= (shMath :: rcvec3<T>& a, const shMath :: rcvec3<T>& b)
{
 T* temp = a;
 const T* temp2 = b;
 (*temp) -= (*temp2);
 (*(++temp)) -= (*(++temp2));
 (*(++temp)) -= (*(++temp2));
 return a;
}

template <class T> inline shMath :: rcvec3<T>& shMath :: operator *= (shMath :: rcvec3<T>& a, const T& b)
{
 T* temp = a;
 (*temp) *= b;
 (*(++temp)) *= b;
 (*(++temp)) *= b;
 return a;
}

template <class T> inline shMath :: rcvec3<T>& shMath :: operator /= (shMath :: rcvec3<T>& a, const T& b)
{
 return a *= shMath :: inverse(b);
}
#endif

