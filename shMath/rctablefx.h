#ifndef SH_MATH_RCTABLEFX
#define SH_MATH_RCTABLEFX

#include <new>
#include <stdlib.h>

namespace shMath
{
 template <class T> class rctablefx
 {
  public:
  rctablefx(void);
  explicit rctablefx(unsigned int size);
  template <unsigned int n> rctablefx(const T (&in)[n]);
  rctablefx(const rctablefx& a);
  ~rctablefx(void);
  rctablefx& operator = (const rctablefx& a);
  const T& operator [] (unsigned int a) const;
  T& operator [] (unsigned int a);
  template <class X> operator const X* () const;
  template <class X> operator X* ();
  template <class X> operator const rctablefx<X> () const;
  template <class X> operator rctablefx<X> ();
  const T& operator * () const;
  T& operator * ();
  void resize(unsigned int size);
  unsigned int getSize() const;
  private:
  void kill();
  void copy();
  T* tab;
  unsigned int num;
  unsigned int* counter;
 };
}

template <class T> inline shMath :: rctablefx <T> :: rctablefx() : tab(NULL), num(0u), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> inline shMath :: rctablefx <T> :: rctablefx(unsigned int size) : tab(NULL), num(size), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  tab = ((size != 0u) ? new T [size] () : NULL);
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> template <unsigned int n> shMath :: rctablefx <T> :: rctablefx(const T (&in) [n]) : tab(NULL), num(n), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  tab = new T [num] ();
  const T* temp1 = in;
  T* temp2 = tab;
  unsigned int i = 0u;
  for(;i < num;++i,++temp1,++temp2)
  {
   (*temp2) = (*temp1);
  }
 }
 catch(...)
 {
  free(counter);
  if (tab != NULL)
  {
   delete [] tab;
  }
  throw;
 }
}

template <class T> inline shMath :: rctablefx <T> :: rctablefx(const shMath :: rctablefx<T>& a) : tab(a.tab), num(a.num), counter(a.counter)
{
 ++(*counter);
}

template <class T> inline shMath :: rctablefx <T> :: ~rctablefx()
{
 kill();
}

template <class T> inline shMath :: rctablefx<T>& shMath :: rctablefx <T> :: operator = (const shMath :: rctablefx<T>& a)
{
 if (&a != this)
 {
  kill();
  tab = a.tab;
  num = a.num;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline const T& shMath :: rctablefx <T> :: operator [] (unsigned int a) const
{
 return tab[a];
}

template <class T> inline T& shMath :: rctablefx <T> :: operator [] (unsigned int a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return tab[a];
}

template <class T> template <class X> inline shMath :: rctablefx <T> :: operator const X* () const
{
 return (const X*)tab;
}

template <class T> template <class X> inline shMath :: rctablefx <T> :: operator X* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (X*)tab;
}

template <class T> template <class X> inline shMath :: rctablefx <T> :: operator const shMath :: rctablefx<X> () const
{
 shMath :: rctablefx<T> ans = *this;
 ans.num = (num * sizeof(T)) / sizeof(X);
 return *((const shMath :: rctablefx<X>*)(&ans));
}

template <class T> template <class X> inline shMath :: rctablefx <T> :: operator shMath :: rctablefx<X> ()
{
 shMath :: rctablefx<T> ans = *this;
 ans.num = (num * sizeof(T)) / sizeof(X);
 return *((shMath :: rctablefx<X>*)(&ans));
}

template <class T> inline const T& shMath :: rctablefx <T> :: operator * () const
{
 return *tab;
}

template <class T> inline T& shMath :: rctablefx <T> :: operator * ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return *tab;
}

template <class T> void shMath :: rctablefx <T> :: resize(unsigned int size)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 T* temp = ((size != 0u) ? new T [size] () : NULL);
 unsigned int i = 0u;
 unsigned int* tempcount = NULL;
 try
 {
  for(;i < ((size < num) ? size : num);++i)
  {
   temp[i] = tab[i];
  }
  if ((*counter) != 0u)
  {
   if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
   {
    throw std :: bad_alloc();
   }
   (*tempcount) = 0u;
  }
 }
 catch(...)
 {
  delete [] temp;
  throw;
 }
 if (tempcount != NULL)
 {
  --(*counter);
  counter = tempcount;
 }
 if (tab != NULL)
 {
  delete [] tab;
 }
 tab = temp;
 num = size;
}

template <class T> inline unsigned int shMath :: rctablefx <T> :: getSize() const
{
 return num;
}

template <class T> inline void shMath :: rctablefx <T> :: kill()
{
 if ((*counter) == 0u)
 {
  free(counter);
  if (tab != NULL)
  {
   delete [] tab;
  }
 }
 else
 {
  --(*counter);
 }
}

template <class T> void shMath :: rctablefx <T> :: copy()
{
 T* temp = NULL;
 unsigned int i = 0u;
 unsigned int* tempcount;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  temp = new T [num] ();
  for(;i < num;++i)
  {
   temp[i] = tab[i];
  }
 }
 catch(...)
 {
  free(tempcount);
  if (temp != NULL)
  {
   delete [] temp;
  }
  throw;
 }
 --(*counter);
 counter = tempcount;
 tab = temp;
}
#endif
