#ifndef SH_MATH_VEC2
#define SH_MATH_VEC2

#include "ops.h"
#include "functions.h"

namespace shMath
{
 template <class T> class vec2
 {
  public:
  vec2();
  vec2(const T& xx, const T& yy = T());
  vec2 operator - () const;
  operator const T* () const;
  operator T* ();
  void normalise();
  const T& x() const;
  const T& y() const;
  T& x();
  T& y();
  private:
  T X;
  T Y;
 };

 template <class T> T slength(const vec2<T>& a);
 template <class T> T length(const vec2<T>& a);
 template <class T> vec2<T> normalise(const vec2<T>& a);

 template <class T> T dot (const vec2<T>& a, const vec2<T>& b);
 template <class T> vec2<T> cross(const vec2<T>& a);

 template <class T> vec2<T> operator + (const vec2<T>& a, const vec2<T>& b);
 template <class T> vec2<T> operator - (const vec2<T>& a, const vec2<T>& b);
 template <class T> vec2<T> operator * (const vec2<T>& a, const T& b);
 template <class T> vec2<T> operator / (const vec2<T>& a, const T& b);
 template <class T> vec2<T> operator * (float a, const vec2<T>& b);
 template <class T> vec2<T> operator * (double a, const vec2<T>& b);
 template <class T> vec2<T> operator * (long double a, const vec2<T>& b);
 template <class T> vec2<T>& operator += (vec2<T>& a, const vec2<T>& b);
 template <class T> vec2<T>& operator -= (vec2<T>& a, const vec2<T>& b);
 template <class T> vec2<T>& operator *= (vec2<T>& a, const T& b);
 template <class T> vec2<T>& operator /= (vec2<T>& a, const T& b);
}

template <class T> inline shMath :: vec2 <T> :: vec2() : X(T()), Y(T())
{
}

template <class T> inline shMath :: vec2 <T> :: vec2(const T& xx, const T& yy) : X(xx), Y(yy)
{
}

template <class T> inline shMath :: vec2<T> shMath :: vec2 <T> :: operator - () const
{
 return shMath :: vec2<T>(-X,-Y);
}

template <class T> inline shMath :: vec2 <T> :: operator const T* () const
{
 return (const T*)this;
}

template <class T> inline shMath :: vec2 <T> :: operator T* ()
{
 return (T*)this;
}

template <class T> inline void shMath :: vec2 <T> :: normalise()
{
 const T len = shMath :: inverse(shMath :: length(*this));
 X *= len;
 Y *= len;
}

template <class T> inline const T& shMath :: vec2 <T> :: x() const
{
 return X;
}

template <class T> inline const T& shMath :: vec2 <T> :: y() const
{
 return Y;
}

template <class T> inline T& shMath :: vec2 <T> :: x()
{
 return X;
}

template <class T> inline T& shMath :: vec2 <T> :: y()
{
 return Y;
}

template <class T> inline T shMath :: slength(const shMath :: vec2<T>& a)
{
 return ((a.x() * a.x()) + (a.y() * a.y()));
}

template <class T> inline T shMath :: length(const shMath :: vec2<T>& a)
{
 return shMath :: sqrt((a.x() * a.x()) + (a.y() * a.y()));
}

template <class T> inline shMath :: vec2<T> shMath :: normalise(const shMath :: vec2<T>& a)
{
 return a / shMath :: length(a);
}

template <class T> inline T shMath :: dot(const shMath :: vec2<T>& a, const shMath :: vec2<T>& b)
{
 return ((a.x() * b.x()) + (a.y() * b.y()));
}

template <class T> inline shMath :: vec2<T> shMath :: cross(const shMath :: vec2<T>& a)
{
 return shMath :: vec2<T>(a.y(),-a.x());
}

template <class T> inline shMath :: vec2<T> shMath :: operator + (const shMath :: vec2<T>& a, const shMath :: vec2<T>& b)
{
 return shMath :: vec2<T>(a.x() + b.x(), a.y() + b.y());
}

template <class T> inline shMath :: vec2<T> shMath :: operator - (const shMath :: vec2<T>& a, const shMath :: vec2<T>& b)
{
 return shMath :: vec2<T>(a.x() - b.x(), a.y() - b.y());
}

template <class T> inline shMath :: vec2<T> shMath :: operator * (const shMath :: vec2<T>& a, const T& b)
{
 return shMath :: vec2<T>(a.x() * b, a.y() * b);
}

template <class T> inline shMath :: vec2<T> shMath :: operator / (const shMath :: vec2<T>& a, const T& b)
{
 const T bb = shMath :: inverse(b);
 return shMath :: vec2<T>(a.x() * bb, a.y() * bb);
}

template <class T> inline shMath :: vec2<T> shMath :: operator * (float a, const shMath :: vec2<T>& b)
{
 return shMath :: vec2<T>(a * b.x(), a * b.y());
}

template <class T> inline shMath :: vec2<T> shMath :: operator * (double a, const shMath :: vec2<T>& b)
{
 return shMath :: vec2<T>(a * b.x(), a * b.y());
}

template <class T> inline shMath :: vec2<T> shMath :: operator * (long double a, const shMath :: vec2<T>& b)
{
 return shMath :: vec2<T>(a * b.x(), a * b.y());
}

template <class T> inline shMath :: vec2<T>& shMath :: operator += (shMath :: vec2<T>& a, const shMath :: vec2<T>& b)
{
 a.x() += b.x();
 a.y() += b.y();
 return a;
}

template <class T> inline shMath :: vec2<T>& shMath :: operator -= (shMath :: vec2<T>& a, const shMath :: vec2<T>& b)
{
 a.x() -= b.x();
 a.y() -= b.y();
 return a;
}

template <class T> inline shMath :: vec2<T>& shMath :: operator *= (shMath :: vec2<T>& a, const T& b)
{
 a.x() *= b;
 a.y() *= b;
 return a;
}

template <class T> inline shMath :: vec2<T>& shMath :: operator /= (shMath :: vec2<T>& a, const T& b)
{
 const T bb = shMath :: inverse(b);
 a.x() *= bb;
 a.y() *= bb;
 return a;
}
#endif
