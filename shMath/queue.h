#ifndef SH_MATH_QUEUE
#define SH_MATH_QUEUE

namespace shMath
{
 template <class T> class queue
 {
  public:
  queue(void);
  queue(const queue& a);
  ~queue(void);
  queue& operator = (const queue& a);
  void push(const T& a);
  T pop();
  unsigned int getSize() const;
  private:
  class nod
  {
   public:
   nod(const T& t);
   ~nod();
   nod* next;
   T cont;
  };
  nod* top;
  nod* down;
  unsigned int num;
 };
}

template <class T> inline shMath :: queue <T> :: queue() : top(NULL), down(NULL), num(0u)
{
}

template <class T> shMath :: queue <T> :: queue(const shMath :: queue<T>& a) : top(NULL), down(NULL), num(a.num)
{
 if (a.getSize() != 0u)
 {
  unsigned int i;
  nod* temp = NULL;
  nod* temp2;
  nod* atemp;
  top = new nod(a.top->cont);
  atemp = a.top->next;
  temp2 = top;
  temp = top;
  try
  {
   for(i = 1u;i < a.num;++i)
   {
    temp2 = new nod(atemp->cont);
    temp->next = temp2;
    temp = temp2;
    atemp = atemp->next;
   }
  }
  catch(...)
  {
   delete top;
   throw;
  }
  down = temp2;
 }
}

template <class T> inline shMath :: queue <T> :: ~queue()
{
 if (top != NULL)
 {
  delete top;
 }
}

template <class T> shMath :: queue<T>& shMath :: queue <T> :: operator = (const shMath :: queue<T>& a)
{
 nod* temptop;
 if (a.getSize() != 0u)
 {
  unsigned int i;
  nod* temp = NULL;
  nod* temp2;
  nod* atemp;
  temptop = new nod(a.top->cont);
  atemp = a.top->next;
  temp2 = temptop;
  try
  {
   for(i = 1u;i < a.num;++i)
   {
    temp2 = new nod(atemp->cont);
    temp->next = temp2;
    temp = temp2;
    atemp = atemp->next;
   }
  }
  catch(...)
  {
   num = 0u;
   delete temptop;
   throw;
  }
  down = temp2;
  delete top;
  top = temptop;
  num = a.num;
 }
 else
 {
  delete top;
  top = NULL;
  down = NULL;
  num = 0u;
 }
 return *this;
}

template <class T> inline void shMath :: queue <T> :: push(const T& a)
{
 if (num == 0u)
 {
  top = new nod(a);
  down = top;
 }
 else
 {
  down->next = new nod(a);
  down = down->next;
 }
 ++num;
}

template <class T> inline T shMath :: queue <T> :: pop()
{
 if (num == 0u)
 {
  return T();
 }
 else
 {
  --num;
  nod* tempptr = top;
  T temp = top->cont;
  top = top->next;
  tempptr->next = NULL;
  delete tempptr;
  return temp;
 }
}

template <class T> inline unsigned int shMath :: queue <T> :: getSize() const
{
 return num;
}

template <class T> inline shMath :: queue <T> :: nod :: nod(const T& t) : next(NULL), cont(t)
{
}

template <class T> inline shMath :: queue <T> :: nod :: ~nod()
{
 if (next != NULL)
 {
  delete next;
 }
}

#endif
