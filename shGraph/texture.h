#ifndef SH_GRAPH_TEXTURE
#define SH_GRAPH_TEXTURE

#include "GLextensions.h"

#include <stdlib.h>
#include <new>

#include "../shMath/vec2.h"
#include "../shMath/vec3.h"
#include "../shMath/vec4.h"
#include "../shMath/stack.h"
#include "../shMath/rctablefx.h"
#include "image.h"
#include "pixel.h"
#include "texturedescr.h"

namespace shGraph
{
 class itexture
 {
  public:
  itexture();
  void bind() const;
  void unbind() const;
  void bind(unsigned int unit) const;
  void unbind(unsigned int unit) const;
  virtual unsigned int getParam() const;
  unsigned int getID() const;
  const itexturedescr& getDescr() const;
  itexturedescr& getDescr();
  static void unbindCurrent(unsigned int unit);
  static void unbindAll(unsigned int unit);
  static void unbindCurrent();
  static void unbindAll();
  static void init();
  static itexture& getCurrent();
  static itexture& getCurrent(unsigned int unit);
  static shMath :: rctablefx<itexture>& getCurrentTab();
  static shMath :: stack<itexture>& getStack();
  static shMath :: stack<itexture>& getStack(unsigned int unit);
  static shMath :: rctablefx<shMath :: stack<itexture> >& getStackTab();
  protected:
  virtual void setActive() const;
  unsigned int id;
  itexturedescr texdescr;
  static shMath :: rctablefx<itexture> current;
  static shMath :: rctablefx<shMath :: stack<itexture> > texstack;
  static unsigned int lastParam;
 };

 template <unsigned char dim = 2> class texture;

 template <> class texture <(unsigned char)(1)> : public itexture
 {
  public:
  texture(void);
  template <class X> texture(const image <X,(unsigned char)(1)>& a, const itexturedescr& descr = defaulttexturedescr<(unsigned char)(1)>());
  texture(unsigned int len, const itexturedescr& descr = defaulttexturedescr<(unsigned char)(1)>());
  texture(const texture& a);
  ~texture(void);
  texture& operator = (const texture& a);
  template <class X> operator const image<X, (unsigned char)(1)> () const;
  unsigned int getID() const;
  unsigned int getWidth() const;
  unsigned char getDim() const;
  void setSWrap(unsigned int wraps);
  void setMinFilter(unsigned int minfilter);
  void setMagFilter(unsigned int magfilter);
  void setEnvMode(unsigned int envmode);
  void setMinLOD(float minlod);
  void setMaxLOD(float maxlod);
  void setBaseLevel(unsigned int baselevel);
  void setMaxLevel(unsigned int maxlevel);
  template <class X> void setBorderColour(const pixel<X>& bordercolour);
  template <class X> void setEnvColour(const pixel<X>& envcolour);
  void genMipMap() const;
  unsigned int getParam() const;
  bool isValid() const;
  static void setSWrapForCurrent(unsigned int swrap);
  static void setMinFilterForCurrent(unsigned int minfilter);
  static void setMagFilterForCurrent(unsigned int magfilter);
  static void setEnvModeForCurrent(unsigned int envmode);
  static void setMinLODForCurrent(float minlod);
  static void setMaxLODForCurrent(float maxlod);
  static void setBaseLevelForCurrent(unsigned int baselevel);
  static void setMaxLevelForCurrent(unsigned int maxlevel);
  template <class X> static void setBorderColourForCurrent(const pixel<X>& bordercolour);
  template <class X> static void setEnvColourForCurrent(const pixel<X>& envcolour);
  static void genMipMapForCurrent();
  static void enable();
  static void disable();
  static bool isEnabled();
  protected:
  void setActive() const;
  private:
  void kill();
  unsigned int width;
  unsigned int* counter;
 };

 template <> class texture <(unsigned char)(2)> : public itexture
 {
  public:
  texture(void);
  template <class X> texture(const image<X>& a, const itexturedescr& descr = defaulttexturedescr<(unsigned char)(2)>());
  texture(unsigned int w, unsigned int h, const itexturedescr& descr = defaulttexturedescr<(unsigned char)(2)>());
  texture(const texture& a);
  ~texture(void);
  texture& operator = (const texture& a);
  template <class X> operator const image<X, (unsigned char)(2)> () const;
  unsigned int getID() const;
  unsigned int getWidth() const;
  unsigned int getHeight() const;
  unsigned char getDim() const;
  void setSWrap(unsigned int wraps);
  void setTWrap(unsigned int wrapt);
  void setMinFilter(unsigned int minfilter);
  void setMagFilter(unsigned int magfilter);
  void setEnvMode(unsigned int envmode);
  void setMinLOD(float minlod);
  void setMaxLOD(float maxlod);
  void setBaseLevel(unsigned int baselevel);
  void setMaxLevel(unsigned int maxlevel);
  template <class X> void setBorderColour(const pixel<X>& bordercolour);
  template <class X> void setEnvColour(const pixel<X>& envcolour);
  static void setSWrapForCurrent(unsigned int wraps);
  static void setTWrapForCurrent(unsigned int wrapt);
  static void setMinFilterForCurrent(unsigned int minfilter);
  static void setMagFilterForCurrent(unsigned int magfilter);
  static void setEnvModeForCurrent(unsigned int envmode);
  static void setMinLODForCurrent(float minlod);
  static void setMaxLODForCurrent(float maxlod);
  static void setBaseLevelForCurrent(unsigned int baselevel);
  static void setMaxLevelForCurrent(unsigned int maxlevel);
  template <class X> static void setBorderColourForCurrent(const pixel<X>& bordercolour);
  template <class X> static void setEnvColourForCurrent(const pixel<X>& envcolour);
  static void genMipMapForCurrent();
  void genMipMap() const;
  unsigned int getParam() const;
  bool isValid() const;
  static void enable();
  static void disable();
  static bool isEnabled();
  protected:
  void setActive() const;
  private:
  void kill();
  unsigned int width;
  unsigned int height;
  unsigned int* counter;
 };

 template <> class texture <(unsigned char)(3)> : public itexture
 {
  public:
  texture(void);
  template <class X> texture(const image <X,(unsigned char)(3)>& a, const itexturedescr& descr = defaulttexturedescr<(unsigned char)(1)>());
  texture(unsigned int w, unsigned int h, unsigned int d, const itexturedescr& = defaulttexturedescr<(unsigned char)(3)>());
  texture(const texture& a);
  ~texture(void);
  texture& operator = (const texture& a);
  template <class X> operator const image<X, (unsigned char)(3)> () const;
  unsigned int getID() const;
  unsigned int getWidth() const;
  unsigned int getHeight() const;
  unsigned int getDepth() const;
  unsigned char getDim() const;
  void setSWrap(unsigned int wraps);
  void setTWrap(unsigned int wrapt);
  void setRWrap(unsigned int wrapr);
  void setMinFilter(unsigned int minfilter);
  void setMagFilter(unsigned int magfilter);
  void setEnvMode(unsigned int envmode);
  void setMinLOD(float minlod);
  void setMaxLOD(float maxlod);
  void setBaseLevel(unsigned int baselevel);
  void setMaxLevel(unsigned int maxlevel);
  template <class X> void setBorderColour(const pixel<X>& bordercolour);
  template <class X> void setEnvColour(const pixel<X>& envcolour);
  static void setSWrapForCurrent(unsigned int wraps);
  static void setTWrapForCurrent(unsigned int wrapt);
  static void setRWrapForCurrent(unsigned int wrapr);
  static void setMinFilterForCurrent(unsigned int minfilter);
  static void setMagFilterForCurrent(unsigned int magfilter);
  static void setEnvModeForCurrent(unsigned int envmode);
  static void setMinLODForCurrent(float minlod);
  static void setMaxLODForCurrent(float maxlod);
  static void setBaseLevelForCurrent(unsigned int baselevel);
  static void setMaxLevelForCurrent(unsigned int maxlevel);
  template <class X> static void setBorderColourForCurrent(const pixel<X>& bordercolour);
  template <class X> static void setEnvColourForCurrent(const pixel<X>& envcolour);
  static void genMipMapForCurrent();
  void genMipMap() const;
  unsigned int getParam() const;
  bool isValid() const;
  static void enable();
  static void disable();
  static bool isEnabled();
  protected:
  void setActive() const;
  private:
  void kill();
  unsigned int width;
  unsigned int height;
  unsigned int depth;
  unsigned int* counter;
 };

 const unsigned int initTexStackSize = 2u;
 unsigned int getMaxTextures();
 unsigned int getMaxTextureSize();
 unsigned int getActiveTextureUnit();
 void setActiveTextureUnit(unsigned int num);
}

inline shGraph :: itexture :: itexture() : id(0u)
{
}

inline unsigned int shGraph :: itexture :: getParam() const
{
 return 0u;
}

inline unsigned int shGraph :: itexture :: getID() const
{
 return id;
}

inline shGraph :: itexturedescr& shGraph :: itexture :: getDescr()
{
 return texdescr;
}

inline const shGraph :: itexturedescr& shGraph :: itexture :: getDescr() const
{
 return texdescr;
}

inline void shGraph :: itexture :: init()
{
 const unsigned int max = shGraph :: getMaxTextures();
 current = shMath :: rctablefx<shGraph :: itexture>(max);
 texstack = shMath :: rctablefx<shMath :: stack<shGraph :: itexture> >(max);
}

inline shGraph :: itexture& shGraph :: itexture :: getCurrent()
{
 unsigned int unit = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&unit));
 return current[unit - GL_TEXTURE0_ARB];
}

inline shGraph :: itexture& shGraph :: itexture :: getCurrent(unsigned int unit)
{
 return current[unit];
}

inline shMath :: rctablefx<shGraph :: itexture>& shGraph :: itexture :: getCurrentTab()
{
 return current;
}

inline shMath :: stack<shGraph :: itexture>& shGraph :: itexture :: getStack()
{
 unsigned int unit = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&unit));
 return texstack[unit - GL_TEXTURE0_ARB];
}

inline shMath :: stack<shGraph :: itexture>& shGraph :: itexture :: getStack(unsigned int unit)
{
 return texstack[unit];
}

inline shMath :: rctablefx<shMath :: stack<shGraph :: itexture> >& shGraph :: itexture :: getStackTab()
{
 return texstack;
}

inline void shGraph :: itexture :: setActive() const
{
 if (lastParam != 0u)
 {
  glBindTexture(lastParam,0);
 }
}

inline shGraph :: texture <(unsigned char)(1)> :: texture() : width(0u), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 itexture :: id = 0u;
 itexture :: texdescr = shGraph :: itexturedescr();
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

inline shGraph :: texture <(unsigned char)(1)> :: texture(const shGraph :: texture<(unsigned char)(1)>& a) : width(a.width), counter(a.counter)
{
 itexture :: id = a.getID();
 itexture :: texdescr = a.getDescr();
 ++(*counter);
}

inline shGraph :: texture <(unsigned char)(1)> :: ~texture()
{
 kill();
}

inline unsigned int shGraph :: texture <(unsigned char)(1)> :: getID() const
{
 return id;
}

inline unsigned int shGraph :: texture <(unsigned char)(1)> :: getWidth() const
{
 return width;
}

inline unsigned char shGraph :: texture <(unsigned char)(1)> :: getDim() const
{
 return 1u;
}

template <class X> inline void shGraph :: texture <(unsigned char)(1)> :: setBorderColour(const shGraph :: pixel<X>& bordercolour)
{
 bind();
 setBorderColourForCurrent(bordercolour);
 unbindCurrent();
}

template <class X> inline void shGraph :: texture <(unsigned char)(1)> :: setEnvColour(const shGraph :: pixel<X>& envcolour)
{
 bind();
 setEnvColourForCurrent(envcolour);
 unbindCurrent();
}

inline void shGraph :: texture <(unsigned char)(1)> :: genMipMap() const
{
 bind();
 glGenerateMipmapEXT(GL_TEXTURE_1D);
 unbind();
}

inline unsigned int shGraph :: texture <(unsigned char)(1)> :: getParam() const
{
 return GL_TEXTURE_1D;
}

inline bool shGraph :: texture <(unsigned char)(1)> :: isValid() const
{
 return (id != 0u);
}

inline void shGraph :: texture <(unsigned char)(1)> :: setSWrapForCurrent(unsigned int swrap)
{
 glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,swrap);
}

inline void shGraph :: texture <(unsigned char)(1)> :: setMinFilterForCurrent(unsigned int minfilter)
{
 glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,minfilter);
}

inline void shGraph :: texture <(unsigned char)(1)> :: setMagFilterForCurrent(unsigned int magfilter)
{
 glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,magfilter);
}

inline void shGraph :: texture <(unsigned char)(1)> :: setEnvModeForCurrent(unsigned int envmode)
{
 glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,envmode);
}

inline void shGraph :: texture <(unsigned char)(1)> :: setMinLODForCurrent(float minlod)
{
 glTexParameterf(GL_TEXTURE_1D,GL_TEXTURE_MIN_LOD,minlod);
}

inline void shGraph :: texture <(unsigned char)(1)> :: setMaxLODForCurrent(float maxlod)
{
 glTexParameterf(GL_TEXTURE_1D,GL_TEXTURE_MAX_LOD,maxlod);
}

inline void shGraph :: texture <(unsigned char)(1)> :: setBaseLevelForCurrent(unsigned int baselevel)
{
 glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_BASE_LEVEL,baselevel);
}

inline void shGraph :: texture <(unsigned char)(1)> :: setMaxLevelForCurrent(unsigned int maxlevel)
{
 glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAX_LEVEL,maxlevel);
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(1)> :: setBorderColourForCurrent(const shGraph :: pixel<unsigned char>& bordercolour)
 {
  const static float umax = (float)(UCHAR_MAX);
  const float col [4u] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_1D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(1)> :: setBorderColourForCurrent(const shGraph :: pixel<unsigned short int>& bordercolour)
 {
  const static float umax = (float)(USHRT_MAX);
  const float col [4u] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_1D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(1)> :: setBorderColourForCurrent(const shGraph :: pixel<unsigned int>& bordercolour)
 {
  const static float umax = (float)(UINT_MAX);
  const float col [4u] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_1D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(1)> :: setBorderColourForCurrent(const shGraph :: pixel<float>& bordercolour)
 {
  glTexParameterfv(GL_TEXTURE_1D,GL_TEXTURE_BORDER_COLOR,(const float*)(&bordercolour));
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(1)> :: setBorderColourForCurrent(const shGraph :: pixel<double>& bordercolour)
 {
  const float col [4u] = {(float)(bordercolour.r),(float)(bordercolour.g),(float)(bordercolour.b),(float)(bordercolour.a)};
  glTexParameterfv(GL_TEXTURE_1D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(1)> :: setEnvColourForCurrent(const shGraph :: pixel<unsigned char>& envcolour)
 {
  const static float umax = (float)(UCHAR_MAX);
  const float col [4u] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(1)> :: setEnvColourForCurrent(const shGraph :: pixel<unsigned short int>& envcolour)
 {
  const static float umax = (float)(USHRT_MAX);
  const float col [4u] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(1)> :: setEnvColourForCurrent(const shGraph :: pixel<unsigned int>& envcolour)
 {
  const static float umax = (float)(UINT_MAX);
  const float col [4u] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(1)> :: setEnvColourForCurrent(const shGraph :: pixel<float>& envcolour)
 {
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,(const float*)(&envcolour));
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(1)> :: setEnvColourForCurrent(const shGraph :: pixel<double>& envcolour)
 {
  const float col [4u] = {(float)(envcolour.r),(float)(envcolour.g),(float)(envcolour.b),(float)(envcolour.a)};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

inline void shGraph :: texture <(unsigned char)(1)> :: genMipMapForCurrent()
{
 glGenerateMipmapEXT(GL_TEXTURE_1D);
}

inline void shGraph :: texture <(unsigned char)(1)> :: enable()
{
 glEnable(GL_TEXTURE_1D);
}

inline void shGraph :: texture <(unsigned char)(1)> :: disable()
{
 glDisable(GL_TEXTURE_1D);
}

inline bool shGraph :: texture <(unsigned char)(1)> :: isEnabled()
{
 return (glIsEnabled(GL_TEXTURE_1D) == GL_TRUE);
}

inline void shGraph :: texture <(unsigned char)(1)> :: setActive() const
{
 glBindTexture(GL_TEXTURE_1D,id);
}

inline shGraph :: texture <(unsigned char)(2)> :: texture() : width(0u), height(0u), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 itexture :: id = 0u;
 itexture :: texdescr = shGraph :: itexturedescr();
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

inline shGraph :: texture <(unsigned char)(2)> :: texture(const shGraph :: texture<2>& a) : width(a.width), height(a.height), counter(a.counter)
{
 itexture :: id = a.getID();
 itexture :: texdescr = a.getDescr();
 ++(*counter);
}

inline shGraph :: texture <(unsigned char)(2)> :: ~texture()
{
 kill();
}

inline unsigned int shGraph :: texture <(unsigned char)(2)> :: getID() const
{
 return id;
}

inline unsigned int shGraph :: texture <(unsigned char)(2)> :: getWidth() const
{
 return width;
}

inline unsigned int shGraph :: texture <(unsigned char)(2)> :: getHeight() const
{
 return height;
}

inline unsigned char shGraph :: texture <(unsigned char)(2)> :: getDim() const
{
 return 2u;
}

template <class X> inline void shGraph :: texture <(unsigned char)(2)> :: setBorderColour(const shGraph :: pixel<X>& bordercolour)
{
 bind();
 setBorderColourForCurrent(bordercolour);
 unbindCurrent();
}

template <class X> inline void shGraph :: texture <(unsigned char)(2)> :: setEnvColour(const shGraph :: pixel<X>& envcolour)
{
 bind();
 setEnvColourForCurrent(envcolour);
 unbindCurrent();
}

inline void shGraph :: texture <(unsigned char)(2)> :: setSWrapForCurrent(unsigned int wraps)
{
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wraps);
}

inline void shGraph :: texture <(unsigned char)(2)> :: setTWrapForCurrent(unsigned int wrapt)
{
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrapt);
}

inline void shGraph :: texture <(unsigned char)(2)> :: setMinFilterForCurrent(unsigned int minfilter)
{
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,minfilter);
}

inline void shGraph :: texture <(unsigned char)(2)> :: setMagFilterForCurrent(unsigned int magfilter)
{
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,magfilter);
}

inline void shGraph :: texture <(unsigned char)(2)> :: setEnvModeForCurrent(unsigned int envmode)
{
 glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,envmode);
}

inline void shGraph :: texture <(unsigned char)(2)> :: setMinLODForCurrent(float minlod)
{
 glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_LOD,minlod);
}

inline void shGraph :: texture <(unsigned char)(2)> :: setMaxLODForCurrent(float maxlod)
{
 glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAX_LOD,maxlod);
}

inline void shGraph :: texture <(unsigned char)(2)> :: setBaseLevelForCurrent(unsigned int baselevel)
{
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_BASE_LEVEL,baselevel);
}

inline void shGraph :: texture <(unsigned char)(2)> :: setMaxLevelForCurrent(unsigned int maxlevel)
{
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAX_LEVEL,maxlevel);
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(2)> :: setBorderColourForCurrent(const shGraph :: pixel<unsigned char>& bordercolour)
 {
  const static float umax = (float)(UCHAR_MAX);
  const float col [4u] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_2D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(2)> :: setBorderColourForCurrent(const shGraph :: pixel<unsigned short int>& bordercolour)
 {
  const static float umax = (float)(USHRT_MAX);
  const float col [4u] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_2D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(2)> :: setBorderColourForCurrent(const shGraph :: pixel<unsigned int>& bordercolour)
 {
  const static float umax = (float)(UINT_MAX);
  const float col [4u] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_2D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(2)> :: setBorderColourForCurrent(const shGraph :: pixel<float>& bordercolour)
 {
  glTexParameterfv(GL_TEXTURE_2D,GL_TEXTURE_BORDER_COLOR,(const float*)(&bordercolour));
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(2)> :: setBorderColourForCurrent(const shGraph :: pixel<double>& bordercolour)
 {
  const float col [4u] = {(float)(bordercolour.r),(float)(bordercolour.g),(float)(bordercolour.b),(float)(bordercolour.a)};
  glTexParameterfv(GL_TEXTURE_2D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(2)> :: setEnvColourForCurrent(const shGraph :: pixel<unsigned char>& envcolour)
 {
  const static float umax = (float)(UCHAR_MAX);
  const float col [4u] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(2)> :: setEnvColourForCurrent(const shGraph :: pixel<unsigned short int>& envcolour)
 {
  const static float umax = (float)(USHRT_MAX);
  const float col [4u] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(2)> :: setEnvColourForCurrent(const shGraph :: pixel<unsigned int>& envcolour)
 {
  const static float umax = (float)(UINT_MAX);
  const float col [4u] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(2)> :: setEnvColourForCurrent(const shGraph :: pixel<float>& envcolour)
 {
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,(const float*)(&envcolour));
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(2)> :: setEnvColourForCurrent(const shGraph :: pixel<double>& envcolour)
 {
  const float col [4u] = {(float)(envcolour.r),(float)(envcolour.g),(float)(envcolour.b),(float)(envcolour.a)};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

inline void shGraph :: texture <(unsigned char)(2)> :: genMipMapForCurrent()
{
 glGenerateMipmapEXT(GL_TEXTURE_2D);
}

inline void shGraph :: texture <(unsigned char)(2)> :: genMipMap() const
{
 bind();
 genMipMapForCurrent();
 unbindCurrent();
}

inline unsigned int shGraph :: texture <(unsigned char)(2)> :: getParam() const
{
 return GL_TEXTURE_2D;
}

inline bool shGraph :: texture <(unsigned char)(2)> :: isValid() const
{
 return (id != 0u);
}

inline void shGraph :: texture <(unsigned char)(2)> :: enable()
{
 glEnable(GL_TEXTURE_2D);
}

inline void shGraph :: texture <(unsigned char)(2)> :: disable()
{
 glDisable(GL_TEXTURE_2D);
}

inline bool shGraph :: texture <(unsigned char)(2)> :: isEnabled()
{
 return (glIsEnabled(GL_TEXTURE_2D) == GL_TRUE);
}

inline void shGraph :: texture <(unsigned char)(2)> :: setActive() const
{
 glBindTexture(GL_TEXTURE_2D,id);
}

inline shGraph :: texture <(unsigned char)(3)> :: texture() : width(0u), height(0u), depth(0u), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 itexture :: id = 0u;
 itexture :: texdescr = shGraph :: itexturedescr();
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

inline shGraph :: texture <(unsigned char)(3)> :: texture(const shGraph :: texture<3>& a) : width(a.width), height(a.height), depth(a.depth), counter(a.counter)
{
 itexture :: id = a.getID();
 itexture :: texdescr = a.getDescr();
 ++(*counter);
}

inline shGraph :: texture <(unsigned char)(3)> :: ~texture()
{
 kill();
}

inline unsigned int shGraph :: texture <(unsigned char)(3)> :: getID() const
{
 return id;
}

inline unsigned int shGraph :: texture <(unsigned char)(3)> :: getWidth() const
{
 return width;
}

inline unsigned int shGraph :: texture <(unsigned char)(3)> :: getHeight() const
{
 return height;
}

inline unsigned int shGraph :: texture <(unsigned char)(3)> :: getDepth() const
{
 return depth;
}

inline unsigned char shGraph :: texture <(unsigned char)(3)> :: getDim() const
{
 return 3u;
}

template <class X> inline void shGraph :: texture <(unsigned char)(3)> :: setBorderColour(const shGraph :: pixel<X>& bordercolour)
{
 bind();
 setBorderColourForCurrent(bordercolour);
 unbindCurrent();
}

template <class X> inline void shGraph :: texture <(unsigned char)(3)> :: setEnvColour(const shGraph :: pixel<X>& envcolour)
{
  bind();
  setEnvColourForCurrent(envcolour);
  unbindCurrent();
}

inline void shGraph :: texture <(unsigned char)(3)> :: setSWrapForCurrent(unsigned int wraps)
{
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wraps);
}

inline void shGraph :: texture <(unsigned char)(3)> :: setTWrapForCurrent(unsigned int wrapt)
{
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrapt);
}

inline void shGraph :: texture <(unsigned char)(3)> :: setRWrapForCurrent(unsigned int wrapr)
{
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrapr);
}

inline void shGraph :: texture <(unsigned char)(3)> :: setMinFilterForCurrent(unsigned int minfilter)
{
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,minfilter);
}

inline void shGraph :: texture <(unsigned char)(3)> :: setMagFilterForCurrent(unsigned int magfilter)
{
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,magfilter);
}

inline void shGraph :: texture <(unsigned char)(3)> :: setEnvModeForCurrent(unsigned int envmode)
{
 glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,envmode);
}

inline void shGraph :: texture <(unsigned char)(3)> :: setMinLODForCurrent(float minlod)
{
 glTexParameterf(GL_TEXTURE_3D,GL_TEXTURE_MIN_LOD,minlod);
}

inline void shGraph :: texture <(unsigned char)(3)> :: setMaxLODForCurrent(float maxlod)
{
 glTexParameterf(GL_TEXTURE_3D,GL_TEXTURE_MAX_LOD,maxlod);
}

inline void shGraph :: texture <(unsigned char)(3)> :: setBaseLevelForCurrent(unsigned int baselevel)
{
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_BASE_LEVEL,baselevel);
}

inline void shGraph :: texture <(unsigned char)(3)> :: setMaxLevelForCurrent(unsigned int maxlevel)
{
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAX_LEVEL,maxlevel);
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(3)> :: setBorderColourForCurrent(const shGraph :: pixel<unsigned char>& bordercolour)
 {
  const static float umax = (float)(UCHAR_MAX);
  const float col [4u] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_3D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(3)> :: setBorderColourForCurrent(const shGraph :: pixel<unsigned short int>& bordercolour)
 {
  const static float umax = (float)(USHRT_MAX);
  const float col [4u] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_3D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(3)> :: setBorderColourForCurrent(const shGraph :: pixel<float>& bordercolour)
 {
  glTexParameterfv(GL_TEXTURE_3D,GL_TEXTURE_BORDER_COLOR,(const float*)(&bordercolour));
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(3)> :: setBorderColourForCurrent(const shGraph :: pixel<double>& bordercolour)
 {
  const float col [4u] = {(float)(bordercolour.r),(float)(bordercolour.g),(float)(bordercolour.b),(float)(bordercolour.a)};
  glTexParameterfv(GL_TEXTURE_3D,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(3)> :: setEnvColourForCurrent(const shGraph :: pixel<unsigned char>& envcolour)
 {
  const static float umax = (float)(UCHAR_MAX);
  const float col [4u] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(3)> :: setEnvColourForCurrent(const shGraph :: pixel<unsigned short int>& envcolour)
 {
  const static float umax = (float)(USHRT_MAX);
  const float col [4u] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(3)> :: setEnvColourForCurrent(const shGraph :: pixel<float>& envcolour)
 {
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,(const float*)(&envcolour));
 }
}

namespace shGraph
{
 template <> inline void texture <(unsigned char)(3)> :: setEnvColourForCurrent(const shGraph :: pixel<double>& envcolour)
 {
  const float col [4u] = {(float)(envcolour.r),(float)(envcolour.g),(float)(envcolour.b),(float)(envcolour.a)};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

inline void shGraph :: texture <(unsigned char)(3)> :: genMipMap() const
{
 bind();
 glGenerateMipmapEXT(GL_TEXTURE_3D);
 unbind();
}

inline unsigned int shGraph :: texture <(unsigned char)(3)> :: getParam() const
{
 return GL_TEXTURE_3D;
}

inline bool shGraph :: texture <(unsigned char)(3)> :: isValid() const
{
 return (id != 0u);
}

inline void shGraph :: texture <(unsigned char)(3)> :: enable()
{
 glEnable(GL_TEXTURE_3D);
}

inline void shGraph :: texture <(unsigned char)(3)> :: disable()
{
 glDisable(GL_TEXTURE_3D);
}

inline bool shGraph :: texture <(unsigned char)(3)> :: isEnabled()
{
 return (glIsEnabled(GL_TEXTURE_3D) == GL_TRUE);
}

inline void shGraph :: texture <(unsigned char)(3)> :: setActive() const
{
 glBindTexture(GL_TEXTURE_3D,id);
}

inline unsigned int shGraph :: getMaxTextures()
{
 unsigned int max = 0u;
 glGetIntegerv(GL_MAX_TEXTURE_UNITS_ARB,(GLint*)(&max));
 return max;
}

inline unsigned int shGraph :: getMaxTextureSize()
{
 unsigned int max = 0u;
 glGetIntegerv(GL_MAX_TEXTURE_SIZE,(GLint*)(&max));
 return max;
}

inline unsigned int shGraph :: getActiveTextureUnit()
{
 unsigned int res = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&res));
 return (res - GL_TEXTURE0_ARB);
}

inline void shGraph :: setActiveTextureUnit(unsigned int num)
{
 glActiveTexture(GL_TEXTURE0_ARB + num);
}
#endif
