#ifndef SH_GRAPH_RBO
#define SH_GRAPH_RBO

#include <new>
#include <stdlib.h>

#include "GLextensions.h"

#include "../shMath/stack.h"
#include "../shMath/vec2.h"
#include "../shMath/vec3.h"
#include "../shMath/vec4.h"

namespace shGraph
{
 class RBO
 {
  public:
  RBO();
  RBO(unsigned int w, unsigned int h, unsigned int comp);
  RBO(const RBO& a);
  ~RBO();
  RBO& operator = (const RBO& a);
  void bind() const;
  void unbind() const;
  const unsigned int& getId() const;
  const unsigned int& getWidth() const;
  const unsigned int& getHeight() const;
  const unsigned int& getComponent() const;
  unsigned int& getId();
  unsigned int& getWidth();
  unsigned int& getHeight();
  unsigned int& getComponent();
  static void unbindCurrent();
  static void unbindAll();
  template <class T> static RBO loadColourBuffer(unsigned int width, unsigned int height);
  private:
  void kill();
  unsigned int id;
  unsigned int width;
  unsigned int height;
  unsigned int component;
  unsigned int* counter;
  static RBO current;
  static shMath :: stack<RBO> RBOstack;
 };
 const unsigned int initRBOstackSize = 2u;
 RBO loadDepthBuffer(unsigned int width, unsigned int height);
 unsigned int getMaxRBOSize();
}

inline shGraph :: RBO :: RBO() : id(0u), width(0u), height(0u), component(GL_RGBA4), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

inline shGraph :: RBO :: RBO(unsigned int w, unsigned int h, unsigned int comp) : id(0u), width(w), height(h), component(comp), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 glGenRenderbuffersEXT(1,&id);
 bind();
 glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,component,width,height);
 unbind();
}

inline shGraph :: RBO :: RBO(const shGraph :: RBO& a) : id(a.id), width(a.width), height(a.height), component(a.component), counter(a.counter)
{
 ++(*counter);
}

inline shGraph :: RBO :: ~RBO()
{
 kill();
}

inline shGraph :: RBO& shGraph :: RBO :: operator = (const shGraph :: RBO& a)
{
 if (&a != this)
 {
  kill();
  id = a.id;
  width = a.width;
  height = a.height;
  component = a.component;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

inline void shGraph :: RBO :: bind() const
{
 RBOstack.push(current);
 glBindRenderbufferEXT(GL_RENDERBUFFER_EXT,(current = *this).id);
}

inline void shGraph :: RBO :: unbind() const
{
 if (current.id == id)
 {
  unbindCurrent();
 }
}

inline const unsigned int& shGraph :: RBO :: getId() const
{
 return id;
}

inline const unsigned int& shGraph :: RBO :: getWidth() const
{
 return width;
}

inline const unsigned int& shGraph :: RBO :: getHeight() const
{
 return height;
}

inline const unsigned int& shGraph :: RBO :: getComponent() const
{
 return component;
}

inline unsigned int& shGraph :: RBO :: getId()
{
 return id;
}

inline unsigned int& shGraph :: RBO :: getWidth()
{
 return width;
}

inline unsigned int& shGraph :: RBO :: getHeight()
{
 return height;
}

inline unsigned int& shGraph :: RBO :: getComponent()
{
 return component;
}

inline void shGraph :: RBO :: unbindCurrent()
{
 glBindRenderbufferEXT(GL_RENDERBUFFER_EXT,(current = RBOstack.pop()).id);
}

inline void shGraph :: RBO :: unbindAll()
{
 current = RBO();
 RBOstack.clear();
 glBindRenderbufferEXT(GL_RENDERBUFFER_EXT,0);
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<signed char>(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_R8I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<unsigned char>(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_R8UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<signed short int>(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_R16I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<unsigned short int>(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_R16UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<signed int>(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_R32I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<unsigned int>(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_R32UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<float>(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_R32F);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec2<unsigned char> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RG8UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec2<signed char> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RG8I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec2<unsigned short int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RG16UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec2<signed short int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RG16I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec2<unsigned int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RG32UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec2<signed int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RG32I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec2<float> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RG32F);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec3<unsigned char> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGB8UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec3<signed char> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGB8I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec3<unsigned short int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGB16UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec3<signed short int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGB16I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec3<unsigned int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGB32UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec3<signed int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGB32I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec3<float> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGB32F);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec4<unsigned char> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGBA8UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec4<signed char> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGBA8I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec4<unsigned short int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGBA16UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec4<signed short int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGBA16I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec4<unsigned int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGBA32UI);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec4<signed int> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGBA32I);
 }
}

namespace shGraph
{
 template <> inline shGraph :: RBO RBO :: loadColourBuffer<shMath :: vec4<float> >(unsigned int width,unsigned int height)
 {
  return shGraph :: RBO(width,height,GL_RGBA32F);
 }
}

inline void shGraph :: RBO :: kill()
{
 if ((*counter) == 0u)
 {
  if (id != 0u)
  {
   glDeleteRenderbuffersEXT(1,&id);
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

inline shGraph :: RBO shGraph :: loadDepthBuffer(unsigned int width,unsigned int height)
{
 return shGraph :: RBO(width,height,GL_DEPTH_COMPONENT);
}

inline unsigned int shGraph :: getMaxRBOSize()
{
 static unsigned int ans = 0u;
 glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE,(GLint*)(&ans));
 return ans;
}
#endif

