#ifndef SH_GRAPH_RCCOORDINATESFX
#define SH_GRAPH_RCCOORDINATESFX

#include <new>
#include <stdlib.h>

#include "../shMath/ops.h"
#include "../shMath/vec2.h"
#include "../shMath/vec3.h"
#include "../shMath/vec4.h"
#include "../shMath/mat2.h"
#include "../shMath/mat3.h"
#include "../shMath/mat4.h"

namespace shGraph
{
 template <class T> class rccoordinatesfx
 {
  public:
  rccoordinatesfx();
  rccoordinatesfx(unsigned char dims,unsigned int number);
  rccoordinatesfx(const rccoordinatesfx& a);
  ~rccoordinatesfx();
  rccoordinatesfx& operator = (const rccoordinatesfx& a);
  unsigned char getDim() const;
  unsigned int getSize() const;
  rccoordinatesfx operator * (const T& a) const;
  rccoordinatesfx operator * (const shMath :: mat2<T>& a) const;
  rccoordinatesfx operator * (const shMath :: mat3<T>& a) const;
  rccoordinatesfx operator * (const shMath :: mat4<T>& a) const;
  rccoordinatesfx operator / (const T& a) const;
  rccoordinatesfx operator / (const shMath :: mat2<T>& a) const;
  rccoordinatesfx operator / (const shMath :: mat3<T>& a) const;
  rccoordinatesfx operator / (const shMath :: mat4<T>& a) const;
  rccoordinatesfx& operator *= (const T& a);
  rccoordinatesfx& operator *= (const shMath :: mat2<T>& a);
  rccoordinatesfx& operator *= (const shMath :: mat3<T>& a);
  rccoordinatesfx& operator *= (const shMath :: mat4<T>& a);
  rccoordinatesfx& operator /= (const T& a);
  rccoordinatesfx& operator /= (const shMath :: mat2<T>& a);
  rccoordinatesfx& operator /= (const shMath :: mat3<T>& a);
  rccoordinatesfx& operator /= (const shMath :: mat4<T>& a);
  operator const T* () const;
  operator T* ();
  operator const shMath :: vec4<T>* () const;
  operator shMath :: vec4<T>* ();
  operator const shMath :: vec3<T>* () const;
  operator shMath :: vec3<T>* ();
  operator const shMath :: vec2<T>* () const;
  operator shMath :: vec2<T>* ();
  void scale();
  private:
  void kill();
  void copy();
  unsigned char dim;
  unsigned int num;
  T* data;
  unsigned int* counter;
 };

 template <class T> rccoordinatesfx<T> operator * (const T& a, const rccoordinatesfx<T>& b);
 template <class T> rccoordinatesfx<T> operator * (const shMath :: mat2<T>& a, const rccoordinatesfx<T>& b);
 template <class T> rccoordinatesfx<T> operator * (const shMath :: mat3<T>& a, const rccoordinatesfx<T>& b);
 template <class T> rccoordinatesfx<T> operator * (const shMath :: mat4<T>& a, const rccoordinatesfx<T>& b);
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: rccoordinatesfx() : dim(0), num(0), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: rccoordinatesfx(unsigned char dims, unsigned int number) : dim(dims), num(number), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 try
 {
  data = new T [dim * num];
 }
 catch(...)
 {
  free(counter);
  throw;
 }
 (*counter) = 0u;
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: rccoordinatesfx(const rccoordinatesfx<T>& a) : dim(a.dim), num(a.num), data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: ~rccoordinatesfx()
{
 kill();
}

template <class T> inline shGraph :: rccoordinatesfx<T>& shGraph :: rccoordinatesfx <T> :: operator = (const shGraph :: rccoordinatesfx<T>& a)
{
 if (&a != this)
 {
  kill();
  dim = a.dim;
  num = a.num;
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline unsigned char shGraph :: rccoordinatesfx <T> :: getDim() const
{
 return dim;
}

template <class T> inline unsigned int shGraph :: rccoordinatesfx <T> :: getSize() const
{
 return num;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: rccoordinatesfx <T> :: operator * (const T& a) const
{
 shGraph :: rccoordinatesfx<T> ans(dim,num);
 const T* temp1 = (const T*)(*this);
 T* temp2 = (T*)(ans.data);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp2++) = ((*temp1++) * a));
 return ans;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: rccoordinatesfx <T> :: operator * (const shMath :: mat2<T>& a) const
{
 shGraph :: rccoordinatesfx<T> ans(dim,num);
 const shMath :: vec2<T>* temp1 = (const shMath :: vec2<T>*)(*this);
 shMath :: vec2<T>* temp2 = (shMath :: vec2<T>*)(ans.data);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp2++) = ((*temp1++) * a));
 return ans;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: rccoordinatesfx <T> :: operator * (const shMath :: mat3<T>& a) const
{
 shGraph :: rccoordinatesfx<T> ans(dim,num);
 const shMath :: vec3<T>* temp1 = (const shMath :: vec3<T>*)(*this);
 shMath :: vec3<T>* temp2 = (shMath :: vec3<T>*)(ans.data);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp2++) = ((*temp1++) * a));
 return ans;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: rccoordinatesfx <T> :: operator * (const shMath :: mat4<T>& a) const
{
 shGraph :: rccoordinatesfx<T> ans(dim,num);
 const shMath :: vec4<T>* temp1 = (const shMath :: vec4<T>*)(*this);
 shMath :: vec4<T>* temp2 = (shMath :: vec4<T>*)(ans.data);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp2++) = ((*temp1++) * a));
 return ans;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: rccoordinatesfx <T> :: operator / (const T& a) const
{
 shGraph :: rccoordinatesfx<T> ans(dim,num);
 const T inv = shMath :: inverse(a);
 const T* temp1 = (const T*)(*this);
 T* temp2 = (T*)(ans.data);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp2++) = ((*temp1++) * inv));
 return ans;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: rccoordinatesfx <T> :: operator / (const shMath :: mat2<T>& a) const
{
 shGraph :: rccoordinatesfx<T> ans(dim,num);
 const T inv = shMath :: inverse(a);
 const shMath :: vec2<T>* temp1 = (const shMath :: vec2<T>*)(*this);
 shMath :: vec2<T>* temp2 = (shMath :: vec2<T>*)(ans.data);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp2++) = ((*temp1++) * inv));
 return ans;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: rccoordinatesfx <T> :: operator / (const shMath :: mat3<T>& a) const
{
 shGraph :: rccoordinatesfx<T> ans(dim,num);
 const T inv = shMath :: inverse(a);
 const shMath :: vec3<T>* temp1 = (const shMath :: vec3<T>*)(*this);
 shMath :: vec3<T>* temp2 = (shMath :: vec3<T>*)(ans.data);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp2++) = ((*temp1++) * inv));
 return ans;
}

template <class T> inline shGraph :: rccoordinatesfx<T>& shGraph :: rccoordinatesfx <T> :: operator *= (const T& a)
{
 T* temp = (T*)(*this);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp++) *= a);
 return *this;
}

template <class T> inline shGraph :: rccoordinatesfx<T>& shGraph :: rccoordinatesfx <T> :: operator *= (const shMath :: mat2<T>& a)
{
 shMath :: vec2<T>* temp = (shMath :: vec2<T>*)(*this);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp++) *= a);
 return *this;
}

template <class T> inline shGraph :: rccoordinatesfx<T>& shGraph :: rccoordinatesfx <T> :: operator *= (const shMath :: mat3<T>& a)
{
 shMath :: vec3<T>* temp = (shMath :: vec3<T>*)(*this);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp++) *= a);
 return *this;
}

template <class T> inline shGraph :: rccoordinatesfx<T>& shGraph :: rccoordinatesfx <T> :: operator *= (const shMath :: mat4<T>& a)
{
 shMath :: vec4<T>* temp = (shMath :: vec4<T>*)(*this);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp++) *= a);
 return *this;
}

template <class T> inline shGraph :: rccoordinatesfx<T>& shGraph :: rccoordinatesfx <T> :: operator /= (const T& a)
{
 const T inv = shMath :: inverse(a);
 T* temp = (T*)(*this);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp++) *= inv);
 return *this;
}

template <class T> inline shGraph :: rccoordinatesfx<T>& shGraph :: rccoordinatesfx <T> :: operator /= (const shMath :: mat2<T>& a)
{
 const T inv = shMath :: inverse(a);
 shMath :: vec2<T>* temp = (shMath :: vec2<T>*)(*this);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp++) *= inv);
 return *this;
}

template <class T> inline shGraph :: rccoordinatesfx<T>& shGraph :: rccoordinatesfx <T> :: operator /= (const shMath :: mat3<T>& a)
{
 const T inv = shMath :: inverse(a);
 shMath :: vec3<T>* temp = (shMath :: vec3<T>*)(*this);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp++) *= inv);
 return *this;
}

template <class T> inline shGraph :: rccoordinatesfx<T>& shGraph :: rccoordinatesfx <T> :: operator /= (const shMath :: mat4<T>& a)
{
 const T inv = shMath :: inverse(a);
 shMath :: vec4<T>* temp = (shMath :: vec4<T>*)(*this);
 unsigned int i = num;
 for(;i != 0u;--i,(*temp++) *= inv);
 return *this;
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: operator const T* () const
{
 return (const T*)(data);
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: operator const shMath :: vec2<T>* () const
{
 return (const shMath :: vec2<T>*)(data);
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: operator shMath :: vec2<T>* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (shMath :: vec2<T>*)(data);
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: operator const shMath :: vec3<T>* () const
{
 return (const shMath :: vec3<T>*)(data);
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: operator shMath :: vec3<T>* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (shMath :: vec3<T>*)(data);
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: operator const shMath :: vec4<T>* () const
{
 return (const shMath :: vec4<T>*)(data);
}

template <class T> inline shGraph :: rccoordinatesfx <T> :: operator shMath :: vec4<T>* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (shMath :: vec4<T>*)(data);
}

template <class T> void shGraph :: rccoordinatesfx <T> :: scale()
{
 unsigned int i = 0u;
 shMath :: vec2<T>* temp2 = NULL;
 shMath :: vec3<T>* temp3 = NULL;
 shMath :: vec4<T>* temp4 = NULL;
 switch(dim)
 {
  case 2:
  for(temp2 = (shMath :: vec2<T>*)(*this);i < num;++i,++temp2)
  {
   (*temp2) /= temp2->y();
  }
  break;
  case 3:
  for(temp3 = (shMath :: vec3<T>*)(*this);i < num;++i,++temp3)
  {
   (*temp3) /= temp3->z();
  }
  break;
  case 4:
  for(temp4 = (shMath :: vec4<T>*)(*this);i < num;++i,++temp4)
  {
   (*temp4) /= temp4->w();
  }
  break;
 };
}

template <class T> inline void shGraph :: rccoordinatesfx <T> :: kill()
{
 if ((*counter) == 0u)
 {
  if (data != NULL)
  {
   delete [] data;
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> void shGraph :: rccoordinatesfx <T> :: copy()
{
 unsigned int* tempcount = NULL;
 T* tempdat = NULL;
 T* temp1 = NULL;
 const T* temp2 = data;
 unsigned int i = 0u;
 unsigned int size = (dim * num);
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  tempdat = new T [size];
  for(temp1 = tempdat;i < size;++i,(*temp1++) = (*temp2++));
 }
 catch(...)
 {
  if (tempdat != NULL)
  {
   delete [] tempdat;
  }
  free(tempcount);
 }
 --(*counter);
 data = tempdat;
 counter = tempcount;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: operator * (const T& a, const shGraph :: rccoordinatesfx<T>& b)
{
 shGraph :: rccoordinatesfx<T> ans(b.getDim(),b.getSize());
 const T* temp1 = (const T*)(b);
 T* temp2 = (T*)(ans);
 unsigned int i = 0u;
 for(;i < b.getSize();++i,(*temp2++) = (a * (*temp1++)));
 return ans;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: operator * (const shMath :: mat2<T>& a, const shGraph :: rccoordinatesfx<T>& b)
{
 shGraph :: rccoordinatesfx<T> ans(b.getDim(),b.getSize());
 const shMath :: vec2<T>* temp1 = (const shMath :: vec2<T>*)(b);
 shMath :: vec2<T>* temp2 = (shMath :: vec2<T>*)(ans);
 unsigned int i = 0u;
 for(;i < b.getSize();++i,(*temp2++) = (a * (*temp1++)));
 return ans;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: operator * (const shMath :: mat3<T>& a, const shGraph :: rccoordinatesfx<T>& b)
{
 shGraph :: rccoordinatesfx<T> ans(b.getDim(),b.getSize());
 const shMath :: vec3<T>* temp1 = (const shMath :: vec3<T>*)(b);
 shMath :: vec3<T>* temp2 = (shMath :: vec3<T>*)(ans);
 unsigned int i = 0u;
 for(;i < b.getSize();++i,(*temp2++) = (a * (*temp1++)));
 return ans;
}

template <class T> inline shGraph :: rccoordinatesfx<T> shGraph :: operator * (const shMath :: mat4<T>& a, const shGraph :: rccoordinatesfx<T>& b)
{
 shGraph :: rccoordinatesfx<T> ans(b.getDim(),b.getSize());
 const shMath :: vec4<T>* temp1 = (const shMath :: vec4<T>*)(b);
 shMath :: vec4<T>* temp2 = (shMath :: vec4<T>*)(ans);
 unsigned int i = 0u;
 for(;i < b.getSize();++i,(*temp2++) = (a * (*temp1++)));
 return ans;
}
#endif

