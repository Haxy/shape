#ifndef SH_GRAPH_PIXEL
#define SH_GRAPH_PIXEL

#include <math.h>
#include <limits.h>
#include "../shMath/ops.h"
#include "../shMath/vec4.h"

namespace shGraph
{
 template <class T> class pixel
 {
  public:
  pixel(void);
  pixel(T rr, T gg, T bb, T aa);
  template <class X> pixel(const shMath :: vec4<X>& v);
  template <class X> operator const shMath :: vec4<X> () const;
  T r;
  T g;
  T b;
  T a;
 };

 template <class T> T luminance(const pixel<T>& p);
 template <class T> pixel<T> RGBAtoHSLA(const pixel<T>& p);
 template <class T> pixel<T> HSLAtoRGBA(const pixel<T>& p);
 template <class T> pixel<T> RGBAtoHSVA(const pixel<T>& p);
 template <class T> pixel<T> HSVAtoRGBA(const pixel<T>& p);
 template <class T> pixel<T> RGBAtoYUVA(const pixel<T>& p);
 template <class T> pixel<T> YUVAtoRGBA(const pixel<T>& p);
}

template <class T> inline shGraph :: pixel <T> :: pixel() : r(shMath :: zero(T())), g(shMath :: zero(T())), b(shMath :: zero(T())), a(shMath :: zero(T()))
{
}

template <class T> inline shGraph :: pixel <T> :: pixel(T rr, T gg, T bb, T aa) : r(rr), g(gg), b(bb), a(aa)
{
}

template <> template <class X> inline shGraph :: pixel <unsigned char> :: pixel(const shMath :: vec4<X>& v) : r((unsigned char)(v.x() * (X)(UCHAR_MAX))), g((unsigned char)(v.y() * (X)(UCHAR_MAX))), b((unsigned char)(v.z() * (X)(UCHAR_MAX))), a((unsigned char)(v.w() * (X)(UCHAR_MAX)))
{
}

template <> template <class X> inline shGraph :: pixel <unsigned short int> :: pixel(const shMath :: vec4<X>& v) : r((unsigned short int)(v.x() * (X)(USHRT_MAX))), g((unsigned short int)(v.y() * (X)(USHRT_MAX))), b((unsigned short int)(v.z() * (X)(USHRT_MAX))), a((unsigned short int)(v.w() * (X)(USHRT_MAX)))
{
}

template <> template <class X> inline shGraph :: pixel <float> :: pixel(const shMath :: vec4<X>& v) : r((float)(v.x())), g((float)(v.y())), b((float)(v.z())), a((float)(v.w()))
{
}

template <> template <class X> inline shGraph :: pixel <double> :: pixel(const shMath :: vec4<X>& v) : r((double)(v.x())), g((double)(v.y())), b((double)(v.z())), a((double)(v.w()))
{
}

namespace shGraph
{
 template <> template <> inline pixel <unsigned char> :: operator const shMath :: vec4<float> () const
 {
  return shMath :: vec4<float>((float)(r) / (float)(UCHAR_MAX),(float)(g) / (float)(UCHAR_MAX),(float)(b) / (float)(UCHAR_MAX),(float)(a) / (float)(UCHAR_MAX));
 }
}

namespace shGraph
{
 template <> template <> inline pixel <unsigned char> :: operator const shMath :: vec4<double> () const
 {
  return shMath :: vec4<double>((double)(r) / (double)(UCHAR_MAX),(double)(g) / (double)(UCHAR_MAX),(double)(b) / (double)(UCHAR_MAX),(double)(a) / (double)(UCHAR_MAX));
 }
}

namespace shGraph
{
 template <> template <> inline pixel <unsigned short int> :: operator const shMath :: vec4<float> () const
 {
  return shMath :: vec4<float>((float)(r) / (float)(USHRT_MAX),(float)(g) / (float)(USHRT_MAX),(float)(b) / (float)(USHRT_MAX),(float)(a) / (float)(USHRT_MAX));
 }
}

namespace shGraph
{
 template <> template <> inline pixel <unsigned short int> :: operator const shMath :: vec4<double> () const
 {
  return shMath :: vec4<double>((double)(r) / (double)(USHRT_MAX),(double)(g) / (double)(USHRT_MAX),(double)(b) / (double)(USHRT_MAX),(double)(a) / (double)(USHRT_MAX));
 }
}

namespace shGraph
{
 template <> template <> inline pixel <float> :: operator const shMath :: vec4<float> () const
 {
  return shMath :: vec4<float>(r,g,b,a);
 }
}

namespace shGraph
{
 template <> template <> inline pixel <float> :: operator const shMath :: vec4<double> () const
 {
  return shMath :: vec4<double>((double)(r),(double)(g),(double)(b),(double)(a));
 }
}

namespace shGraph
{
 template <> template <> inline pixel <double> :: operator const shMath :: vec4<float> () const
 {
  return shMath :: vec4<float>((float)(r),(float)(g),(float)(b),(float)(a));
 }
}

namespace shGraph
{
 template <> template <> inline pixel <double> :: operator const shMath :: vec4<double> () const
 {
  return shMath :: vec4<double>(r,g,b,a);
 }
}

template <class T> inline T shGraph :: luminance(const shGraph :: pixel<T>& p)
{
 return((double)(p.r) * 0.299) + ((double)(p.g) * 0.587) + ((double)(p.b) * 0.114);
}
#endif
