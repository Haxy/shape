#ifndef SH_GRAPH_BUFFER
#define SH_GRAPH_BUFFER

#include "GLextensions.h"

#include <new>
#include <stdlib.h>

namespace shGraph
{
 class buffer
 {
  public:
  buffer();
  buffer(const buffer& a);
  ~buffer();
  buffer& operator = (const buffer& a);
  unsigned int getId() const;
  bool isValid() const;
  void create();
  private:
  void kill();
  unsigned int id;
  unsigned int* counter;
 };
}

inline shGraph :: buffer :: buffer() : id(0u), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

inline shGraph :: buffer :: buffer(const shGraph :: buffer& a) : id(a.id), counter(a.counter)
{
 ++(*counter);
}

inline shGraph :: buffer :: ~buffer()
{
 kill();
}

inline shGraph :: buffer& shGraph :: buffer :: operator = (const shGraph :: buffer& a)
{
 if (&a != this)
 {
  kill();
  id = a.id;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

inline unsigned int shGraph :: buffer :: getId() const
{
 return id;
}

inline bool shGraph :: buffer :: isValid() const
{
 return (id != 0u);
}

inline void shGraph :: buffer :: create()
{
 if ((*counter) != 0u)
 {
  unsigned int* tempCounter = NULL;
  if ((tempCounter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
  {
   throw std :: bad_alloc();
  }
  --(*counter);
  counter = tempCounter;
  (*counter) = 0u;
 }
 else
 {
  if (isValid())
  {
   glDeleteBuffersARB(1,&id);
  }
 }
 glGenBuffersARB(1,&id);
}

inline void shGraph :: buffer :: kill()
{
 if ((*counter) == 0u)
 {
  if (isValid())
  {
   glDeleteBuffersARB(1,&id);
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}
#endif
