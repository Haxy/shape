#include "cubemap.h"
#include "pixel.h"

shGraph :: cubemap :: cubemap(unsigned int dimension, const shGraph :: itexturedescr& descr) : texdescr(descr), dim(dimension), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 glGenTextures(1,&id);
 bind();
 descr.set();
 glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),dimension,dimension,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),NULL);
 glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),dimension,dimension,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),NULL);
 glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),dimension,dimension,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),NULL);
 glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),dimension,dimension,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),NULL);
 glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),dimension,dimension,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),NULL);
 glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),dimension,dimension,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),NULL);
 unbindCurrent();
}

namespace shGraph
{
 template <> cubemap :: cubemap(const image<shGraph :: pixel<unsigned char> >& negx, const image<shGraph :: pixel<unsigned char> >& posx, const image<shGraph :: pixel<unsigned char> >& negy, const image<shGraph :: pixel<unsigned char> >& posy, const image<shGraph :: pixel<unsigned char> >& negz, const image<shGraph :: pixel<unsigned char> >& posz, const itexturedescr& descr) : texdescr(descr), dim(negx.getWidth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  texdescr.getDataFormat() = GL_RGBA;
  texdescr.getDataType() = GL_UNSIGNED_BYTE;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negz));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posz));
  unbindCurrent();
 }
}

namespace shGraph
{
 template <> cubemap :: cubemap(const image<shGraph :: pixel<unsigned short int> >& negx, const image<shGraph :: pixel<unsigned short int> >& posx, const image<shGraph :: pixel<unsigned short int> >& negy, const image<shGraph :: pixel<unsigned short int> >& posy, const image<shGraph :: pixel<unsigned short int> >& negz, const image<shGraph :: pixel<unsigned short int> >& posz, const itexturedescr& descr) : texdescr(descr), dim(negx.getWidth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  texdescr.getDataFormat() = GL_RGBA;
  texdescr.getDataType() = GL_UNSIGNED_SHORT;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negz));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posz));
  unbindCurrent();
 }
}
namespace shGraph
{
 template <> cubemap :: cubemap(const image<shGraph :: pixel<unsigned int> >& negx, const image<shGraph :: pixel<unsigned int> >& posx, const image<shGraph :: pixel<unsigned int> >& negy, const image<shGraph :: pixel<unsigned int> >& posy, const image<shGraph :: pixel<unsigned int> >& negz, const image<shGraph :: pixel<unsigned int> >& posz, const itexturedescr& descr) : texdescr(descr), dim(negx.getWidth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  texdescr.getDataFormat() = GL_RGBA;
  texdescr.getDataType() = GL_UNSIGNED_INT;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negz));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posz));
  unbindCurrent();
 }
}

namespace shGraph
{
 template <> cubemap :: cubemap(const image<shGraph :: pixel<float> >& negx, const image<shGraph :: pixel<float> >& posx, const image<shGraph :: pixel<float> >& negy, const image<shGraph :: pixel<float> >& posy, const image<shGraph :: pixel<float> >& negz, const image<shGraph :: pixel<float> >& posz, const itexturedescr& descr) : texdescr(descr), dim(negx.getWidth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  texdescr.getDataFormat() = GL_RGBA;
  texdescr.getDataType() = GL_FLOAT;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negz));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posz));
  unbindCurrent();
 }
}

namespace shGraph
{
 template <> cubemap :: cubemap(const image<unsigned char>& negx, const image<unsigned char>& posx, const image<unsigned char>& negy, const image<unsigned char>& posy, const image<unsigned char>& negz, const image<unsigned char>& posz, const itexturedescr& descr) : texdescr(descr), dim(negx.getWidth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  texdescr.getDataFormat() = GL_RED;
  texdescr.getDataType() = GL_UNSIGNED_BYTE;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negz));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posz));
  unbindCurrent();
 }
}

namespace shGraph
{
 template <> cubemap :: cubemap(const image<unsigned short int>& negx, const image<unsigned short int>& posx, const image<unsigned short int>& negy, const image<unsigned short int>& posy, const image<unsigned short int>& negz, const image<unsigned short int>& posz, const itexturedescr& descr) : texdescr(descr), dim(negx.getWidth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  texdescr.getDataFormat() = GL_RED;
  texdescr.getDataType() = GL_UNSIGNED_SHORT;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negz));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posz));
  unbindCurrent();
 }
}

namespace shGraph
{
 template <> cubemap :: cubemap(const image<unsigned int>& negx, const image<unsigned int>& posx, const image<unsigned int>& negy, const image<unsigned int>& posy, const image<unsigned int>& negz, const image<unsigned int>& posz, const itexturedescr& descr) : texdescr(descr), dim(negx.getWidth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  texdescr.getDataFormat() = GL_RED;
  texdescr.getDataType() = GL_UNSIGNED_INT;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negz));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posz));
  unbindCurrent();
 }
}

namespace shGraph
{
 template <> cubemap :: cubemap(const image<float>& negx, const image<float>& posx, const image<float>& negy, const image<float>& posy, const image<float>& negz, const image<float>& posz, const itexturedescr& descr) : texdescr(descr), dim(negx.getWidth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  texdescr.getDataFormat() = GL_RED;
  texdescr.getDataType() = GL_FLOAT;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posx));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posy));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(negz));
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),dim,dim,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),(const void*)(posz));
  unbindCurrent();
 }
}

void shGraph :: cubemap :: setSWrapForCurrent(unsigned int wraps)
{
 glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_S,wraps);
}

void shGraph :: cubemap :: setTWrapForCurrent(unsigned int wrapt)
{
 glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_T,wrapt);
}

 void shGraph :: cubemap :: setRWrapForCurrent(unsigned int wrapr)
 {
  glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_R,wrapr);
 }

 void shGraph :: cubemap :: setMinFilterForCurrent(unsigned int minfilter)
 {
  glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_MIN_FILTER,minfilter);
 }

void shGraph :: cubemap :: setMagFilterForCurrent(unsigned int magfilter)
{
 glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_MAG_FILTER,magfilter);
}

void shGraph :: cubemap :: setEnvModeForCurrent(unsigned int envmode)
{
 glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,envmode);
}

void shGraph :: cubemap :: setMinLODForCurrent(float minlod)
{
 glTexParameterf(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_MIN_LOD,minlod);
}

void shGraph :: cubemap :: setMaxLODForCurrent(float maxlod)
{
 glTexParameterf(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_MAX_LOD,maxlod);
}

void shGraph :: cubemap :: setBaseLevelForCurrent(unsigned int baselevel)
{
 glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_BASE_LEVEL,baselevel);
}

void shGraph :: cubemap :: setMaxLevelForCurrent(unsigned int maxlevel)
{
 glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_MAX_LEVEL,maxlevel);
}

namespace shGraph
{
 template <> void shGraph :: cubemap :: setBorderColourForCurrent(const shGraph :: pixel<unsigned char>& bordercolour)
 {
  const static float umax = (float)(UCHAR_MAX);
  const float col [4] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> void shGraph :: cubemap :: setBorderColourForCurrent(const shGraph :: pixel<unsigned short int>& bordercolour)
 {
  const static float umax = (float)(USHRT_MAX);
  const float col [4] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> void shGraph :: cubemap :: setBorderColourForCurrent(const shGraph :: pixel<unsigned int>& bordercolour)
 {
  const static float umax = (float)(UINT_MAX);
  const float col [4] = {(float)(bordercolour.r) / umax,(float)(bordercolour.g) / umax,(float)(bordercolour.b) / umax,(float)(bordercolour.a) / umax};
  glTexParameterfv(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> void shGraph :: cubemap :: setBorderColourForCurrent(const shGraph :: pixel<float>& bordercolour)
 {
  glTexParameterfv(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_BORDER_COLOR,(const float*)(&bordercolour));
 }
}

namespace shGraph
{
 template <> void shGraph :: cubemap :: setBorderColourForCurrent(const shGraph :: pixel<double>& bordercolour)
 {
  const float col [4] = {(float)(bordercolour.r),(float)(bordercolour.g),(float)(bordercolour.b),(float)(bordercolour.a)};
  glTexParameterfv(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_BORDER_COLOR,col);
 }
}

namespace shGraph
{
 template <> void shGraph :: cubemap :: setEnvColourForCurrent(const shGraph :: pixel<unsigned char>& envcolour)
 {
  const static float umax = (float)(UCHAR_MAX);
  const float col [4] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> void shGraph :: cubemap :: setEnvColourForCurrent(const shGraph :: pixel<unsigned short int>& envcolour)
 {
  const static float umax = (float)(USHRT_MAX);
  const float col [4] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> void shGraph :: cubemap :: setEnvColourForCurrent(const shGraph :: pixel<unsigned int>& envcolour)
 {
  const static float umax = (float)(UINT_MAX);
  const float col [4] = {(float)(envcolour.r) / umax,(float)(envcolour.g) / umax,(float)(envcolour.b) / umax,(float)(envcolour.a) / umax};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

namespace shGraph
{
 template <> void shGraph :: cubemap :: setEnvColourForCurrent(const shGraph :: pixel<float>& envcolour)
 {
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,(const float*)(&envcolour));
 }
}

namespace shGraph
{
 template <> void shGraph :: cubemap :: setEnvColourForCurrent(const shGraph :: pixel<double>& envcolour)
 {
  const float col [4] = {(float)(envcolour.r),(float)(envcolour.g),(float)(envcolour.b),(float)(envcolour.a)};
  glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,col);
 }
}

void shGraph :: cubemap :: genMipMapForCurrent()
{
 glGenerateMipmapEXT(GL_TEXTURE_CUBE_MAP);
}

shGraph :: cubemap shGraph :: cubemap :: createWithoutFaceData(unsigned int dimension, const shGraph :: itexturedescr& descr)
{
 shGraph :: cubemap ans = shGraph :: cubemap();
 ans.texdescr = descr;
 ans.dim = dimension;
 glGenTextures(1,&(ans.id));
 return ans;
}

void shGraph :: cubemap :: kill()
{
 if ((*counter) == 0u)
 {
  if (id != 0u)
  {
   glDeleteTextures(1,&id);
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned char> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned char> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned char> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned char> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned char> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned char> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned short int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned short int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned short int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned short int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned short int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned short int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<unsigned int> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<float> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<float> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<float> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<float> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<float> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: attachImageToCurrent(const shGraph :: image<shGraph :: pixel<float> >& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: attachImageToCurrent(const shGraph :: image<unsigned char>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: attachImageToCurrent(const shGraph :: image<unsigned char>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: attachImageToCurrent(const shGraph :: image<unsigned char>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: attachImageToCurrent(const shGraph :: image<unsigned char>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: attachImageToCurrent(const shGraph :: image<unsigned char>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: attachImageToCurrent(const shGraph :: image<unsigned char>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_BYTE,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: attachImageToCurrent(const shGraph :: image<unsigned short int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: attachImageToCurrent(const shGraph :: image<unsigned short int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: attachImageToCurrent(const shGraph :: image<unsigned short int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: attachImageToCurrent(const shGraph :: image<unsigned short int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: attachImageToCurrent(const shGraph :: image<unsigned short int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: attachImageToCurrent(const shGraph :: image<unsigned short int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_SHORT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: attachImageToCurrent(const shGraph :: image<unsigned int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: attachImageToCurrent(const shGraph :: image<unsigned int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: attachImageToCurrent(const shGraph :: image<unsigned int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: attachImageToCurrent(const shGraph :: image<unsigned int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: attachImageToCurrent(const shGraph :: image<unsigned int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: attachImageToCurrent(const shGraph :: image<unsigned int>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_UNSIGNED_INT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: attachImageToCurrent(const shGraph :: image<float>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: attachImageToCurrent(const shGraph :: image<float>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: attachImageToCurrent(const shGraph :: image<float>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: attachImageToCurrent(const shGraph :: image<float>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: attachImageToCurrent(const shGraph :: image<float>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template <> template <> void cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: attachImageToCurrent(const shGraph :: image<float>& a)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),descr.getInternalFormat(),a.getWidth(),a.getHeight(),descr.getBorder(),GL_RED,GL_FLOAT,(const void*)(a));
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned char> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned char> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned char> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned char> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned char> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned char> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned char> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned char> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned char> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned char> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned char> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned char> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned short int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned short int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned short int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned short int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned short int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned short int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned short int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned short int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned short int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned short int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned short int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned short int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<unsigned int> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<unsigned int> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<float> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<float> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<float> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<float> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<float> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<float> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<float> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<float> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<float> > cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<float> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<shGraph :: pixel<float> > cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: getImageFromCurrent(shGraph :: image<shGraph :: pixel<float> >& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned char> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: getImageFromCurrent(shGraph :: image<unsigned char>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),GL_RED,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned char> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: getImageFromCurrent(shGraph :: image<unsigned char>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),GL_RED,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned char> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: getImageFromCurrent(shGraph :: image<unsigned char>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),GL_RED,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned char> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: getImageFromCurrent(shGraph :: image<unsigned char>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),GL_RED,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned char> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: getImageFromCurrent(shGraph :: image<unsigned char>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),GL_RED,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned char> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: getImageFromCurrent(shGraph :: image<unsigned char>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),GL_RED,GL_UNSIGNED_BYTE,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned short int> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: getImageFromCurrent(shGraph :: image<unsigned short int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),GL_RED,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned short int> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: getImageFromCurrent(shGraph :: image<unsigned short int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),GL_RED,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned short int> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: getImageFromCurrent(shGraph :: image<unsigned short int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),GL_RED,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned short int> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: getImageFromCurrent(shGraph :: image<unsigned short int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),GL_RED,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned short int> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: getImageFromCurrent(shGraph :: image<unsigned short int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),GL_RED,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned short int> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: getImageFromCurrent(shGraph :: image<unsigned short int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),GL_RED,GL_UNSIGNED_SHORT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned int> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: getImageFromCurrent(shGraph :: image<unsigned int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),GL_RED,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned int> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: getImageFromCurrent(shGraph :: image<unsigned int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),GL_RED,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned int> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: getImageFromCurrent(shGraph :: image<unsigned int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),GL_RED,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned int> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: getImageFromCurrent(shGraph :: image<unsigned int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),GL_RED,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned int> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: getImageFromCurrent(shGraph :: image<unsigned int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),GL_RED,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<unsigned int> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: getImageFromCurrent(shGraph :: image<unsigned int>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),GL_RED,GL_UNSIGNED_INT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<float> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_X> :: getImageFromCurrent(shGraph :: image<float>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_X,descr.getLevel(),GL_RED,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<float> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X> :: getImageFromCurrent(shGraph :: image<float>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X,descr.getLevel(),GL_RED,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<float> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> :: getImageFromCurrent(shGraph :: image<float>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,descr.getLevel(),GL_RED,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<float> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y> :: getImageFromCurrent(shGraph :: image<float>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,descr.getLevel(),GL_RED,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<float> cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> :: getImageFromCurrent(shGraph :: image<float>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,descr.getLevel(),GL_RED,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template  <> template <> const shGraph :: image<float> cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z> :: getImageFromCurrent(shGraph :: image<float>& helper)
 {
  const shGraph :: itexturedescr descr = itexture :: getCurrent().getDescr();
  glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,descr.getLevel(),GL_RED,GL_FLOAT,(void*)(helper));
  return helper;
 }
}

