#ifndef SH_GRAPH_ABO
#define SH_GRAPH_ABO

#include "GLextensions.h"

#include <new>
#include <stdlib.h>

#include "../shMath/mat2.h"
#include "../shMath/mat3.h"
#include "../shMath/mat4.h"
#include "../shMath/rctablefx.h"
#include "../shMath/stack.h"
#include "../shMath/vec2.h"
#include "../shMath/vec3.h"
#include "../shMath/vec4.h"

#include "AO.h"
#include "attribute.h"
#include "rccoordinatesfx.h"

namespace shGraph
{
 template <class T, bool normalise = false> class ABO : public AO
 {
  public:
  ABO();
  ABO(const shGraph :: buffer& buf);
  ABO(const shMath :: rctablefx<T>& tab, unsigned int usage = GL_STATIC_DRAW_ARB);
  template <class X> ABO(const rccoordinatesfx<X>& a, unsigned int usage = GL_STATIC_DRAW_ARB);
  static void sendCurrentTo(const attribute<T>& a);
 };
}

template <class T, bool normalise> inline shGraph :: ABO <T, normalise> :: ABO() : AO(shGraph :: AO(shGraph :: buffer(),(void*)(&shGraph :: ABO <T,normalise> :: sendCurrentTo),NULL,/*(unsigned int*)malloc(sizeof(unsigned int))*/NULL))
{
}


template <class T, bool normalise> shGraph :: ABO <T, normalise> :: ABO(const shGraph :: buffer& buf) : AO(shGraph :: AO(buf,(void*)(&shGraph :: ABO <T,normalise> :: sendCurrentTo),NULL))
{
}

template <class T, bool normalise> shGraph :: ABO <T, normalise> :: ABO(const shMath :: rctablefx<T>& tab, unsigned int usage) : AO(shGraph :: AO(shGraph :: buffer(),(void*)(&shGraph :: ABO <T,normalise> :: sendCurrentTo),NULL))
{
 id.create();
 glBindBufferARB(GL_ARRAY_BUFFER_ARB,id.getId());
 glBufferDataARB(GL_ARRAY_BUFFER_ARB,tab.getSize() * sizeof(T),(const T*)(tab),usage);
 glBindBufferARB(GL_ARRAY_BUFFER_ARB,shGraph :: AO :: getCurrent().getId());
}

template <class T, bool normalise> template <class X> shGraph :: ABO <T, normalise> :: ABO(const shGraph :: rccoordinatesfx<X>& a, unsigned int usage) : AO(shGraph :: AO(shGraph :: buffer(),(void*)(&shGraph :: ABO <T,normalise> :: sendCurrentTo),NULL))
{
 id.create();
 glBindBufferARB(GL_ARRAY_BUFFER_ARB,id.getId());
 glBufferDataARB(GL_ARRAY_BUFFER_ARB,a.getSize() * a.getDim() * sizeof(X),(const X*)(a),usage);
 glBindBufferARB(GL_ARRAY_BUFFER_ARB,shGraph :: AO :: getCurrent().getId());
}

namespace shGraph
{
 template <> inline void ABO <float, false> :: sendCurrentTo(const shGraph :: attribute<float>& a)
 {
  glVertexAttribPointer(a.getLocation(),1,GL_FLOAT,GL_FALSE,sizeof(float),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <signed int, false> :: sendCurrentTo(const shGraph :: attribute<signed int>& a)
 {
  glVertexAttribPointer(a.getLocation(),1,GL_INT,GL_FALSE,sizeof(signed int),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec2<float>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec2<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_FLOAT,GL_FALSE,sizeof(shMath :: vec2<float>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec2<signed int>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec2<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_INT,GL_FALSE,sizeof(shMath :: vec2<signed int>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec3<float>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec3<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_FLOAT,GL_FALSE,sizeof(shMath :: vec3<float>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec3<signed int>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec3<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_INT,GL_FALSE,sizeof(shMath :: vec3<signed int>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec4<float>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec4<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_FLOAT,GL_FALSE,sizeof(shMath :: vec4<float>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec4<signed int>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec4<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_INT,GL_FALSE,sizeof(shMath :: vec4<signed int>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat2<float>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat2<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat2<float>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,2,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat2<float>),((const char*)(NULL) + (sizeof(float) * 2)));
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat2<signed int>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat2<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_INT,GL_FALSE,sizeof(shMath :: mat2<signed int>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,2,GL_INT,GL_FALSE,sizeof(shMath :: mat2<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 2)));
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat3<float>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat3<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat3<float>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,3,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat3<float>),((const char*)(NULL) + (sizeof(float) * 3)));
  glVertexAttribPointer(a.getLocation() + 2,3,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat3<float>),((const char*)(NULL) + (sizeof(float) * 6)));
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat3<signed int>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat3<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_INT,GL_FALSE,sizeof(shMath :: mat3<signed int>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,3,GL_INT,GL_FALSE,sizeof(shMath :: mat3<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 3)));
  glVertexAttribPointer(a.getLocation() + 2,3,GL_INT,GL_FALSE,sizeof(shMath :: mat3<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 6)));
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat4<float>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat4<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat4<float>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,4,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat4<float>),((const char*)(NULL) + (sizeof(float) * 4)));
  glVertexAttribPointer(a.getLocation() + 2,4,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat4<float>),((const char*)(NULL) + (sizeof(float) * 8)));
  glVertexAttribPointer(a.getLocation() + 3,4,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat4<float>),((const char*)(NULL) + (sizeof(float) * 12)));
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat4<signed int>, false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat4<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_INT,GL_FALSE,sizeof(shMath :: mat4<signed int>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,4,GL_INT,GL_FALSE,sizeof(shMath :: mat4<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 4)));
  glVertexAttribPointer(a.getLocation() + 2,4,GL_INT,GL_FALSE,sizeof(shMath :: mat4<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 8)));
  glVertexAttribPointer(a.getLocation() + 3,4,GL_INT,GL_FALSE,sizeof(shMath :: mat4<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 12)));
 }
}

namespace shGraph
{
 template <> inline void ABO <float, true> :: sendCurrentTo(const shGraph :: attribute<float>& a)
 {
  glVertexAttribPointer(a.getLocation(),1,GL_FLOAT,GL_TRUE,sizeof(float),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <signed int, true> :: sendCurrentTo(const shGraph :: attribute<signed int>& a)
 {
  glVertexAttribPointer(a.getLocation(),1,GL_INT,GL_TRUE,sizeof(signed int),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec2<float>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec2<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_FLOAT,GL_TRUE,sizeof(shMath :: vec2<float>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec2<signed int>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec2<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_INT,GL_TRUE,sizeof(shMath :: vec2<signed int>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec3<float>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec3<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_FLOAT,GL_TRUE,sizeof(shMath :: vec3<float>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec3<signed int>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec3<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_INT,GL_TRUE,sizeof(shMath :: vec3<signed int>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec4<float>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec4<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_FLOAT,GL_TRUE,sizeof(shMath :: vec4<float>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: vec4<signed int>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec4<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_INT,GL_TRUE,sizeof(shMath :: vec4<signed int>),NULL);
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat2<float>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat2<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat2<float>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,2,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat2<float>),((const char*)(NULL) + (sizeof(float) * 2)));
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat2<signed int>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat2<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_INT,GL_TRUE,sizeof(shMath :: mat2<signed int>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,2,GL_INT,GL_TRUE,sizeof(shMath :: mat2<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 2)));
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat3<float>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat3<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat3<float>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,3,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat3<float>),((const char*)(NULL) + (sizeof(float) * 3)));
  glVertexAttribPointer(a.getLocation() + 2,3,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat3<float>),((const char*)(NULL) + (sizeof(float) * 6)));
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat3<signed int>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat3<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_INT,GL_TRUE,sizeof(shMath :: mat3<signed int>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,3,GL_INT,GL_TRUE,sizeof(shMath :: mat3<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 3)));
  glVertexAttribPointer(a.getLocation() + 2,3,GL_INT,GL_TRUE,sizeof(shMath :: mat3<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 6)));
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat4<float>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat4<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat4<float>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,4,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat4<float>),((const char*)(NULL) + (sizeof(float) * 4)));
  glVertexAttribPointer(a.getLocation() + 2,4,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat4<float>),((const char*)(NULL) + (sizeof(float) * 8)));
  glVertexAttribPointer(a.getLocation() + 3,4,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat4<float>),((const char*)(NULL) + (sizeof(float) * 12)));
 }
}

namespace shGraph
{
 template <> inline void ABO <shMath :: mat4<signed int>, true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat4<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_INT,GL_TRUE,sizeof(shMath :: mat4<signed int>),NULL);
  glVertexAttribPointer(a.getLocation() + 1,4,GL_INT,GL_TRUE,sizeof(shMath :: mat4<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 4)));
  glVertexAttribPointer(a.getLocation() + 2,4,GL_INT,GL_TRUE,sizeof(shMath :: mat4<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 8)));
  glVertexAttribPointer(a.getLocation() + 3,4,GL_INT,GL_TRUE,sizeof(shMath :: mat4<signed int>),((const char*)(NULL)+ (sizeof(signed int) * 12)));
 }
}
#endif

