#ifndef SH_GRAPH_VARIABLE
#define SH_GRAPH_VARIABLE

#include "GLextensions.h"

#include "shader.h"
#include "../shMath/vec2.h"
#include "../shMath/vec3.h"
#include "../shMath/vec4.h"
#include "../shMath/mat2.h"
#include "../shMath/mat3.h"
#include "../shMath/mat4.h"
#include "../shMath/rctablefx.h"

namespace shGraph
{
 template <class T> class variable
 {
  public:
  typedef signed int location;
  variable(void);
  explicit variable(location a, shader shad = shader());
  const location& getLocation() const;
  location& getLocation();
  const shader& getOwner() const;
  shader& getOwner();
  variable& operator = (const T& a);
  operator const T () const;
  bool isValid() const;
  const static location invalidLocation = -1;
  private:
  location loc;
  shader owner;
 };
 template <class T> variable<T> getVariable(const unsigned char* name,const shader& shad);
 template <class T> variable<T> getVariable(const signed char* name,const shader& shad);
 template <class T> variable<T> getVariable(const char* name,const shader& shad);
}

template <class T> inline shGraph :: variable <T> :: variable() : loc(shGraph :: vatiable <T> :: invalidLocation), owner(shGraph :: shader())
{
}

template <class T> inline shGraph :: variable <T> :: variable(typename shGraph :: variable <T> :: location a, shader shad) : loc(a), owner(shad)
{
}

template <class T> inline const typename shGraph :: variable <T> :: location& shGraph :: variable <T> :: getLocation() const
{
 return loc;
}

template <class T> inline typename shGraph :: variable <T> :: location& shGraph :: variable <T> :: getLocation()
{
 return loc;
}

template <class T> inline const shGraph :: shader& shGraph :: variable <T> :: getOwner() const
{
 return owner;
}

template <class T> inline shGraph :: shader& shGraph :: variable <T> :: getOwner()
{
 return owner;
}

namespace shGraph
{
 template <> inline shGraph :: variable<float>& variable <float> :: operator = (const float& a)
 {
  glUniform1fARB(loc,a);
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<signed int>& variable <signed int> :: operator = (const signed int& a)
 {
  glUniform1iARB(loc,a);
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: vec2<float> >& variable <shMath :: vec2<float> > :: operator = (const shMath :: vec2<float>& a)
 {
  glUniform2fvARB(loc,1,(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: vec3<float> >& variable <shMath :: vec3<float> > :: operator = (const shMath :: vec3<float>& a)
 {
  glUniform3fvARB(loc,1,(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: vec4<float> >& variable <shMath :: vec4<float> > :: operator = (const shMath :: vec4<float>& a)
 {
  glUniform4fvARB(loc,1,(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: mat2<float> >& variable <shMath :: mat2<float> > :: operator = (const shMath :: mat2<float>& a)
 {
  glUniformMatrix2fvARB(loc,1,false,(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: mat3<float> >& variable <shMath :: mat3<float> > :: operator = (const shMath :: mat3<float>& a)
 {
  glUniformMatrix3fvARB(loc,1,false,(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: mat4<float> >& variable <shMath :: mat4<float> > :: operator = (const shMath :: mat4<float>& a)
 {
  glUniformMatrix4fvARB(loc,1,false,(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: vec2<signed int> >& variable <shMath :: vec2<signed int> > :: operator = (const shMath :: vec2<signed int>& a)
 {
  glUniform2ivARB(loc,1,(const signed int*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: vec3<signed int> >& variable <shMath :: vec3<signed int> > :: operator = (const shMath :: vec3<signed int>& a)
 {
  glUniform3ivARB(loc,1,(const signed int*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: vec4<signed int> >& variable <shMath :: vec4<signed int> > :: operator = (const shMath :: vec4<signed int>& a)
 {
  glUniform4ivARB(loc,1,(const signed int*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<float> >& variable <shMath :: rctablefx<float> > :: operator = (const shMath :: rctablefx<float>& a)
 {
  glUniform1fvARB(loc,a.getSize(),(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<signed int> >& variable <shMath :: rctablefx<signed int> > :: operator = (const shMath :: rctablefx<signed int>& a)
 {
  glUniform1ivARB(loc,a.getSize(),(const signed int*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<shMath :: vec2<float> > >& variable <shMath :: rctablefx<shMath :: vec2<float> > > :: operator = (const shMath :: rctablefx<shMath :: vec2<float> >& a)
 {
  glUniform2fvARB(loc,a.getSize(),(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<shMath :: vec3<float> > >& variable <shMath :: rctablefx<shMath :: vec3<float> > > :: operator = (const shMath :: rctablefx<shMath :: vec3<float> >& a)
 {
  glUniform3fvARB(loc,a.getSize(),(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<shMath :: vec4<float> > >& variable <shMath :: rctablefx<shMath :: vec4<float> > > :: operator = (const shMath :: rctablefx<shMath :: vec4<float> >& a)
 {
  glUniform4fvARB(loc,a.getSize(),(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<shMath :: mat2<float> > >& variable <shMath :: rctablefx<shMath :: mat2<float> > > :: operator = (const shMath :: rctablefx<shMath :: mat2<float> >& a)
 {
  glUniformMatrix2fvARB(loc,a.getSize(),false,(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<shMath :: mat3<float> > >& variable <shMath :: rctablefx<shMath :: mat3<float> > > :: operator = (const shMath :: rctablefx<shMath :: mat3<float> >& a)
 {
  glUniformMatrix3fvARB(loc,a.getSize(),false,(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<shMath :: mat4<float> > >& variable <shMath :: rctablefx<shMath :: mat4<float> > > :: operator = (const shMath :: rctablefx<shMath :: mat4<float> >& a)
 {
  glUniformMatrix4fvARB(loc,a.getSize(),false,(const float*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<shMath :: vec2<signed int> > >& variable <shMath :: rctablefx<shMath :: vec2<signed int> > > :: operator = (const shMath :: rctablefx<shMath :: vec2<signed int> >& a)
 {
  glUniform2ivARB(loc,a.getSize(),(const signed int*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<shMath :: vec3<signed int> > >& variable <shMath :: rctablefx<shMath :: vec3<signed int> > > :: operator = (const shMath :: rctablefx<shMath :: vec3<signed int> >& a)
 {
  glUniform3ivARB(loc,a.getSize(),(const signed int*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: variable<shMath :: rctablefx<shMath :: vec4<signed int> > >& variable <shMath :: rctablefx<shMath :: vec4<signed int> > > :: operator = (const shMath :: rctablefx<shMath :: vec4<signed int> >& a)
 {
  glUniform4ivARB(loc,a.getSize(),(const signed int*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline variable <float> :: operator const float () const
 {
  float res = 0.0f;
  glGetUniformfvARB(owner.getId(),loc,(GLfloat*)(&res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <signed int> :: operator const signed int () const
 {
  signed int res = 0;
  glGetUniformivARB(owner.getId(),loc,(GLint*)(&res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: vec2<float> > :: operator const shMath :: vec2<float> () const
 {
  shMath :: vec2<float> res;
  glGetUniformfvARB(owner.getId(),loc,(float*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: vec3<float> > :: operator const shMath :: vec3<float> () const
 {
  shMath :: vec3<float> res;
  glGetUniformfvARB(owner.getId(),loc,(float*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: vec4<float> > :: operator const shMath :: vec4<float> () const
 {
  shMath :: vec4<float> res;
  glGetUniformfvARB(owner.getId(),loc,(float*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: mat2<float> > :: operator const shMath :: mat2<float> () const
 {
  shMath :: mat2<float> res;
  glGetUniformfvARB(owner.getId(),loc,(float*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: mat3<float> > :: operator const shMath :: mat3<float> () const
 {
  shMath :: mat3<float> res;
  glGetUniformfvARB(owner.getId(),loc,(float*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: mat4<float> > :: operator const shMath :: mat4<float> () const
 {
  shMath :: mat4<float> res;
  glGetUniformfvARB(owner.getId(),loc,(float*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: vec2<signed int> > :: operator const shMath :: vec2<signed int> () const
 {
  shMath :: vec2<signed int> res;
  glGetUniformivARB(owner.getId(),loc,(signed int*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: vec3<signed int> > :: operator const shMath :: vec3<signed int> () const
 {
  shMath :: vec3<signed int> res;
  glGetUniformivARB(owner.getId(),loc,(signed int*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: vec4<signed int> > :: operator const shMath :: vec4<signed int> () const
 {
  shMath :: vec4<signed int> res;
  glGetUniformivARB(owner.getId(),loc,(signed int*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: mat2<signed int> > :: operator const shMath :: mat2<signed int> () const
 {
  shMath :: mat2<signed int> res;
  glGetUniformivARB(owner.getId(),loc,(signed int*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: mat3<signed int> > :: operator const shMath :: mat3<signed int> () const
 {
  shMath :: mat3<signed int> res;
  glGetUniformivARB(owner.getId(),loc,(signed int*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline variable <shMath :: mat4<signed int> > :: operator const shMath :: mat4<signed int> () const
 {
  shMath :: mat4<signed int> res;
  glGetUniformivARB(owner.getId(),loc,(signed int*)(res));
  return res;
 }
}

template <class T> inline bool shGraph :: variable <T> :: isValid() const
{
 return (loc != invalidLocation);
}

template <class T> shGraph :: variable<T> shGraph :: getVariable(const unsigned char* name,const shGraph :: shader& shad)
{
 return shGraph :: variable<T>(glGetUniformLocationARB(shad.getId(),(const GLchar*)(name)),shad);
}

template <class T> shGraph :: variable<T> shGraph :: getVariable(const signed char* name,const shGraph :: shader& shad)
{
 return shGraph :: variable<T>(glGetUniformLocationARB(shad.getId(),(const GLchar*)(name)),shad);
}

template <class T> shGraph :: variable<T> shGraph :: getVariable(const char* name,const shGraph :: shader& shad)
{
 return shGraph :: variable<T>(glGetUniformLocationARB(shad.getId(),(const GLchar*)(name)),shad);
}
#endif

