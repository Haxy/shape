#ifndef SH_GRAPH_IAO
#define SH_GRAPH_IAO

#include <new>
#include <stdlib.h>

#include "GLextensions.h"

#include "IO.h"
#include "rccoordinatesfx.h"

namespace shGraph
{
 class IAO : public IO
 {
  public:
  IAO();
  IAO(const IAO& a);
  IAO(const rccoordinatesfx<unsigned int>& a);
  ~IAO();
  IAO& operator = (const IAO& a);
  void display() const;
  static void displayCurrent();
  unsigned int getType() const;
  unsigned int getSize() const;
  operator const rccoordinatesfx<unsigned int> () const;
  operator rccoordinatesfx<unsigned int> ();
  private:
  void kill();
 };
}

inline shGraph :: IAO :: IAO() : IO(shGraph :: buffer(),0u,0u,(&shGraph :: IAO :: displayCurrent),new shGraph :: rccoordinatesfx<unsigned int>(),NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete data;
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

inline shGraph :: IAO :: IAO(const shGraph :: rccoordinatesfx<unsigned int> &a) : IO(shGraph :: buffer(),0u,0u,(&shGraph :: IAO :: displayCurrent),new shGraph :: rccoordinatesfx<unsigned int>(a),NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete data;
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

inline shGraph :: IAO :: ~IAO()
{
 kill();
}

inline shGraph :: IAO& shGraph :: IAO :: operator = (const shGraph :: IAO& a)
{
 if (&a != this)
 {
  kill();
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

void shGraph :: IAO :: display() const
{
 unsigned int temptype = GL_POINTS;
 bind();
 switch(((const shGraph :: rccoordinatesfx<unsigned int>*)(data))->getDim())
 {
  case 1u:
  temptype = GL_POINTS;
  break;
  case 2u:
  temptype = GL_LINES;
  break;
  case 3u:
  temptype = GL_TRIANGLES;
  break;
  case 4u:
  temptype = GL_QUADS;
  break;
 }
 glDrawElements(temptype,((const shGraph :: rccoordinatesfx<unsigned int>*)(data))->getDim() * ((const shGraph :: rccoordinatesfx<unsigned int>*)(data))->getSize(),GL_UNSIGNED_INT,(const unsigned int*)(*((const shGraph :: rccoordinatesfx<unsigned int>*)(data))));
 shGraph :: IO :: unbindCurrent();
}

void shGraph :: IAO :: displayCurrent()
{
 unsigned int type = GL_POINTS;
 switch(((const shGraph :: rccoordinatesfx<unsigned int>*)(current.getData()))->getDim())
 {
  case 1u:
  type = GL_POINTS;
  break;
  case 2u:
  type = GL_LINES;
  break;
  case 3u:
  type = GL_TRIANGLES;
  break;
  case 4u:
  type = GL_QUADS;
  break;
 }
 glDrawElements(type,((const shGraph :: rccoordinatesfx<unsigned int>*)(current.getData()))->getDim() * ((const shGraph :: rccoordinatesfx<unsigned int>*)(current.getData()))->getSize(),GL_UNSIGNED_INT,(const unsigned int*)(*((const shGraph :: rccoordinatesfx<unsigned int>*)(current.getData()))));
}

inline unsigned int shGraph :: IAO :: getType() const
{
 switch(((const shGraph :: rccoordinatesfx<unsigned int>*)(data))->getDim())
 {
  case 1u:
  return GL_POINTS;
  break;
  case 2u:
  return GL_LINES;
  break;
  case 3u:
  return GL_TRIANGLES;
  break;
  case 4u:
  return GL_QUADS;
  break;
  default:
  return 0u;
 }
}

inline unsigned int shGraph :: IAO :: getSize() const
{
 return (((const shGraph :: rccoordinatesfx<unsigned int>*)(data))->getDim() * ((const shGraph :: rccoordinatesfx<unsigned int>*)(data))->getSize());
}

inline shGraph :: IAO :: operator const shGraph :: rccoordinatesfx <unsigned int> () const
{
 return *((const shGraph :: rccoordinatesfx<unsigned int>*)(data));
}

inline shGraph :: IAO :: operator shGraph :: rccoordinatesfx <unsigned int> ()
{
 return *((shGraph :: rccoordinatesfx<unsigned int>*)(data));
}

inline void shGraph :: IAO :: kill()
{
 if ((*counter) == 0u)
 {
  delete data;
  free(counter);
 }
 else
 {
  --(*counter);
 }
}
#endif

