#ifndef SH_GRAPH_TEXTUREVARIABLE
#define SH_GRAPH_TEXTUREVARIABLE

#include "GLextensions.h"

#include "shader.h"
#include "texture.h"
#include "variable.h"

namespace shGraph
{
 template <unsigned char dim> class variable <texture<dim> >
 {
  public:
  typedef signed int location;
  variable(void);
  explicit variable(location a, const shader& shad = shader());
  const location& getLocation() const;
  location& getLocation();
  const shader& getOwner() const;
  shader& getOwner();
  variable& operator = (const signed int& a);
  variable& operator = (const unsigned int& a);
  operator const texture<dim> () const;
  bool isValid() const;
  const static location invalidLocation = -1;
  private:
  location loc;
  shader owner;
 };
}

template <unsigned char dim> inline shGraph :: variable <shGraph :: texture<dim> > :: variable() : loc(shGraph :: variable <shGraph :: texture<dim> > :: invalidLocation), owner(shGraph :: shader())
{
}

template <unsigned char dim> inline shGraph :: variable <shGraph :: texture<dim> > :: variable(typename shGraph :: variable <shGraph :: texture<dim> > :: location a, const shGraph :: shader& shad) : loc(a), owner(shad)
{
}

template <unsigned char dim> inline const typename shGraph :: variable <shGraph :: texture<dim> > :: location& shGraph :: variable <shGraph :: texture<dim> > :: getLocation() const
{
 return loc;
}

template <unsigned char dim> inline typename shGraph :: variable <shGraph :: texture<dim> > :: location& shGraph :: variable <shGraph :: texture<dim> > :: getLocation()
{
 return loc;
}

template <unsigned char dim> inline const shGraph :: shader& shGraph :: variable <shGraph :: texture<dim> > :: getOwner() const
{
 return owner;
}

template <unsigned char dim> inline shGraph :: shader& shGraph :: variable <shGraph :: texture<dim> > :: getOwner()
{
 return owner;
}

template <unsigned char dim> inline shGraph :: variable<shGraph :: texture<dim> >& shGraph :: variable <shGraph :: texture<dim> > :: operator = (const signed int& a)
{
 glUniform1iARB(loc,a);
 return *this;
}

template <unsigned char dim> inline shGraph :: variable<shGraph :: texture<dim> >& shGraph :: variable <shGraph :: texture<dim> > :: operator = (const unsigned int& a)
{
 glUniform1iARB(loc,a);
 return *this;
}

template <unsigned char dim> inline shGraph :: variable <shGraph :: texture<dim> > :: operator const shGraph :: texture<dim> () const
{
 signed int res = 0;
 glGetUniformivARB(owner.getId(),loc,(GLint*)(&res));
 return shGraph :: itexture :: getCurrent(res);
}

template <unsigned char dim> inline bool shGraph :: variable <shGraph :: texture<dim> > :: isValid() const
{
 return (loc != shGraph :: variable <shGraph :: texture<dim> > :: invalidLocation);
}
#endif