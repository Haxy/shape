#ifndef SH_GRAPH_CLEARCOLOUR
#define SH_GRAPH_CLEARCOLOUR

#include "GLstandard.h"
#include "pixel.h"
#include "../shMath/vec4.h"
#include "../shMath/rcvec4.h"

namespace shGraph
{
 class render :: clearcolour
 {
  public:
  template <class X> clearcolour& operator = (const pixel<X>& colour);
  template <class X> clearcolour& operator = (const shMath :: rcvec4<X>& colour);
  template <class X> clearcolour& operator = (const shMath :: vec4<X>& colour);
  template <class X> operator const pixel<X> () const;
  template <class X> operator const shMath :: rcvec4<X> () const;
  template <class X> operator const shMath :: vec4<X> () const;
  template <class X> const shGraph :: pixel<X> getColour() const;
  template <class X> const shMath :: rcvec4<X> rcgetColour() const;
  template <class X> const shMath :: rcvec4<X> rcgetColour(shMath :: rcvec4<X>& helper) const;
 };
}

namespace shGraph
{
 template <> inline shGraph :: render :: clearcolour& render :: clearcolour :: operator = (const shGraph :: pixel<float>& colour)
 {
  glClearColor((GLclampf)(colour.r),(GLclampf)(colour.g),(GLclampf)(colour.b),(GLclampf)(colour.a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: render :: clearcolour& render :: clearcolour :: operator = (const shGraph :: pixel<double>& colour)
 {
  glClearColor((GLclampf)(colour.r),(GLclampf)(colour.g),(GLclampf)(colour.b),(GLclampf)(colour.a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: render :: clearcolour& render :: clearcolour :: operator = (const shMath :: rcvec4<float>& colour)
 {
  glClearColor((GLclampf)(colour.x()),(GLclampf)(colour.y()),(GLclampf)(colour.z()),(GLclampf)(colour.w()));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: render :: clearcolour& render :: clearcolour :: operator = (const shMath :: rcvec4<double>& colour)
 {
  glClearColor((GLclampf)(colour.x()),(GLclampf)(colour.y()),(GLclampf)(colour.z()),(GLclampf)(colour.w()));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: render :: clearcolour& render :: clearcolour :: operator = (const shMath :: vec4<float>& colour)
 {
  glClearColor((GLclampf)(colour.x()),(GLclampf)(colour.y()),(GLclampf)(colour.z()),(GLclampf)(colour.w()));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: render :: clearcolour& render :: clearcolour :: operator = (const shMath :: vec4<double>& colour)
 {
  glClearColor((GLclampf)(colour.x()),(GLclampf)(colour.y()),(GLclampf)(colour.z()),(GLclampf)(colour.w()));
  return *this;
 }
}

namespace shGraph
{
 template <> inline render :: clearcolour :: operator const shGraph :: pixel<float> () const
 {
  static shGraph :: pixel<float> res;
  glGetFloatv(GL_COLOR_CLEAR_VALUE,(float*)(&res));
  return res;
 }
}

namespace shGraph
{
 template <> inline render :: clearcolour :: operator const shGraph :: pixel<double> () const
 {
  static shGraph :: pixel<double> res;
  glGetDoublev(GL_COLOR_CLEAR_VALUE,(double*)(&res));
  return res;
 }
}

namespace shGraph
{
 template <> inline render :: clearcolour :: operator const shMath :: rcvec4<float> () const
 {
  static shMath :: rcvec4<float> res;
  glGetFloatv(GL_COLOR_CLEAR_VALUE,(float*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline render :: clearcolour :: operator const shMath :: rcvec4<double> () const
 {
  static shMath :: rcvec4<double> res;
  glGetDoublev(GL_COLOR_CLEAR_VALUE,(double*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline render :: clearcolour :: operator const shMath :: vec4<float> () const
 {
  static shMath :: vec4<float> res;
  glGetFloatv(GL_COLOR_CLEAR_VALUE,(float*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline render :: clearcolour :: operator const shMath :: vec4<double> () const
 {
  static shMath :: vec4<double> res;
  glGetDoublev(GL_COLOR_CLEAR_VALUE,(double*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline const shGraph :: pixel<float> render :: clearcolour :: getColour() const
 {
  static shGraph :: pixel<float> res;
  glGetFloatv(GL_COLOR_CLEAR_VALUE,(float*)(&res));
  return res;
 }
}

namespace shGraph
{
 template <> inline const shGraph :: pixel<double> render :: clearcolour :: getColour() const
 {
  static shGraph :: pixel<double> res;
  glGetDoublev(GL_COLOR_CLEAR_VALUE,(double*)(&res));
  return res;
 }
}

namespace shGraph
{
 template <> inline const shMath :: rcvec4<float> render :: clearcolour :: rcgetColour() const
 {
  static shMath :: rcvec4<float> res;
  glGetFloatv(GL_COLOR_CLEAR_VALUE,(float*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline const shMath :: rcvec4<double> render :: clearcolour :: rcgetColour() const
 {
  static shMath :: rcvec4<double> res;
  glGetDoublev(GL_COLOR_CLEAR_VALUE,(double*)(res));
  return res;
 }
}

namespace shGraph
{
 template <> inline const shMath :: rcvec4<float> render :: clearcolour :: rcgetColour(shMath :: rcvec4<float>& helper) const
 {
  glGetFloatv(GL_COLOR_CLEAR_VALUE,(float*)(helper));
  return helper;
 }
}

namespace shGraph
{
 template <> inline const shMath :: rcvec4<double> render :: clearcolour :: rcgetColour(shMath :: rcvec4<double>& helper) const
 {
  glGetDoublev(GL_COLOR_CLEAR_VALUE,(double*)(helper));
  return helper;
 }
}
#endif

