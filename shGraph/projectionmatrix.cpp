#include "render.h"
#include "projectionmatrix.h"

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator = (const shMath :: mat4<float>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf((const float*)(projection));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator = (const shMath :: mat4<double>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixd((const double*)(projection));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator = (const shMath :: rcmat4<float>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf((const float*)(projection));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator = (const shMath :: rcmat4<double>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixd((const double*)(projection));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator *= (const shMath :: mat4<float>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glMultMatrixf((const float*)(projection));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator *= (const shMath :: mat4<double>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glMultMatrixd((const double*)(projection));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator *= (const shMath :: rcmat4<float>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glMultMatrixf((const float*)(projection));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator *= (const shMath :: rcmat4<double>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glMultMatrixd((const double*)(projection));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator /= (const shMath :: mat4<float>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glMultMatrixf((const float*)(shMath :: inverse(projection)));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator /= (const shMath :: mat4<double>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glMultMatrixd((const double*)(shMath :: inverse(projection)));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator /= (const shMath :: rcmat4<float>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glMultMatrixf((const float*)(shMath :: inverse(projection)));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: projectionmatrix& render :: projectionmatrix :: operator /= (const shMath :: rcmat4<double>& projection)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_PROJECTION);
  glMultMatrixd((const double*)(shMath :: inverse(projection)));
  glMatrixMode(res);
  return *this;
 }
}

