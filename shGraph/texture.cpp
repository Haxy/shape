#include "texture.h"

unsigned int shGraph :: itexture :: lastParam = 0u;
shMath :: rctablefx<shGraph :: itexture> shGraph :: itexture :: current = shMath :: rctablefx<shGraph :: itexture>(initTexStackSize);
shMath :: rctablefx<shMath :: stack<shGraph :: itexture> > shGraph :: itexture :: texstack = shMath :: rctablefx<shMath :: stack<shGraph :: itexture> >(initTexStackSize);

void shGraph :: itexture :: bind() const
{
 unsigned int unit = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&unit));
 unit -= GL_TEXTURE0_ARB;
 (texstack[unit]).push(current[unit]);
 current[unit] = *this;
 lastParam = getParam();
 setActive();
}

void shGraph :: itexture :: unbind() const
{
 unsigned int unit = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&unit));
 unit -= GL_TEXTURE0_ARB;
 if ((current[unit]).getID() == getID())
 {
  current[unit] = (texstack[unit]).pop();
  lastParam = (((current[unit]).getParam() == 0u) ? lastParam : (current[unit]).getParam());
  (current[unit]).setActive();
 }
}

void shGraph :: itexture :: bind(unsigned int unit) const
{
 unsigned int prev = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&prev));
 (texstack[unit]).push(current[unit]);
 current[unit] = *this;
 lastParam = getParam();
 setActiveTextureUnit(unit);
 setActive();
 glActiveTexture(prev);
}

void shGraph :: itexture :: unbind(unsigned int unit) const
{
 unsigned int prev = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&prev));
 setActiveTextureUnit(unit);
 if ((current[unit]).getID() == getID())
 {
  current[unit] = (texstack[unit]).pop();
  lastParam = (((current[unit]).getParam() == 0u) ? lastParam : (current[unit]).getParam());
  (current[unit]).setActive();
 }
 glActiveTexture(prev);
}

void shGraph :: itexture :: unbindCurrent(unsigned int unit)
{
 unsigned int prev = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&prev));
 setActiveTextureUnit(unit);
 current[unit] = (texstack[unit]).pop();
 lastParam = (((current[unit]).getParam() == 0u) ? lastParam : (current[unit]).getParam());
 (current[unit]).setActive();
 setActiveTextureUnit(prev - GL_TEXTURE0_ARB);
}

void shGraph :: itexture :: unbindAll(unsigned int unit)
{
 unsigned int prev = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&prev));
 setActiveTextureUnit(unit);
 (texstack[unit]).clear();
 lastParam = (((current[unit]).getParam() == 0u) ? lastParam : (current[unit]).getParam());
 current[unit] = itexture();
 (current[unit]).setActive();
 glActiveTexture(prev);
}

void shGraph :: itexture :: unbindCurrent()
{
 unsigned int unit = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&unit));
 unit -= GL_TEXTURE0_ARB;
 current[unit] = (texstack[unit]).pop();
 lastParam = (((current[unit]).getParam() == 0u) ? lastParam : (current[unit]).getParam());
 (current[unit]).setActive();
}

void shGraph :: itexture :: unbindAll()
{
 unsigned int unit = GL_TEXTURE0_ARB;
 glGetIntegerv(GL_ACTIVE_TEXTURE,(GLint*)(&unit));
 unit -= GL_TEXTURE0_ARB;
 (texstack[unit]).clear();
 lastParam = (((current[unit]).getParam() == 0u) ? lastParam : (current[unit]).getParam());
 current[unit] = itexture();
 (current[unit]).setActive();
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <signed char, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RED,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <unsigned char, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RED,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <signed short int, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RED,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <unsigned short int, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RED,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <signed int, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RED,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <unsigned int, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RED,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <float, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RED,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec2<signed char>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RG,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec2<unsigned char>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RG,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec2<signed short int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RG,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec2<unsigned short int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RG,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec2<signed int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RG,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec2<unsigned int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RG,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec2<float>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RG,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec3<signed char>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGB,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec3<unsigned char>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGB,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec3<signed short int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGB,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec3<unsigned short int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGB,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec3<signed int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGB,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec3<unsigned int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGB,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec3<float>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGB,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec4<signed char>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec4<unsigned char>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec4<signed short int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec4<unsigned short int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec4<signed int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec4<unsigned int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shMath :: vec4<float>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shGraph :: pixel<signed char>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shGraph :: pixel<unsigned char>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shGraph :: pixel<signed short int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shGraph :: pixel<unsigned short int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shGraph :: pixel<signed int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shGraph :: pixel<unsigned int>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: texture(const shGraph :: image <shGraph :: pixel<float>, (unsigned char)(1)>& a, const shGraph :: itexturedescr& descr) : width(a.getLength()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),width,descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

shGraph :: texture <(unsigned char)(1)>  :: texture(unsigned int len, const shGraph :: itexturedescr& descr) : width(len), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 glGenTextures(1,&id);
 itexture :: texdescr = descr;
 bind();
 descr.set();
 glTexImage1D(GL_TEXTURE_1D,descr.getLevel(),descr.getInternalFormat(),len,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),NULL);
 shGraph :: itexture :: unbindCurrent();
}

shGraph :: texture<1>& shGraph :: texture <(unsigned char)(1)> :: operator = (const shGraph :: texture<1>& a)
{
 if (&a != this)
 {
  kill();
  id = a.id;
  width = a.width;
  texdescr = a.texdescr;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<signed char, (unsigned char)(1)> () const
 {
  shGraph :: image<signed char, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RED,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<unsigned char, (unsigned char)(1)> () const
 {
  shGraph :: image<unsigned char, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RED,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<signed short int, (unsigned char)(1)> () const
 {
  shGraph :: image<signed short int, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RED,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<unsigned short int, (unsigned char)(1)> () const
 {
  shGraph :: image<unsigned short int, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RED,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<signed int, (unsigned char)(1)> () const
 {
  shGraph :: image<signed int, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RED,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<unsigned int, (unsigned char)(1)> () const
 {
  shGraph :: image<unsigned int, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RED,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<float, (unsigned char)(1)> () const
 {
  shGraph :: image<float, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RED,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec2<signed char>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec2<signed char>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RG,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec2<unsigned char>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec2<unsigned char>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RG,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec2<signed short int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec2<signed short int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RG,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec2<unsigned short int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec2<unsigned short int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RG,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec2<signed int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec2<signed int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RG,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec2<unsigned int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec2<unsigned int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RG,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec2<float>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec2<float>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RG,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec3<signed char>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec3<signed char>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGB,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec3<unsigned char>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec3<unsigned char>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGB,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec3<signed short int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec3<signed short int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGB,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec3<unsigned short int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec3<unsigned short int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGB,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec3<signed int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec3<signed int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGB,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec3<unsigned int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec3<unsigned int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGB,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec3<float>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec3<float>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGB,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec4<signed char>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec4<signed char>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec4<unsigned char>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec4<unsigned char>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec4<signed short int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec4<signed short int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec4<unsigned short int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec4<unsigned short int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec4<signed int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec4<signed int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec4<unsigned int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec4<unsigned int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shMath :: vec4<float>, (unsigned char)(1)> () const
 {
  shGraph :: image<shMath :: vec4<float>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shGraph :: pixel<signed char>, (unsigned char)(1)> () const
 {
  shGraph :: image<shGraph :: pixel<signed char>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shGraph :: pixel<unsigned char>, (unsigned char)(1)> () const
 {
  shGraph :: image<shGraph :: pixel<unsigned char>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shGraph :: pixel<signed short int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shGraph :: pixel<signed short int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shGraph :: pixel<unsigned short int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shGraph :: pixel<unsigned short int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shGraph :: pixel<signed int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shGraph :: pixel<signed int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shGraph :: pixel<unsigned int>, (unsigned char)(1)> () const
 {
  shGraph :: image<shGraph :: pixel<unsigned int>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(1)> :: operator const shGraph :: image<shGraph :: pixel<float>, (unsigned char)(1)> () const
 {
  shGraph :: image<shGraph :: pixel<float>, (unsigned char)(1)> res(width);
  bind();
  glGetTexImage(GL_TEXTURE_1D,texdescr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

void shGraph :: texture <(unsigned char)(1)> :: setSWrap(unsigned int wraps)
{
 bind();
 shGraph :: texture <(unsigned char)(1)> :: setSWrapForCurrent(wraps);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(1)> :: setMinFilter(unsigned int minfilter)
{
 bind();
 shGraph :: texture <(unsigned char)(1)> :: setMinFilterForCurrent(minfilter);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(1)> :: setMagFilter(unsigned int magfilter)
{
 bind();
 shGraph :: texture <(unsigned char)(1)> :: setMagFilterForCurrent(magfilter);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(1)> :: setEnvMode(unsigned int envmode)
{
 bind();
 shGraph :: texture <(unsigned char)(1)> :: setEnvModeForCurrent(envmode);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(1)> :: setMinLOD(float minlod)
{
 bind();
 shGraph :: texture <(unsigned char)(1)> :: setMinLODForCurrent(minlod);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(1)> :: setMaxLOD(float maxlod)
{
 bind();
 shGraph :: texture <(unsigned char)(1)> :: setMaxLODForCurrent(maxlod);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(1)> :: setBaseLevel(unsigned int baselevel)
{
 bind();
 shGraph :: texture <(unsigned char)(1)> :: setBaseLevelForCurrent(baselevel);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(1)> :: setMaxLevel(unsigned int maxlevel)
{
 bind();
 shGraph :: texture <(unsigned char)(1)> :: setMaxLevelForCurrent(maxlevel);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(1)> :: kill()
{
 if ((*counter) == 0u)
 {
  if (id != 0u)
  {
   glDeleteTextures(1,&id);
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shGraph :: pixel<signed char>, (unsigned char)(2)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shGraph :: pixel<unsigned char>, (unsigned char)(2)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shGraph :: pixel<signed short int>, (unsigned char)(2)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shGraph :: pixel<unsigned short int>, (unsigned char)(2)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shGraph :: pixel<signed int>, (unsigned char)(2)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shGraph :: pixel<unsigned int>, (unsigned char)(2)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shGraph :: pixel<float>, (unsigned char)(2)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<signed char>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RED,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<unsigned char>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RED,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<signed short int>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RED,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<unsigned short int>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RED,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<signed int>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RED,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<unsigned int>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RED,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<float>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RED,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec2<signed char> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RG,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec2<unsigned char> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RG,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec2<signed short int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RG,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec2<unsigned short int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RG,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec2<signed int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RG,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec2<unsigned int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RG,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec2<float> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RG,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec3<signed char> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGB,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec3<unsigned char> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGB,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec3<signed short int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGB,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec3<unsigned short int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGB,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec3<signed int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGB,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec3<unsigned int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGB,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec3<float> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGB,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec4<signed char> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec4<unsigned char> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec4<signed short int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec4<unsigned short int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec4<signed int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec4<unsigned int> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: texture(const shGraph :: image<shMath :: vec4<float> >& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  itexture :: texdescr = descr;
  glGenTextures(1,&id);
  bind();
  descr.set();
  glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

shGraph :: texture <(unsigned char)(2)> :: texture(unsigned int w, unsigned int h, const shGraph :: itexturedescr& descr) : width(w), height(h), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 itexture :: texdescr = descr;
 glGenTextures(1,&id);
 bind();
 descr.set();
 glTexImage2D(GL_TEXTURE_2D,descr.getLevel(),descr.getInternalFormat(),width,height,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),NULL);
 shGraph :: itexture :: unbindCurrent();
}

shGraph :: texture<(unsigned char)(2)>& shGraph :: texture <(unsigned char)(2)> :: operator = (const shGraph :: texture<(unsigned char)(2)>& a)
{
 if (&a != this)
 {
  kill();
  id = a.id;
  width = a.width;
  height = a.height;
  texdescr = a.texdescr;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shGraph :: pixel<signed char>, (unsigned char)(2)> () const
 {
  shGraph :: image<shGraph :: pixel<signed char>, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shGraph :: pixel<unsigned char>, (unsigned char)(2)> () const
 {
  shGraph :: image<shGraph :: pixel<unsigned char>, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shGraph :: pixel<signed short int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shGraph :: pixel<signed short int>, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shGraph :: pixel<unsigned short int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shGraph :: pixel<unsigned short int>, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shGraph :: pixel<signed int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shGraph :: pixel<signed int>, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shGraph :: pixel<unsigned int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shGraph :: pixel<unsigned int>, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shGraph :: pixel<float>, (unsigned char)(2)> () const
 {
  shGraph :: image<shGraph :: pixel<float>, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<signed char, (unsigned char)(2)> () const
 {
  shGraph :: image<signed char, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RED,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<unsigned char, (unsigned char)(2)> () const
 {
  shGraph :: image<unsigned char, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RED,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<signed short int, (unsigned char)(2)> () const
 {
  shGraph :: image<signed short int, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RED,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<unsigned short int, (unsigned char)(2)> () const
 {
  shGraph :: image<unsigned short int, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RED,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<signed int, (unsigned char)(2)> () const
 {
  shGraph :: image<signed int, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RED,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<unsigned int, (unsigned char)(2)> () const
 {
  shGraph :: image<unsigned int, (unsigned char)(2)> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RED,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<float, (unsigned char)(2)> () const
 {
  shGraph :: image<float> res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RED,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec2<signed char>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec2<signed char> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RG,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec2<unsigned char>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec2<unsigned char> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RG,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec2<signed short int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec2<signed short int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RG,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec2<unsigned short int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec2<unsigned short int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RG,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec2<signed int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec2<signed int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RG,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec2<unsigned int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec2<unsigned int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RG,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec2<float>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec2<float> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RG,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec3<signed char>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec3<signed char> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGB,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec3<unsigned char>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec3<unsigned char> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGB,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec3<signed short int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec3<signed short int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGB,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec3<unsigned short int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec3<unsigned short int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGB,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec3<signed int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec3<signed int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGB,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec3<unsigned int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec3<unsigned int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGB,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec3<float>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec3<float> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGB,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec4<signed char>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec4<signed char> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec4<unsigned char>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec4<unsigned char> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec4<signed short int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec4<signed short int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec4<unsigned short int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec4<unsigned short int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec4<signed int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec4<signed int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec4<unsigned int>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec4<unsigned int> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(2)> :: operator const shGraph :: image<shMath :: vec4<float>, (unsigned char)(2)> () const
 {
  shGraph :: image<shMath :: vec4<float> > res(width,height);
  bind();
  glGetTexImage(GL_TEXTURE_2D,texdescr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

void shGraph :: texture <(unsigned char)(2)> :: setSWrap(unsigned int wraps)
{
 bind();
 setSWrapForCurrent(wraps);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(2)> :: setTWrap(unsigned int wrapt)
{
 bind();
 setTWrapForCurrent(wrapt);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(2)> :: setMinFilter(unsigned int minfilter)
{
 bind();
 setMinFilterForCurrent(minfilter);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(2)> :: setMagFilter(unsigned int magfilter)
{
 bind();
 setMagFilterForCurrent(magfilter);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(2)> :: setEnvMode(unsigned int envmode)
{
 bind();
 setEnvModeForCurrent(envmode);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(2)> :: setMinLOD(float minlod)
{
 bind();
 setMinLODForCurrent(minlod);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(2)> :: setMaxLOD(float maxlod)
{
 bind();
 setMaxLODForCurrent(maxlod);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(2)> :: setBaseLevel(unsigned int baselevel)
{
 bind();
 setBaseLevelForCurrent(baselevel);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(2)> :: setMaxLevel(unsigned int maxlevel)
{
 bind();
 setMaxLevelForCurrent(maxlevel);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(2)> :: kill()
{
 if ((*counter) == 0u)
 {
  if (id != 0u)
  {
   glDeleteTextures(1,&id);
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <signed char, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RED,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <unsigned char, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RED,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <signed short int, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RED,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <unsigned short int, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RED,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <signed int, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RED,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <unsigned int, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RED,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <float, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RED,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec2<signed char>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RG,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec2<unsigned char>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RG,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec2<signed short int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RG,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec2<unsigned short int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RG,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec2<signed int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RG,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec2<unsigned int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RG,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec2<float>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RG,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec3<signed char>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGB,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec3<unsigned char>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec3<signed short int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGB,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec3<unsigned short int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGB,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec3<signed int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGB,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec3<unsigned int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGB,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec3<float>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGB,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec4<signed char>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec4<unsigned char>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec4<signed short int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec4<unsigned short int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec4<signed int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec4<unsigned int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shMath :: vec4<float>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shGraph :: pixel<signed char>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shGraph :: pixel<unsigned char>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_UNSIGNED_BYTE,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shGraph :: pixel<signed short int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shGraph :: pixel<unsigned short int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_UNSIGNED_SHORT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shGraph :: pixel<signed int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shGraph :: pixel<unsigned int>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_UNSIGNED_INT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: texture(const shGraph :: image <shGraph :: pixel<float>, (unsigned char)(3)>& a, const shGraph :: itexturedescr& descr) : width(a.getWidth()), height(a.getHeight()), depth(a.getDepth()), counter((unsigned int*)malloc(sizeof(unsigned int)))
 {
  if (counter == NULL)
  {
   throw std :: bad_alloc();
  }
  (*counter) = 0u;
  glGenTextures(1,&id);
  itexture :: texdescr = descr;
  bind();
  descr.set();
  glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),GL_RGBA,GL_FLOAT,(const void*)(a));
  shGraph :: itexture :: unbindCurrent();
 }
}

shGraph :: texture <(unsigned char)(3)> :: texture(unsigned int w, unsigned int h, unsigned int d, const shGraph :: itexturedescr& descr) : width(w), height(h), depth(d), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 itexture :: texdescr = descr;
 glGenTextures(1,&id);
 bind();
 descr.set();
 glTexImage3D(GL_TEXTURE_3D,descr.getLevel(),descr.getInternalFormat(),width,height,depth,descr.getBorder(),descr.getDataFormat(),descr.getDataType(),NULL);
 shGraph :: itexture :: unbindCurrent();
}

shGraph :: texture<(unsigned char)(3)>& shGraph :: texture <(unsigned char)(3)> :: operator = (const shGraph :: texture<(unsigned char)(3)>& a)
{
 if (&a != this)
 {
  kill();
  id = a.id;
  width = a.width;
  height = a.height;
  depth = a.depth;
  texdescr = a.texdescr;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<signed char, (unsigned char)(3)> () const
 {
  shGraph :: image<signed char, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RED,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<unsigned char, (unsigned char)(3)> () const
 {
  shGraph :: image<unsigned char, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RED,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<signed short int, (unsigned char)(3)> () const
 {
  shGraph :: image<signed short int, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RED,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<unsigned short int, (unsigned char)(3)> () const
 {
  shGraph :: image<unsigned short int, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RED,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<signed int, (unsigned char)(3)> () const
 {
  shGraph :: image<signed int, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RED,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<unsigned int, (unsigned char)(3)> () const
 {
  shGraph :: image<unsigned int, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RED,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<float, (unsigned char)(3)> () const
 {
  shGraph :: image<float, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RED,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec2<signed char>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec2<signed char>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RG,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec2<unsigned char>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec2<unsigned char>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RG,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec2<signed short int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec2<signed short int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RG,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec2<unsigned short int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec2<unsigned short int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RG,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec2<signed int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec2<signed int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RG,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec2<unsigned int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec2<unsigned int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RG,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec2<float>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec2<float>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RG,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec3<signed char>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec3<signed char>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGB,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec3<unsigned char>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec3<unsigned char>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGB,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec3<signed short int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec3<signed short int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGB,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec3<unsigned short int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec3<unsigned short int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGB,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec3<signed int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec3<signed int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGB,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec3<unsigned int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec3<unsigned int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGB,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec3<float>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec3<float>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGB,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec4<signed char>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec4<signed char>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec4<unsigned char>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec4<unsigned char>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec4<signed short int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec4<signed short int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec4<unsigned short int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec4<unsigned short int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec4<signed int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec4<signed int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec4<unsigned int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec4<unsigned int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shMath :: vec4<float>, (unsigned char)(3)> () const
 {
  shGraph :: image<shMath :: vec4<float>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shGraph :: pixel<signed char>, (unsigned char)(3)> () const
 {
  shGraph :: image<shGraph :: pixel<signed char>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shGraph :: pixel<unsigned char>, (unsigned char)(3)> () const
 {
  shGraph :: image<shGraph :: pixel<unsigned char>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_BYTE,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shGraph :: pixel<signed short int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shGraph :: pixel<signed short int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shGraph :: pixel<unsigned short int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shGraph :: pixel<unsigned short int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_SHORT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shGraph :: pixel<signed int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shGraph :: pixel<signed int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shGraph :: pixel<unsigned int>, (unsigned char)(3)> () const
 {
  shGraph :: image<shGraph :: pixel<unsigned int>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_UNSIGNED_INT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

namespace shGraph
{
 template <> texture <(unsigned char)(3)> :: operator const shGraph :: image<shGraph :: pixel<float>, (unsigned char)(3)> () const
 {
  shGraph :: image<shGraph :: pixel<float>, (unsigned char)(3)> res(width, height, depth);
  bind();
  glGetTexImage(GL_TEXTURE_3D,texdescr.getLevel(),GL_RGBA,GL_FLOAT,(void*)(res));
  shGraph :: itexture :: unbindCurrent();
  return res;
 }
}

void shGraph :: texture <(unsigned char)(3)> :: setSWrap(unsigned int wraps)
{
 bind();
 setSWrapForCurrent(wraps);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(3)> :: setTWrap(unsigned int wrapt)
{
 bind();
 setTWrapForCurrent(wrapt);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(3)> :: setRWrap(unsigned int wrapr)
{
 bind();
 setRWrapForCurrent(wrapr);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(3)> :: setMinFilter(unsigned int minfilter)
{
 bind();
 setMinFilterForCurrent(minfilter);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(3)> :: setMagFilter(unsigned int magfilter)
{
 bind();
 setMagFilterForCurrent(magfilter);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(3)> :: setEnvMode(unsigned int envmode)
{
 bind();
 setEnvModeForCurrent(envmode);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(3)> :: setMinLOD(float minlod)
{
 bind();
 setMinLODForCurrent(minlod);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(3)> :: setMaxLOD(float maxlod)
{
 bind();
 setMaxLODForCurrent(maxlod);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(3)> :: setBaseLevel(unsigned int baselevel)
{
 bind();
 setBaseLevelForCurrent(baselevel);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(3)> :: setMaxLevel(unsigned int maxlevel)
{
 bind();
 setMaxLevelForCurrent(maxlevel);
 shGraph :: itexture :: unbindCurrent();
}

void shGraph :: texture <(unsigned char)(3)> :: kill()
{
 if ((*counter) == 0u)
 {
  if (id != 0u)
  {
   glDeleteTextures(1,&id);
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}
