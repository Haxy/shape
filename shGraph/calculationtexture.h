#ifndef SH_GRAPH_CALCULATIONTEXTURE
#define	SH_GRAPH_CALCULATIONTEXTURE

#include "texturedescr.h"

#include "../shMath/vec2.h"
#include "../shMath/vec3.h"
#include "../shMath/vec4.h"

namespace shGraph
{
 template <unsigned char dim,class T> class calculationtexturedescr : public itexturedescr
 {
  public:
  calculationtexturedescr();
  calculationtexturedescr(unsigned int filters,unsigned int wraps);
  unsigned int getFilters() const;
  unsigned int getWraps() const;
  unsigned int& getFilters();
  unsigned int& getWraps();
  void set() const;
  private:
  unsigned int filter;
  unsigned int wrap;
 };
 template <unsigned char dim,class T> calculationtexturedescr<dim,T> loadCalculationTextureNearest();
 template <unsigned char dim,class T> calculationtexturedescr<dim,T> loadCalculationTextureLinear();
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,signed char> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R8I;
  format = GL_RED;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,signed char> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R8I;
  format = GL_RED;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,signed char> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R8I;
  format = GL_RED;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,unsigned char> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R8UI;
  format = GL_RED;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,unsigned char> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R8UI;
  format = GL_RED;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,unsigned char> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R8UI;
  format = GL_RED;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,signed int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R32I;
  format = GL_RED;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,signed int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R32I;
  format = GL_RED;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,signed int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R32I;
  format = GL_RED;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,unsigned int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R32UI;
  format = GL_RED;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,unsigned int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R32UI;
  format = GL_RED;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,unsigned int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R32UI;
  format = GL_RED;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,signed short int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R16I;
  format = GL_RED;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,signed short int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R16I;
  format = GL_RED;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,signed short int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R16I;
  format = GL_RED;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,unsigned short int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R16UI;
  format = GL_RED;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,unsigned short int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R16UI;
  format = GL_RED;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,unsigned short int> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R16UI;
  format = GL_RED;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,float> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R32F;
  format = GL_RED;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,float> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R32F;
  format = GL_RED;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,float> :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_R32F;
  format = GL_RED;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<signed char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG8I;
  format = GL_RG;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<signed char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG8I;
  format = GL_RG;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<signed char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG8I;
  format = GL_RG;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<unsigned char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG8UI;
  format = GL_RG;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<unsigned char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG8UI;
  format = GL_RG;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<unsigned char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG8UI;
  format = GL_RG;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<signed int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG32I;
  format = GL_RG;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<signed int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG32I;
  format = GL_RG;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<signed int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG32I;
  format = GL_RG;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<unsigned int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG32UI;
  format = GL_RG;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<unsigned int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG32UI;
  format = GL_RG;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<unsigned int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG32UI;
  format = GL_RG;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<signed short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG16I;
  format = GL_RG;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<signed short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG16I;
  format = GL_RG;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<signed short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG16I;
  format = GL_RG;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<unsigned short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG16UI;
  format = GL_RG;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<unsigned short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG16UI;
  format = GL_RG;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<unsigned short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG16UI;
  format = GL_RG;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<float> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG32F;
  format = GL_RG;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<float> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG32F;
  format = GL_RG;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<float> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RG32F;
  format = GL_RG;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<signed char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB8I;
  format = GL_RGB;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<signed char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB8I;
  format = GL_RGB;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<signed char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB8I;
  format = GL_RGB;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<unsigned char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB8UI;
  format = GL_RGB;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<unsigned char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB8UI;
  format = GL_RGB;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<unsigned char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB8UI;
  format = GL_RGB;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<signed int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB32I;
  format = GL_RGB;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<signed int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB32I;
  format = GL_RGB;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<signed int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB32I;
  format = GL_RGB;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<unsigned int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB32UI;
  format = GL_RGB;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<unsigned int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB32UI;
  format = GL_RGB;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<unsigned int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB32UI;
  format = GL_RGB;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<signed short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB16I;
  format = GL_RGB;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<signed short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB16I;
  format = GL_RGB;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<signed short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB16I;
  format = GL_RGB;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<unsigned short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB16UI;
  format = GL_RGB;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<unsigned short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB16UI;
  format = GL_RGB;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<unsigned short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB16UI;
  format = GL_RGB;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<float> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB32F;
  format = GL_RGB;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<float> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB32F;
  format = GL_RGB;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<float> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGB32F;
  format = GL_RGB;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<signed char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA8I;
  format = GL_RGBA;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<signed char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA8I;
  format = GL_RGBA;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<signed char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA8I;
  format = GL_RGBA;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<unsigned char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA8UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<unsigned char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA8UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<unsigned char> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA8UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<signed int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA32I;
  format = GL_RGBA;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<signed int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA32I;
  format = GL_RGBA;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<signed int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA32I;
  format = GL_RGBA;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<unsigned int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA32UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<unsigned int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA32UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<unsigned int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA32UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<signed short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA16I;
  format = GL_RGBA;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<signed short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA16I;
  format = GL_RGBA;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<signed short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA16I;
  format = GL_RGBA;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<unsigned short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA16UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<unsigned short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA16UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<unsigned short int> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA16UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<float> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA32F;
  format = GL_RGBA;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<float> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA32F;
  format = GL_RGBA;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<float> > :: calculationtexturedescr() : filter(GL_NEAREST), wrap(GL_CLAMP_TO_EDGE)
 {
  intern = GL_RGBA32F;
  format = GL_RGBA;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,signed char> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R8I;
  format = GL_RED;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,signed char> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R8I;
  format = GL_RED;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,signed char> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R8I;
  format = GL_RED;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,unsigned char> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R8UI;
  format = GL_RED;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,unsigned char> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R8UI;
  format = GL_RED;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,unsigned char> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R8UI;
  format = GL_RED;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,signed int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R32I;
  format = GL_RED;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,signed int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R32I;
  format = GL_RED;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,signed int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R32I;
  format = GL_RED;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,unsigned int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R32UI;
  format = GL_RED;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,unsigned int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R32UI;
  format = GL_RED;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,unsigned int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R32UI;
  format = GL_RED;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,signed short int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R16I;
  format = GL_RED;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,signed short int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R16I;
  format = GL_RED;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,signed short int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R16I;
  format = GL_RED;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,unsigned short int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R16UI;
  format = GL_RED;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,unsigned short int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R16UI;
  format = GL_RED;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,unsigned short int> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R16UI;
  format = GL_RED;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,float> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R32F;
  format = GL_RED;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,float> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R32F;
  format = GL_RED;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,float> :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_R32F;
  format = GL_RED;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<signed char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG8I;
  format = GL_RG;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<signed char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG8I;
  format = GL_RG;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<signed char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG8I;
  format = GL_RG;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<unsigned char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG8UI;
  format = GL_RG;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<unsigned char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG8UI;
  format = GL_RG;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<unsigned char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG8UI;
  format = GL_RG;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<signed int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG32I;
  format = GL_RG;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<signed int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG32I;
  format = GL_RG;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<signed int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG32I;
  format = GL_RG;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<unsigned int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG32UI;
  format = GL_RG;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<unsigned int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG32UI;
  format = GL_RG;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<unsigned int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG32UI;
  format = GL_RG;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<signed short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG16I;
  format = GL_RG;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<signed short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG16I;
  format = GL_RG;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<signed short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG16I;
  format = GL_RG;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<unsigned short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG16UI;
  format = GL_RG;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<unsigned short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG16UI;
  format = GL_RG;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<unsigned short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG16UI;
  format = GL_RG;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec2<float> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG32F;
  format = GL_RG;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec2<float> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG32F;
  format = GL_RG;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec2<float> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RG32F;
  format = GL_RG;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<signed char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB8I;
  format = GL_RGB;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<signed char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB8I;
  format = GL_RGB;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<signed char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB8I;
  format = GL_RGB;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<unsigned char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB8UI;
  format = GL_RGB;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<unsigned char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB8UI;
  format = GL_RGB;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<unsigned char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB8UI;
  format = GL_RGB;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<signed int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB32I;
  format = GL_RGB;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<signed int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB32I;
  format = GL_RGB;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<signed int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB32I;
  format = GL_RGB;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<unsigned int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB32UI;
  format = GL_RGB;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<unsigned int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB32UI;
  format = GL_RGB;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<unsigned int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB32UI;
  format = GL_RGB;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<signed short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB16I;
  format = GL_RGB;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<signed short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB16I;
  format = GL_RGB;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<signed short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB16I;
  format = GL_RGB;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<unsigned short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB16UI;
  format = GL_RGB;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<unsigned short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB16UI;
  format = GL_RGB;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<unsigned short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB16UI;
  format = GL_RGB;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace  shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec3<float> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB32F;
  format = GL_RGB;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec3<float> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB32F;
  format = GL_RGB;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec3<float> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGB32F;
  format = GL_RGB;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<signed char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA8I;
  format = GL_RGBA;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<signed char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA8I;
  format = GL_RGBA;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<signed char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA8I;
  format = GL_RGBA;
  type = GL_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<unsigned char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA8UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<unsigned char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA8UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<unsigned char> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA8UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_BYTE;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<signed int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA32I;
  format = GL_RGBA;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<signed int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA32I;
  format = GL_RGBA;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<signed int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA32I;
  format = GL_RGBA;
  type = GL_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<unsigned int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA32UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<unsigned int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA32UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<unsigned int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA32UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_INT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<signed short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA16I;
  format = GL_RGBA;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<signed short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA16I;
  format = GL_RGBA;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<signed short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA16I;
  format = GL_RGBA;
  type = GL_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<unsigned short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA16UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<unsigned short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA16UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<unsigned short int> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA16UI;
  format = GL_RGBA;
  type = GL_UNSIGNED_SHORT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <1,shMath :: vec4<float> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA32F;
  format = GL_RGBA;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <2,shMath :: vec4<float> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA32F;
  format = GL_RGBA;
  type = GL_FLOAT;
 }
}

namespace shGraph
{
 template <> inline calculationtexturedescr <3,shMath :: vec4<float> > :: calculationtexturedescr(unsigned int filters,unsigned int wraps) : filter(filters), wrap(wraps)
 {
  intern = GL_RGBA32F;
  format = GL_RGBA;
  type = GL_FLOAT;
 }
}

template <unsigned char dim,class T> inline unsigned int shGraph :: calculationtexturedescr <dim,T> :: getFilters() const
{
 return filter;
}

template <unsigned char dim,class T> inline unsigned int shGraph :: calculationtexturedescr <dim,T> :: getWraps() const
{
 return wrap;
}

template <unsigned char dim,class T> inline unsigned int& shGraph :: calculationtexturedescr <dim,T> :: getFilters()
{
 return filter;
}

template <unsigned char dim,class T> inline unsigned int& shGraph :: calculationtexturedescr <dim,T> :: getWraps()
{
 return wrap;
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,signed char> :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,signed char> :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,signed char> :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,unsigned char> :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,unsigned char> :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,unsigned char> :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,signed int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,signed int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,signed int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,unsigned int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,unsigned int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,unsigned int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,signed short int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,signed short int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,signed short int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,unsigned short int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,unsigned short int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,unsigned short int> :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,float> :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,float> :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,float> :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec2<signed char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec2<signed char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec2<signed char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec2<unsigned char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec2<unsigned char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec2<unsigned char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec2<signed int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec2<signed int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec2<signed int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec2<unsigned int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec2<unsigned int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec2<unsigned int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec2<signed short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec2<signed short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec2<signed short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec2<unsigned short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec2<unsigned short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec2<unsigned short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec2<float> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec2<float> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec2<float> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec3<signed char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec3<signed char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec3<signed char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec3<unsigned char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec3<unsigned char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec3<unsigned char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec3<signed int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec3<signed int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec3<signed int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec3<unsigned int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec3<unsigned int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec3<unsigned int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec3<signed short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec3<signed short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec3<signed short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec3<unsigned short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec3<unsigned short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec3<unsigned short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec3<float> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec3<float> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec3<float> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec4<signed char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec4<signed char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec4<signed char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec4<unsigned char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec4<unsigned char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec4<unsigned char> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec4<signed int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec4<signed int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec4<signed int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec4<unsigned int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec4<unsigned int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec4<unsigned int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec4<signed short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec4<signed short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec4<signed short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec4<unsigned short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec4<unsigned short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec4<unsigned short int> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <1,shMath :: vec4<float> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <2,shMath :: vec4<float> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

namespace shGraph
{
 template <> inline void calculationtexturedescr <3,shMath :: vec4<float> > :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,filter);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,filter);
 }
}

template <unsigned char dim,class T> inline shGraph :: calculationtexturedescr<dim,T> shGraph :: loadCalculationTextureNearest()
{
 return shGraph :: calculationtexturedescr<dim,T>();
}

template <unsigned char dim,class T> inline shGraph :: calculationtexturedescr<dim,T> shGraph :: loadCalculationTextureLinear()
{
 shGraph :: calculationtexturedescr<dim,T> ans = shGraph :: calculationtexturedescr<dim,T>();
 ans.getFilters() = GL_LINEAR;
 return ans;
}
#endif
