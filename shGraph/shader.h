#ifndef SH_GRAPH_SHADER
#define SH_GRAPH_SHADER

#include "GLextensions.h"

#include <new>
#include <stdlib.h>

#include "../shMath/stack.h"

namespace shGraph
{
 class shader
 {
  public:
  typedef unsigned int location;
  shader(void);
  explicit shader(location ID);
  shader(const shader& a);
  ~shader(void);
  shader& operator = (const shader& a);
  location getId() const;
  void bind() const;
  void unbind() const;
  bool isValid() const;
  bool isBound() const;
  static void unbindAll();
  static bool isAnyBound();
  static void unbindCurrent();
  static shader& getCurrent();
  static location getCurrentId();
  static shMath :: stack<shader>& getStack();
  const static location invalidLocation = 0u;
  private:
  void kill();
  location id;
  unsigned int* counter;
  static shader current;
  static shMath :: stack<shader> shadstack;
 };
 const unsigned int initShaderStackSize = 2u;
}

inline shGraph :: shader :: shader() : id(shGraph :: shader :: invalidLocation), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

inline shGraph :: shader :: shader(unsigned int ID) : id(ID), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

inline shGraph :: shader :: shader(const shGraph :: shader& a) : id(a.id), counter(a.counter)
{
 ++(*counter);
}

inline shGraph :: shader :: ~shader()
{
 kill();
}

inline shGraph :: shader& shGraph :: shader :: operator = (const shGraph :: shader& a)
{
 if (&a != this)
 {
  kill();
  id = a.id;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

inline shGraph :: shader :: location shGraph :: shader :: getId() const
{
 return id;
}

inline void shGraph :: shader :: bind() const
{
 shadstack.push(current);
 current = *this;
 glUseProgramObjectARB(id);
}

inline void shGraph :: shader :: unbind() const
{
 if (current.id == id)
 {
  current = shadstack.pop();
  glUseProgramObjectARB(current.id);
 }
}

inline bool shGraph :: shader :: isValid() const
{
 return (id != shGraph :: shader :: invalidLocation);
}

inline bool shGraph :: shader :: isBound() const
{
 return (id == getCurrentId());
}

inline void shGraph :: shader :: unbindAll()
{
 shadstack.clear();
 glUseProgramObjectARB(shGraph :: shader :: invalidLocation);
}

inline bool shGraph :: shader :: isAnyBound()
{
 return (getCurrentId() != shGraph :: shader :: invalidLocation);
}

inline void shGraph :: shader :: unbindCurrent()
{
 current = shadstack.pop();
 glUseProgramObjectARB(current.id);
}

inline shGraph :: shader& shGraph ::shader :: getCurrent()
{
 return current;
}

inline unsigned int shGraph :: shader :: getCurrentId()
{
 unsigned int shad = shGraph :: shader :: invalidLocation;
 glGetIntegerv(GL_CURRENT_PROGRAM,(GLint*)(&shad));
 return shad;
}

inline shMath :: stack<shGraph :: shader>& shGraph :: shader :: getStack()
{
 return shadstack;
}

inline void shGraph :: shader :: kill()
{
 if ((*counter) == 0u)
 {
  if (id != shGraph :: shader :: invalidLocation)
  {
   glDeleteObjectARB(id);
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}
#endif
