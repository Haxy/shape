﻿#ifndef SH_GRAPH_RENDER
#define SH_GRAPH_RENDER

#include "GLextensions.h"

#include "../shMath/handler.h"
#include "../shMath/rcgloballyfreelistfx.h"

namespace shGraph
{
 class render
 {
  public:
  class viewport;
  class clearcolour;
  class colourmatrix;
  class modelviewmatrix;
  class projectionmatrix;
  class texturematrix;
  class depthtest;
  const static unsigned int clearStencil = GL_STENCIL_BUFFER_BIT;
  const static unsigned int clearColour = GL_COLOR_BUFFER_BIT;
  const static unsigned int clearDepth = GL_DEPTH_BUFFER_BIT;
  const static unsigned int clearAll = GL_STENCIL_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;
  render(unsigned int flags = clearAll);
  void clear();
  unsigned int& getClearBits();
  unsigned int getClearBits() const;
  unsigned int getId() const;
  const static unsigned char preserveNone = (unsigned char)(0x00);
  const static unsigned char preserveColourMatrix = (unsigned char)(0x01);
  const static unsigned char preserveModelVeiwMatrix = (unsigned char)(0x02);
  const static unsigned char preserveProjectionMatrix = (unsigned char)(0x04);
  const static unsigned char preserveTextureMatrix = (unsigned char)(0x08);
  const static unsigned char defaultPreservation = (unsigned char)(0x06);
  template <unsigned char flags/* = preserveModelVeiwMatrix*/> void bind() const;
  void unbind() const;
  static void unbindCurrent();
  static void flush();
  static viewport getViewport();
  static clearcolour getClearColour();
  static colourmatrix getColourMatrix();
  static modelviewmatrix getModelviewMatrix();
  static projectionmatrix getProjectionMatrix();
  static texturematrix getTextureMatrix();
  static depthtest getDepthTest();
  static render& getCurrent();
  static void setSingleBuffering();
  static void setDoubleBuffering();
  static bool isDoubleBuffering();
  private:
  template <class X> class irenderstorage;
  template <class X,unsigned char flags> class renderstorage;
  unsigned int bits;
  unsigned int id;
  typedef double storagetype;
  static void (*flushfun)(void);
  static unsigned int idcounter;
  static render current;
  static shMath :: rcgloballyfreelistfx<shMath :: handler<irenderstorage<storagetype> > > renderstack;
 };
}

#include "clearcolour.h"
#include "colourmatrix.h"
#include "depthtest.h"
#include "modelviewmatrix.h"
#include "projectionmatrix.h"
#include "texturematrix.h"
#include "viewport.h"
#include "irenderstorage.h"
#include "renderstorage.h"

inline shGraph :: render :: render(unsigned int flags) : bits(flags), id(idcounter++)
{
}

inline void shGraph :: render :: clear()
{
 glClear(bits);
}

inline unsigned int& shGraph :: render :: getClearBits()
{
 return bits;
}

inline unsigned int shGraph :: render :: getClearBits() const
{
 return bits;
}

inline unsigned int shGraph :: render :: getId() const
{
 return id;
}

template <unsigned char flags> void shGraph :: render :: bind() const
{
 renderstack.addEmpty();
 shGraph :: render :: renderstorage<shGraph :: render :: storagetype,flags> temp = (shGraph :: render :: renderstorage<shGraph :: render :: storagetype,flags>)(*(renderstack.getLast()));
 temp.getRender() = current;
 temp.saveState();
 (*(renderstack.pickLast())) = temp;
 glPushAttrib(GL_ALL_ATTRIB_BITS);
}

inline void shGraph :: render :: unbind() const
{
 if (current.id == id)
 {
  unbindCurrent();
 }
}

inline void shGraph :: render :: flush()
{
 flushfun();
}

inline shGraph :: render :: viewport shGraph :: render :: getViewport()
{
 return viewport();
}

inline shGraph :: render :: clearcolour shGraph :: render :: getClearColour()
{
 return clearcolour();
}

inline shGraph :: render :: colourmatrix shGraph :: render :: getColourMatrix()
{
 return colourmatrix();
}

inline shGraph :: render :: modelviewmatrix shGraph :: render :: getModelviewMatrix()
{
 return modelviewmatrix();
}

inline shGraph :: render :: projectionmatrix shGraph :: render :: getProjectionMatrix()
{
 return projectionmatrix();
}

inline shGraph :: render :: texturematrix shGraph :: render :: getTextureMatrix()
{
 return texturematrix();
}

inline shGraph :: render :: depthtest shGraph :: render :: getDepthTest()
{
 return depthtest();
}

inline shGraph :: render& shGraph :: render :: getCurrent()
{
 return current;
}
#endif
