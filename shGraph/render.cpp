#include "render.h"

void shSwapBuf();

void (* shGraph :: render :: flushfun)(void) = (void(*)())(&shSwapBuf);
unsigned int shGraph :: render :: idcounter = 0u;
shGraph :: render shGraph :: render :: current = shGraph :: render();
shMath :: rcgloballyfreelistfx<shMath :: handler<shGraph :: render :: irenderstorage<shGraph :: render :: storagetype> > > shGraph :: render :: renderstack = shMath :: rcgloballyfreelistfx<shMath :: handler<shGraph :: render :: irenderstorage<shGraph :: render :: storagetype> > >();

void shGraph :: render :: setSingleBuffering()
{
 flushfun = (void(*)())(&glFlush);
}

void shGraph :: render :: setDoubleBuffering()
{
 flushfun = (void(*)())(&shSwapBuf);
}

bool shGraph :: render :: isDoubleBuffering()
{
 return (flushfun == (void(*)())(&shSwapBuf));
}

void shGraph :: render :: unbindCurrent()
{
 glPopAttrib();
 shGraph :: render :: irenderstorage<shGraph :: render :: storagetype> temp = *(renderstack.getLast());
 current = temp.getRender();
 temp.restoreState();
 renderstack.remove();
 renderstack.gotoLast();
}

void shSwapBuf()
{
 const static unsigned int swapflag = GL_FRONT ^ GL_BACK;
 unsigned int currentdraw = 0u;
 glGetIntegerv(GL_DRAW_BUFFER,(GLint*)(&currentdraw));
 glReadBuffer(currentdraw);
 glFlush();
 glDrawBuffer(currentdraw ^ swapflag);
}
