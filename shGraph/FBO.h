#ifndef SH_GRAPH_FBO
#define SH_GRAPH_FBO

#include <new>
#include <stdlib.h>

#include "GLextensions.h"

#include "RBO.h"
#include "cubemap.h"
#include "texture.h"
#include "../shMath/stack.h"

namespace shGraph
{
 class FBO
 {
  public:
  FBO();
  template <unsigned char dim> FBO(const texture<dim>& colour,const RBO& depth);
  FBO(const FBO& a);
  ~FBO();
  FBO& operator = (const FBO& a);
  void bind() const;
  void unbind() const;
  unsigned int getId() const;
  unsigned int checkStatus() const;
  void attachDepth(const RBO& depth);
  void attachStencil(const RBO& stencil);
  void attachColour(const RBO& colour, unsigned int buffor = 0u);
  template <unsigned char dim> void attachColour(const texture<dim>& colour,unsigned int buffor = 0u);
  void attachColour(const texture<(unsigned char)(3)>& colour,unsigned int layer,unsigned int buffor = 0u);
  template <unsigned int faceParam> void attachColour(const cubemap :: face<faceParam>& colour,unsigned int buffor = 0u);
  void create();
  static void attachDepthtoCurrent(const RBO& depth);
  static void attachStenciltoCurrent(const RBO& stencil);
  static void attachColourtoCurrent(const RBO& colour, unsigned int buffor = 0u);
  template <unsigned char dim> static void attachColourtoCurrent(const texture<dim>& colour,unsigned int buffor = 0u);
  static void attachColourtoCurrent(const texture<(unsigned char)(3)>& colour,unsigned int layer,unsigned int buffor = 0u);
  template <unsigned int faceParam> static void attachColourtoCurrent(const cubemap :: face<faceParam>& colour,unsigned int buffor = 0u);
  static unsigned int checkCurrentStatus();
  static void unbindCurrent();
  static void unbindAll();
  private:
  void kill();
  unsigned int id;
  unsigned int* counter;
  static FBO current;
  static shMath :: stack<FBO> FBOstack;
 };
 const unsigned int initFBOstackSize = 2u;
}

inline shGraph :: FBO :: FBO() : id(0u), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <unsigned char dim> inline shGraph :: FBO :: FBO(const shGraph :: texture<dim>& colour,const shGraph :: RBO& depth) : id(0u), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 create();
 bind();
 attachDepthtoCurrent(depth);
 attachColourtoCurrent(colour);
 unbindCurrent();
}

inline shGraph :: FBO :: FBO(const shGraph :: FBO& a) : id(a.id), counter(a.counter)
{
 ++(*counter);
}

inline shGraph :: FBO :: ~FBO()
{
 kill();
}

inline shGraph :: FBO& shGraph :: FBO :: operator = (const shGraph :: FBO& a)
{
 if (&a != this)
 {
  kill();
  id = a.id;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

inline void shGraph :: FBO :: bind() const
{
 FBOstack.push(current);
 glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,(current = *this).getId());
 glPushAttrib(GL_VIEWPORT_BIT);
}

inline void shGraph :: FBO :: unbind() const
{
 if (current.id == id)
 {
  unbindCurrent();
 }
}

inline unsigned int shGraph :: FBO :: getId() const
{
 return id;
}

inline unsigned int shGraph :: FBO :: checkStatus() const
{
 unsigned int status;
 bind();
 status = checkCurrentStatus();
 unbind();
 return status;
}

inline void shGraph :: FBO :: attachDepth(const shGraph :: RBO& depth)
{
 bind();
 attachDepthtoCurrent(depth);
 unbindCurrent();
}

inline void shGraph :: FBO :: attachStencil(const shGraph :: RBO& stencil)
{
 bind();
 attachStenciltoCurrent(stencil);
 unbindCurrent();
}

inline void shGraph :: FBO :: attachColour(const shGraph :: RBO& colour, unsigned int buffor)
{
 bind();
 attachColourtoCurrent(colour,buffor);
 unbindCurrent();
}

template <unsigned char dim> inline void shGraph :: FBO :: attachColour(const shGraph :: texture<dim>& colour, unsigned int buffor)
{
 bind();
 attachColourtoCurrent(colour,buffor);
 unbindCurrent();
}

inline void shGraph :: FBO :: attachColour(const shGraph :: texture<(unsigned char)(3)>& colour, unsigned int layer, unsigned int buffor)
{
 bind();
 attachColourtoCurrent(colour,layer,buffor);
 unbindCurrent();
}

template <unsigned int faceParam> inline void shGraph :: FBO :: attachColour(const shGraph :: cubemap :: face<faceParam>& colour, unsigned int buffor)
{
 bind();
 attachColourtoCurrent(colour,buffor);
 unbindCurrent();
}

inline void shGraph :: FBO :: create()
{
 glGenFramebuffersEXT(1, &id);
}

inline void shGraph :: FBO :: attachDepthtoCurrent(const shGraph :: RBO& depth)
{
 glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,GL_DEPTH_ATTACHMENT_EXT,GL_RENDERBUFFER_EXT,depth.getId());
}

inline void shGraph :: FBO :: attachStenciltoCurrent(const shGraph :: RBO& stencil)
{
 glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,GL_STENCIL_ATTACHMENT_EXT,GL_RENDERBUFFER_EXT,stencil.getId());
}

inline void shGraph :: FBO :: attachColourtoCurrent(const shGraph :: RBO& colour, unsigned int buffor)
{
 glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,GL_COLOR_ATTACHMENT0_EXT + buffor,GL_RENDERBUFFER_EXT,colour.getId());
}

namespace shGraph
{
 template <> inline void FBO :: attachColourtoCurrent(const shGraph :: texture<(unsigned char)(1)>& colour,unsigned int buffor)
 {
  glFramebufferTexture1DEXT(GL_FRAMEBUFFER_EXT,GL_COLOR_ATTACHMENT0_EXT + buffor,GL_TEXTURE_1D,colour.getID(),colour.getDescr().getLOM());
 }
}

namespace shGraph
{
 template <> inline void FBO :: attachColourtoCurrent(const shGraph :: texture<(unsigned char)(2)>& colour,unsigned int buffor)
 {
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,GL_COLOR_ATTACHMENT0_EXT + buffor,GL_TEXTURE_2D,colour.getID(),colour.getDescr().getLOM());
 }
}

inline void shGraph :: FBO :: attachColourtoCurrent(const shGraph :: texture<(unsigned char)(3)>& colour,unsigned int layer,unsigned int buffor)
{
 glFramebufferTextureLayerEXT(GL_FRAMEBUFFER_EXT,GL_COLOR_ATTACHMENT0_EXT + buffor,colour.getID(),colour.getDescr().getLOM(),layer);
}

template <unsigned int faceParam> inline void shGraph :: FBO :: attachColourtoCurrent(const shGraph :: cubemap :: face<faceParam>& colour,unsigned int buffor)
{
 glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,GL_COLOR_ATTACHMENT0_EXT + buffor,faceParam,colour.getID(),colour.getDescr().getLOM());
}


inline unsigned int shGraph :: FBO :: checkCurrentStatus()
{
 return glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
}

inline void shGraph :: FBO :: unbindCurrent()
{
 glPopAttrib();
 glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,(current = FBOstack.pop()).getId());
}

inline void shGraph :: FBO :: unbindAll()
{
 const static shGraph :: FBO empty = shGraph :: FBO();
 current = empty;
 FBOstack.clear();
 glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,current.getId());
}

inline void shGraph :: FBO :: kill()
{
 if ((*counter) == 0u)
 {
  if (id != 0u)
  {
   glDeleteFramebuffersEXT(1,&id);
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}
#endif
