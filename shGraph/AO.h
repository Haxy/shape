#ifndef SH_GRAPH_AO
#define SH_GRAPH_AO

#include "GLextensions.h"
#include "attribute.h"
#include "buffer.h"
#include "../shMath/stack.h"

namespace shGraph
{
 class AO
 {
  public:
  AO();
  virtual ~AO();
  void bind() const;
  void unbind() const;
  template <class X> void sendTo(const attribute<X>& a) const;
  template <class X> static void sendCurrentTo(const attribute<X>& a);
  static void unbindCurrent();
  static void unbindAll();
  static AO& getCurrent();
  unsigned int getId() const;
  const void* getData() const;
  const static unsigned int initialStackSize = 2;
  template <class X> static void enableArray(const attribute<X>& a);
  template <class X> static void disableArray(const attribute<X>& a);
  template <class X> static bool isArrayEnabled(const attribute<X>& a);
  AO(const shGraph :: buffer& Id, void* Sendfun, void* Data, unsigned int* Counter = NULL);
  protected:
  static shMath :: stack<AO> stack;
  static AO current;
  buffer id;
  void* sendfun;
  void* data;
  unsigned int* counter;
 };
}

inline shGraph :: AO :: AO() : id(shGraph :: buffer()), sendfun(NULL), data(NULL), counter(NULL)
{
}

inline shGraph :: AO :: ~AO()
{
}

inline void shGraph :: AO :: bind() const
{
 stack.push(current);
 current = *this;
 glBindBufferARB(GL_ARRAY_BUFFER_ARB,id.getId());
}

inline void shGraph :: AO :: unbind()  const
{
 if ((current.id.getId() == id.getId()) && (current.data == data))
 {
  unbindCurrent();
 }
}

template <class X> inline void shGraph :: AO :: sendTo(const shGraph :: attribute<X>& a) const
{
 bind();
 sendCurrentTo(a);
 shGraph :: AO :: unbindCurrent();
}

template <class X> inline void shGraph :: AO :: sendCurrentTo(const shGraph :: attribute<X>& a)
{
 ((void (*)(const shGraph :: attribute<X>&))(shGraph :: AO :: current.sendfun))(a);
}

inline void shGraph :: AO :: unbindCurrent()
{
 current = stack.pop();
 glBindBufferARB(GL_ARRAY_BUFFER_ARB,current.id.getId());
}

inline void shGraph :: AO :: unbindAll()
{
 const static shGraph :: AO empty = shGraph :: AO();
 current = empty;
 stack.clear();
 glBindBufferARB(GL_ARRAY_BUFFER_ARB,0);
}

inline shGraph :: AO& shGraph :: AO :: getCurrent()
{
 return current;
}

inline unsigned int shGraph :: AO :: getId() const
{
 return id.getId();
}

inline const void* shGraph :: AO :: getData() const
{
 return data;
}

template <class T> inline void shGraph :: AO :: enableArray(const shGraph :: attribute<T>& a)
{
 glEnableVertexAttribArray(a.getLocation());
}

template <class T> inline void shGraph :: AO :: disableArray(const shGraph :: attribute<T>& a)
{
 glDisableVertexAttribArray(a.getLocation());
}

template <class T> inline bool shGraph :: AO :: isArrayEnabled(const shGraph :: attribute<T>& a)
{
 unsigned int ans;
 glGetVertexAttribiv(a.getLocation(),GL_VERTEX_ATTRIB_ARRAY_ENABLED,&ans);
 return ans;
}

inline shGraph :: AO :: AO(const shGraph :: buffer& Id, void* Sendfun, void* Data, unsigned int* Counter) : id(Id), sendfun(Sendfun), data(Data), counter(Counter)
{
}
#endif
