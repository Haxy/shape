#include "render.h"
#include "colourmatrix.h"

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator = (const shMath :: mat4<float>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glLoadMatrixf((const float*)(colour));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator = (const shMath :: mat4<double>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glLoadMatrixd((const double*)(colour));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator = (const shMath :: rcmat4<float>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glLoadMatrixf((const float*)(colour));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator = (const shMath :: rcmat4<double>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glLoadMatrixd((const double*)(colour));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator *= (const shMath :: mat4<float>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glMultMatrixf((const float*)(colour));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator *= (const shMath :: mat4<double>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glMultMatrixd((const double*)(colour));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator *= (const shMath :: rcmat4<float>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glMultMatrixf((const float*)(colour));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator *= (const shMath :: rcmat4<double>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glMultMatrixd((const double*)(colour));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator /= (const shMath :: mat4<float>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glMultMatrixf((const float*)(shMath :: inverse(colour)));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator /= (const shMath :: mat4<double>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glMultMatrixd((const double*)(shMath :: inverse(colour)));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator /= (const shMath :: rcmat4<float>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glMultMatrixf((const float*)(shMath :: inverse(colour)));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: colourmatrix& render :: colourmatrix :: operator /= (const shMath :: rcmat4<double>& colour)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_COLOR);
  glMultMatrixd((const double*)(shMath :: inverse(colour)));
  glMatrixMode(mode);
  return *this;
 }
}

