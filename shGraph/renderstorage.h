#ifndef SH_GRAPH_RENDERSTORAGE
#define SH_GRAPH_RENDERSTORAGE

#include <new>
#include <stdlib.h>

#include "render.h"
#include "irenderstorage.h"

#include "../shMath/mat4.h"

namespace shGraph
{
 template <class X> class render :: renderstorage<X,(unsigned char)(0x0F)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getColourMatrix() const;
  const shMath :: mat4<X>& getModelViewMatrix() const;
  const shMath :: mat4<X>& getProjectionMatrix() const;
  const shMath :: mat4<X>& getTextureMatrix() const;
  shMath :: mat4<X>& getColourMatrix();
  shMath :: mat4<X>& getModelViewMatrix();
  shMath :: mat4<X>& getProjectionMatrix();
  shMath :: mat4<X>& getTextureMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x0E)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getModelViewMatrix() const;
  const shMath :: mat4<X>& getProjectionMatrix() const;
  const shMath :: mat4<X>& getTextureMatrix() const;
  shMath :: mat4<X>& getModelViewMatrix();
  shMath :: mat4<X>& getProjectionMatrix();
  shMath :: mat4<X>& getTextureMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x0D)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getColourMatrix() const;
  const shMath :: mat4<X>& getProjectionMatrix() const;
  const shMath :: mat4<X>& getTextureMatrix() const;
  shMath :: mat4<X>& getColourMatrix();
  shMath :: mat4<X>& getProjectionMatrix();
  shMath :: mat4<X>& getTextureMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x0C)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getProjectionMatrix() const;
  const shMath :: mat4<X>& getTextureMatrix() const;
  shMath :: mat4<X>& getProjectionMatrix();
  shMath :: mat4<X>& getTextureMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x0B)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getColourMatrix() const;
  const shMath :: mat4<X>& getModelViewMatrix() const;
  const shMath :: mat4<X>& getTextureMatrix() const;
  shMath :: mat4<X>& getColourMatrix();
  shMath :: mat4<X>& getModelViewMatrix();
  shMath :: mat4<X>& getTextureMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x0A)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getModelViewMatrix() const;
  const shMath :: mat4<X>& getTextureMatrix() const;
  shMath :: mat4<X>& getModelViewMatrix();
  shMath :: mat4<X>& getTextureMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x09)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getColourMatrix() const;
  const shMath :: mat4<X>& getTextureMatrix() const;
  shMath :: mat4<X>& getColourMatrix();
  shMath :: mat4<X>& getTextureMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x08)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getTextureMatrix() const;
  shMath :: mat4<X>& getTextureMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x07)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getColourMatrix() const;
  const shMath :: mat4<X>& getModelViewMatrix() const;
  const shMath :: mat4<X>& getProjectionMatrix() const;
  shMath :: mat4<X>& getColourMatrix();
  shMath :: mat4<X>& getModelViewMatrix();
  shMath :: mat4<X>& getProjectionMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x06)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getModelViewMatrix() const;
  const shMath :: mat4<X>& getProjectionMatrix() const;
  shMath :: mat4<X>& getModelViewMatrix();
  shMath :: mat4<X>& getProjectionMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x05)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getColourMatrix() const;
  const shMath :: mat4<X>& getProjectionMatrix() const;
  shMath :: mat4<X>& getColourMatrix();
  shMath :: mat4<X>& getProjectionMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x04)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getProjectionMatrix() const;
  shMath :: mat4<X>& getProjectionMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x03)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getColourMatrix() const;
  const shMath :: mat4<X>& getModelViewMatrix() const;
  shMath :: mat4<X>& getColourMatrix();
  shMath :: mat4<X>& getModelViewMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x02)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getModelViewMatrix() const;
  shMath :: mat4<X>& getModelViewMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x01)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  const shMath :: mat4<X>& getColourMatrix() const;
  shMath :: mat4<X>& getColourMatrix();
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };

 template <class X> class render :: renderstorage<X,(unsigned char)(0x00)> : public render :: irenderstorage<X>
 {
  public:
  renderstorage(const irenderstorage<X>& a);
  void saveState();
  void restoreState() const;
  private:
  void copy();
 };
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x0E)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x0D)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x0C)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x0B)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x0A)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x09)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x08)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x07)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x06)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x05)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x04)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x03)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x02)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x01)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline shGraph :: render :: renderstorage <X,(unsigned char)(0x00)> :: renderstorage(const shGraph :: render :: irenderstorage<X>& a) : irenderstorage<X>(a)
{
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: getColourMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0D)> :: getColourMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0B)> :: getColourMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x09)> :: getColourMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x07)> :: getColourMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x05)> :: getColourMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x03)> :: getColourMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x01)> :: getColourMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: getModelViewMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0E)> :: getModelViewMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0B)> :: getModelViewMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0A)> :: getModelViewMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x07)> :: getModelViewMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x06)> :: getModelViewMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x03)> :: getModelViewMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x02)> :: getModelViewMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: getProjectionMatrix() const
{
 return *((irenderstorage <X> :: data) + 2);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0E)> :: getProjectionMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0D)> :: getProjectionMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0C)> :: getProjectionMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x07)> :: getProjectionMatrix() const
{
 return *((irenderstorage <X> :: data) + 2);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x06)> :: getProjectionMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x05)> :: getProjectionMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x04)> :: getProjectionMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: getTextureMatrix() const
{
 return *((irenderstorage <X> :: data) + 3);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0E)> :: getTextureMatrix() const
{
 return *((irenderstorage <X> :: data) + 2);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0D)> :: getTextureMatrix() const
{
 return *((irenderstorage <X> :: data) + 2);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0C)> :: getTextureMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0B)> :: getTextureMatrix() const
{
 return *((irenderstorage <X> :: data) + 2);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0A)> :: getTextureMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x09)> :: getTextureMatrix() const
{
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline const shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x08)> :: getTextureMatrix() const
{
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: getColourMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0D)> :: getColourMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0B)> :: getColourMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x09)> :: getColourMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x07)> :: getColourMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x05)> :: getColourMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x03)> :: getColourMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x01)> :: getColourMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: getModelViewMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0E)> :: getModelViewMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0B)> :: getModelViewMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0A)> :: getModelViewMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x07)> :: getModelViewMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x06)> :: getModelViewMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x03)> :: getModelViewMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x02)> :: getModelViewMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: getProjectionMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 2);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0E)> :: getProjectionMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0D)> :: getProjectionMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0C)> :: getProjectionMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x07)> :: getProjectionMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 2);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x06)> :: getProjectionMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x05)> :: getProjectionMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x04)> :: getProjectionMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: getTextureMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 3);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0E)> :: getTextureMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 2);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0D)> :: getTextureMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 2);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0C)> :: getTextureMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0B)> :: getTextureMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 2);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x0A)> :: getTextureMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x09)> :: getTextureMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *((irenderstorage <X> :: data) + 1);
}

template <class X> inline shMath :: mat4<X>& shGraph :: render :: renderstorage <X,(unsigned char)(0x08)> :: getTextureMatrix()
{
 if ((*(irenderstorage <X> :: counter)) != 0u)
 {
  copy();
 }
 return *(irenderstorage <X> :: data);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix().getMatrix(*temp);
 shGraph :: render :: getModelviewMatrix().getMatrix(*(++temp));
 shGraph :: render :: getProjectionMatrix().getMatrix(*(++temp));
 shGraph :: render :: getTextureMatrix().getMatrix(*(++temp));
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0E)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getModelviewMatrix().getMatrix(*temp);
 shGraph :: render :: getProjectionMatrix().getMatrix(*(++temp));
 shGraph :: render :: getTextureMatrix().getMatrix(*(++temp));
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0D)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix().getMatrix(*temp);
 shGraph :: render :: getProjectionMatrix().getMatrix(*(++temp));
 shGraph :: render :: getTextureMatrix().getMatrix(*(++temp));
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0C)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getProjectionMatrix().getMatrix(*temp);
 shGraph :: render :: getTextureMatrix().getMatrix(*(++temp));
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0B)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix().getMatrix(*temp);
 shGraph :: render :: getModelviewMatrix().getMatrix(*(++temp));
 shGraph :: render :: getTextureMatrix().getMatrix(*(++temp));
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0A)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getModelviewMatrix().getMatrix(*temp);
 shGraph :: render :: getTextureMatrix().getMatrix(*(++temp));
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x09)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix().getMatrix(*temp);
 shGraph :: render :: getTextureMatrix().getMatrix(*(++temp));
}

template <class X> inline void shGraph :: render :: renderstorage <X,(unsigned char)(0x08)> :: saveState()
{
 shGraph :: render :: getTextureMatrix().getMatrix(*(irenderstorage <X> :: data));
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x07)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix().getMatrix(*temp);
 shGraph :: render :: getModelviewMatrix().getMatrix(*(++temp));
 shGraph :: render :: getProjectionMatrix().getMatrix(*(++temp));
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x06)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getModelviewMatrix().getMatrix(*temp);
 shGraph :: render :: getProjectionMatrix().getMatrix(*(++temp));
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x05)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix().getMatrix(*temp);
 shGraph :: render :: getProjectionMatrix().getMatrix(*(++temp));
}

template <class X> inline void shGraph :: render :: renderstorage <X,(unsigned char)(0x04)> :: saveState()
{
 shGraph :: render :: getProjectionMatrix().getMatrix(*(irenderstorage <X> :: data));
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x03)> :: saveState()
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix().getMatrix(*temp);
 shGraph :: render :: getModelviewMatrix().getMatrix(*(++temp));
}

template <class X> inline void shGraph :: render :: renderstorage <X,(unsigned char)(0x02)> :: saveState()
{
 shGraph :: render :: getModelviewMatrix().getMatrix(*(irenderstorage <X> :: data));
}

template <class X> inline void shGraph :: render :: renderstorage <X,(unsigned char)(0x01)> :: saveState()
{
 shGraph :: render :: getColourMatrix().getMatrix(*(irenderstorage <X> :: data));
}

template <class X> inline void shGraph :: render :: renderstorage <X,(unsigned char)(0x00)> :: saveState()
{
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix() = *temp;
 shGraph :: render :: getModelviewMatrix() = *(++temp);
 shGraph :: render :: getProjectionMatrix() = *(++temp);
 shGraph :: render :: getTextureMatrix() = *(++temp);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0E)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getModelviewMatrix() = *temp;
 shGraph :: render :: getProjectionMatrix() = *(++temp);
 shGraph :: render :: getTextureMatrix() = *(++temp);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0D)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix() = *temp;
 shGraph :: render :: getProjectionMatrix() = *(++temp);
 shGraph :: render :: getTextureMatrix() = *(++temp);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0C)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getProjectionMatrix() = *temp;
 shGraph :: render :: getTextureMatrix() = *(++temp);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0B)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix() = *temp;
 shGraph :: render :: getModelviewMatrix() = *(++temp);
 shGraph :: render :: getTextureMatrix() = *(++temp);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0A)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getModelviewMatrix() = *temp;
 shGraph :: render :: getTextureMatrix() = *(++temp);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x09)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix() = *temp;
 shGraph :: render :: getTextureMatrix() = *(++temp);
}

template <class X> inline void shGraph :: render :: renderstorage <X,(unsigned char)(0x08)> :: restoreState() const
{
 shGraph :: render :: getTextureMatrix() = *(irenderstorage <X> :: data);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x07)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix() = *temp;
 shGraph :: render :: getModelviewMatrix() = *(++temp);
 shGraph :: render :: getProjectionMatrix() = *(++temp);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x06)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getModelviewMatrix() = *temp;
 shGraph :: render :: getProjectionMatrix() = *(++temp);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x05)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix() = *temp;
 shGraph :: render :: getProjectionMatrix() = *(++temp);
}

template <class X> inline void shGraph :: render :: renderstorage <X,(unsigned char)(0x04)> :: restoreState() const
{
 shGraph :: render :: getProjectionMatrix() = *(irenderstorage <X> :: data);
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x03)> :: restoreState() const
{
 shMath :: mat4<X>* temp = irenderstorage <X> :: data;
 shGraph :: render :: getColourMatrix() = *temp;
 shGraph :: render :: getModelviewMatrix() = *(++temp);
}

template <class X> inline void shGraph :: render :: renderstorage <X,(unsigned char)(0x02)> :: restoreState() const
{
 shGraph :: render :: getModelviewMatrix() = *(irenderstorage <X> :: data);
}

template <class X> inline void shGraph :: render :: renderstorage <X,(unsigned char)(0x01)> :: restoreState() const
{
 shGraph :: render :: getColourMatrix() = *(irenderstorage <X> :: data);
}

template <class X> inline void shGraph :: render :: renderstorage <X,(unsigned char)(0x00)> :: restoreState() const
{
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0F)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
  *(++tempdat2) = *(++tempdato);
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0E)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0D)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0C)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0B)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x0A)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x09)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x08)> :: copy()
{
 shMath :: mat4<X>* tempdat = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat = *(irenderstorage <X> :: data);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x07)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x06)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x05)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x04)> :: copy()
{
 shMath :: mat4<X>* tempdat = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat = *(irenderstorage <X> :: data);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x03)> :: copy()
{
 shMath :: mat4<X>* tempdat2;
 const shMath :: mat4<X>* tempdato = irenderstorage <X> :: data;
 shMath :: mat4<X>* tempdat = tempdat2 = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat2 = *tempdato;
  *(++tempdat2) = *(++tempdato);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x02)> :: copy()
{
 shMath :: mat4<X>* tempdat = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat = *(irenderstorage <X> :: data);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x01)> :: copy()
{
 shMath :: mat4<X>* tempdat = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 try
 {
  *tempdat = *(irenderstorage <X> :: data);
 }
 catch(...)
 {
  delete [] tempdat;
  free(tempcount);
  throw;
 }
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}

template <class X> void shGraph :: render :: renderstorage <X,(unsigned char)(0x00)> :: copy()
{
 shMath :: mat4<X>* tempdat = new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ();
 unsigned int* tempcount = NULL;
 if ((tempcount = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] tempdat;
  throw std :: bad_alloc();
 }
 (*tempcount) = 0u;
 --(*(irenderstorage <X> :: counter));
 irenderstorage <X> :: counter = tempcount;
 irenderstorage <X> :: data = tempdat;
}
#endif

