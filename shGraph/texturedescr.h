#ifndef SH_GRAPH_TEXTUREDESCR
#define SH_GRAPH_TEXTUREDESCR

#include <limits.h>

#include "GLextensions.h"
#include "pixel.h"

namespace shGraph
{
 class itexturedescr
 {
  public:
  itexturedescr();
  virtual void set() const;
  unsigned int getLevel() const;
  unsigned int getInternalFormat() const;
  unsigned int getBorder() const;
  unsigned int getDataFormat() const;
  unsigned int getDataType() const;
  unsigned int getLOM() const;
  unsigned int& getLevel();
  unsigned int& getInternalFormat();
  unsigned int& getBorder();
  unsigned int& getDataFormat();
  unsigned int& getDataType();
  unsigned int& getLOM();
  protected:
  unsigned int level;
  unsigned int intern;
  unsigned int border;
  unsigned int format;
  unsigned int type;
  unsigned int lom;
 };

 template <unsigned char dim> class defaulttexturedescr : public itexturedescr
 {
  public:
  defaulttexturedescr();
  defaulttexturedescr(unsigned int minfilter, unsigned int magfilter, unsigned int wraps);
  unsigned int getMinFilter() const;
  unsigned int getMagFilter() const;
  unsigned int getWraps() const;
  unsigned int& getMinFilter();
  unsigned int& getMagFilter();
  unsigned int& getWraps();
  void set() const;
  private:
  unsigned int minf;
  unsigned int magf;
  unsigned int wrap;
 };

 template <unsigned char dim> class fulltexturedescr : public itexturedescr
 {
  public:
  fulltexturedescr();
  void set() const;
 };

 template <> class fulltexturedescr <1> : public itexturedescr
 {
  public:
  fulltexturedescr();
  unsigned int getEnvMode() const;
  unsigned int getSWrap() const;
  float getMinLod() const;
  float getMaxLod() const;
  unsigned int getMaxLevel() const;
  unsigned int getBaseLevel() const;
  unsigned int getMinFilter() const;
  unsigned int getMagFilter() const;
  template <class X> pixel<X> getEnvColour() const;
  template <class X> pixel<X> getBorderColour() const;
  unsigned int& getEnvMode();
  unsigned int& getSWrap();
  float& getMinLod();
  float& getMaxLod();
  unsigned int& getMaxLevel();
  unsigned int& getBaseLevel();
  unsigned int& getMinFilter();
  unsigned int& getMagFilter();
  template <class X> void setEnvColour(const pixel<X>& col);
  template <class X> void setBorderColour(const pixel<X>& col);
  void set() const;
  private:
  unsigned int emode;
  pixel<float> env;
  unsigned int swrap;
  float minlod;
  float maxlod;
  unsigned int maxl;
  unsigned int basel;
  unsigned int minfil;
  unsigned int magfil;
  pixel<float> bor;
 };

 template <> class fulltexturedescr <2> : public itexturedescr
 {
  public:
  fulltexturedescr();
  unsigned int getEnvMode() const;
  unsigned int getSWrap() const;
  unsigned int getTWrap() const;
  float getMinLod() const;
  float getMaxLod() const;
  unsigned int getMaxLevel() const;
  unsigned int getBaseLevel() const;
  unsigned int getMinFilter() const;
  unsigned int getMagFilter() const;
  bool getGenerateMipmap() const;
  template <class X> pixel<X> getEnvColour() const;
  template <class X> pixel<X> getBorderColour() const;
  unsigned int& getEnvMode();
  unsigned int& getSWrap();
  unsigned int& getTWrap();
  float& getMinLod();
  float& getMaxLod();
  unsigned int& getMaxLevel();
  unsigned int& getBaseLevel();
  unsigned int& getMinFilter();
  unsigned int& getMagFilter();
  bool& getGenerateMipmap();
  template <class X> void setEnvColour(const pixel<X>& col);
  template <class X> void setBorderColour(const pixel<X>& col);
  void set() const;
  private:
  unsigned int emode;
  pixel<float> env;
  unsigned int swrap;
  unsigned int twrap;
  float minlod;
  float maxlod;
  unsigned int maxl;
  unsigned int basel;
  unsigned int minfil;
  unsigned int magfil;
  pixel<float> bor;
  bool genmipmap;
 };

 template <> class fulltexturedescr <3> : public itexturedescr
 {
  public:
  fulltexturedescr();
  unsigned int getEnvMode() const;
  unsigned int getSWrap() const;
  unsigned int getTWrap() const;
  unsigned int getRWrap() const;
  float getMinLod() const;
  float getMaxLod() const;
  unsigned int getMaxLevel() const;
  unsigned int getBaseLevel() const;
  unsigned int getMinFilter() const;
  unsigned int getMagFilter() const;
  template <class X> pixel<X> getEnvColour() const;
  template <class X> pixel<X> getBorderColour() const;
  unsigned int& getEnvMode();
  unsigned int& getSWrap();
  unsigned int& getTWrap();
  unsigned int& getRWrap();
  float& getMinLod();
  float& getMaxLod();
  unsigned int& getMaxLevel();
  unsigned int& getBaseLevel();
  unsigned int& getMinFilter();
  unsigned int& getMagFilter();
  template <class X> void setEnvColour(const pixel<X>& col);
  template <class X> void setBorderColour(const pixel<X>& col);
  void set() const;
  private:
  unsigned int emode;
  pixel<float> env;
  unsigned int swrap;
  unsigned int twrap;
  unsigned int rwrap;
  float minlod;
  float maxlod;
  unsigned int maxl;
  unsigned int basel;
  unsigned int minfil;
  unsigned int magfil;
  pixel<float> bor;
 };
}

inline shGraph :: itexturedescr :: itexturedescr() : level(0), intern(GL_RGBA8), border(0), format(GL_RGBA), type(GL_UNSIGNED_BYTE), lom(0)
{
}

inline void shGraph :: itexturedescr :: set() const
{
}

inline unsigned int shGraph :: itexturedescr :: getLevel() const
{
 return level;
}

inline unsigned int shGraph :: itexturedescr :: getInternalFormat() const
{
 return intern;
}

inline unsigned int shGraph :: itexturedescr :: getBorder() const
{
 return border;
}

inline unsigned int shGraph :: itexturedescr :: getDataFormat() const
{
 return format;
}

inline unsigned int shGraph :: itexturedescr :: getDataType() const
{
 return type;
}

inline unsigned int shGraph :: itexturedescr :: getLOM() const
{
 return lom;
}

inline unsigned int& shGraph :: itexturedescr :: getLevel()
{
 return level;
}

inline unsigned int& shGraph :: itexturedescr :: getInternalFormat()
{
 return intern;
}

inline unsigned int& shGraph :: itexturedescr :: getBorder()
{
 return border;
}

inline unsigned int& shGraph :: itexturedescr :: getDataFormat()
{
 return format;
}

inline unsigned int& shGraph :: itexturedescr :: getDataType()
{
 return type;
}

inline unsigned int& shGraph :: itexturedescr :: getLOM()
{
 return lom;
}

template <unsigned char dim> inline shGraph :: defaulttexturedescr <dim> :: defaulttexturedescr() : minf(GL_NEAREST_MIPMAP_LINEAR), magf(GL_LINEAR), wrap(GL_REPEAT)
{
}

template <unsigned char dim> inline unsigned int shGraph :: defaulttexturedescr <dim> :: getMinFilter() const
{
 return minf;
}

template <unsigned char dim> inline unsigned int shGraph :: defaulttexturedescr <dim> :: getMagFilter() const
{
 return magf;
}

template <unsigned char dim> inline unsigned int shGraph :: defaulttexturedescr <dim> :: getWraps() const
{
 return wrap;
}

template <unsigned char dim> inline unsigned int& shGraph :: defaulttexturedescr <dim> :: getMinFilter()
{
 return minf;
}

template <unsigned char dim> inline unsigned int& shGraph :: defaulttexturedescr <dim> :: getMagFilter()
{
 return magf;
}

template <unsigned char dim> inline unsigned int& shGraph :: defaulttexturedescr <dim> :: getWraps()
{
 return wrap;
}

namespace shGraph
{
 template <> inline void defaulttexturedescr <1> :: set() const
 {
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,minf);
  glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,magf);
 }
}

namespace shGraph
{
 template <> inline void defaulttexturedescr <2> :: set() const
 {
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,minf);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,magf);
 }
}

namespace shGraph
{
 template <> inline void defaulttexturedescr <3> :: set() const
 {
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,wrap);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,minf);
  glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,magf);
 }
}

inline shGraph :: fulltexturedescr <1> :: fulltexturedescr() : emode(GL_MODULATE), env(shGraph :: pixel<float>(0.0f,0.0f,0.0f,0.0f)), swrap(GL_REPEAT), minlod(-1000.0f), maxlod(1000.0), maxl(1000), basel(0), minfil(GL_NEAREST_MIPMAP_LINEAR), magfil(GL_LINEAR), bor(shGraph :: pixel<float>(0.0f,0.0f,0.0f,0.0f))
{
}

inline unsigned int shGraph :: fulltexturedescr <1> :: getEnvMode() const
{
 return emode;
}

inline unsigned int shGraph :: fulltexturedescr <1> :: getSWrap() const
{
 return swrap;
}

inline float shGraph :: fulltexturedescr <1> :: getMinLod() const
{
 return minlod;
}

inline float shGraph :: fulltexturedescr <1> :: getMaxLod() const
{
 return maxlod;
}

inline unsigned int shGraph :: fulltexturedescr <1> :: getMaxLevel() const
{
 return maxl;
}

inline unsigned int shGraph :: fulltexturedescr <1> :: getBaseLevel() const
{
 return basel;
}

inline unsigned int shGraph :: fulltexturedescr <1> :: getMinFilter() const
{
 return minfil;
}

inline unsigned int shGraph :: fulltexturedescr <1> :: getMagFilter() const
{
 return magfil;
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned char> fulltexturedescr <1> :: getEnvColour() const
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  return shGraph :: pixel<unsigned char>((unsigned char)(env.r * ucharmax),(unsigned char)(env.g * ucharmax),(unsigned char)(env.b * ucharmax),(unsigned char)(env.a * ucharmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned short int> fulltexturedescr <1> :: getEnvColour() const
 {
  const static float ushortmax = (float)(USHRT_MAX);
  return shGraph :: pixel<unsigned short int>((unsigned short int)(env.r * ushortmax),(unsigned short int)(env.g * ushortmax),(unsigned short int)(env.b * ushortmax),(unsigned short int)(env.a * ushortmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned int> fulltexturedescr <1> :: getEnvColour() const
 {
  const static float uintmax = (float)(UINT_MAX);
  return shGraph :: pixel<unsigned int>((unsigned int)(env.r * uintmax),(unsigned int)(env.g * uintmax),(unsigned int)(env.b * uintmax),(unsigned int)(env.a * uintmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<float> fulltexturedescr <1> :: getEnvColour() const
 {
  return env;
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<double> fulltexturedescr <1> :: getEnvColour() const
 {
  return shGraph :: pixel<double>((double)(env.r),(double)(env.g),(double)(env.b),(double)(env.a));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned char> fulltexturedescr <1> :: getBorderColour() const
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  return shGraph :: pixel<unsigned char>((unsigned char)(bor.r * ucharmax),(unsigned char)(bor.g * ucharmax),(unsigned char)(bor.b * ucharmax),(unsigned char)(bor.a * ucharmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned short int> fulltexturedescr <1> :: getBorderColour() const
 {
  const static float ushortmax = (float)(USHRT_MAX);
  return shGraph :: pixel<unsigned short int>((unsigned short int)(bor.r * ushortmax),(unsigned short int)(bor.g * ushortmax),(unsigned short int)(bor.b * ushortmax),(unsigned short int)(bor.a * ushortmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned int> fulltexturedescr <1> :: getBorderColour() const
 {
  const static float uintmax = (float)(UINT_MAX);
  return shGraph :: pixel<unsigned int>((unsigned int)(bor.r * uintmax),(unsigned int)(bor.g * uintmax),(unsigned int)(bor.b * uintmax),(unsigned int)(bor.a * uintmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<float> fulltexturedescr <1> :: getBorderColour() const
 {
  return bor;
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<double> fulltexturedescr <1> :: getBorderColour() const
 {
  return shGraph :: pixel<double>((double)(bor.r),(double)(bor.g),(double)(bor.b),(double)(bor.a));
 }
}

inline unsigned int& shGraph :: fulltexturedescr <1> :: getEnvMode()
{
 return emode;
}

inline unsigned int& shGraph :: fulltexturedescr <1> :: getSWrap()
{
 return swrap;
}

inline float& shGraph :: fulltexturedescr <1> :: getMinLod()
{
 return minlod;
}

inline float& shGraph :: fulltexturedescr <1> :: getMaxLod()
{
 return maxlod;
}

inline unsigned int& shGraph :: fulltexturedescr <1> :: getMaxLevel()
{
 return maxl;
}

inline unsigned int& shGraph :: fulltexturedescr <1> :: getBaseLevel()
{
 return basel;
}

inline unsigned int& shGraph :: fulltexturedescr <1> :: getMinFilter()
{
 return minfil;
}

inline unsigned int& shGraph :: fulltexturedescr <1> :: getMagFilter()
{
 return magfil;
}

namespace shGraph
{
 template <> inline void fulltexturedescr <1> :: setEnvColour(const shGraph :: pixel<unsigned char>& col)
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  env = shGraph :: pixel<float>((float)(col.r) / ucharmax,(float)(col.g) / ucharmax,(float)(col.b) / ucharmax,(float)(col.a) / ucharmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <1> :: setEnvColour(const shGraph :: pixel<unsigned short int>& col)
 {
  const static float ushortmax = (float)(USHRT_MAX);
  env = shGraph :: pixel<float>((float)(col.r) / ushortmax,(float)(col.g) / ushortmax,(float)(col.b) / ushortmax,(float)(col.a) / ushortmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <1> :: setEnvColour(const shGraph :: pixel<unsigned int>& col)
 {
  const static float uintmax = (float)(UINT_MAX);
  env = shGraph :: pixel<float>((float)(col.r) / uintmax,(float)(col.g) / uintmax,(float)(col.b) / uintmax,(float)(col.a) / uintmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <1> :: setEnvColour(const shGraph :: pixel<float>& col)
 {
  env = col;
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <1> :: setEnvColour(const shGraph :: pixel<double>& col)
 {
  env = shGraph :: pixel<float>((float)(col.r),(float)(col.g),(float)(col.b),(float)(col.a));
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <1> :: setBorderColour(const shGraph :: pixel<unsigned char>& col)
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  bor = shGraph :: pixel<float>((float)(col.r) / ucharmax,(float)(col.g) / ucharmax,(float)(col.b) / ucharmax,(float)(col.a) / ucharmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <1> :: setBorderColour(const shGraph :: pixel<unsigned short int>& col)
 {
  const static float ushortmax = (float)(USHRT_MAX);
  bor = shGraph :: pixel<float>((float)(col.r) / ushortmax,(float)(col.g) / ushortmax,(float)(col.b) / ushortmax,(float)(col.a) / ushortmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <1> :: setBorderColour(const shGraph :: pixel<unsigned int>& col)
 {
  const static float uintmax = (float)(UINT_MAX);
  bor = shGraph :: pixel<float>((float)(col.r) / uintmax,(float)(col.g) / uintmax,(float)(col.b) / uintmax,(float)(col.a) / uintmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <1> :: setBorderColour(const shGraph :: pixel<float>& col)
 {
  bor = col;
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <1> :: setBorderColour(const shGraph :: pixel<double>& col)
 {
  bor = shGraph :: pixel<float>((float)(col.r),(float)(col.g),(float)(col.b),(float)(col.a));
 }
}

inline void shGraph :: fulltexturedescr <1> :: set() const
{
 glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,emode);
 glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,(float*)(&env));
 glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_WRAP_S,swrap);
 glTexParameterf(GL_TEXTURE_1D,GL_TEXTURE_MIN_LOD,minlod);
 glTexParameterf(GL_TEXTURE_1D,GL_TEXTURE_MAX_LOD,maxlod);
 glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAX_LEVEL,maxl);
 glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_BASE_LEVEL,basel);
 glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,minfil);
 glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,magfil);
 glTexParameterfv(GL_TEXTURE_1D,GL_TEXTURE_BORDER_COLOR,(float*)(&bor));
}

inline shGraph :: fulltexturedescr <2> :: fulltexturedescr() : emode(GL_MODULATE), env(shGraph :: pixel<float>(0.0f,0.0f,0.0f,0.0f)), swrap(GL_REPEAT), twrap(GL_REPEAT), minlod(-1000.0f), maxlod(1000.0), maxl(1000), basel(0), minfil(GL_NEAREST_MIPMAP_LINEAR), magfil(GL_LINEAR), bor(shGraph :: pixel<float>(0.0f,0.0f,0.0f,0.0f)), genmipmap(true)
{
}

inline unsigned int shGraph :: fulltexturedescr <2> :: getEnvMode() const
{
 return emode;
}

inline unsigned int shGraph :: fulltexturedescr <2> :: getSWrap() const
{
 return swrap;
}

inline unsigned int shGraph :: fulltexturedescr <2> :: getTWrap() const
{
 return twrap;
}

inline float shGraph :: fulltexturedescr <2> :: getMinLod() const
{
 return minlod;
}

inline float shGraph :: fulltexturedescr <2> :: getMaxLod() const
{
 return maxlod;
}

inline unsigned int shGraph :: fulltexturedescr <2> :: getMaxLevel() const
{
 return maxl;
}

inline unsigned int shGraph :: fulltexturedescr <2> :: getBaseLevel() const
{
 return basel;
}

inline unsigned int shGraph :: fulltexturedescr <2> :: getMinFilter() const
{
 return minfil;
}

inline unsigned int shGraph :: fulltexturedescr <2> :: getMagFilter() const
{
 return magfil;
}

inline bool shGraph :: fulltexturedescr <2> :: getGenerateMipmap() const
{
 return genmipmap;
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned char> fulltexturedescr <2> :: getEnvColour() const
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  return shGraph :: pixel<unsigned char>((unsigned char)(env.r * ucharmax),(unsigned char)(env.g * ucharmax),(unsigned char)(env.b * ucharmax),(unsigned char)(env.a * ucharmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned short int> fulltexturedescr <2> :: getEnvColour() const
 {
  const static float ushortmax = (float)(USHRT_MAX);
  return shGraph :: pixel<unsigned short int>((unsigned short int)(env.r * ushortmax),(unsigned short int)(env.g * ushortmax),(unsigned short int)(env.b * ushortmax),(unsigned short int)(env.a * ushortmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned int> fulltexturedescr <2> :: getEnvColour() const
 {
  const static float uintmax = (float)(UINT_MAX);
  return shGraph :: pixel<unsigned int>((unsigned int)(env.r * uintmax),(unsigned int)(env.g * uintmax),(unsigned int)(env.b * uintmax),(unsigned int)(env.a * uintmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<float> fulltexturedescr <2> :: getEnvColour() const
 {
  return env;
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<double> fulltexturedescr <2> :: getEnvColour() const
 {
  return shGraph :: pixel<double>((double)(env.r),(double)(env.g),(double)(env.b),(double)(env.a));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned char> fulltexturedescr <2> :: getBorderColour() const
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  return shGraph :: pixel<unsigned char>((unsigned char)(bor.r * ucharmax),(unsigned char)(bor.g * ucharmax),(unsigned char)(bor.b * ucharmax),(unsigned char)(bor.a * ucharmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned short int> fulltexturedescr <2> :: getBorderColour() const
 {
  const static float ushortmax = (float)(USHRT_MAX);
  return shGraph :: pixel<unsigned short int>((unsigned short int)(bor.r * ushortmax),(unsigned short int)(bor.g * ushortmax),(unsigned short int)(bor.b * ushortmax),(unsigned short int)(bor.a * ushortmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned int> fulltexturedescr <2> :: getBorderColour() const
 {
  const static float uintmax = (float)(UINT_MAX);
  return shGraph :: pixel<unsigned int>((unsigned int)(bor.r * uintmax),(unsigned int)(bor.g * uintmax),(unsigned int)(bor.b * uintmax),(unsigned int)(bor.a * uintmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<float> fulltexturedescr <2> :: getBorderColour() const
 {
  return bor;
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<double> fulltexturedescr <2> :: getBorderColour() const
 {
  return shGraph :: pixel<double>((double)(bor.r),(double)(bor.g),(double)(bor.b),(double)(bor.a));
 }
}

inline unsigned int& shGraph :: fulltexturedescr <2> :: getEnvMode()
{
 return emode;
}

inline unsigned int& shGraph :: fulltexturedescr <2> :: getSWrap()
{
 return swrap;
}

inline unsigned int& shGraph :: fulltexturedescr <2> :: getTWrap()
{
 return twrap;
}

inline float& shGraph :: fulltexturedescr <2> :: getMinLod()
{
 return minlod;
}

inline float& shGraph :: fulltexturedescr <2> :: getMaxLod()
{
 return maxlod;
}

inline unsigned int& shGraph :: fulltexturedescr <2> :: getMaxLevel()
{
 return maxl;
}

inline unsigned int& shGraph :: fulltexturedescr <2> :: getBaseLevel()
{
 return basel;
}

inline unsigned int& shGraph :: fulltexturedescr <2> :: getMinFilter()
{
 return minfil;
}

inline unsigned int& shGraph :: fulltexturedescr <2> :: getMagFilter()
{
 return magfil;
}

inline bool& shGraph :: fulltexturedescr <2> :: getGenerateMipmap()
{
 return genmipmap;
}

namespace shGraph
{
 template <> inline void fulltexturedescr <2> :: setEnvColour(const shGraph :: pixel<unsigned char>& col)
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  env = shGraph :: pixel<float>((float)(col.r) / ucharmax,(float)(col.g) / ucharmax,(float)(col.b) / ucharmax,(float)(col.a) / ucharmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <2> :: setEnvColour(const shGraph :: pixel<unsigned short int>& col)
 {
  const static float ushortmax = (float)(USHRT_MAX);
  env = shGraph :: pixel<float>((float)(col.r) / ushortmax,(float)(col.g) / ushortmax,(float)(col.b) / ushortmax,(float)(col.a) / ushortmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <2> :: setEnvColour(const shGraph :: pixel<unsigned int>& col)
 {
  const static float uintmax = (float)(UINT_MAX);
  env = shGraph :: pixel<float>((float)(col.r) / uintmax,(float)(col.g) / uintmax,(float)(col.b) / uintmax,(float)(col.a) / uintmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <2> :: setEnvColour(const shGraph :: pixel<float>& col)
 {
  env = col;
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <2> :: setEnvColour(const shGraph :: pixel<double>& col)
 {
  env = shGraph :: pixel<float>((float)(col.r),(float)(col.g),(float)(col.b),(float)(col.a));
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <2> :: setBorderColour(const shGraph :: pixel<unsigned char>& col)
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  bor = shGraph :: pixel<float>((float)(col.r) / ucharmax,(float)(col.g) / ucharmax,(float)(col.b) / ucharmax,(float)(col.a) / ucharmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <2> :: setBorderColour(const shGraph :: pixel<unsigned short int>& col)
 {
  const static float ushortmax = (float)(USHRT_MAX);
  bor = shGraph :: pixel<float>((float)(col.r) / ushortmax,(float)(col.g) / ushortmax,(float)(col.b) / ushortmax,(float)(col.a) / ushortmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <2> :: setBorderColour(const shGraph :: pixel<unsigned int>& col)
 {
  const static float uintmax = (float)(UINT_MAX);
  bor = shGraph :: pixel<float>((float)(col.r) / uintmax,(float)(col.g) / uintmax,(float)(col.b) / uintmax,(float)(col.a) / uintmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <2> :: setBorderColour(const shGraph :: pixel<float>& col)
 {
  bor = col;
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <2> :: setBorderColour(const shGraph :: pixel<double>& col)
 {
  bor = shGraph :: pixel<float>((float)(col.r),(float)(col.g),(float)(col.b),(float)(col.a));
 }
}

inline void shGraph :: fulltexturedescr <2> :: set() const
{
 glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,emode);
 glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,(float*)(&env));
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,swrap);
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,twrap);
 glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_LOD,minlod);
 glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAX_LOD,maxlod);
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAX_LEVEL,maxl);
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_BASE_LEVEL,basel);
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,minfil);
 glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,magfil);
 glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, (genmipmap) ? GL_TRUE : GL_FALSE);
 glTexParameterfv(GL_TEXTURE_2D,GL_TEXTURE_BORDER_COLOR,(float*)(&bor));
}

inline shGraph :: fulltexturedescr <3> :: fulltexturedescr() : emode(GL_MODULATE), env(shGraph :: pixel<float>(0.0f,0.0f,0.0f,0.0f)), swrap(GL_REPEAT), twrap(GL_REPEAT), minlod(-1000.0f), maxlod(1000.0), maxl(1000), basel(0), minfil(GL_NEAREST_MIPMAP_LINEAR), magfil(GL_LINEAR), bor(shGraph :: pixel<float>(0.0f,0.0f,0.0f,0.0f))
{
}

inline unsigned int shGraph :: fulltexturedescr <3> :: getEnvMode() const
{
 return emode;
}

inline unsigned int shGraph :: fulltexturedescr <3> :: getSWrap() const
{
 return swrap;
}

inline unsigned int shGraph :: fulltexturedescr <3> :: getTWrap() const
{
 return twrap;
}

inline unsigned int shGraph :: fulltexturedescr <3> :: getRWrap() const
{
 return rwrap;
}

inline float shGraph :: fulltexturedescr <3> :: getMinLod() const
{
 return minlod;
}

inline float shGraph :: fulltexturedescr <3> :: getMaxLod() const
{
 return maxlod;
}

inline unsigned int shGraph :: fulltexturedescr <3> :: getMaxLevel() const
{
 return maxl;
}

inline unsigned int shGraph :: fulltexturedescr <3> :: getBaseLevel() const
{
 return basel;
}

inline unsigned int shGraph :: fulltexturedescr <3> :: getMinFilter() const
{
 return minfil;
}

inline unsigned int shGraph :: fulltexturedescr <3> :: getMagFilter() const
{
 return magfil;
}

namespace  shGraph
{
 template <> inline shGraph :: pixel<unsigned char> fulltexturedescr <3> :: getEnvColour() const
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  return shGraph :: pixel<unsigned char>((unsigned char)(env.r * ucharmax),(unsigned char)(env.g * ucharmax),(unsigned char)(env.b * ucharmax),(unsigned char)(env.a * ucharmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned short int> fulltexturedescr <3> :: getEnvColour() const
 {
  const static float ushortmax = (float)(USHRT_MAX);
  return shGraph :: pixel<unsigned short int>((unsigned short int)(env.r * ushortmax),(unsigned short int)(env.g * ushortmax),(unsigned short int)(env.b * ushortmax),(unsigned short int)(env.a * ushortmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned int> fulltexturedescr <3> :: getEnvColour() const
 {
  const static float uintmax = (float)(UINT_MAX);
  return shGraph :: pixel<unsigned int>((unsigned int)(env.r * uintmax),(unsigned int)(env.g * uintmax),(unsigned int)(env.b * uintmax),(unsigned int)(env.a * uintmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<float> fulltexturedescr <3> :: getEnvColour() const
 {
  return env;
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<double> fulltexturedescr <3> :: getEnvColour() const
 {
  return shGraph :: pixel<double>((double)(env.r),(double)(env.g),(double)(env.b),(double)(env.a));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned char> fulltexturedescr <3> :: getBorderColour() const
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  return shGraph :: pixel<unsigned char>((unsigned char)(bor.r * ucharmax),(unsigned char)(bor.g * ucharmax),(unsigned char)(bor.b * ucharmax),(unsigned char)(bor.a * ucharmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned short int> fulltexturedescr <3> :: getBorderColour() const
 {
  const static float ushortmax = (float)(USHRT_MAX);
  return shGraph :: pixel<unsigned short int>((unsigned short int)(bor.r * ushortmax),(unsigned short int)(bor.g * ushortmax),(unsigned short int)(bor.b * ushortmax),(unsigned short int)(bor.a * ushortmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<unsigned int> fulltexturedescr <3> :: getBorderColour() const
 {
  const static float uintmax = (float)(UINT_MAX);
  return shGraph :: pixel<unsigned int>((unsigned int)(bor.r * uintmax),(unsigned int)(bor.g * uintmax),(unsigned int)(bor.b * uintmax),(unsigned int)(bor.a * uintmax));
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<float> fulltexturedescr <3> :: getBorderColour() const
 {
  return bor;
 }
}

namespace shGraph
{
 template <> inline shGraph :: pixel<double> fulltexturedescr <3> :: getBorderColour() const
 {
  return shGraph :: pixel<double>((double)(bor.r),(double)(bor.g),(double)(bor.b),(double)(bor.a));
 }
}

inline unsigned int& shGraph :: fulltexturedescr <3> :: getEnvMode()
{
 return emode;
}

inline unsigned int& shGraph :: fulltexturedescr <3> :: getSWrap()
{
 return swrap;
}

inline unsigned int& shGraph :: fulltexturedescr <3> :: getTWrap()
{
 return twrap;
}

inline unsigned int& shGraph :: fulltexturedescr <3> :: getRWrap()
{
 return rwrap;
}

inline float& shGraph :: fulltexturedescr <3> :: getMinLod()
{
 return minlod;
}

inline float& shGraph :: fulltexturedescr <3> :: getMaxLod()
{
 return maxlod;
}

inline unsigned int& shGraph :: fulltexturedescr <3> :: getMaxLevel()
{
 return maxl;
}

inline unsigned int& shGraph :: fulltexturedescr <3> :: getBaseLevel()
{
 return basel;
}

inline unsigned int& shGraph :: fulltexturedescr <3> :: getMinFilter()
{
 return minfil;
}

inline unsigned int& shGraph :: fulltexturedescr <3> :: getMagFilter()
{
 return magfil;
}

namespace shGraph
{
 template <> inline void fulltexturedescr <3> :: setEnvColour(const shGraph :: pixel<unsigned char>& col)
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  env = shGraph :: pixel<float>((float)(col.r) / ucharmax,(float)(col.g) / ucharmax,(float)(col.b) / ucharmax,(float)(col.a) / ucharmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <3> :: setEnvColour(const shGraph :: pixel<unsigned short int>& col)
 {
  const static float ushortmax = (float)(USHRT_MAX);
  env = shGraph :: pixel<float>((float)(col.r) / ushortmax,(float)(col.g) / ushortmax,(float)(col.b) / ushortmax,(float)(col.a) / ushortmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <3> :: setEnvColour(const shGraph :: pixel<unsigned int>& col)
 {
  const static float uintmax = (float)(UINT_MAX);
  env = shGraph :: pixel<float>((float)(col.r) / uintmax,(float)(col.g) / uintmax,(float)(col.b) / uintmax,(float)(col.a) / uintmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <3> :: setEnvColour(const shGraph :: pixel<float>& col)
 {
  env = col;
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <3> :: setEnvColour(const shGraph :: pixel<double>& col)
 {
  env = shGraph :: pixel<float>((float)(col.r),(float)(col.g),(float)(col.b),(float)(col.a));
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <3> :: setBorderColour(const shGraph :: pixel<unsigned char>& col)
 {
  const static float ucharmax = (float)(UCHAR_MAX);
  bor = shGraph :: pixel<float>((float)(col.r) / ucharmax,(float)(col.g) / ucharmax,(float)(col.b) / ucharmax,(float)(col.a) / ucharmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <3> :: setBorderColour(const shGraph :: pixel<unsigned short int>& col)
 {
  const static float ushortmax = (float)(USHRT_MAX);
  bor = shGraph :: pixel<float>((float)(col.r) / ushortmax,(float)(col.g) / ushortmax,(float)(col.b) / ushortmax,(float)(col.a) / ushortmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <3> :: setBorderColour(const shGraph :: pixel<unsigned int>& col)
 {
  const static float uintmax = (float)(UINT_MAX);
  bor = shGraph :: pixel<float>((float)(col.r) / uintmax,(float)(col.g) / uintmax,(float)(col.b) / uintmax,(float)(col.a) / uintmax);
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <3> :: setBorderColour(const shGraph :: pixel<float>& col)
 {
  bor = col;
 }
}

namespace shGraph
{
 template <> inline void fulltexturedescr <3> :: setBorderColour(const shGraph :: pixel<double>& col)
 {
  bor = shGraph :: pixel<float>((float)(col.r),(float)(col.g),(float)(col.b),(float)(col.a));
 }
}

inline void shGraph :: fulltexturedescr <3> :: set() const
{
 glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,emode);
 glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR,(float*)(&env));
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,swrap);
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,twrap);
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,rwrap);
 glTexParameterf(GL_TEXTURE_3D,GL_TEXTURE_MIN_LOD,minlod);
 glTexParameterf(GL_TEXTURE_3D,GL_TEXTURE_MAX_LOD,maxlod);
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAX_LEVEL,maxl);
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_BASE_LEVEL,basel);
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,minfil);
 glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,magfil);
 glTexParameterfv(GL_TEXTURE_3D,GL_TEXTURE_BORDER_COLOR,(float*)(&bor));
}
#endif

