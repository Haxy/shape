#include "render.h"
#include "texturematrix.h"

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator = (const shMath :: mat4<float>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glLoadMatrixf((const float*)(tex));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator = (const shMath :: mat4<double>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glLoadMatrixd((const double*)(tex));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator = (const shMath :: rcmat4<float>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glLoadMatrixf((const float*)(tex));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator = (const shMath :: rcmat4<double>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glLoadMatrixd((const double*)(tex));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator *= (const shMath :: mat4<float>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glMultMatrixf((const float*)(tex));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator *= (const shMath :: mat4<double>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glMultMatrixd((const double*)(tex));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator *= (const shMath :: rcmat4<float>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glMultMatrixf((const float*)(tex));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator *= (const shMath :: rcmat4<double>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glMultMatrixd((const double*)(tex));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator /= (const shMath :: mat4<float>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glMultMatrixf((const float*)(shMath :: inverse(tex)));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator /= (const shMath :: mat4<double>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glMultMatrixd((const double*)(shMath :: inverse(tex)));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator /= (const shMath :: rcmat4<float>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glMultMatrixf((const float*)(shMath :: inverse(tex)));
  glMatrixMode(mode);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: texturematrix& render :: texturematrix :: operator /= (const shMath :: rcmat4<double>& tex)
 {
  static unsigned int mode = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&mode));
  glMatrixMode(GL_TEXTURE);
  glMultMatrixd((const double*)(shMath :: inverse(tex)));
  glMatrixMode(mode);
  return *this;
 }
}

