#ifndef SH_GRAPH_IMAGE
#define SH_GRAPH_IMAGE

#include <new>
#include <stdlib.h>

#include "../shMath/vectorNfx.h"

namespace shGraph
{
 template <class T, unsigned char dim = (unsigned char)(2)> class image;

 template <class T> class image <T, (unsigned char)(1)>
 {
  public:
  image();
  image(const shMath :: vectorNfx<1u,unsigned short int>& dimension);
  image(const unsigned short int& dimension);
  image(const image& a);
  ~image();
  image& operator = (const image& a);
  const T& operator [] (const unsigned short int& a) const;
  T& operator [] (const unsigned short int& a);
  operator const void* () const;
  operator void* ();
  operator const T* () const;
  operator T* ();
  const shMath :: vectorNfx<1u,unsigned short int> getDimensions() const;
  const unsigned short int getLength() const;
  const unsigned int getTotal() const;
  private:
  void copy();
  void kill();
  unsigned short int dim;
  T* origin;
  T* data;
  unsigned int* counter;
  friend class image <T, (unsigned char)(2)>;
 };

 template <class T> class image <T, (unsigned char)(2)>
 {
  public:
  image();
  image(const shMath :: vectorNfx<2u,unsigned short int>& dimensions);
  image(const unsigned short int& w, const unsigned short int& h);
  image(const image& a);
  ~image();
  image& operator = (const image& a);
  const image<T, (unsigned char)(1)> operator [] (const unsigned short int& a) const;
  image<T, (unsigned char)(1)> operator [] (const unsigned short int& a);
  operator const void* () const;
  operator void* ();
  operator const T* () const;
  operator T* ();
  const shMath :: vectorNfx<2u,unsigned short int> getDimensions() const;
  const unsigned short int getWidth() const;
  const unsigned short int getHeight() const;
  const unsigned int getTotal() const;
  private:
  void copy();
  void kill();
  unsigned short int width;
  unsigned short int height;
  T* origin;
  T* data;
  unsigned int* counter;
  friend class image <T, (unsigned char)(3)>;
 };
 
 template <class T> class image <T, (unsigned char)(3)>
 {
  public:
  image();
  image(const shMath :: vectorNfx<3u,unsigned short int>& dimensions);
  image(const unsigned short int& width, const unsigned short int& height, const unsigned short int& depth);
  image(const image& a);
  ~image();
  image& operator = (const image& a);
  const image<T, (unsigned char)(2)> operator [] (const unsigned short int& a) const;
  image<T, (unsigned char)(2)> operator [] (const unsigned short int& a);
  operator const void* () const;
  operator void* ();
  operator const T* () const;
  operator T* ();
  const shMath :: vectorNfx<3u,unsigned short int>& getDimensions() const;
  const unsigned short int getWidth() const;
  const unsigned short int getHeight() const;
  const unsigned short int getDepth() const;
  const unsigned int getTotal() const;
  private:
  void copy();
  void kill();
  shMath :: vectorNfx<3u,unsigned short int> dims;
  unsigned int subspacesize;
  unsigned int total;
  T* origin;
  T* data;
  unsigned int* counter;
  friend class image <T, (unsigned char)(4)>;
 };

 template <class T, unsigned char dim> class image
 {
  public:
  image();
  image(const shMath :: vectorNfx<(unsigned int)(dim),unsigned short int>& dimensions);
  image(const image& a);
  ~image();
  image& operator = (const image& a);
  const image<T,(dim - 1)> operator [] (const unsigned short int& a) const;
  image<T,(dim - 1)> operator [] (const unsigned short int& a);
  operator const void* () const;
  operator void* ();
  operator const T* () const;
  operator T* ();
  const shMath :: vectorNfx<(unsigned int)(dim),unsigned short int>& getDimensions() const;
  const unsigned int getTotal() const;
  private:
  void copy();
  void kill();
  shMath :: vectorNfx<(unsigned int)(dim),unsigned short int> dims;
  unsigned int subspacesize;
  unsigned int total;
  T* origin;
  T* data;
  unsigned int* counter;
  friend class image <T, dim + 1>;
 };

 template <class T> image<T, (unsigned char)(2)> loadMonoColour(unsigned short int width, unsigned short int height, const T& colour);
 template <class T, unsigned char dim> image<T, dim> loadMonoColour(const shMath :: vectorNfx<dim, unsigned short int>& dimensions, const T& colour);
 template <class X, class Y, unsigned char dim> image<X, dim> localFilter(X (* const & filter)(const Y&), const image<Y, dim>& img);
 template <class X, class Y, unsigned char dim> image<X, dim> localFilter(X (* const & filter)(const Y&), const image<Y, dim>& img, image<X, dim>& helper);
}

template <class T, unsigned char dim> inline shGraph :: image <T, dim> :: image() : dims(shMath :: zero(shMath :: vectorNfx<(unsigned int)(dim), unsigned short int>())), subspacesize(0u), total(0u), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> inline shGraph :: image <T, (unsigned char)(3)> :: image() : dims(shMath :: zero(shMath :: vectorNfx<3u, unsigned short int>())), subspacesize(0u), total(0u), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> inline shGraph :: image <T, (unsigned char)(2)> :: image() : width((const unsigned short int)(0)), height((const unsigned short int)(0)), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> inline shGraph :: image <T, (unsigned char)(1)> :: image() : dim((const unsigned short int)(0)), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T> shGraph :: image <T, (unsigned char)(1)> :: image(const shMath :: vectorNfx<1u, unsigned short int>& dimension) : dim(*((const unsigned short int*)(dimension))), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  origin = data = new T [dim] ();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> shGraph :: image <T, (unsigned char)(2)> :: image(const shMath :: vectorNfx<2u, unsigned short int>& dimensions) : width(dimensions[0u]), height(dimensions[1u]), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  origin = data = new T [(const unsigned int)(width) * (const unsigned int)(height)] ();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> shGraph :: image <T, (unsigned char)(3)> :: image(const shMath :: vectorNfx<3u, unsigned short int>& dimensions) : dims(dimensions), subspacesize(0u), total(0u), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 subspacesize = (const unsigned int)(*dimensions) * (const unsigned int)(dimensions[1u]);
 total = subspacesize * (const unsigned int)(dimensions[2u]);
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  origin = data = new T [total] ();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T, unsigned char dim> shGraph :: image <T, dim> :: image(const shMath :: vectorNfx<(unsigned int)(dim), unsigned short int>& dimensions) : dims(dimensions), subspacesize(0u), total(0u), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 unsigned int i = 1u;
 const unsigned short int* temp = (const unsigned short int*)(dimensions);
 total = (const unsigned int)(*temp);
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 for(;i < dim;++i,total *= ((const unsigned int)(*++temp)));
 subspacesize = total / (dimensions[(unsigned int)(dim) - 1u]);
 try
 {
  origin = data = new T [total] ();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> shGraph :: image <T, (unsigned char)(3)> :: image(const unsigned short int& width, const unsigned short int& height, const unsigned short int& depth) : dims(shMath :: vec3<unsigned short int>(width, height, depth)), subspacesize((const unsigned int)(width) * (const unsigned int)(height)), total(0u), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  origin = data = new T [(total = subspacesize * (const unsigned int)(depth))] ();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> shGraph :: image <T, (unsigned char)(2)> :: image(const unsigned short int& w, const unsigned short int& h) : width(w), height(h), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  origin = data = new T [(const unsigned int)(width) * (const unsigned int)(height)] ();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T> shGraph :: image <T, (unsigned char)(1)> :: image(const unsigned short int& dimension) : dim(dimension), origin(NULL), data(NULL), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
 try
 {
  origin = data = new T [dim] ();
 }
 catch(...)
 {
  free(counter);
  throw;
 }
}

template <class T, unsigned char dim> inline shGraph :: image <T, dim> :: image(const shGraph :: image<T, dim>& a) : dims(a.dims), subspacesize(a.subspacesize), total(a.total), origin(a.origin), data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T> inline shGraph :: image <T, (unsigned char)(3)> :: image(const shGraph :: image<T, (unsigned char)(3)>& a) : dims(a.dims), subspacesize(a.subspacesize), total(a.total), origin(a.origin), data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T> inline shGraph :: image <T, (unsigned char)(2)> :: image(const shGraph :: image<T, (unsigned char)(2)>& a) : width(a.width), height(a.height), origin(a.origin), data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T> inline shGraph :: image <T, (unsigned char)(1)> :: image(const shGraph :: image<T, (unsigned char)(1)>& a) : dim(a.dim), origin(a.origin), data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class T, unsigned char dim> inline shGraph :: image <T, dim> :: ~image()
{
 kill();
}

template <class T> inline shGraph :: image <T, (unsigned char)(3)> :: ~image()
{
 kill();
}

template <class T> inline shGraph :: image <T, (unsigned char)(2)> :: ~image()
{
 kill();
}

template <class T> inline shGraph :: image <T, (unsigned char)(1)> :: ~image()
{
 kill();
}

template <class T, unsigned char dim> inline shGraph :: image<T, dim>& shGraph :: image <T, dim> :: operator = (const shGraph :: image<T, dim>& a)
{
 if (&a != this)
 {
  kill();
  dims = a.dims;
  subspacesize = a.subspacesize;
  total = a.total;
  origin = a.origin;
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline shGraph :: image<T, (unsigned char)(3)>& shGraph :: image <T, (unsigned char)(3)> :: operator = (const shGraph :: image<T, (unsigned char)(3)>& a)
{
 if (&a != this)
 {
  kill();
  dims = a.dims;
  subspacesize = a.subspacesize;
  total = a.total;
  origin = a.origin;
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline shGraph :: image<T, (unsigned char)(2)>& shGraph :: image <T, (unsigned char)(2)> :: operator = (const shGraph :: image<T, (unsigned char)(2)>& a)
{
 if (&a != this)
 {
  kill();
  width = a.width;
  height = a.height;
  origin = a.origin;
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T> inline shGraph :: image<T, (unsigned char)(1)>& shGraph :: image <T, (unsigned char)(1)> :: operator = (const shGraph :: image<T, (unsigned char)(1)>& a)
{
 if (&a != this)
 {
  kill();
  dim = a.dim;
  origin = a.origin;
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class T, unsigned char dim> inline const shGraph :: image<T, (dim - 1)> shGraph :: image <T, dim> :: operator [] (const unsigned short int& a) const
{
 char space[sizeof(shGraph :: image<T, (dim - 1)>)];
 const unsigned short int* tempDims = (const unsigned short int*)(dims);
 shGraph :: image<T, (dim - 1)>* ans = ((shGraph :: image<T, (dim - 1)>*)(&space));
 ans->dims = *((const unsigned short int (*) [dim - 1])(tempDims));
 ans->subspacesize = subspacesize / (const unsigned int)(*((const unsigned short int*)(dims)));
 ans->total = subspacesize;
 ans->origin = origin;
 ans->data = data + ((const unsigned int)(a) * subspacesize);
 ans->counter = counter;
 ++(*counter);
 return *((const shGraph :: image<T, (dim - 1)>*)(ans));
}

template <class T> inline const shGraph :: image<T, (unsigned char)(2)> shGraph :: image <T, (unsigned char)(3)> :: operator [] (const unsigned short int& a) const
{
 const unsigned short int* tempdims = dims;
 char space[sizeof(shGraph :: image<T, (unsigned char)(2)>)];
 shGraph :: image<T, (unsigned char)(2)>* ans = ((shGraph :: image<T, (unsigned char)(2)>*)(&space));
 ans->width = (const unsigned int)(*tempdims);
 ans->height = (const unsigned int)(*(++tempdims));
 ans->origin = origin;
 ans->data = data + ((const unsigned int)(a) * subspacesize);
 ans->counter = counter;
 ++(*counter);
 return *((const shGraph :: image<T, (unsigned char)(1)>*)(ans));
}

template <class T> inline const shGraph :: image<T, (unsigned char)(1)> shGraph :: image <T, (unsigned char)(2)> :: operator [] (const unsigned short int& a) const
{
 char space[sizeof(shGraph :: image<T, (unsigned char)(1)>)];
 shGraph :: image<T, (unsigned char)(1)>* ans = ((shGraph :: image<T, (unsigned char)(1)>*)(&(space[0])));
 ans->dim = width;
 ans->origin = origin;
 ans->data = data + ((const unsigned int)(a) * (const unsigned int)(width));
 ans->counter = counter;
 ++(*counter);
 return *((const shGraph :: image<T, (unsigned char)(1)>*)(ans));
}

template <class T> inline const T& shGraph :: image <T, (unsigned char)(1)> :: operator [] (const unsigned short int& a) const
{
 return data[a];
}

template <class T, unsigned char dim> inline shGraph :: image<T, (dim - 1)> shGraph :: image <T, dim> :: operator [] (const unsigned short int& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 char space[sizeof(shGraph :: image<T, (dim - 1)>)];
 const unsigned short int* shortDims = (const unsigned short int*)(dims);
 shGraph :: image<T, (dim - 1)>* ans = ((shGraph :: image<T, (dim - 1)>*)(&space));
 ans->dims = shMath :: vectorNfx<(unsigned int)(dim - 1), unsigned short int>(*((const unsigned short int (*) [dim - 1])(shortDims)));
 ans->subspacesize = subspacesize / (const unsigned int)(*((const unsigned short int*)(dims)));
 ans->total = subspacesize;
 ans->origin = origin;
 ans->data = data + ((const unsigned int)(a) * subspacesize);
 ans->counter = counter;
 ++(*counter);
 return *(ans);
}

template <class T> inline shGraph :: image<T, (unsigned char)(2)> shGraph :: image <T, (unsigned char)(3)> :: operator [] (const unsigned short int& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 const unsigned short int* tempdims = dims;
 char space[sizeof(shGraph :: image<T, (unsigned char)(2)>)];
 shGraph :: image<T, (unsigned char)(2)>* ans = ((shGraph :: image<T, (unsigned char)(2)>*)(&space));
 ans->width = (const unsigned int)(*tempdims);
 ans->height = (const unsigned int)(*(++tempdims));
 ans->origin = origin;
 ans->data = data + ((const unsigned int)(a) * subspacesize);
 ans->counter = counter;
 ++(*counter);
 return *(ans);
}

template <class T> inline shGraph :: image<T, (unsigned char)(1)> shGraph :: image <T, (unsigned char)(2)> :: operator [] (const unsigned short int& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 char space[sizeof(shGraph :: image<T, (unsigned char)(1)>)];
 shGraph :: image<T, (unsigned char)(1)>* ans = ((shGraph :: image<T, (unsigned char)(1)>*)(&(space[0])));
 ans->dim = width;
 ans->origin = origin;
 ans->data = data + ((const unsigned int)(a) * (const unsigned int)(height));
 ans->counter = counter;
 ++(*counter);
 return *(ans);
}

template <class T> inline T& shGraph :: image <T, (unsigned char)(1)> :: operator [] (const unsigned short int& a)
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data[a];
}

template <class T, unsigned char dim> inline shGraph :: image <T, dim> :: operator const void* () const
{
 return (const void*)(data);
}

template <class T> inline shGraph :: image <T, (unsigned char)(3)> :: operator const void* () const
{
 return (const void*)(data);
}

template <class T> inline shGraph :: image <T, (unsigned char)(2)> :: operator const void* () const
{
 return (const void*)(data);
}

template <class T> inline shGraph :: image <T, (unsigned char)(1)> :: operator const void* () const
{
 return (const void*)(data);
}

template <class T, unsigned char dim> inline shGraph :: image <T, dim> :: operator void* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (void*)(data);
}

template <class T> inline shGraph :: image <T, (unsigned char)(3)> :: operator void* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (void*)(data);
}

template <class T> inline shGraph :: image <T, (unsigned char)(2)> :: operator void* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (void*)(data);
}

template <class T> inline shGraph :: image <T, (unsigned char)(1)> :: operator void* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return (void*)(data);
}

template <class T, unsigned char dim> inline shGraph :: image <T, dim> :: operator const T* () const
{
 return (const T*)(data);
}

template <class T> inline shGraph :: image <T, (unsigned char)(3)> :: operator const T* () const
{
 return (const T*)(data);
}

template <class T> inline shGraph :: image <T, (unsigned char)(2)> :: operator const T* () const
{
 return (const T*)(data);
}

template <class T> inline shGraph :: image <T, (unsigned char)(1)> :: operator const T* () const
{
 return (const T*)(data);
}

template <class T, unsigned char dim> inline shGraph :: image <T, dim> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T> inline shGraph :: image <T, (unsigned char)(3)> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T> inline shGraph :: image <T, (unsigned char)(2)> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T> inline shGraph :: image <T, (unsigned char)(1)> :: operator T* ()
{
 if ((*counter) != 0u)
 {
  copy();
 }
 return data;
}

template <class T, unsigned char dim> inline const shMath :: vectorNfx<(unsigned int)(dim), unsigned short int>& shGraph :: image <T, dim> :: getDimensions() const
{
 return dims;
}

template <class T> inline const shMath :: vectorNfx<3u, unsigned short int>& shGraph :: image <T, (unsigned char)(3)> :: getDimensions() const
{
 return dims;
}

template <class T> inline const shMath :: vectorNfx<2u, unsigned short int> shGraph :: image <T, (unsigned char)(2)> :: getDimensions() const
{
 return shMath :: vec2<unsigned short int>(width, height);
}

template <class T> inline const shMath :: vectorNfx<1u, unsigned short int> shGraph :: image <T, (unsigned char)(1)> :: getDimensions() const
{
 return shMath :: vectorNfx<1u, unsigned short int>(*((const unsigned short int (*) [1])(&dim)));
}

template <class T> inline const unsigned short int shGraph :: image <T, (unsigned char)(3)> :: getWidth() const
{
 return *dims;
}

template <class T> inline const unsigned short int shGraph :: image <T, (unsigned char)(3)> :: getHeight() const
{
 return dims[1u];
}

template <class T> inline const unsigned short int shGraph :: image <T, (unsigned char)(3)> :: getDepth() const
{
 return dims[2u];
}

template <class T> inline const unsigned short int shGraph :: image <T, (unsigned char)(2)> :: getWidth() const
{
 return width;
}

template <class T> inline const unsigned short int shGraph :: image <T, (unsigned char)(2)> :: getHeight() const
{
 return height;
}

template <class T> inline const unsigned short int shGraph :: image <T, (unsigned char)(1)> :: getLength() const
{
 return dim;
}

template <class T, unsigned char dim> inline const unsigned int shGraph :: image <T, dim> :: getTotal() const
{
 return total;
}

template <class T> inline const unsigned int shGraph :: image <T, (unsigned char)(3)> :: getTotal() const
{
 return total;
}

template <class T> inline const unsigned int shGraph :: image <T, (unsigned char)(2)> :: getTotal() const
{
 return ((const unsigned int)(width) * (const unsigned int)(height));
}

template <class T> inline const unsigned int shGraph :: image <T, (unsigned char)(1)> :: getTotal() const
{
 return (const unsigned int)(dim);
}

template <class T, unsigned char dim> void shGraph :: image <T, dim> :: copy()
{
 unsigned int* const tempcounter = (unsigned int*)malloc(sizeof(unsigned int));
 unsigned int i = 0u;
 T* tempdata = NULL;
 T* tempdata2 = NULL;
 const T* newdata = data;
 if (tempcounter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*tempcounter) = 0u;
 try
 {
  tempdata = tempdata2 = new T [total] ();
  for(;i < total;++i,(*(tempdata2++)) = (*(newdata++)));
 }
 catch(...)
 {
  if (tempdata != NULL)
  {
   delete [] tempdata;
  }
  throw;
 }
 --(*counter);
 origin = data = tempdata;
 counter = tempcounter;
}

template <class T> void shGraph :: image <T, (unsigned char)(3)> :: copy()
{
 unsigned int* const tempcounter = (unsigned int*)malloc(sizeof(unsigned int));
 unsigned int i = 0u;
 T* tempdata = NULL;
 T* tempdata2 = NULL;
 const T* newdata = data;
 if (tempcounter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*tempcounter) = 0u;
 try
 {
  tempdata = tempdata2 = new T [total] ();
  for(;i < total;++i,(*(tempdata2++)) = (*(newdata++)));
 }
 catch(...)
 {
  if (tempdata != NULL)
  {
   delete [] tempdata;
  }
  throw;
 }
 --(*counter);
 origin = data = tempdata;
 counter = tempcounter;
}

template <class T> void shGraph :: image <T, (unsigned char)(2)> :: copy()
{
 const unsigned int total = (const unsigned int)(width) * (const unsigned int)(height);
 unsigned int* const tempcounter = (unsigned int*)malloc(sizeof(unsigned int));
 unsigned int i = 0u;
 T* tempdata = NULL;
 T* tempdata2 = NULL;
 const T* newdata = data;
 if (tempcounter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*tempcounter) = 0u;
 try
 {
  tempdata = tempdata2 = new T [total] ();
  for(;i < total;++i,(*(tempdata2++)) = (*(newdata++)));
 }
 catch(...)
 {
  if (tempdata != NULL)
  {
   delete [] tempdata;
  }
  throw;
 }
 --(*counter);
 origin = data = tempdata;
 counter = tempcounter;
}

template <class T> void shGraph :: image <T, (unsigned char)(1)> :: copy()
{
 const unsigned int total = (const unsigned int)(dim);
 unsigned int* const tempcounter = (unsigned int*)malloc(sizeof(unsigned int));
 unsigned int i = 0u;
 T* tempdata = NULL;
 T* tempdata2 = NULL;
 const T* newdata = data;
 if (tempcounter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*tempcounter) = 0u;
 try
 {
  tempdata = tempdata2 = new T [total] ();
  for(;i < total;++i,(*(tempdata2++)) = (*(newdata++)));
 }
 catch(...)
 {
  if (tempdata != NULL)
  {
   delete [] tempdata;
  }
  throw;
 }
 --(*counter);
 origin = data = tempdata;
 counter = tempcounter;
}

template <class T, unsigned char dim> void shGraph :: image <T, dim> :: kill()
{
 if ((*counter) == 0u)
 {
  if (origin != NULL)
  {
   delete [] origin;
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> void shGraph :: image <T, (unsigned char)(3)> :: kill()
{
 if ((*counter) == 0u)
 {
  if (origin != NULL)
  {
   delete [] origin;
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> void shGraph :: image <T, (unsigned char)(2)> :: kill()
{
 if ((*counter) == 0u)
 {
  if (origin != NULL)
  {
   delete [] origin;
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> void shGraph :: image <T, (unsigned char)(1)> :: kill()
{
 if ((*counter) == 0u)
 {
  if (origin != NULL)
  {
   delete [] origin;
  }
  free(counter);
 }
 else
 {
  --(*counter);
 }
}

template <class T> shGraph :: image<T, (unsigned char)(2)> shGraph :: loadMonoColour(unsigned short int width, unsigned short int height, const T& colour)
{
 shGraph :: image<T, (unsigned char)(2)> ans = shGraph :: image<T, (unsigned char)(2)>(width, height);
 const unsigned int total = ans.getTotal();
 unsigned int i = 0u;
 T* data = (T*)(ans);
 for(;i < total;++i, (*(data++)) = colour);
 return ans;
}

template <class T, unsigned char dim> shGraph :: image<T, dim> shGraph :: loadMonoColour(const shMath :: vectorNfx<dim, unsigned short int>& dimensions, const T& colour)
{
 shGraph :: image<T, dim> ans = shGraph :: image<T, dim>(dimensions);
 const unsigned int total = ans.getTotal();
 unsigned int i = 0u;
 T* data = (T*)(ans);
 for(;i < total;++i, (*(data++)) = colour);
 return ans;
}

template <class X, class Y, unsigned char dim> shGraph :: image<X, dim> shGraph :: localFilter(X (* const & filter)(const Y&), const shGraph :: image<Y, dim>& img)
{
 shGraph :: image<X, dim> helper = shGraph :: image<X, dim>(img.getDimensions());
 return shGraph :: localFilter(filter, img, helper);
}

template <class X, class Y, unsigned char dim> shGraph :: image<X, dim> shGraph :: localFilter(X (* const & filter)(const Y&), const shGraph :: image<Y, dim>& img, shGraph :: image<X, dim>& helper)
{
 unsigned int i = 0u;
 const unsigned int total = img.getTotal();
 X* helperData = (X*)(helper);
 const Y* imgData = (const Y*)(img);
 for(;i < total;++i, (*(helperData++)) = (*filter)(*(imgData++)));
 return helper;
}
#endif
