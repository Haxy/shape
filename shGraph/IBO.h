#ifndef SH_GRAPH_IBO
#define SH_GRAPH_IBO

#include "GLextensions.h"

#include "IO.h"
#include "rccoordinatesfx.h"

#include "../shMath/vec2.h"
#include "../shMath/vec3.h"
#include "../shMath/vec4.h"

namespace shGraph
{
 template <class T> class IBO : public IO
 {
  public:
  IBO();
  template <class X> IBO(const rccoordinatesfx<X>& a, unsigned int usage = GL_STATIC_DRAW_ARB);
  IBO(const buffer& buf, unsigned int size);
  void display() const;
  unsigned int getSize() const;
  static void displayCurrent();
 };
}

template <class T> inline shGraph :: IBO <T> :: IBO() : IO(shGraph :: buffer(),0u,0u,(&shGraph :: IBO <T> :: displayCurrent),NULL,NULL)
{
}

template <class T> template <class X> shGraph :: IBO <T> :: IBO(const shGraph :: rccoordinatesfx<X>& a, unsigned int usage) : IO(shGraph :: buffer(),0u,(a.getSize() * a.getDim()),(&shGraph :: IBO <T> :: displayCurrent),NULL,NULL)
{
 id.create();
 glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,id.getId());
 glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, size * sizeof(X),(const unsigned int*)(a),usage);
 glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,current.getID());
}

template <class T> inline void shGraph :: IBO <T> :: display() const
{
 bind();
 displayCurrent();
 shGraph :: IO :: unbindCurrent();
}

template <class T> inline unsigned int shGraph :: IBO <T> :: getSize() const
{
 return size;
}

namespace shGraph
{
 template <> inline void IBO <unsigned char> :: displayCurrent()
 {
  glDrawElements(GL_POINTS,current.getSize(),GL_UNSIGNED_BYTE,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <unsigned short int> :: displayCurrent()
 {
  glDrawElements(GL_POINTS,current.getSize(),GL_UNSIGNED_SHORT,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <unsigned int> :: displayCurrent()
 {
  glDrawElements(GL_POINTS,current.getSize(),GL_UNSIGNED_INT,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <shMath :: vec2<unsigned char> > :: displayCurrent()
 {
  glDrawElements(GL_LINES,current.getSize(),GL_UNSIGNED_BYTE,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <shMath :: vec2<unsigned short int> > :: displayCurrent()
 {
  glDrawElements(GL_LINES,current.getSize(),GL_UNSIGNED_SHORT,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <shMath :: vec2<unsigned int> > :: displayCurrent()
 {
  glDrawElements(GL_LINES,current.getSize(),GL_UNSIGNED_INT,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <shMath :: vec3<unsigned char> > :: displayCurrent()
 {
  glDrawElements(GL_TRIANGLES,current.getSize(),GL_UNSIGNED_BYTE,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <shMath :: vec3<unsigned short int> > :: displayCurrent()
 {
  glDrawElements(GL_TRIANGLES,current.getSize(),GL_UNSIGNED_SHORT,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <shMath :: vec3<unsigned int> > :: displayCurrent()
 {
  glDrawElements(GL_TRIANGLES,current.getSize(),GL_UNSIGNED_INT,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <shMath :: vec4<unsigned char> > :: displayCurrent()
 {
  glDrawElements(GL_QUADS,current.getSize(),GL_UNSIGNED_BYTE,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <shMath :: vec4<unsigned short int> > :: displayCurrent()
 {
  glDrawElements(GL_QUADS,current.getSize(),GL_UNSIGNED_SHORT,NULL);
 }
}

namespace shGraph
{
 template <> inline void IBO <shMath :: vec4<unsigned int> > :: displayCurrent()
 {
  glDrawElements(GL_QUADS,current.getSize(),GL_UNSIGNED_INT,NULL);
 }
}
#endif

