#ifndef SH_GRAPH_ATTRIBUTE
#define SH_GRAPH_ATTRIBUTE

#include "GLextensions.h"

#include "shader.h"
#include "../shMath/vec2.h"
#include "../shMath/vec3.h"
#include "../shMath/vec4.h"
#include "../shMath/mat2.h"
#include "../shMath/mat3.h"
#include "../shMath/mat4.h"

namespace shGraph
{
 template <class T> class attribute
 {
  public:
  typedef signed int location;
  const static location invalidLocation;
  const static location defaultVertexLocation;
  explicit attribute(location l = invalidLocation, const shader& shad = shGraph :: shader());
  const location& getLocation() const;
  const shader& getOwner() const;
  location& getLocation();
  shader& getOwner();
  attribute& operator = (const T& a);
  operator const T () const;
  bool isValid() const;
  private:
  location loc;
  shader owner;
 };

 template <class T> const typename attribute <T> :: location attribute <T> :: invalidLocation = -1;
 template <class T> const typename attribute <T> :: location attribute <T> :: defaultVertexLocation = 0;

 template <class T> attribute<T> getAttribute(const char* name, const shader& shad);
 template <class T> attribute<T> getAttribute(const signed char* name, const shader& shad);
 template <class T> attribute<T> getAttribute(const unsigned char* name, const shader& shad);
 template <class T> attribute<T> getAttribute(const char* name, const shader& shad, const typename attribute <T> :: location& loc);
 template <class T> attribute<T> getAttribute(const signed char* name, const shader& shad, const typename attribute <T> :: location& loc);
 template <class T> attribute<T> getAttribute(const unsigned char* name, const shader& shad, const typename attribute <T> :: location& loc);
}

template <class T> inline shGraph :: attribute <T> :: attribute(typename shGraph :: attribute <T> :: location l, const shGraph :: shader& shad) : loc(l), owner(shad)
{
}

template <class T> inline const typename shGraph :: attribute <T> :: location& shGraph :: attribute <T> :: getLocation() const
{
 return loc;
}

template <class T> inline const shGraph :: shader& shGraph :: attribute <T> :: getOwner() const
{
 return owner;
}

template <class T> inline typename shGraph :: attribute <T> :: location& shGraph :: attribute <T> :: getLocation()
{
 return loc;
}

template <class T> inline shGraph :: shader& shGraph :: attribute <T> :: getOwner()
{
 return owner;
}

namespace shGraph
{
 template <> inline shGraph :: attribute<float>& attribute <float> :: operator = (const float& a)
 {
  glVertexAttrib1fARB(loc,a);
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: attribute<shMath :: vec2<float> >& attribute <shMath :: vec2<float> > :: operator = (const shMath :: vec2<float>& a)
 {
  glVertexAttrib2fvARB(loc,(const GLfloat*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: attribute<shMath :: vec3<float> >& attribute <shMath :: vec3<float> > :: operator = (const shMath :: vec3<float>& a)
 {
  glVertexAttrib3fvARB(loc,(const GLfloat*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: attribute<shMath :: vec4<float> >& attribute <shMath :: vec4<float> > :: operator = (const shMath :: vec4<float>& a)
 {
  glVertexAttrib4fvARB(loc,(const GLfloat*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: attribute<shMath :: vec4<signed int> >& attribute <shMath :: vec4<signed int> > :: operator = (const shMath :: vec4<signed int>& a)
 {
  glVertexAttrib4ivARB(loc,(const GLint*)(a));
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: attribute<shMath :: mat2<float> >& attribute <shMath :: mat2<float> > :: operator = (const shMath :: mat2<float>& a)
 {
  const float* temp = (const float*)(a);
  signed int tloc = loc;
  glVertexAttrib2fvARB(tloc,temp);
  glVertexAttrib2fvARB(++tloc,temp += 2);
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: attribute<shMath :: mat3<float> >& attribute <shMath :: mat3<float> > :: operator = (const shMath :: mat3<float>& a)
 {
  const float* temp = (const float*)(a);
  signed int tloc = loc;
  glVertexAttrib3fvARB(tloc,temp);
  glVertexAttrib3fvARB(++tloc,temp += 3);
  glVertexAttrib3fvARB(++tloc,temp += 3);
  return *this;
 }
}

namespace shGraph
{
 template <> inline shGraph :: attribute<shMath :: mat4<float> >& attribute <shMath :: mat4<float> > :: operator = (const shMath :: mat4<float>& a)
 {
  const float* temp = (const float*)(a);
  signed int tloc = loc;
  glVertexAttrib4fvARB(tloc,temp);
  glVertexAttrib4fvARB(++tloc,temp += 4);
  glVertexAttrib4fvARB(++tloc,temp += 4);
  glVertexAttrib4fvARB(++tloc,temp += 4);
  return *this;
 }
}

namespace shGraph
{
 template <> inline attribute <float> :: operator const float () const
 {
  shMath :: vec4<float> ans;
  glGetVertexAttribfv(loc,GL_CURRENT_VERTEX_ATTRIB,(GLfloat*)(&ans));
  return ans.x();
 }
}

namespace shGraph
{
 template <> inline attribute <signed int> :: operator const signed int () const
 {
  shMath :: vec4<signed int> ans;
  glGetVertexAttribiv(loc,GL_CURRENT_VERTEX_ATTRIB,(GLint*)(&ans));
  return ans.x();
 }
}

namespace shGraph
{
 template <> inline attribute <shMath :: vec2<float> > :: operator const shMath :: vec2<float> () const
 {
  shMath :: vec4<float> ans;
  glGetVertexAttribfv(loc,GL_CURRENT_VERTEX_ATTRIB,(GLfloat*)(&ans));
  return ans.xy();
 }
}

namespace shGraph
{
 template <> inline attribute <shMath :: vec2<signed int> > :: operator const shMath :: vec2<signed int> () const
 {
  shMath :: vec4<signed int> ans;
  glGetVertexAttribiv(loc,GL_CURRENT_VERTEX_ATTRIB,(GLint*)(&ans));
  return ans.xy();
 }
}

namespace shGraph
{
 template <> inline attribute <shMath :: vec3<float> > :: operator const shMath :: vec3<float> () const
 {
  shMath :: vec4<float> ans;
  glGetVertexAttribfv(loc,GL_CURRENT_VERTEX_ATTRIB,(GLfloat*)(&ans));
  return ans.xyz();
 }
}

namespace shGraph
{
 template <> inline attribute <shMath :: vec3<signed int> > :: operator const shMath :: vec3<signed int> () const
 {
  shMath :: vec4<signed int> ans;
  glGetVertexAttribiv(loc,GL_CURRENT_VERTEX_ATTRIB,(GLint*)(&ans));
  return ans.xyz();
 }
}

namespace shGraph
{
 template <> inline attribute <shMath :: vec4<float> > :: operator const shMath :: vec4<float> () const
 {
  shMath :: vec4<float> ans;
  glGetVertexAttribfv(loc,GL_CURRENT_VERTEX_ATTRIB,(GLfloat*)(&ans));
  return ans;
 }
}

namespace shGraph
{
 template <> inline attribute <shMath :: vec4<signed int> > :: operator const shMath :: vec4<signed int> () const
 {
  shMath :: vec4<signed int> ans;
  glGetVertexAttribiv(loc,GL_CURRENT_VERTEX_ATTRIB,(GLint*)(&ans));
  return ans;
 }
}

namespace shGraph
{
 template <> inline attribute <shMath :: mat4<float> > :: operator const shMath :: mat4<float> () const
 {
  shMath :: mat4<float> ans;
  float* temp = (float*)(ans);
  signed int tloc = loc;
  glGetVertexAttribfv(tloc,GL_CURRENT_VERTEX_ATTRIB,temp);
  glGetVertexAttribfv(++tloc,GL_CURRENT_VERTEX_ATTRIB,temp += 4);
  glGetVertexAttribfv(++tloc,GL_CURRENT_VERTEX_ATTRIB,temp += 4);
  glGetVertexAttribfv(++tloc,GL_CURRENT_VERTEX_ATTRIB,temp += 4);
  return ans;
 }
}

namespace shGraph
{
 template <> inline attribute <shMath :: mat4<signed int> > :: operator const shMath :: mat4<signed int> () const
 {
  shMath :: mat4<signed int> ans;
  signed int* temp = (signed int*)(ans);
  signed int tloc = loc;
  glGetVertexAttribiv(tloc,GL_CURRENT_VERTEX_ATTRIB,temp);
  glGetVertexAttribiv(++tloc,GL_CURRENT_VERTEX_ATTRIB,temp += 4);
  glGetVertexAttribiv(++tloc,GL_CURRENT_VERTEX_ATTRIB,temp += 4);
  glGetVertexAttribiv(++tloc,GL_CURRENT_VERTEX_ATTRIB,temp += 4);
  return ans;
 }
}

template <class T> inline bool shGraph :: attribute <T> :: isValid() const
{
 return (loc != shGraph :: attribute <T> :: invalidLocation);
}

template <class T> shGraph :: attribute<T> shGraph :: getAttribute(const char* name, const shGraph :: shader& shad)
{
 return shGraph :: attribute<T>(glGetAttribLocationARB(shad.getId(),(const GLchar*)(name)),shad);
}

template <class T> shGraph :: attribute<T> shGraph :: getAttribute(const signed char* name, const shGraph :: shader& shad)
{
 return shGraph :: attribute<T>(glGetAttribLocationARB(shad.getId(),(const GLchar*)(name)),shad);
}

template <class T> shGraph :: attribute<T> shGraph :: getAttribute(const unsigned char* name, const shGraph :: shader& shad)
{
 return shGraph :: attribute<T>(glGetAttribLocationARB(shad.getId(),(const GLchar*)(name)),shad);
}

template <class T> shGraph :: attribute<T> shGraph :: getAttribute(const char* name, const shGraph :: shader& shad, const typename shGraph :: attribute <T> :: location& loc)
{
 glBindAttribLocationARB(shad.getId(),loc,(const GLchar*)(name));
 return shGraph :: attribute<T>(loc,shad);
}

template <class T> shGraph :: attribute<T> shGraph :: getAttribute(const signed char* name, const shGraph :: shader& shad, const typename shGraph :: attribute <T> :: location& loc)
{
 glBindAttribLocationARB(shad.getId(),loc,(const GLchar*)(name));
 return shGraph :: attribute<T>(loc,shad);
}

template <class T> shGraph :: attribute<T> shGraph :: getAttribute(const unsigned char* name, const shGraph :: shader& shad, const typename shGraph :: attribute <T> :: location& loc)
{
 glBindAttribLocationARB(shad.getId(),loc,(const GLchar*)(name));
 return shGraph :: attribute<T>(loc,shad);
}
#endif
