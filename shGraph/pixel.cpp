#include "pixel.h"

double tc(double t,double p,double q)
{
 if (t < (1.0 / 6.0))
 {
  return (p + ((q - p) * 6.0 * t));
 }
 else
 {
  if (t < 0.5)
  {
   return q;
  }
  else
  {
   if (t < (2.0 / 3.0))
   {
    return (p + ((q - p) * ((2.0 / 3.0) - t) * 6.0));
   }
   else
   {
    return p;
   }
  }
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned char> RGBAtoHSLA(const shGraph :: pixel<unsigned char>& p)
 {
  shGraph :: pixel<unsigned char> out = p;
  unsigned char max = ((p.r > p.g) ? p.r : p.g);
  unsigned char min = ((p.r < p.g) ? p.r : p.g);
  const double umax = (double)(UCHAR_MAX);
  const double nr = ((double)p.r / umax);
  const double ng = ((double)p.g / umax);
  const double nb = ((double)p.b / umax);
  double ndmax;
  double ndmin;
  double dmax;
  double dmin;
  double h;
  max = ((max > p.b) ? max : p.b);
  min = ((min < p.b) ? min : p.b);
  dmax = (double)(max);
  dmin = (double)(min);
  ndmax = (dmax / umax);
  ndmin = (dmin / umax);
  out.b = (unsigned char)(0.5 * (ndmax + ndmin) * umax);
  if (max == min)
  {
   h = 0.0;
   out.g = 0;
  }
  else
  {
   if ((ndmax + ndmin) <= 0.5)
   {
    out.g = (unsigned char)(((ndmax - dmin) / (ndmax + dmin)) * umax);
   }
   else
   {
    if (out.b != UCHAR_MAX)
    {
     out.g = (unsigned char)(((ndmax - ndmin) / (2.0 - (ndmax + ndmin))) * umax);
    }
    else
    {
     out.g = UCHAR_MAX;
    }
   }
   if (max == p.r)
   {
    if (p.g >= p.b)
    {
     h = 60.0 * ((ng - nb) / (ndmax - ndmin));
    }
    else
    {
     h = (60.0 * ((ng - nb) / (ndmax - ndmin))) + 360.0;
    }
   }
   else
   {
    if (max == p.g)
    {
     h = (60.0 * ((nb - nr) / (ndmax - ndmin))) + 120.0;
    }
    else
    {
     h = (60.0 * ((nr - ng) / (ndmax - ndmin))) + 240.0;
    }
   }
  }
  out.r = (unsigned char)(h * (umax / 360.0));
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned short int> RGBAtoHSLA(const shGraph :: pixel<unsigned short int>& p)
 {
  shGraph :: pixel<unsigned short int> out = p;
  unsigned short int max = ((p.r > p.g) ? p.r : p.g);
  unsigned short int min = ((p.r < p.g) ? p.r : p.g);
  const double umax = (double)(USHRT_MAX);
  const double nr = ((double)p.r / umax);
  const double ng = ((double)p.g / umax);
  const double nb = ((double)p.b / umax);
  double ndmax;
  double ndmin;
  double dmax;
  double dmin;
  double h;
  max = ((max > p.b) ? max : p.b);
  min = ((min < p.b) ? min : p.b);
  dmax = (double)(max);
  dmin = (double)(min);
  ndmax = (dmax / umax);
  ndmin = (dmin / umax);
  out.b = (unsigned short int)(0.5 * (ndmax + ndmin) * umax);
  if (max == min)
  {
   h = 0.0;
   out.g = 0;
  }
  else
  {
   if ((ndmax + ndmin) <= 0.5)
   {
    out.g = (unsigned short int)(((ndmax - ndmin) / (ndmax + ndmin)) * umax);
   }
   else
   {
    if (out.b != USHRT_MAX)
    {
     out.g = (unsigned short int)(((ndmax - ndmin) / (2.0 - (ndmax + ndmin))) * umax);
    }
    else
    {
     out.g = USHRT_MAX;
    }
   }
   if (max == p.r)
   {
    if (p.g >= p.b)
    {
     h = 60.0 * ((ng - nb) / (ndmax - ndmin));
    }
    else
    {
     h = (60.0 * ((ng - nb) / (ndmax - ndmin))) + 360.0;
    }
   }
   else
   {
    if (max == p.g)
    {
     h = (60.0 * ((nb - nr) / (ndmax - ndmin))) + 120.0;
    }
    else
    {
     h = (60.0 * ((nr - ng) / (ndmax - ndmin))) + 240.0;
    }
   }
  }
  out.r = (unsigned short int)(h * (umax / 360.0));
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<float> RGBAtoHSLA(const shGraph :: pixel<float>& p)
 {
  shGraph :: pixel<float> out = p;
  float max = ((p.r > p.g) ? p.r : p.g);
  float min = ((p.r < p.g) ? p.r : p.g);
  float h;
  max = ((max > p.b) ? max : p.b);
  min = ((min < p.b) ? min : p.b);
  out.b = (0.5f * (max + min));
  if (max == min)
  {
   h = 0.0f;
   out.g = 0.0f;
  }
  else
  {
   if ((max + min) <= 0.5f)
   {
    out.g = ((max - min) / (max + min));
   }
   else
   {
    if (out.b != 1.0f)
    {
     out.g = ((max - min) / (2.0f - (max + min)));
    }
    else
    {
     out.g = 1.0f;
    }
   }
   if (max == p.r)
   {
    if (p.g >= p.b)
    {
     h = 60.0f * ((p.g - p.b) / (max - min));
    }
    else
    {
     h = (60.0f * ((p.g - p.b) / (max - min))) + 360.0f;
    }
   }
   else
   {
    if (max == p.g)
    {
     h = (60.0f * ((p.b - p.r) / (max - min))) + 120.0f;
    }
    else
    {
     h = (60.0f * ((p.r - p.g) / (max - min))) + 240.0f;
    }
   }
  }
  out.r = (h / 360.0f);
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<double> RGBAtoHSLA(const shGraph :: pixel<double>& p)
 {
  shGraph :: pixel<double> out = p;
  double max = ((p.r > p.g) ? p.r : p.g);
  double min = ((p.r < p.g) ? p.r : p.g);
  double h;
  max = ((max > p.b) ? max : p.b);
  min = ((min < p.b) ? min : p.b);
  out.b = (0.5 * (max + min));
  if (max == min)
  {
   h = 0.0;
   out.g = 0.0;
  }
  else
  {
   if ((max + min) <= 0.5)
   {
    out.g = ((max - min) / (max + min));
   }
   else
   {
    if (out.b != 1.0)
    {
     out.g = ((max - min) / (2.0f - (max + min)));
    }
    else
    {
     out.g = 1.0;
    }
   }
   if (max == p.r)
   {
    if (p.g >= p.b)
    {
     h = 60.0 * ((p.g - p.b) / (max - min));
    }
    else
    {
     h = (60.0 * ((p.g - p.b) / (max - min))) + 360.0;
    }
   }
   else
   {
    if (max == p.g)
    {
     h = (60.0 * ((p.b - p.r) / (max - min))) + 120.0;
    }
    else
    {
     h = (60.0 * ((p.r - p.g) / (max - min))) + 240.0;
    }
   }
  }
  out.r = (h / 360.0);
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned char> HSLAtoRGBA(const shGraph :: pixel<unsigned char>& p)
 {
  shGraph :: pixel<unsigned char> out = p;
  const double umax = (double)(UCHAR_MAX);
  const double nh = ((double)(p.r) / umax);
  const double ns = ((double)(p.g) / umax);
  const double nl = ((double)(p.b) / umax);
  const double dq = ((nl < 0.5) ? /*1*/(nl * (1.0 + ns)) : /*2*/(nl + ns - (nl * ns)));
  const double dp = (2.0 * nl - dq);
  double tr = (nh + (1.0 / 3.0));
  double tg = nh;
  double tb = (nh - (1.0 / 3.0));
  if (tr > 1.0)
  {
   tr -= 1.0;
  }
  if (tb < 0.0)
  {
   tb += 1.0;
  }
  out.r = (unsigned char)(tc(tr,dp,dq) * umax);
  out.g = (unsigned char)(tc(tg,dp,dq) * umax);
  out.b = (unsigned char)(tc(tb,dp,dq) * umax);
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned short int> HSLAtoRGBA(const shGraph :: pixel<unsigned short int>& p)
 {
  shGraph :: pixel<unsigned short int> out = p;
  const double umax = (double)(USHRT_MAX);
  const double nh = ((double)(p.r) / umax);
  const double ns = ((double)(p.g) / umax);
  const double nl = ((double)(p.b) / umax);
  const double dq = ((nl < 0.5) ? /*1*/(nl * (1.0 + ns)) : /*2*/(nl + ns - (nl * ns)));
  const double dp = (2.0 * nl - dq);
  double tr = (nh + (1.0 / 3.0));
  double tg = nh;
  double tb = (nh - (1.0 / 3.0));
  if (tr > 1.0)
  {
   tr -= 1.0;
  }
  if (tb < 0.0)
  {
   tb += 1.0;
  }
  out.r = (unsigned short int)(tc(tr,dp,dq) * umax);
  out.g = (unsigned short int)(tc(tg,dp,dq) * umax);
  out.b = (unsigned short int)(tc(tb,dp,dq) * umax);
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<float> HSLAtoRGBA(const shGraph :: pixel<float>& p)
 {
  shGraph :: pixel<float> out = p;
  const double nh = (double)(p.r);
  const double ns = (double)(p.g);
  const double nl = (double)(p.b);
  const double dq = ((nl < 0.5) ? /*1*/(nl * (1.0 + ns)) : /*2*/(nl + ns - (nl * ns)));
  const double dp = (2.0 * nl - dq);
  double tr = (nh + (1.0 / 3.0));
  double tb = (nh - (1.0 / 3.0));
  if (tr > 1.0)
  {
   tr -= 1.0;
  }
  if (tb < 0.0)
  {
   tb += 1.0;
  }
  out.r = (float)(tc(tr,dp,dq));
  out.g = (float)(tc(nh,dp,dq));
  out.b = (float)(tc(tb,dp,dq));
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<double> HSLAtoRGBA(const shGraph :: pixel<double>& p)
 {
  shGraph :: pixel<double> out = p;
  const double dq = ((p.b < 0.5) ? /*1*/(p.b * (1.0 + p.g)) : /*2*/(p.b + p.g - (p.b * p.g)));
  const double dp = (2.0 * p.b - dq);
  double tr = (p.r + (1.0 / 3.0));
  double tb = (p.r - (1.0 / 3.0));
  if (tr > 1.0)
  {
   tr -= 1.0;
  }
  if (tb < 0.0)
  {
   tb += 1.0;
  }
  out.r = tc(tr,dp,dq);
  out.g = tc(p.r,dp,dq);
  out.b = tc(tb,dp,dq);
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned char> RGBAtoHSVA(const shGraph :: pixel<unsigned char>& p)
 {
  shGraph :: pixel<unsigned char> out = p;
  unsigned char max = ((p.r > p.g) ? p.r : p.g);
  unsigned char min = ((p.r < p.g) ? p.r : p.g);
  const double umax = (double)(UCHAR_MAX);
  const double nr = ((double)p.r / umax);
  const double ng = ((double)p.g / umax);
  const double nb = ((double)p.b / umax);
  double ndmax;
  double ndmin;
  double dmax;
  double dmin;
  double h;
  max = ((max > p.b) ? max : p.b);
  min = ((min < p.b) ? min : p.b);
  dmax = (double)(max);
  dmin = (double)(min);
  ndmax = (dmax / umax);
  ndmin = (dmin / umax);
  out.b = max;
  if (min == max)
  {
   h = 0.0;
   out.g = 0;
  }
  else
  {
   out.g = (unsigned char)((1.0 - (ndmin / ndmax)) * umax);
   if (max == p.r)
   {
    if (p.g >= p.b)
    {
     h = 60.0 * ((ng - nb) / (ndmax - ndmin));
     }
    else
    {
     h = (60.0 * ((ng - nb) / (ndmax - ndmin))) + 360.0;
    }
   }
   else
   {
    if (max == p.g)
    {
     h = (60.0 * ((nb - nr) / (ndmax - ndmin))) + 120.0;
    }
    else
    {
     h = (60.0 * ((nr - ng) / (ndmax - ndmin))) + 240.0;
    }
   }
  }
  out.r = (unsigned char)(h * (umax / 360.0));
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned short int> RGBAtoHSVA(const shGraph :: pixel<unsigned short int>& p)
 {
  shGraph :: pixel<unsigned short int> out = p;
  unsigned short int max = ((p.r > p.g) ? p.r : p.g);
  unsigned short int min = ((p.r < p.g) ? p.r : p.g);
  const double umax = (double)(USHRT_MAX);
  const double nr = ((double)p.r / umax);
  const double ng = ((double)p.g / umax);
  const double nb = ((double)p.b / umax);
  double ndmax;
  double ndmin;
  double dmax;
  double dmin;
  double h;
  max = ((max > p.b) ? max : p.b);
  min = ((min < p.b) ? min : p.b);
  dmax = (double)(max);
  dmin = (double)(min);
  ndmax = (dmax / umax);
  ndmin = (dmin / umax);
  out.b = max;
  if (min == max)
  {
   h = 0.0;
   out.g = 0;
  }
  else
  {
   out.g = (unsigned short int)((1.0 - (ndmin / ndmax)) * umax);
   if (max == p.r)
   {
    if (p.g >= p.b)
    {
     h = 60.0 * ((ng - nb) / (ndmax - ndmin));
    }
    else
    {
     h = (60.0 * ((ng - nb) / (ndmax - ndmin))) + 360.0;
    }
   }
   else
   {
    if (max == p.g)
    {
     h = (60.0 * ((nb - nr) / (ndmax - ndmin))) + 120.0;
    }
    else
    {
     h = (60.0 * ((nr - ng) / (ndmax - ndmin))) + 240.0;
    }
   }
  }
  out.r = (unsigned short int)(h * (umax / 360.0));
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<float> RGBAtoHSVA(const shGraph :: pixel<float>& p)
 {
  shGraph :: pixel<float> out = p;
  float max = ((p.r > p.g) ? p.r : p.g);
  float min = ((p.r < p.g) ? p.r : p.g);
  float h;
  max = ((max > p.b) ? max : p.b);
  min = ((min < p.b) ? min : p.b);
  out.b = max;
  if (min == max)
  {
   h = 0.0f;
   out.g = 0.0f;
  }
  else
  {
   out.g = (1.0f - (min / max));
   if (max == p.r)
   {
    if (p.g >= p.b)
    {
     h = 60.0f * ((p.g - p.b) / (max - min));
    }
    else
    {
     h = (60.0f * ((p.g - p.b) / (max - min))) + 360.0f;
    }
   }
   else
   {
    if (max == p.g)
    {
     h = (60.0f * ((p.b - p.r) / (max - min))) + 120.0f;
    }
    else
    {
     h = (60.0f * ((p.r - p.g) / (max - min))) + 240.0f;
    }
   }
  }
  out.r = (h / 360.0f);
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<double> RGBAtoHSVA(const shGraph :: pixel<double>& p)
 {
  shGraph :: pixel<double> out = p;
  double max = ((p.r > p.g) ? p.r : p.g);
  double min = ((p.r < p.g) ? p.r : p.g);
  double h;
  max = ((max > p.b) ? max : p.b);
  min = ((min < p.b) ? min : p.b);
  out.b = max;
  if (min == max)
  {
   h = 0.0;
   out.g = 0.0;
  }
  else
  {
   out.g = (1.0 - (min / max));
   if (max == p.r)
   {
    if (p.g >= p.b)
    {
     h = 60.0 * ((p.g - p.b) / (max - min));
    }
    else
    {
     h = (60.0 * ((p.g - p.b) / (max - min))) + 360.0;
    }
   }
   else
   {
    if (max == p.g)
    {
     h = (60.0 * ((p.b - p.r) / (max - min))) + 120.0;
    }
    else
    {
     h = (60.0 * ((p.r - p.g) / (max - min))) + 240.0;
    }
   }
  }
  out.r = (h / 360.0);
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned char> HSVAtoRGBA(const shGraph :: pixel<unsigned char>& p)
 {
  shGraph :: pixel<unsigned char> out = p;
  const double umax = (double)(UCHAR_MAX);
  const double nh = ((double)(p.r) * (360.0 / umax));
  const double ns = ((double)(p.g) / umax);
  const double nv = ((double)(p.b) / umax);
  const double h = floor(nh / 60.0);
  const double f = ((nh / 60.0) - h);
  const unsigned char dp = (unsigned char)((nv * (1.0 - ns)) * umax);
  const unsigned char dq = (unsigned char)((nv * (1.0 - (f * ns))) * umax);
  const unsigned char dt = (unsigned char)((nv * (1.0 - ((1.0 - f) * ns))) * umax);
  const unsigned char hi = (unsigned char)((unsigned char)(h) % 6);
  switch(hi)
  {
   case 0:
   return shGraph :: pixel<unsigned char>(p.b,dt,dp,p.a);
   break;
   case 1:
   return shGraph :: pixel<unsigned char>(dq,p.b,dp,p.a);
   break;
   case 2:
   return shGraph :: pixel<unsigned char>(dp,p.b,dt,p.a);
   break;
   case 3:
   return shGraph :: pixel<unsigned char>(dp,dq,p.b,p.a);
   break;
   case 4:
   return shGraph :: pixel<unsigned char>(dt,dp,p.b,p.a);
   break;
   case 5:
   return shGraph :: pixel<unsigned char>(p.b,dp,dq,p.a);
   break;
  }
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned short int> HSVAtoRGBA(const shGraph :: pixel<unsigned short int>& p)
 {
  shGraph :: pixel<unsigned short int> out = p;
  const double umax = (double)(USHRT_MAX);
  const double nh = ((double)(p.r) * (360.0 / umax));
  const double ns = ((double)(p.g) / umax);
  const double nv = ((double)(p.b) / umax);
  const double h = floor(nh / 60.0);
  const double f = ((nh / 60.0) - h);
  const unsigned short int dp = (unsigned short int)((nv * (1.0 - ns)) * umax);
  const unsigned short int dq = (unsigned short int)((nv * (1.0 - (f * ns))) * umax);
  const unsigned short int dt = (unsigned short int)((nv * (1.0 - ((1.0 - f) * ns))) * umax);
  const unsigned short int hi = (unsigned short int)((unsigned short int)(h) % 6);
  switch(hi)
  {
   case 0:
   return shGraph :: pixel<unsigned short int>(p.b,dt,dp,p.a);
   break;
   case 1:
   return shGraph :: pixel<unsigned short int>(dq,p.b,dp,p.a);
   break;
   case 2:
   return shGraph :: pixel<unsigned short int>(dp,p.b,dt,p.a);
   break;
   case 3:
   return shGraph :: pixel<unsigned short int>(dp,dq,p.b,p.a);
   break;
   case 4:
   return shGraph :: pixel<unsigned short int>(dt,dp,p.b,p.a);
   break;
   case 5:
   return shGraph :: pixel<unsigned short int>(p.b,dp,dq,p.a);
   break;
  }
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<float> HSVAtoRGBA(const shGraph :: pixel<float>& p)
 {
  shGraph :: pixel<float> out = p;
  const float nh = (p.r * 360.0f);
  const float h = floor(nh / 60.0f);
  const float f = ((nh / 60.0f) - h);
  const float dp = (p.b * (1.0f - p.g));
  const float dq = (p.b * (1.0f - (f * p.g)));
  const float dt = (p.b * (1.0f - ((1.0f - f) * p.g)));
  const unsigned int hi = ((unsigned int)(h) % 6);
  switch(hi)
  {
   case 0:
   return shGraph :: pixel<float>(p.b,dt,dp,p.a);
   break;
   case 1:
   return shGraph :: pixel<float>(dq,p.b,dp,p.a);
   break;
   case 2:
   return shGraph :: pixel<float>(dp,p.b,dt,p.a);
   break;
   case 3:
   return shGraph :: pixel<float>(dp,dq,p.b,p.a);
   break;
   case 4:
   return shGraph :: pixel<float>(dt,dp,p.b,p.a);
   break;
   case 5:
   return shGraph :: pixel<float>(p.b,dp,dq,p.a);
   break;
  }
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<double> HSVAtoRGBA(const shGraph :: pixel<double>& p)
 {
  shGraph :: pixel<double> out = p;
  const double nh = (p.r * 360.0);
  const double h = floor(nh / 60.0);
  const double f = ((nh / 60.0) - h);
  const double dp = (p.b * (1.0 - p.g));
  const double dq = (p.b * (1.0 - (f * p.g)));
  const double dt = (p.b * (1.0 - ((1.0 - f) * p.g)));
  const unsigned int hi = ((unsigned int)(h) % 6);
  switch(hi)
  {
   case 0:
   return shGraph :: pixel<double>(p.b,dt,dp,p.a);
   break;
   case 1:
   return shGraph :: pixel<double>(dq,p.b,dp,p.a);
   break;
   case 2:
   return shGraph :: pixel<double>(dp,p.b,dt,p.a);
   break;
   case 3:
   return shGraph :: pixel<double>(dp,dq,p.b,p.a);
   break;
   case 4:
   return shGraph :: pixel<double>(dt,dp,p.b,p.a);
   break;
   case 5:
   return shGraph :: pixel<double>(p.b,dp,dq,p.a);
   break;
  }
  return out;
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned char> RGBAtoYUVA(const shGraph :: pixel<unsigned char>& p)
 {
  const double dr = (double)(p.r);
  const double dg = (double)(p.g);
  const double db = (double)(p.b);
  const double umax = (double)(UCHAR_MAX);
  const double ndr = dr / umax;
  const double ndg = dg / umax;
  const double ndb = db / umax;
  const unsigned char nr = (unsigned char)(((0.299 * ndr) + (0.587 * ndg) + (0.114 * ndb)) * umax);
  const unsigned char ng = (unsigned char)(((-0.14713 * ndr) + (-0.28886 * ndg) + (0.436 * ndb) + 0.436) * (umax / 0.872));
  const unsigned char nb = (unsigned char)(((0.615 * ndr) + (-0.51499 * ndg) + (-0.10001 * ndb) + 0.615) * (umax / 1.23));
  return shGraph :: pixel<unsigned char>(nr,ng,nb,p.a);
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned short int> RGBAtoYUVA(const shGraph :: pixel<unsigned short int>& p)
 {
  const double dr = (double)(p.r);
  const double dg = (double)(p.g);
  const double db = (double)(p.b);
  const double umax = (double)(USHRT_MAX);
  const double ndr = dr / umax;
  const double ndg = dg / umax;
  const double ndb = db / umax;
  const unsigned short int nr = (unsigned short int)(((0.299 * ndr) + (0.587 * ndg) + (0.114 * ndb)) * umax);
  const unsigned short int ng = (unsigned short int)(((-0.14713 * ndr) + (-0.28886 * ndg) + (0.436 * ndb) + 0.436) * (umax / 0.872));
  const unsigned short int nb = (unsigned short int)(((0.615 * ndr) + (-0.51499 * ndg) + (-0.10001 * ndb) + 0.615) * (umax / 1.23));
  return shGraph :: pixel<unsigned short int>(nr,ng,nb,p.a);
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<float> RGBAtoYUVA(const shGraph :: pixel<float>& p)
 {
  const float nr = ((0.299f * p.r) + (0.587f * p.g) + (0.114f * p.b));
  const float ng = (((-0.14713f * p.r) + (-0.28886f * p.g) + (0.436f * p.b) + 0.436f) / 0.872f);
  const float nb = (((0.615f * p.r) + (-0.51499f * p.g) + (-0.10001f * p.b) + 0.615f) / 1.23f);
  return shGraph :: pixel<float>(nr,ng,nb,p.a);
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<double> RGBAtoYUVA(const shGraph :: pixel<double>& p)
 {
  const double nr = ((0.299 * p.r) + (0.587 * p.g) + (0.114 * p.b));
  const double ng = (((-0.14713 * p.r) + (-0.28886 * p.g) + (0.436 * p.b) + 0.436) / 0.872);
  const double nb = (((0.615 * p.r) + (-0.51499 * p.g) + (-0.10001 * p.b) + 0.615) / 1.23);
  return shGraph :: pixel<double>(nr,ng,nb,p.a);
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned char> YUVAtoRGBA(const shGraph :: pixel<unsigned char>& p)
 {
  const double umax = (double)(UCHAR_MAX);
  const double ndy = ((double)(p.r) / umax);
  const double ndu = (((double)(p.g) * (0.872 / umax)) - 0.436);
  const double ndv = (((double)(p.b) * (1.23 / umax)) - 0.615);
  const unsigned char nr = (unsigned char)((ndy + (1.13983 * ndv)) * umax);
  const unsigned char ng = (unsigned char)((ndy + (-0.39465 * ndu) + (-0.5806 * ndv)) * umax);
  const unsigned char nb = (unsigned char)((ndy + (2.03211 * ndu)) * umax);
  return shGraph :: pixel<unsigned char>(nr,ng,nb,p.a);
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<unsigned short int> YUVAtoRGBA(const shGraph :: pixel<unsigned short int>& p)
 {
  const double umax = (double)(USHRT_MAX);
  const double ndy = ((double)(p.r) / umax);
  const double ndu = (((double)(p.g) * (0.872 / umax)) - 0.436);
  const double ndv = (((double)(p.b) * (1.23 / umax)) - 0.615);
  const unsigned short int nr = (unsigned short int)((ndy + (1.13983 * ndv)) * umax);
  const unsigned short int ng = (unsigned short int)((ndy + (-0.39465 * ndu) + (-0.5806 * ndv)) * umax);
  const unsigned short int nb = (unsigned short int)((ndy + (2.03211 * ndu)) * umax);
  return shGraph :: pixel<unsigned short int>(nr,ng,nb,p.a);
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<float> YUVAtoRGBA(const shGraph :: pixel<float>& p)
 {
  const float ndu = ((p.g * 0.872f) - 0.436f);
  const float ndv = ((p.b * 1.23f) - 0.615f);
  const float nr = ((p.r + (1.13983f * ndv)));
  const float ng = ((p.r + (-0.39465f * ndu) + (-0.5806f * ndv)));
  const float nb = ((p.r + (2.03211f * ndu)));
  return shGraph :: pixel<float>(nr,ng,nb,p.a);
 }
}

namespace shGraph
{
 template <> shGraph :: pixel<double> YUVAtoRGBA(const shGraph :: pixel<double>& p)
 {
  const double ndu = ((p.g * 0.872) - 0.436);
  const double ndv = ((p.b * 1.23) - 0.615);
  const double nr = (p.r + (1.13983 * ndv));
  const double ng = (p.r + (-0.39465 * ndu) + (-0.5806 * ndv));
  const double nb = (p.r + (2.03211 * ndu));
  return shGraph :: pixel<double>(nr,ng,nb,p.a);
 }
}
