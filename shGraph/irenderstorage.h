#ifndef SH_GRAPH_IRENDERSTORAGE
#define SH_GRAPH_IRENDERSTORAGE

#include <new>
#include <stdlib.h>

#include "render.h"
#include "../shMath/mat4.h"

namespace shGraph
{
 template <class X> class render :: irenderstorage
 {
  public:
  irenderstorage();
  explicit irenderstorage(const render& r);
  irenderstorage(const irenderstorage& a);
  ~irenderstorage();
  irenderstorage& operator = (const irenderstorage& a);
  const render& getRender() const;
  render& getRender();
  virtual void saveState();
  virtual void restoreState();
  protected:
  virtual void copy();
  void kill();
  render rend;
  shMath :: mat4<X>* data;
  unsigned int* counter;
  const static unsigned int maxMatricesStored = 4u;
 };
}

template <class X> shGraph :: render :: irenderstorage <X> :: irenderstorage() : rend(shGraph :: render()), data(new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ()), counter(NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] data;
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class X> shGraph :: render :: irenderstorage <X> :: irenderstorage(const shGraph :: render& r) : rend(r), data(new shMath :: mat4<X> [shGraph :: render :: irenderstorage <X> :: maxMatricesStored] ()), counter(NULL)
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete [] data;
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class X> inline shGraph :: render :: irenderstorage <X> :: irenderstorage(const shGraph :: render :: irenderstorage<X>& a) : rend(a.rend), data(a.data), counter(a.counter)
{
 ++(*counter);
}

template <class X> inline shGraph :: render :: irenderstorage <X> :: ~irenderstorage()
{
 kill();
}

template <class X> shGraph :: render :: irenderstorage<X>& shGraph :: render :: irenderstorage <X> :: operator = (const shGraph :: render :: irenderstorage<X>& a)
{
 if (&a != this)
 {
  kill();
  rend = a.rend;
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

template <class X> inline const shGraph :: render& shGraph :: render :: irenderstorage <X> :: getRender() const
{
 return rend;
}

template <class X> inline shGraph :: render& shGraph :: render :: irenderstorage <X> :: getRender()
{
 return rend;
}

template <class X> inline void shGraph :: render :: irenderstorage <X> :: saveState()
{
}

template <class X> inline void shGraph :: render :: irenderstorage <X> :: restoreState()
{
}

template <class X> inline void shGraph :: render :: irenderstorage <X> :: copy()
{
}

template <class X> void shGraph :: render :: irenderstorage <X> :: kill()
{
 if ((*counter) == 0u)
 {
  free(counter);
  delete [] data;
 }
 else
 {
  --(*counter);
 }
}
#endif
