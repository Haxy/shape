#ifndef SH_GRAPH_CUBEMAP
#define SH_GRAPH_CUBEMAP

#include "GLextensions.h"

#include <new>
#include <stdlib.h>

#include "image.h"
#include "texture.h"

namespace shGraph
{
 class cubemap : public itexture
 {
  public:
  cubemap(void);
  cubemap(unsigned int dimension,const itexturedescr& descr);
  template <class X> cubemap(const image<X>& negx, const image<X>& posx, const image<X>& negy, const image<X>& posy, const image<X>& negz, const image<X>& posz, const itexturedescr& descr);
  cubemap(const cubemap& a);
  ~cubemap(void);
  cubemap& operator = (const cubemap& a);
  unsigned int getDim() const;
  unsigned int getParam() const;
  template <unsigned int param> class face;
  const face<GL_TEXTURE_CUBE_MAP_NEGATIVE_X> getNegativeXFace() const;
  const face<GL_TEXTURE_CUBE_MAP_POSITIVE_X> getPositiveXFace() const;
  const face<GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> getNegativeYFace() const;
  const face<GL_TEXTURE_CUBE_MAP_POSITIVE_Y> getPositiveYFace() const;
  const face<GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> getNegativeZFace() const;
  const face<GL_TEXTURE_CUBE_MAP_POSITIVE_Z> getPositiveZFace() const;
  face<GL_TEXTURE_CUBE_MAP_NEGATIVE_X> getNegativeXFace();
  face<GL_TEXTURE_CUBE_MAP_POSITIVE_X> getPositiveXFace();
  face<GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> getNegativeYFace();
  face<GL_TEXTURE_CUBE_MAP_POSITIVE_Y> getPositiveYFace();
  face<GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> getNegativeZFace();
  face<GL_TEXTURE_CUBE_MAP_POSITIVE_Z> getPositiveZFace();
  void setSWrap(unsigned int wraps);
  void setTWrap(unsigned int wrapt);
  void setRWrap(unsigned int wrapr);
  void setMinFilter(unsigned int minfilter);
  void setMagFilter(unsigned int magfilter);
  void setEnvMode(unsigned int envmode);
  void setMinLOD(float minlod);
  void setMaxLOD(float maxlod);
  void setBaseLevel(unsigned int baselevel);
  void setMaxLevel(unsigned int maxlevel);
  template <class X> void setBorderColour(const pixel<X>& bordercolour);
  template <class X> void setEnvColour(const pixel<X>& envcolour);
  static void setSWrapForCurrent(unsigned int wraps);
  static void setTWrapForCurrent(unsigned int wrapt);
  static void setRWrapForCurrent(unsigned int wrapr);
  static void setMinFilterForCurrent(unsigned int minfilter);
  static void setMagFilterForCurrent(unsigned int magfilter);
  static void setEnvModeForCurrent(unsigned int envmode);
  static void setMinLODForCurrent(float minlod);
  static void setMaxLODForCurrent(float maxlod);
  static void setBaseLevelForCurrent(unsigned int baselevel);
  static void setMaxLevelForCurrent(unsigned int maxlevel);
  template <class X> static void setBorderColourForCurrent(const pixel<X>& bordercolour);
  template <class X> static void setEnvColourForCurrent(const pixel<X>& envcolour);
  static void genMipMapForCurrent();
  void genMipMap() const;
  bool isValid() const;
  static void enable();
  static void disable();
  static bool isEnabled();
  static cubemap createWithoutFaceData(unsigned int dimension,const itexturedescr& descr);
  protected:
  void setActive() const;
  private:
  void kill();
  itexturedescr texdescr;
  unsigned int dim;
  unsigned int* counter;
 };

 template <unsigned int param> class cubemap :: face
 {
  public:
  face();
  face(const cubemap& Owner);
  const cubemap& getOwner() const;
  template <class X> void atttachImage(const image<X>& a);
  template <class X> static void attachImageToCurrent(const image<X>& a);
  template <class X> const image<X> getImage() const;
  template <class X> const image<X> getImage(image<X>& helper) const;
  template <class X> static const image<X> getImageFromCurrent();
  template <class X> static const image<X> getImageFromCurrent(image<X>& helper);
  template <class X> operator const image<X> () const;
  private:
  cubemap owner;
 };
}

inline shGraph :: cubemap :: cubemap() : texdescr(shGraph :: itexturedescr()), dim(0u), counter((unsigned int*)malloc(sizeof(unsigned int)))
{
 if (counter == NULL)
 {
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

inline shGraph :: cubemap :: cubemap(const shGraph :: cubemap& a) : texdescr(a.texdescr), dim(a.dim), counter(a.counter)
{
 ++(*counter);
}

inline shGraph :: cubemap :: ~cubemap()
{
 kill();
}

inline shGraph :: cubemap& shGraph :: cubemap :: operator = (const shGraph :: cubemap& a)
{
 if (&a != this)
 {
  kill();
  id = a.id;
  texdescr = a.texdescr;
  dim = a.dim;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

inline unsigned int shGraph :: cubemap :: getDim() const
{
 return dim;
}

inline unsigned int shGraph :: cubemap :: getParam() const
{
 return GL_TEXTURE_CUBE_MAP;
}

inline const shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_NEGATIVE_X> shGraph :: cubemap :: getNegativeXFace() const
{
 return shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_NEGATIVE_X>(*this);
}

inline const shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_POSITIVE_X> shGraph :: cubemap :: getPositiveXFace() const
{
 return shGraph :: cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X>(*this);
}

inline const shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> shGraph :: cubemap :: getNegativeYFace() const
{
 return shGraph :: cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y>(*this);
}

inline const shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_POSITIVE_Y> shGraph :: cubemap :: getPositiveYFace() const
{
 return shGraph :: cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y>(*this);
}

inline const shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> shGraph :: cubemap :: getNegativeZFace() const
{
 return shGraph :: cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z>(*this);
}

inline const shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_POSITIVE_Z> shGraph :: cubemap :: getPositiveZFace() const
{
 return shGraph :: cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z>(*this);
}

inline shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_NEGATIVE_X> shGraph :: cubemap :: getNegativeXFace()
{
 return shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_NEGATIVE_X>(*this);
}

inline shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_POSITIVE_X> shGraph :: cubemap :: getPositiveXFace()
{
 return shGraph :: cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_X>(*this);
}

inline shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_NEGATIVE_Y> shGraph :: cubemap :: getNegativeYFace()
{
 return shGraph :: cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Y>(*this);
}

inline shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_POSITIVE_Y> shGraph :: cubemap :: getPositiveYFace()
{
 return shGraph :: cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Y>(*this);
}

inline shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_NEGATIVE_Z> shGraph :: cubemap :: getNegativeZFace()
{
 return shGraph :: cubemap :: face <GL_TEXTURE_CUBE_MAP_NEGATIVE_Z>(*this);
}

inline shGraph :: cubemap :: face<GL_TEXTURE_CUBE_MAP_POSITIVE_Z> shGraph :: cubemap :: getPositiveZFace()
{
 return shGraph :: cubemap :: face <GL_TEXTURE_CUBE_MAP_POSITIVE_Z>(*this);
}

inline void shGraph :: cubemap :: setSWrap(unsigned int wraps)
{
 bind();
 setSWrapForCurrent(wraps);
 unbindCurrent();
}

inline void shGraph :: cubemap :: setTWrap(unsigned int wrapt)
{
 bind();
 setTWrapForCurrent(wrapt);
 unbindCurrent();
}

inline void shGraph :: cubemap :: setRWrap(unsigned int wrapr)
{
 bind();
 setRWrapForCurrent(wrapr);
 unbindCurrent();
}

inline void shGraph :: cubemap :: setMinFilter(unsigned int minfilter)
{
 bind();
 setMinFilterForCurrent(minfilter);
 unbindCurrent();
}

inline void shGraph :: cubemap :: setMagFilter(unsigned int magfilter)
{
 bind();
 setMagFilterForCurrent(magfilter);
 unbindCurrent();
}

inline void shGraph :: cubemap :: setEnvMode(unsigned int envmode)
{
 bind();
 setEnvModeForCurrent(envmode);
 unbindCurrent();
}

inline void shGraph :: cubemap :: setMinLOD(float minlod)
{
 bind();
 setMinLODForCurrent(minlod);
 unbindCurrent();
}

inline void shGraph :: cubemap :: setMaxLOD(float maxlod)
{
 bind();
 setMaxLODForCurrent(maxlod);
 unbindCurrent();
}

inline void shGraph :: cubemap :: setBaseLevel(unsigned int baselevel)
{
 bind();
 setBaseLevelForCurrent(baselevel);
 unbindCurrent();
}

inline void shGraph :: cubemap :: setMaxLevel(unsigned int maxlevel)
{
 bind();
 setMaxLevelForCurrent(maxlevel);
 unbindCurrent();
}

template <class X> inline void shGraph :: cubemap :: setBorderColour(const shGraph :: pixel<X>& bordercolour)
{
 bind();
 setBorderColourForCurrent(bordercolour);
 unbindCurrent();
}

template <class X> inline void shGraph :: cubemap :: setEnvColour(const shGraph :: pixel<X>& envcolour)
{
 bind();
 setEnvColourForCurrent(envcolour);
 unbindCurrent();
}

inline void shGraph :: cubemap :: genMipMap() const
{
 bind();
 genMipMapForCurrent();
 unbindCurrent();
}

inline bool shGraph :: cubemap :: isValid() const
{
 return (id != 0u);
}

inline void shGraph :: cubemap :: enable()
{
 glEnable(GL_TEXTURE_CUBE_MAP);
}

inline void shGraph :: cubemap :: disable()
{
 glDisable(GL_TEXTURE_CUBE_MAP);
}

inline bool shGraph :: cubemap :: isEnabled()
{
 return (glIsEnabled(GL_TEXTURE_CUBE_MAP) == GL_TRUE);
}

inline void shGraph :: cubemap :: setActive() const
{
 glBindTexture(GL_TEXTURE_CUBE_MAP,id);
}

template <unsigned int param> inline shGraph :: cubemap :: face <param> :: face() : owner(shGraph :: cubemap())
{
}

template <unsigned int param> inline shGraph :: cubemap :: face <param> :: face(const shGraph :: cubemap& Owner) : owner(Owner)
{
}

template <unsigned int param> inline const shGraph :: cubemap& shGraph :: cubemap :: face <param> :: getOwner() const
{
 return owner;
}

template <unsigned int param> template <class X> inline void shGraph :: cubemap :: face <param> :: atttachImage(const shGraph :: image<X>& a)
{
 owner.bind();
 attachImageToCurrent(a);
 shGraph :: itexture :: unbindCurrent();
}

template <unsigned int param> template <class X> inline const shGraph :: image<X> shGraph :: cubemap :: face <param> :: getImage() const
{
 shGraph :: image<X> helper(owner.getDim(),owner.getDim());
 return getImage(helper);
}

template <unsigned int param> template <class X> inline const shGraph :: image<X> shGraph :: cubemap :: face <param> :: getImage(shGraph :: image<X>& helper) const
{
 owner.bind();
 getImageFromCurrent(helper);
 shGraph :: itexture :: unbindCurrent();
 return helper;
}

template <unsigned int param> template <class X> const shGraph :: image<X> shGraph :: cubemap :: face <param> :: getImageFromCurrent()
{
 unsigned int width;
 unsigned int height;
 glGetTexLevelParameteriv(param,0,GL_TEXTURE_WIDTH,(GLint*)(&width));
 glGetTexLevelParameteriv(param,0,GL_TEXTURE_HEIGHT,(GLint*)(&height));
 return shGraph :: cubemap :: face <param> :: getImageFromCurrent(shGraph :: image<X>(width,height));
}

template <unsigned int param> template <class X> inline shGraph :: cubemap :: face <param> :: operator const shGraph :: image<X> () const
{
 return getImage();
}
#endif

