#ifndef SH_GRAPH_AAO
#define SH_GRAPH_AAO

#include <new>
#include <stdlib.h>

#include "GLextensions.h"

#include "AO.h"
#include "attribute.h"
#include "rccoordinatesfx.h"

namespace shGraph
{
 template <class T,bool normalise = false> class AAO : public AO
 {
  public:
  AAO();
  AAO(const AAO& a);
  template <class X> AAO(const rccoordinatesfx<X>& data);
  ~AAO();
  AAO& operator = (const AAO& a);
  static void sendCurrentTo(const attribute<T>& a);
  private:
  void kill();
 };
}

template <class T,bool normalise> inline shGraph :: AAO <T,normalise> :: AAO() : AO(shGraph :: AO(shGraph :: buffer(),(void*)(&shGraph :: AAO <T,normalise> :: sendCurrentTo),new shGraph :: rccoordinatesfx<T>()))
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete data;
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T,bool normalise> inline shGraph :: AAO <T,normalise> :: AAO(const shGraph :: AAO<T,normalise>& a) : AO(shGraph :: buffer(),(void*)(&shGraph :: AAO <T,normalise> :: sendCurrentTo),a.data,a.counter)
{
 ++(*counter);
}

template <class T,bool normalise> template <class X> inline shGraph :: AAO <T,normalise> :: AAO(const shGraph :: rccoordinatesfx<X>& data) : AO(shGraph :: AO(shGraph :: buffer(),(void*)(&shGraph :: AAO <T,normalise> :: sendCurrentTo),new shGraph :: rccoordinatesfx<X>(data)))
{
 if ((counter = (unsigned int*)malloc(sizeof(unsigned int))) == NULL)
 {
  delete data;
  throw std :: bad_alloc();
 }
 (*counter) = 0u;
}

template <class T,bool normalise> inline shGraph :: AAO <T,normalise> :: ~AAO()
{
 kill();
}

template <class T,bool normalise> shGraph :: AAO<T,normalise>& shGraph :: AAO <T,normalise> :: operator = (const shGraph :: AAO<T,normalise>& a)
{
 if (&a != this)
 {
  kill();
  data = a.data;
  counter = a.counter;
  ++(*counter);
 }
 return *this;
}

namespace shGraph
{
 template <> inline void AAO <float,false> :: sendCurrentTo(const shGraph :: attribute<float>& a)
 {
  glVertexAttribPointer(a.getLocation(),1,GL_FLOAT,GL_FALSE,sizeof(float),(const float*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <signed int,false> :: sendCurrentTo(const shGraph :: attribute<signed int>& a)
 {
  glVertexAttribPointer(a.getLocation(),1,GL_INT,GL_FALSE,sizeof(signed int),(const signed int*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec2<float>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec2<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_FLOAT,GL_FALSE,sizeof(shMath :: vec2<float>),(const shMath :: vec2<float>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec2<signed int>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec2<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_INT,GL_FALSE,sizeof(shMath :: vec2<signed int>),(const shMath :: vec2<signed int>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec3<float>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec3<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_FLOAT,GL_FALSE,sizeof(shMath :: vec3<float>),(const shMath :: vec3<float>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec3<signed int>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec3<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_INT,GL_FALSE,sizeof(shMath :: vec3<signed int>),(const shMath :: vec3<signed int>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec4<float>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec4<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_FLOAT,GL_FALSE,sizeof(shMath :: vec4<float>),(const shMath :: vec4<float>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec4<signed int>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec4<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_INT,GL_FALSE,sizeof(shMath :: vec4<signed int>),(const shMath :: vec4<signed int>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat2<float>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat2<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat2<float>),(const shMath :: vec2<float>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,2,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat2<float>),(const shMath :: vec2<float>*)(shGraph :: AO :: getCurrent().getData()) + 1);
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat2<signed int>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat2<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_INT,GL_FALSE,sizeof(shMath :: mat2<signed int>),(const shMath :: vec2<signed int>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,2,GL_INT,GL_FALSE,sizeof(shMath :: mat2<signed int>),(const shMath :: vec2<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 1);
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat3<float>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat3<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat3<float>),(const shMath :: vec3<float>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,3,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat3<float>),(const shMath :: vec3<float>*)(shGraph :: AO :: getCurrent().getData()) + 1);
  glVertexAttribPointer(a.getLocation() + 2,3,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat3<float>),(const shMath :: vec3<float>*)(shGraph :: AO :: getCurrent().getData()) + 2);
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat3<signed int>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat3<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_INT,GL_FALSE,sizeof(shMath :: mat3<signed int>),(const shMath :: vec3<signed int>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,3,GL_INT,GL_FALSE,sizeof(shMath :: mat3<signed int>),(const shMath :: vec3<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 1);
  glVertexAttribPointer(a.getLocation() + 2,3,GL_INT,GL_FALSE,sizeof(shMath :: mat3<signed int>),(const shMath :: vec3<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 2);
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat4<float>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat4<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat4<float>),(const shMath :: vec4<float>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,4,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat4<float>),(const shMath :: vec4<float>*)(shGraph :: AO :: getCurrent().getData()) + 1);
  glVertexAttribPointer(a.getLocation() + 2,4,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat4<float>),(const shMath :: vec4<float>*)(shGraph :: AO :: getCurrent().getData()) + 2);
  glVertexAttribPointer(a.getLocation() + 3,4,GL_FLOAT,GL_FALSE,sizeof(shMath :: mat4<float>),(const shMath :: vec4<float>*)(shGraph :: AO :: getCurrent().getData()) + 3);
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat4<signed int>,false> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat4<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_INT,GL_FALSE,sizeof(shMath :: mat4<signed int>),(const shMath :: vec4<signed int>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,4,GL_INT,GL_FALSE,sizeof(shMath :: mat4<signed int>),(const shMath :: vec4<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 1);
  glVertexAttribPointer(a.getLocation() + 2,4,GL_INT,GL_FALSE,sizeof(shMath :: mat4<signed int>),(const shMath :: vec4<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 2);
  glVertexAttribPointer(a.getLocation() + 3,4,GL_INT,GL_FALSE,sizeof(shMath :: mat4<signed int>),(const shMath :: vec4<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 3);
 }
}

namespace shGraph
{
 template <> inline void AAO <float,true> :: sendCurrentTo(const shGraph :: attribute<float>& a)
 {
  glVertexAttribPointer(a.getLocation(),1,GL_FLOAT,GL_TRUE,sizeof(float),(const float*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <signed int,true> :: sendCurrentTo(const shGraph :: attribute<signed int>& a)
 {
  glVertexAttribPointer(a.getLocation(),1,GL_INT,GL_TRUE,sizeof(signed int),(const signed int*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec2<float>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec2<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_FLOAT,GL_TRUE,sizeof(shMath :: vec2<float>),(const shMath :: vec2<float>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec2<signed int>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec2<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_INT,GL_TRUE,sizeof(shMath :: vec2<signed int>),(const shMath :: vec2<signed int>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec3<float>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec3<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_FLOAT,GL_TRUE,sizeof(shMath :: vec3<float>),(const shMath :: vec3<float>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec3<signed int>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec3<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_INT,GL_TRUE,sizeof(shMath :: vec3<signed int>),(const shMath :: vec3<signed int>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec4<float>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec4<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_FLOAT,GL_TRUE,sizeof(shMath :: vec4<float>),(const shMath :: vec4<float>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: vec4<signed int>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: vec4<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_INT,GL_TRUE,sizeof(shMath :: vec4<signed int>),(const shMath :: vec4<signed int>*)(shGraph :: AO :: getCurrent().getData()));
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat2<float>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat2<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat2<float>),(const shMath :: vec2<float>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,2,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat2<float>),(const shMath :: vec2<float>*)(shGraph :: AO :: getCurrent().getData()) + 1);
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat2<signed int>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat2<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),2,GL_INT,GL_TRUE,sizeof(shMath :: mat2<signed int>),(const shMath :: vec2<signed int>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,2,GL_INT,GL_TRUE,sizeof(shMath :: mat2<signed int>),(const shMath :: vec2<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 1);
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat3<float>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat3<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat3<float>),(const shMath :: vec3<float>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,3,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat3<float>),(const shMath :: vec3<float>*)(shGraph :: AO :: getCurrent().getData()) + 1);
  glVertexAttribPointer(a.getLocation() + 2,3,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat3<float>),(const shMath :: vec3<float>*)(shGraph :: AO :: getCurrent().getData()) + 2);
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat3<signed int>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat3<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),3,GL_INT,GL_TRUE,sizeof(shMath :: mat3<signed int>),(const shMath :: vec3<signed int>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,3,GL_INT,GL_TRUE,sizeof(shMath :: mat3<signed int>),(const shMath :: vec3<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 1);
  glVertexAttribPointer(a.getLocation() + 2,3,GL_INT,GL_TRUE,sizeof(shMath :: mat3<signed int>),(const shMath :: vec3<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 2);
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat4<float>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat4<float> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat4<float>),(const shMath :: vec4<float>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,4,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat4<float>),(const shMath :: vec4<float>*)(shGraph :: AO :: getCurrent().getData()) + 1);
  glVertexAttribPointer(a.getLocation() + 2,4,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat4<float>),(const shMath :: vec4<float>*)(shGraph :: AO :: getCurrent().getData()) + 2);
  glVertexAttribPointer(a.getLocation() + 3,4,GL_FLOAT,GL_TRUE,sizeof(shMath :: mat4<float>),(const shMath :: vec4<float>*)(shGraph :: AO :: getCurrent().getData()) + 3);
 }
}

namespace shGraph
{
 template <> inline void AAO <shMath :: mat4<signed int>,true> :: sendCurrentTo(const shGraph :: attribute<shMath :: mat4<signed int> >& a)
 {
  glVertexAttribPointer(a.getLocation(),4,GL_INT,GL_TRUE,sizeof(shMath :: mat4<signed int>),(const shMath :: vec4<signed int>*)(shGraph :: AO :: getCurrent().getData()));
  glVertexAttribPointer(a.getLocation() + 1,4,GL_INT,GL_TRUE,sizeof(shMath :: mat4<signed int>),(const shMath :: vec4<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 1);
  glVertexAttribPointer(a.getLocation() + 2,4,GL_INT,GL_TRUE,sizeof(shMath :: mat4<signed int>),(const shMath :: vec4<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 2);
  glVertexAttribPointer(a.getLocation() + 3,4,GL_INT,GL_TRUE,sizeof(shMath :: mat4<signed int>),(const shMath :: vec4<signed int>*)(shGraph :: AO :: getCurrent().getData()) + 3);
 }
}

template <class T,bool normalise> void shGraph :: AAO <T,normalise> :: kill()
{
 if ((*counter) == 0u)
 {
  delete data;
  free(counter);
 }
 else
 {
  --(*counter);
 }
}
#endif

