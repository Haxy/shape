#ifndef SH_GRAPH_RENDER_DEPTHTEST
#define SH_GRAPH_RENDER_DEPTHTEST

namespace shGraph
{
 class render :: depthtest
 {
  public:
  depthtest& operator = (const bool& test);
  operator const bool () const;
 };
}

inline shGraph :: render :: depthtest& shGraph :: render :: depthtest :: operator = (const bool& test)
{
 test ? glEnable(GL_DEPTH_TEST) : glDisable(GL_DEPTH_TEST);
 return *this;
}

inline shGraph :: render :: depthtest :: operator const bool () const
{
 return (glIsEnabled(GL_DEPTH_TEST) == GL_TRUE);
}

#endif

