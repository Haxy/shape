#include "render.h"
#include "modelviewmatrix.h"

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator = (const shMath :: mat4<float>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf((const float*)(modelview));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator = (const shMath :: mat4<double>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixd((const double*)(modelview));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator = (const shMath :: rcmat4<float>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf((const float*)(modelview));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator = (const shMath :: rcmat4<double>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixd((const double*)(modelview));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator *= (const shMath :: mat4<float>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glMultMatrixf((const float*)(modelview));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator *= (const shMath :: mat4<double>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glMultMatrixd((const double*)(modelview));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator *= (const shMath :: rcmat4<float>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glMultMatrixf((const float*)(modelview));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator *= (const shMath :: rcmat4<double>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glMultMatrixd((const double*)(modelview));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator /= (const shMath :: mat4<float>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glMultMatrixf((const float*)(shMath :: inverse(modelview)));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator /= (const shMath :: mat4<double>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glMultMatrixd((const double*)(shMath :: inverse(modelview)));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator /= (const shMath :: rcmat4<float>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glMultMatrixf((const float*)(shMath :: inverse(modelview)));
  glMatrixMode(res);
  return *this;
 }
}

namespace shGraph
{
 template <> shGraph :: render :: modelviewmatrix& render :: modelviewmatrix :: operator /= (const shMath :: rcmat4<double>& modelview)
 {
  static unsigned int res = 0u;
  glGetIntegerv(GL_MATRIX_MODE,(GLint*)(&res));
  glMatrixMode(GL_MODELVIEW);
  glMultMatrixd((const double*)(shMath :: inverse(modelview)));
  glMatrixMode(res);
  return *this;
 }
}

