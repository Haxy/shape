#ifndef SH_GRAPH_IO
#define SH_GRAPH_IO

#include "GLextensions.h"

#include "buffer.h"

#include "../shMath/stack.h"

namespace shGraph
{
 class IO
 {
  public:
  IO();
  virtual ~IO();
  void bind() const;
  void unbind() const;
  virtual void display() const;
  unsigned int getID() const;
  const void* getData() const;
  virtual unsigned int getType() const;
  virtual unsigned int getSize() const;
  static void displayCurrent();
  static void unbindCurrent();
  static void unbindAll();
  protected:
  IO(const shGraph :: buffer& Id,unsigned int Type,unsigned int Size,void (*Displayfun)(),void* Data,unsigned int* Counter = NULL);
  const static unsigned int initialStackSize = 2u;
  static shMath :: stack<IO> stack;
  static IO current;
  buffer id;
  unsigned int type;
  unsigned int size;
  void (*displayfun)();
  void* data;
  unsigned int* counter;
 };
}

inline shGraph :: IO :: IO() : id(shGraph :: buffer()), type(0u), size(0u), displayfun(NULL), data(NULL), counter(NULL)
{
}

inline shGraph :: IO :: ~IO()
{
}

inline void shGraph :: IO :: bind() const
{
 stack.push(current);
 current = *this;
 glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,id.getId());
}

inline void shGraph :: IO :: unbind() const
{
 if ((id.getId() == current.id.getId()) && (data == current.data))
 {
  shGraph :: IO :: unbindCurrent();
 }
}

inline void shGraph :: IO :: display() const
{
 bind();
 shGraph :: IO :: displayCurrent();
 shGraph :: IO :: unbindCurrent();
}

inline unsigned int shGraph :: IO :: getID() const
{
 return id.getId();
}

inline const void* shGraph :: IO :: getData() const
{
 return data;
}

inline unsigned int shGraph :: IO :: getType() const
{
 return type;
}

inline unsigned int shGraph :: IO :: getSize() const
{
 return size;
}

inline void shGraph :: IO :: displayCurrent()
{
 current.displayfun();
}

inline void shGraph :: IO :: unbindCurrent()
{
 current = stack.pop();
 glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,current.getID());
}

inline void shGraph :: IO :: unbindAll()
{
 stack.clear();
 current = shGraph :: IO();
 glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB,0);
}

inline shGraph :: IO :: IO(const shGraph :: buffer& Id,unsigned int Type,unsigned int Size,void (*Displayfun)(),void* Data,unsigned int* Counter) : id(Id), type(Type), size(Size), displayfun(Displayfun), data(Data), counter(Counter)
{
}
#endif

