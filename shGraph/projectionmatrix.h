#ifndef SH_GRAPH_PROJECTIONMATRIX
#define SH_GRAPH_PROJECTIONMATRIX

#include "GLstandard.h"
#include "../shMath/mat4.h"
#include "../shMath/rcmat4.h"

namespace shGraph
{
 class render :: projectionmatrix
 {
  public:
  template <class X> projectionmatrix& operator = (const shMath :: mat4<X>& projection);
  template <class X> projectionmatrix& operator = (const shMath :: rcmat4<X>& projection);
  template <class X> projectionmatrix& operator *= (const shMath :: mat4<X>& projection);
  template <class X> projectionmatrix& operator *= (const shMath :: rcmat4<X>& projection);
  template <class X> projectionmatrix& operator /= (const shMath :: mat4<X>& projection);
  template <class X> projectionmatrix& operator /= (const shMath :: rcmat4<X>& projection);
  template <class X> operator const shMath :: mat4<X> () const;
  template <class X> operator const shMath :: rcmat4<X> () const;
  template <class X> const shMath :: mat4<X> getMatrix() const;
  template <class X> const shMath :: mat4<X> getMatrix(shMath :: mat4<X>& helper) const;
  template <class X> const shMath :: rcmat4<X> rcgetMatrix() const;
  template <class X> const shMath :: rcmat4<X> rcgetMatrix(shMath :: rcmat4<X>& helper) const;
 };
}

namespace shGraph
{
 template <> inline render :: projectionmatrix :: operator const shMath :: mat4<float> () const
 {
  static shMath :: mat4<float> res;
  glGetFloatv(GL_PROJECTION_MATRIX,(float*)res);
  return res;
 }
}

namespace shGraph
{
 template <> inline render :: projectionmatrix :: operator const shMath :: mat4<double> () const
 {
  static shMath :: mat4<double> res;
  glGetDoublev(GL_PROJECTION_MATRIX,(double*)res);
  return res;
 }
}

namespace shGraph
{
 template <> inline render :: projectionmatrix :: operator const shMath :: rcmat4<float> () const
 {
  static shMath :: rcmat4<float> res;
  glGetFloatv(GL_PROJECTION_MATRIX,(float*)res);
  return res;
 }
}

namespace shGraph
{
 template <> inline render :: projectionmatrix :: operator const shMath :: rcmat4<double> () const
 {
  static shMath :: rcmat4<double> res;
  glGetDoublev(GL_PROJECTION_MATRIX,(double*)res);
  return res;
 }
}

namespace shGraph
{
 template <> inline const shMath :: mat4<float> render :: projectionmatrix :: getMatrix() const
 {
  static shMath :: mat4<float> res;
  glGetFloatv(GL_PROJECTION_MATRIX,(float*)res);
  return res;
 }
}

namespace shGraph
{
 template <> inline const shMath :: mat4<double> render :: projectionmatrix :: getMatrix() const
 {
  static shMath :: mat4<double> res;
  glGetDoublev(GL_PROJECTION_MATRIX,(double*)res);
  return res;
 }
}

namespace shGraph
{
 template <> inline const shMath :: mat4<float> render :: projectionmatrix :: getMatrix(shMath :: mat4<float>& helper) const
 {
  glGetFloatv(GL_PROJECTION_MATRIX,(float*)helper);
  return helper;
 }
}

namespace shGraph
{
 template <> inline const shMath :: mat4<double> render :: projectionmatrix :: getMatrix(shMath :: mat4<double>& helper) const
 {
  glGetDoublev(GL_PROJECTION_MATRIX,(double*)helper);
  return helper;
 }
}

namespace shGraph
{
 template <> inline const shMath :: rcmat4<float> render :: projectionmatrix :: rcgetMatrix() const
 {
  static shMath :: rcmat4<float> res;
  glGetFloatv(GL_PROJECTION_MATRIX,(float*)res);
  return res;
 }
}

namespace shGraph
{
 template <> inline const shMath :: rcmat4<double> render :: projectionmatrix :: rcgetMatrix() const
 {
  static shMath :: rcmat4<double> res;
  glGetDoublev(GL_PROJECTION_MATRIX,(double*)res);
  return res;
 }
}

namespace shGraph
{
 template <> inline const shMath :: rcmat4<float> render :: projectionmatrix :: rcgetMatrix(shMath :: rcmat4<float>& helper) const
 {
  glGetFloatv(GL_PROJECTION_MATRIX,(float*)helper);
  return helper;
 }
}

namespace shGraph
{
 template <> inline const shMath :: rcmat4<double> render :: projectionmatrix :: rcgetMatrix(shMath :: rcmat4<double>& helper) const
 {
  glGetDoublev(GL_PROJECTION_MATRIX,(double*)helper);
  return helper;
 }
}
#endif

