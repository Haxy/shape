#ifndef SH_GRAPH_VIEWPORT
#define SH_GRAPH_VIEWPORT

namespace shGraph
{
 class render :: viewport
 {
  public:
  viewport& operator = (const shMath :: rcvec2<signed int>& dim);
  viewport& operator = (const shMath :: rcvec4<signed int>& all);
  viewport& operator = (const shMath :: vec2<unsigned int>& dim);
  viewport& operator = (const shMath :: vec4<unsigned int>& all);
  viewport& operator = (const shMath :: vec2<signed int>& dim);
  viewport& operator = (const shMath :: vec4<signed int>& all);
  operator const shMath :: rcvec2<signed int> () const;
  operator const shMath :: rcvec4<signed int> () const;
  operator const shMath :: vec2<signed int> () const;
  operator const shMath :: vec4<signed int> () const;
  static const shMath :: rcvec2<signed int> rcgetDimentions(shMath :: rcvec2<signed int>& helper);
  static const shMath :: rcvec2<signed int> rcgetOrygin(shMath :: rcvec2<signed int>& helper);
  static const shMath :: rcvec2<signed int> rcgetDimentions();
  static const shMath :: rcvec2<signed int> rcgetOrygin();
  static const shMath :: vec2<signed int> getDimentions();
  static const shMath :: vec2<signed int> getOrygin();
  static void setDimentions(const shMath :: rcvec2<signed int>& dimentions);
  static void setDimentions(const shMath :: vec2<signed int>& dimentions);
  static void setOrygin(const shMath :: rcvec2<signed int>& orygin);
  static void setOrygin(const shMath :: vec2<signed int>& orytin);
 };
}

inline shGraph :: render :: viewport& shGraph :: render :: viewport :: operator = (const shMath :: rcvec2<signed int>& dim)
{
 glViewport(0,0,dim.x(),dim.y());
 return *this;
}

inline shGraph :: render :: viewport& shGraph :: render :: viewport :: operator = (const shMath :: rcvec4<signed int>& all)
{
 glViewport(all.x(),all.y(),all.z(),all.w());
 return *this;
}

inline shGraph :: render :: viewport& shGraph :: render :: viewport :: operator = (const shMath :: vec2<unsigned int>& dim)
{
 glViewport(0,0,dim.x(),dim.y());
 return *this;
}

inline shGraph :: render :: viewport& shGraph :: render :: viewport :: operator = (const shMath :: vec4<unsigned int>& all)
{
 glViewport(all.x(),all.y(),all.z(),all.w());
 return *this;
}

inline shGraph :: render :: viewport& shGraph :: render :: viewport :: operator = (const shMath :: vec2<signed int>& dim)
{
 glViewport(0,0,dim.x(),dim.y());
 return *this;
}

inline shGraph :: render :: viewport& shGraph :: render :: viewport :: operator = (const shMath :: vec4<signed int>& all)
{
 glViewport(all.x(),all.y(),all.z(),all.w());
 return *this;
}

inline shGraph :: render :: viewport :: operator const shMath :: rcvec2<signed int> () const
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 return shMath :: rcvec2<signed int>(res[2],res[3]);
}

inline shGraph :: render :: viewport :: operator const shMath :: rcvec4<signed int> () const
{
 static shMath :: rcvec4<signed int> res = shMath :: rcvec4<signed int>();
 glGetIntegerv(GL_VIEWPORT,(signed int*)(res));
 return res;
}

inline shGraph :: render :: viewport :: operator const shMath :: vec2<signed int> () const
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 return shMath :: vec2<signed int>(res[2],res[3]);
}

inline shGraph :: render :: viewport :: operator const shMath :: vec4<signed int> () const
{
 static shMath :: vec4<signed int> res = shMath :: vec4<signed int>();
 glGetIntegerv(GL_VIEWPORT,(signed int*)(res));
 return res;
}

inline const shMath :: rcvec2<signed int> shGraph :: render :: viewport :: rcgetDimentions(shMath :: rcvec2<signed int>& helper)
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 helper.x() = res[2];
 helper.y() = res[3];
 return helper;
}

inline const shMath :: rcvec2<signed int> shGraph :: render :: viewport :: rcgetOrygin(shMath :: rcvec2<signed int>& helper)
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 helper.x() = res[0];
 helper.y() = res[1];
 return helper;
}

inline const shMath :: rcvec2<signed int> shGraph :: render :: viewport :: rcgetDimentions()
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 return shMath :: rcvec2<signed int>(res[2],res[3]);
}

inline const shMath :: rcvec2<signed int> shGraph :: render :: viewport :: rcgetOrygin()
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 return shMath :: rcvec2<signed int>(res[0],res[1]);
}

inline const shMath :: vec2<signed int> shGraph :: render :: viewport :: getDimentions()
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 return shMath :: vec2<signed int>(res[2],res[3]);
}

inline const shMath :: vec2<signed int> shGraph :: render :: viewport :: getOrygin()
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 return shMath :: vec2<signed int>(res[0],res[1]);
}

inline void shGraph :: render :: viewport :: setDimentions(const shMath :: rcvec2<signed int>& dimentions)
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 glViewport(res[0],res[1],dimentions.x(),dimentions.y());
}

inline void shGraph :: render :: viewport :: setDimentions(const shMath :: vec2<signed int>& dimentions)
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 glViewport(res[0],res[1],dimentions.x(),dimentions.y());
}

inline void shGraph :: render :: viewport :: setOrygin(const shMath :: rcvec2<signed int>& orygin)
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 glViewport(orygin.x(),orygin.y(),res[2],res[3]);
}

inline void shGraph :: render :: viewport :: setOrygin(const shMath :: vec2<signed int>& orygin)
{
 static signed int res[4];
 glGetIntegerv(GL_VIEWPORT,res);
 glViewport(orygin.x(),orygin.y(),res[2],res[3]);
}
#endif

